﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for DemoAddDrug.xaml
    /// </summary>
    /// 

    public partial class DemoAddDrug : Page
    {
       // public Model.Meds d = null;
       public Boolean stopp = false;
        public DemoAddDrug()
        {
            InitializeComponent();
            //this.d = med;
        }

        private void CancelAddDrugButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DrugBackButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void DeleteDrugButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void ConfirmAddDrugButton_Click(object sender, RoutedEventArgs e)
        {
            


        }

        private void ConfirmEditDrugButton_Click(object sender, RoutedEventArgs e)
        {
            


        }

        private void CancelEditDrugButton_Click(object sender, RoutedEventArgs e)
        {

            
        }

        private void EditDrugButton_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void StopDemo_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.stop = true;
            stopp = true;
            this.NavigationService.Navigate(new Medicine());
        }
    }
}
