﻿using Controller;
using Model.Users;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wpfKlinika.Model2;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for UserTimeRoom.xaml
    /// </summary>


    public partial class UserTimeRoom : Page
    {
        //public static WorkersDoctors wd = new WorkersDoctors()

        public List<String> Room { get; set; }
        public List<String> Smene { get; set; }

        public static List<String> Times { get; set; }
        public List<DateTime> Vremena { get; set; }

        public static List<DateTime> zadatiDatumi = new List<DateTime>();
        private static ObservableCollection<Model2.WorkingT> _dates = new ObservableCollection<Model2.WorkingT>();
        public static ObservableCollection<Model2.WorkingT> Dt
        {
            get { return _dates; }
            set { _dates = value; }
        }

        public Doctor d = null;




        public UserTimeRoom(Doctor doc)
        {
            InitializeComponent();
            this.DataContext = this;
            this.dateFromBox.BlackoutDates.AddDatesInPast();
            this.monBox.BlackoutDates.AddDatesInPast();

            this.d = doc;
            if (MainWindow.doctorController.GetByID(d.PersonalID) == null)
            {
                this.addFinishButton.Visibility = Visibility.Visible;
                this.addForExistingUser.Visibility = Visibility.Hidden;
            }
            else
            {
               
                    this.addFinishButton.Visibility = Visibility.Hidden;
                    this.addForExistingUser.Visibility = Visibility.Visible;
                
            }
            
            
            Times = new List<string>();

            for (int i = 0; i < 24; i++)
            {
                String s = null;
                if (i < 10)
                {
                    s = "0" + i + ":00";
                }
                else
                {
                    s = i + ":00";
                }
                Times.Add(s);
            }


            //this.secretaryRoomBox.IsEnabled = this.nedBox.IsEnabled = this.petBox.IsEnabled = this.subBox.IsEnabled = this.cetBox.IsEnabled = this.monBox.IsEnabled = this.sreBox.IsEnabled = this.utoBox.IsEnabled = false ;



            Room = new List<string>();
            foreach (Room r in Home.Room)
            {
                Room.Add(r.RoomName);
            }
            Smene = new List<string>();
            foreach (Smene r in Home.Smene)
            {
                Smene.Add(r.Time);
            }
            

        }

        private void EditTimeRoomButton_Click(object sender, RoutedEventArgs e)
        {
            //this.secretaryRoomBox.IsEnabled = this.nedBox.IsEnabled = this.petBox.IsEnabled = this.subBox.IsEnabled = this.cetBox.IsEnabled = this.monBox.IsEnabled = this.sreBox.IsEnabled = this.utoBox.IsEnabled = true;
            this.cancleEditTimeRoomButton.Visibility = Visibility.Visible;
            this.editConfirmButton.Visibility = Visibility.Visible;

            this.editTimeRoomButton.Visibility = Visibility.Hidden;
            this.addFinishButton.Visibility = Visibility.Hidden;
            //this.backFromViewUserTimeRoom.Visibility = Visibility.Hidden;
            //this.backTimeRoomFromAddUser.Visibility = Visibility.Hidden;
        }

        public async void taskDel()
        {
            await Task.Delay(5000);
            this.cancleEditTimeRoomButton.IsEnabled = true;
            this.editConfirmButton.IsEnabled = true;

            this.editTimeRoomButton.IsEnabled = true;
            this.addFinishButton.IsEnabled = true;
           // this.backFromViewUserTimeRoom.IsEnabled = true;
            //this.backTimeRoomFromAddUser.IsEnabled = true;

        }

        private void AddFinishButton_Click(object sender, RoutedEventArgs e)
        {
            List<WorkingTime> wtt = new List<WorkingTime>();
            WorkingPeriod wp = new WorkingPeriod(0, DateTime.Parse(this.dateFromBox.Text), DateTime.Parse(this.monBox.Text), wtt);

            foreach (WorkingT ww in Dt)
            {
                if (ww.StartTime != "" && ww.EndTime != "")
                {

                    WorkingTime workingTime = new WorkingTime(0, DateTime.Parse(ww.StartTime), DateTime.Parse(ww.EndTime), ww.Dates);
                    wtt.Add(workingTime);
                    MainWindow.workingTimeController.Create(workingTime);
                }
            }
            wp.WorkingTime = wtt;

            MainWindow.workingPeriodController.Create(wp);


            WorkingPeriod[] wp2 = new WorkingPeriod[d.WorkingPeriod.Length + 1];
            for (int i = 0; i < wp2.Length; i++)
            {
                if (i == wp2.Length - 1)
                {
                    wp2[i] = wp;
                }
                else
                {
                    wp2[i] = d.WorkingPeriod[i];
                }


            }


            d.WorkingPeriod = wp2;
            //MainWindow.doctorController.Update(d);
            this.pickTimeBorder.Visibility = Visibility.Hidden;
            //

           

           // MainWindow.workingPeriodController.Create(wp);
            //this.NavigationService.GoBack();
            this.NavigationService.Navigate(new UserWorkingPeriod(d));
        }

        private void BackTimeRoomFromAddUser_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void BackFromViewUserTimeRoom_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void DooctorRoomBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void EditConfirmButton_Click(object sender, RoutedEventArgs e)
        {
          
            this.cancleEditTimeRoomButton.Visibility = Visibility.Hidden;
            this.editConfirmButton.Visibility = Visibility.Hidden;

            this.editTimeRoomButton.Visibility = Visibility.Visible;
            this.addFinishButton.Visibility = Visibility.Hidden;
           //this.backFromViewUserTimeRoom.Visibility = Visibility.Visible;
            //this.backTimeRoomFromAddUser.Visibility = Visibility.Visible;

           



           
            



            List<WorkingTime> wtt = new List<WorkingTime>();
            WorkingPeriod wp = new WorkingPeriod(0, DateTime.Parse(this.dateFromBox.Text), DateTime.Parse(this.monBox.Text), wtt);

            foreach (WorkingT ww in Dt)
            {
                if (ww.StartTime != "" && ww.EndTime != "")
                {

                    WorkingTime workingTime = new WorkingTime(0, DateTime.Parse(ww.StartTime), DateTime.Parse(ww.EndTime), ww.Dates);
                    wtt.Add(workingTime);
                    MainWindow.workingTimeController.Create(workingTime);
                }
            }
            wp.WorkingTime = wtt;

            MainWindow.workingPeriodController.Create(wp);


            WorkingPeriod[] wp2 = new WorkingPeriod[d.WorkingPeriod.Length + 1];
            for (int i = 0; i < wp2.Length; i++)
            {
                if (i == wp2.Length - 1)
                {
                    wp2[i] = wp;
                }
                else
                {
                    wp2[i] = d.WorkingPeriod[i];
                }


            }


            d.WorkingPeriod = wp2;
            MainWindow.doctorController.Update(d);
            MainWindow.userController.Update(d);
            this.pickTimeBorder.Visibility = Visibility.Hidden;

        }

        private void CancleEditTimeRoomButton_Click(object sender, RoutedEventArgs e)
        {
            //this.secretaryRoomBox.IsEnabled = this.nedBox.IsEnabled = this.petBox.IsEnabled = this.subBox.IsEnabled = this.cetBox.IsEnabled = this.monBox.IsEnabled = this.sreBox.IsEnabled = this.utoBox.IsEnabled = false;
            this.NavigationService.GoBack();
        }

        private void MonBox_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //METODA KADA SE DRUGI COMBOBOX POPUNI ONDA TREBA DA NAPRAVIMO LISTU OD SVIH DATUMA U ZADATOM PERIODU
          
            if (UserWorkingPeriod.selectedWP == null)
            {
                for (var dt = this.dateFromBox.SelectedDate.Value.Date; dt <= this.monBox.SelectedDate.Value.Date; dt = dt.AddDays(1))
                {
                    zadatiDatumi.Add(dt);
                }
                foreach (DateTime zadati in zadatiDatumi)
                {
                    //string zd = zadati.ToString("dd.MM.yyyy.");

                    //Vremena.Add(zadati);
                    
                    Dt.Add(new WorkingT { Dates = zadati, Times = Times, StartTime = "", EndTime = "" });

                }
            }
           

           

            this.datesGrid.ItemsSource = Dt;



        }

        private void EndTimeComboBoxBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            

        }

        private void AddNewDaysTime_Click(object sender, RoutedEventArgs e)
        {
            //LOGIKA KADA SE POPUNI VREME ON TREBA DA U LISTI IZLITA SVE DATUME SA POPUNJENIM TIM VREMENOM
            //OVO SE MENJA KADA SELEKTUJEMO/ODSELEKTUJEMO DAN PREKO CHECKBOXA
            //U LISTI NAM IZLISTAVA SAMO SELEKTOVANE DANE I POPUNJAVA IH SA
            //this.datesGrid.ItemsSource = Dt;
            foreach (Model2.WorkingT z in Dt)
            {

                if (z.Dates.DayOfWeek.ToString().Equals("Monday") && ponCheck.IsChecked == true)
                {
                    z.StartTime = this.fromComboBoxBox.Text;
                    z.EndTime = this.endTimeComboBoxBox.Text;
                }
                else if (z.Dates.DayOfWeek.ToString().Equals("Tuesday") && utCheck.IsChecked == true)
                {
                    z.StartTime = this.fromComboBoxBox.Text;
                    z.EndTime = this.endTimeComboBoxBox.Text;
                }
                else if (z.Dates.DayOfWeek.ToString().Equals("Wednesday") && srCheck.IsChecked == true)
                {
                    z.StartTime = this.fromComboBoxBox.Text;
                    z.EndTime = this.endTimeComboBoxBox.Text;
                }
                else if (z.Dates.DayOfWeek.ToString().Equals("Thursday") && cetCheck.IsChecked == true)
                {
                    z.StartTime = this.fromComboBoxBox.Text;
                    z.EndTime = this.endTimeComboBoxBox.Text;
                }
                else if (z.Dates.DayOfWeek.ToString().Equals("Friday") && petCheck.IsChecked == true)
                {
                    z.StartTime = this.fromComboBoxBox.Text;
                    z.EndTime = this.endTimeComboBoxBox.Text;
                }
                else if (z.Dates.DayOfWeek.ToString().Equals("Saturday") && subCheck.IsChecked == true)
                {
                    z.StartTime = this.fromComboBoxBox.Text;
                    z.EndTime = this.endTimeComboBoxBox.Text;
                }
                else if (z.Dates.DayOfWeek.ToString().Equals("Sunday") && nedCheck.IsChecked == true)
                {
                    z.StartTime = this.fromComboBoxBox.Text;
                    z.EndTime = this.endTimeComboBoxBox.Text;
                }



            }



            this.datesGrid.ItemsSource = null;
            this.datesGrid.ItemsSource = Dt;
        }

      

       

        private void PonCheck_Checked(object sender, RoutedEventArgs e)
        {
            if ((ponCheck.IsChecked == true) || (utCheck.IsChecked == true) || (srCheck.IsChecked == true) || (cetCheck.IsChecked == true) || (petCheck.IsChecked == true) || (subCheck.IsChecked == true) || (nedCheck.IsChecked == true))
            {

                addNewDaysTime.IsEnabled = true;


            }
            else
            {
                addNewDaysTime.IsEnabled = false;
            }
        }

        private void PonCheck_Unchecked(object sender, RoutedEventArgs e)
        {
            if ((ponCheck.IsChecked == true) || (utCheck.IsChecked == true) || (srCheck.IsChecked == true) || (cetCheck.IsChecked == true) || (petCheck.IsChecked == true) || (subCheck.IsChecked == true) || (nedCheck.IsChecked == true))
            {

                addNewDaysTime.IsEnabled = true;


            }
            else
            {
                addNewDaysTime.IsEnabled = false;
            }
        }

        private void DatesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.pickTimeBorder.Visibility = Visibility.Visible;
            WorkingT wt = datesGrid.SelectedItem as WorkingT;
            this.dateText.Text = wt.Dates.ToString("dd.MM.yyyy.");
           
        }

        private void ConfirmTime_Click(object sender, RoutedEventArgs e)
        {

            WorkingT wt = datesGrid.SelectedItem as WorkingT;
            wt.StartTime = this.fromCombo.Text;
            wt.EndTime = this.endCombo.Text;
            
            this.pickTimeBorder.Visibility = Visibility.Hidden;


        }

        private void CancelTime_Click(object sender, RoutedEventArgs e)
        {
            this.pickTimeBorder.Visibility = Visibility.Collapsed;
        }

        private void AddForExistingUser_Click(object sender, RoutedEventArgs e)
        {
            List<WorkingTime> wtt = new List<WorkingTime>();
            WorkingPeriod wp = new WorkingPeriod(0, DateTime.Parse(this.dateFromBox.Text), DateTime.Parse(this.monBox.Text), wtt);

            foreach (WorkingT ww in Dt)
            {
                if (ww.StartTime != "" && ww.EndTime != "")
                {

                    WorkingTime workingTime = new WorkingTime(0, DateTime.Parse(ww.StartTime), DateTime.Parse(ww.EndTime), ww.Dates);
                    wtt.Add(workingTime);
                    MainWindow.workingTimeController.Create(workingTime);
                }
            }
            wp.WorkingTime = wtt;

            MainWindow.workingPeriodController.Create(wp);


            WorkingPeriod[] wp2 = new WorkingPeriod[d.WorkingPeriod.Length + 1];
            for (int i = 0; i < wp2.Length; i++)
            {
                if (i == wp2.Length - 1)
                {
                    wp2[i] = wp;
                }
                else
                {
                    wp2[i] = d.WorkingPeriod[i];
                }


            }


            d.WorkingPeriod = wp2;
            //MainWindow.doctorController.Update(d);
            this.pickTimeBorder.Visibility = Visibility.Hidden;
            //



            // MainWindow.workingPeriodController.Create(wp);
            //this.NavigationService.GoBack();
            UserWorkingPeriod zz = new UserWorkingPeriod(d);
            zz.addUser.Visibility = Visibility.Hidden;
            this.NavigationService.Navigate(zz);
        }
    }
}
