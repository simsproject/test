﻿#pragma checksum "..\..\RoomMore.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E504FEC7E04ED95B8BDE83585C8FC8D4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using wpfKlinika;


namespace wpfKlinika {
    
    
    /// <summary>
    /// RoomMore
    /// </summary>
    public partial class RoomMore : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid roomMoreGrid;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock redName;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border nameBorder;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox roomName;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock redFloor;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border floorBorder;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox roomFloor;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border infoBorder;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox moreInfoBox;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelAddRoomButton;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button roomBackButton;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button deleteRoomButton;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button confirmAddRoomButton;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button confirmEditButton;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelEditButton;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button editRoomButton;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\RoomMore.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal wpfKlinika.RoomRemoveMessage RemoveRoomMessage;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/wpfKlinika;component/roommore.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\RoomMore.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.roomMoreGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.redName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.nameBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.roomName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.redFloor = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.floorBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 7:
            this.roomFloor = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.infoBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 9:
            this.moreInfoBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.cancelAddRoomButton = ((System.Windows.Controls.Button)(target));
            
            #line 66 "..\..\RoomMore.xaml"
            this.cancelAddRoomButton.Click += new System.Windows.RoutedEventHandler(this.CancelAddRoomButton_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.roomBackButton = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\RoomMore.xaml"
            this.roomBackButton.Click += new System.Windows.RoutedEventHandler(this.RoomBackButton_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.deleteRoomButton = ((System.Windows.Controls.Button)(target));
            
            #line 82 "..\..\RoomMore.xaml"
            this.deleteRoomButton.Click += new System.Windows.RoutedEventHandler(this.DeleteRoomButton_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.confirmAddRoomButton = ((System.Windows.Controls.Button)(target));
            
            #line 90 "..\..\RoomMore.xaml"
            this.confirmAddRoomButton.Click += new System.Windows.RoutedEventHandler(this.ConfirmAddRoomButton_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.confirmEditButton = ((System.Windows.Controls.Button)(target));
            
            #line 98 "..\..\RoomMore.xaml"
            this.confirmEditButton.Click += new System.Windows.RoutedEventHandler(this.ConfirmEditButton_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.cancelEditButton = ((System.Windows.Controls.Button)(target));
            
            #line 106 "..\..\RoomMore.xaml"
            this.cancelEditButton.Click += new System.Windows.RoutedEventHandler(this.CancelEditButton_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.editRoomButton = ((System.Windows.Controls.Button)(target));
            
            #line 114 "..\..\RoomMore.xaml"
            this.editRoomButton.Click += new System.Windows.RoutedEventHandler(this.EditRoomButton_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.RemoveRoomMessage = ((wpfKlinika.RoomRemoveMessage)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

