﻿#pragma checksum "..\..\UserTimeRoom.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "42E11771DD15065E32B5E8A814E79009"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using wpfKlinika;


namespace wpfKlinika {
    
    
    /// <summary>
    /// UserTimeRoom
    /// </summary>
    public partial class UserTimeRoom : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 30 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border dateFromBorder;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateFromBox;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border fromComboBoxBorder;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox fromComboBoxBox;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border endTimeComboBoxBorder;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox endTimeComboBoxBox;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addNewDaysTime;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ponCheck;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox utCheck;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox srCheck;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cetCheck;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox petCheck;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox subCheck;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox nedCheck;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border redMon;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker monBox;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid datesGrid;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn startTime;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn endTime;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancleEditTimeRoomButton;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button editTimeRoomButton;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addFinishButton;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addForExistingUser;
        
        #line default
        #line hidden
        
        
        #line 193 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button editConfirmButton;
        
        #line default
        #line hidden
        
        
        #line 206 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal wpfKlinika.AddUserMessage AddUserMessage;
        
        #line default
        #line hidden
        
        
        #line 210 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border pickTimeBorder;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock dateText;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox fromCombo;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox endCombo;
        
        #line default
        #line hidden
        
        
        #line 221 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button confirmTime;
        
        #line default
        #line hidden
        
        
        #line 230 "..\..\UserTimeRoom.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelTime;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/wpfKlinika;component/usertimeroom.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\UserTimeRoom.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dateFromBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 2:
            this.dateFromBox = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 3:
            this.fromComboBoxBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.fromComboBoxBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 5:
            this.endTimeComboBoxBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 6:
            this.endTimeComboBoxBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 59 "..\..\UserTimeRoom.xaml"
            this.endTimeComboBoxBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EndTimeComboBoxBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.addNewDaysTime = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\UserTimeRoom.xaml"
            this.addNewDaysTime.Click += new System.Windows.RoutedEventHandler(this.AddNewDaysTime_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ponCheck = ((System.Windows.Controls.CheckBox)(target));
            
            #line 75 "..\..\UserTimeRoom.xaml"
            this.ponCheck.Checked += new System.Windows.RoutedEventHandler(this.PonCheck_Checked);
            
            #line default
            #line hidden
            
            #line 75 "..\..\UserTimeRoom.xaml"
            this.ponCheck.Unchecked += new System.Windows.RoutedEventHandler(this.PonCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 9:
            this.utCheck = ((System.Windows.Controls.CheckBox)(target));
            
            #line 76 "..\..\UserTimeRoom.xaml"
            this.utCheck.Checked += new System.Windows.RoutedEventHandler(this.PonCheck_Checked);
            
            #line default
            #line hidden
            
            #line 76 "..\..\UserTimeRoom.xaml"
            this.utCheck.Unchecked += new System.Windows.RoutedEventHandler(this.PonCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 10:
            this.srCheck = ((System.Windows.Controls.CheckBox)(target));
            
            #line 77 "..\..\UserTimeRoom.xaml"
            this.srCheck.Checked += new System.Windows.RoutedEventHandler(this.PonCheck_Checked);
            
            #line default
            #line hidden
            
            #line 77 "..\..\UserTimeRoom.xaml"
            this.srCheck.Unchecked += new System.Windows.RoutedEventHandler(this.PonCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 11:
            this.cetCheck = ((System.Windows.Controls.CheckBox)(target));
            
            #line 78 "..\..\UserTimeRoom.xaml"
            this.cetCheck.Checked += new System.Windows.RoutedEventHandler(this.PonCheck_Checked);
            
            #line default
            #line hidden
            
            #line 78 "..\..\UserTimeRoom.xaml"
            this.cetCheck.Unchecked += new System.Windows.RoutedEventHandler(this.PonCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 12:
            this.petCheck = ((System.Windows.Controls.CheckBox)(target));
            
            #line 79 "..\..\UserTimeRoom.xaml"
            this.petCheck.Checked += new System.Windows.RoutedEventHandler(this.PonCheck_Checked);
            
            #line default
            #line hidden
            
            #line 79 "..\..\UserTimeRoom.xaml"
            this.petCheck.Unchecked += new System.Windows.RoutedEventHandler(this.PonCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 13:
            this.subCheck = ((System.Windows.Controls.CheckBox)(target));
            
            #line 80 "..\..\UserTimeRoom.xaml"
            this.subCheck.Checked += new System.Windows.RoutedEventHandler(this.PonCheck_Checked);
            
            #line default
            #line hidden
            
            #line 80 "..\..\UserTimeRoom.xaml"
            this.subCheck.Unchecked += new System.Windows.RoutedEventHandler(this.PonCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 14:
            this.nedCheck = ((System.Windows.Controls.CheckBox)(target));
            
            #line 81 "..\..\UserTimeRoom.xaml"
            this.nedCheck.Checked += new System.Windows.RoutedEventHandler(this.PonCheck_Checked);
            
            #line default
            #line hidden
            
            #line 81 "..\..\UserTimeRoom.xaml"
            this.nedCheck.Unchecked += new System.Windows.RoutedEventHandler(this.PonCheck_Unchecked);
            
            #line default
            #line hidden
            return;
            case 15:
            this.redMon = ((System.Windows.Controls.Border)(target));
            return;
            case 16:
            this.monBox = ((System.Windows.Controls.DatePicker)(target));
            
            #line 94 "..\..\UserTimeRoom.xaml"
            this.monBox.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.MonBox_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 17:
            this.datesGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 112 "..\..\UserTimeRoom.xaml"
            this.datesGrid.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.DatesGrid_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 18:
            this.startTime = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 19:
            this.endTime = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 20:
            this.cancleEditTimeRoomButton = ((System.Windows.Controls.Button)(target));
            
            #line 150 "..\..\UserTimeRoom.xaml"
            this.cancleEditTimeRoomButton.Click += new System.Windows.RoutedEventHandler(this.CancleEditTimeRoomButton_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.editTimeRoomButton = ((System.Windows.Controls.Button)(target));
            
            #line 160 "..\..\UserTimeRoom.xaml"
            this.editTimeRoomButton.Click += new System.Windows.RoutedEventHandler(this.EditTimeRoomButton_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.addFinishButton = ((System.Windows.Controls.Button)(target));
            
            #line 172 "..\..\UserTimeRoom.xaml"
            this.addFinishButton.Click += new System.Windows.RoutedEventHandler(this.AddFinishButton_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.addForExistingUser = ((System.Windows.Controls.Button)(target));
            
            #line 183 "..\..\UserTimeRoom.xaml"
            this.addForExistingUser.Click += new System.Windows.RoutedEventHandler(this.AddForExistingUser_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.editConfirmButton = ((System.Windows.Controls.Button)(target));
            
            #line 193 "..\..\UserTimeRoom.xaml"
            this.editConfirmButton.Click += new System.Windows.RoutedEventHandler(this.EditConfirmButton_Click);
            
            #line default
            #line hidden
            return;
            case 25:
            this.AddUserMessage = ((wpfKlinika.AddUserMessage)(target));
            return;
            case 26:
            this.pickTimeBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 27:
            this.dateText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 28:
            this.fromCombo = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 29:
            this.endCombo = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 30:
            this.confirmTime = ((System.Windows.Controls.Button)(target));
            
            #line 221 "..\..\UserTimeRoom.xaml"
            this.confirmTime.Click += new System.Windows.RoutedEventHandler(this.ConfirmTime_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.cancelTime = ((System.Windows.Controls.Button)(target));
            
            #line 230 "..\..\UserTimeRoom.xaml"
            this.cancelTime.Click += new System.Windows.RoutedEventHandler(this.CancelTime_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

