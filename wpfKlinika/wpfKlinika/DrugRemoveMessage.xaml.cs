﻿using Controller;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for DrugRemoveMessage.xaml
    /// </summary>
    public partial class DrugRemoveMessage : UserControl
    {
        public DrugRemoveMessage()
        {
            InitializeComponent();
        }

        private void CancelDeleteDrugButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            
        }

        private void ConfirmDeleteDrugButton_Click(object sender, RoutedEventArgs e)
        {
            //Medicine.Meds.Remove()

           
            MainWindow.drugController.Delete(Medicine.meds);
           

            Medicine.Meds.Remove(Medicine.meds);
            NavigationService nav = NavigationService.GetNavigationService(this);
            this.Visibility = Visibility.Hidden;
            nav.Navigate(new Medicine());
        }
    }
}
