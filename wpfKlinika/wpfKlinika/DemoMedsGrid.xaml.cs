﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for DemoMedsGrid.xaml
    /// </summary>
    public partial class DemoMedsGrid : Page
    {
        public  Boolean stopp = false;
        public static DrugMore addDrug;
        public static DrugMore drugMore;
        public static DemoValList validationList;

        public static Model2.Meds meds = null;

        private static ObservableCollection<Model2.Meds> _meds = new ObservableCollection<Model2.Meds>();
        public static ObservableCollection<Model2.Meds> MedsDEMO
        {
            get { return _meds; }
            set { value = _meds; }

        }

        static DemoMedsGrid()
        {
            MedsDEMO = new ObservableCollection<Model2.Meds>();
            MedsDEMO.Add(new Model2.Meds { Name = "BRUFEN", Code = "1123AA", Quantity = "100", Exparation = "10/2020", Ingredients = "*************************" });
          

        }

        public DemoMedsGrid()
        {
            InitializeComponent();
            this.DataContext = this;
           


        }

       
        

        private void SearchMeds_Button_Click(object sender, RoutedEventArgs e)
        {

        }


        private void SearchBox_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            


        }

        private void SearchBox_MouseLeave(object sender, MouseEventArgs e)
        {
           
        }

        private void MedsMoreButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void ValidationListButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void StopDemo_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.stop = true;
            stopp = true;
            this.NavigationService.Navigate(new Medicine());
        }

        /*
          private async Task startingDemo(Object sender, EventArgs)
          {
              Console.WriteLine("ds");
              await Task.Delay(1000);


          }
          */
    }
}
