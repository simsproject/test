﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RateApp.xaml
    /// </summary>
    public partial class RateApp : Page
    {
        private static ObservableCollection<Model2.Rate> _rate = new ObservableCollection<Model2.Rate>();
        public static ObservableCollection<Model2.Rate> Rate
        {
            get { return _rate; }
            set { _rate = value; }
        }

        public RateApp()
        {
            InitializeComponent();

            this.DataContext = this;
            //Doctors = new ObservableCollection<Model.Doctors>();
            if (Rate.Count == 0)
            {
                Rate.Add(new Model2.Rate {Grade = "1" });
                Rate.Add(new Model2.Rate { Grade = "2" });
                Rate.Add(new Model2.Rate { Grade = "3" });
                Rate.Add(new Model2.Rate { Grade = "4" });
                Rate.Add(new Model2.Rate { Grade = "5" });

            }

           // rateComboBox.ItemsSource = Rate;

        }

        private void RateButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.disableHeadButtons();
            this.rateAppMessage.Visibility = Visibility.Visible;
            this.rateButton.IsEnabled = false;
            this.quitRateButton.IsEnabled = false;
        }

        private void QuitRateButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Home());


        }

        
    }
}
