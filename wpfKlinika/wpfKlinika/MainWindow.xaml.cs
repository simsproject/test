﻿using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System.ComponentModel;




using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Repository;
using Service;
using Controller;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Grid buttons;

        //DEMO LOGIKA
        public static Home home = new Home();
        //public static Boolean stop = false;
        
        //public static Medicine meds = new Medicine();
        public  DemoMedsGrid demoMeds = new DemoMedsGrid();
        public  DemoValList demoValList = new DemoValList();
        public  DemoValList2 demoValList2 = new DemoValList2();
        public  DemoAddDrug demoAddDrug = new DemoAddDrug();
        public  DemoSendValList demoSend = new DemoSendValList();
        public  Boolean demoMode = false; //da znam da li da dodajem u tabelu info ili ne
        public  Button demoButt;

        /*
        public static StorageRepository storageRepository = new StorageRepository();
        public static OfficeRepository officeRepository = new OfficeRepository();
        public static MedicalRoomRepository medicalRoomRepository = new MedicalRoomRepository();
        public static RoomRepository roomRepository = new RoomRepository();
        public static RenovationRepository renovationRepository = new RenovationRepository();
        public static BedRepository bedRepository = new BedRepository();
        public static PatientRoomRepository patientRoomRepository = new PatientRoomRepository();
        public static ValidationListRepository validationListRepository = new ValidationListRepository();
        public static DoctorRepository doctorRepository = new DoctorRepository();
        public static CityRepository cityRepository = new CityRepository();
        public static AddressRepository addressRepository = new AddressRepository();
        public static AppointmentRepository appointmentRepository = new AppointmentRepository();
        public static DrugRepository drugRepository = new DrugRepository();
        public static PatientRepository patientRepository = new PatientRepository();
        public static UserRepository userRepository = new UserRepository();
        public static HospitalTreatmentRepository hospitalTreatmentRepository = new HospitalTreatmentRepository();
        public static WorkingPeriodRepository workingPeriodRepository = new WorkingPeriodRepository();
        public static WorkingTimeRepository workingTimeRepository = new WorkingTimeRepository();
        public static InventoryRepository inventoryRepository = new InventoryRepository();
        public static ManagerRepository managerRepository = new ManagerRepository();


        public static StorageService storageService = new StorageService(storageRepository);
        public static OfficeService officeService = new OfficeService(officeRepository);
        public static MedicalRoomService medicalRoomService = new MedicalRoomService(medicalRoomRepository);
        public static RoomService roomService = new RoomService(roomRepository);
        public static RenovationService renovationService = new RenovationService(renovationRepository);
        public static BedService bedService = new BedService(bedRepository);
        public static PatientRoomService patientRoomService = new PatientRoomService(patientRoomRepository);
        public static ValidationListService validationListService = new ValidationListService(validationListRepository);
        public static DoctorService doctorService = new DoctorService(doctorRepository);
        public static CityService cityService = new CityService(cityRepository);
        public static AddressService addressService = new AddressService(addressRepository);
        public static AppointmentService appointmentService = new AppointmentService(appointmentRepository);
        public static DrugService drugService = new DrugService(drugRepository);
        public static PatientService patientService = new PatientService(patientRepository);
        public static UserService userService = new UserService(userRepository);
        public static WorkingPeriodService workingPeriodService = new WorkingPeriodService(workingPeriodRepository);
        public static WorkingTimeService workingTimeService = new WorkingTimeService(workingTimeRepository);
        public static InventoryService inventoryService = new InventoryService(inventoryRepository);
        public static ManagerService managerService = new ManagerService(managerRepository);
        */

        public static CountryController countryController = CountryController.GetInstance();
        public static StorageController storageController = StorageController.GetInstance();
        public static SecretaryController secretaryController = SecretaryController.GetInstance();
        public static OfficeController officeController = OfficeController.GetInstance();
        public static MedicalRoomController medicalRoomController = MedicalRoomController.GetInstance();
        public static RoomController roomController = RoomController.GetInstance();
        public static RenovationController renovationController = RenovationController.GetInstance();
        public static BedController bedController = BedController.GetInstance();
        public static PatientRoomController patientRoomController = PatientRoomController.GetInstance();
        
        public static DoctorController doctorController = DoctorController.GetInstance();
        public static CityController cityController = CityController.GetInstance();
        public static AdressController adressController = AdressController.GetInstance();
        public static AppointmentController appointmentController = AppointmentController.GetInstance();
        public static DrugController drugController = DrugController.GetInstance();
        public static PatientController patientController = PatientController.GetInstance();
        public static UserController userController = UserController.GetInstance();
        public static WorkingPeriodController workingPeriodController = WorkingPeriodController.GetInstance();
        public static WorkingTimeController workingTimeController = WorkingTimeController.GetInstance();
        public static InventoryController inventoryController = InventoryController.GetInstance();
        public static ManagerController managerController = ManagerController.GetInstance();




        public MainWindow()
        {
            InitializeComponent();

            buttons = HeadButtons;
            demoButt = Demo;
           
            Main.Content = new Login();
            //Console.WriteLine("lg");


            //Page currentPage = Main.Content as Frame;
        }

        private void Home_Click(object sender, RoutedEventArgs e)
        {
            //Page home = new Home();
           // home = new Home();
            Main.Content = home;
        }

        private void Account_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new Account();

        }

        public static void hideHeadButtons()
        {
            if (buttons != null)
                buttons.Visibility = Visibility.Hidden;
        }

        public static void displayHeadButtons()
        {
            if (buttons != null)
                buttons.Visibility = Visibility.Visible;
        }

        public static void disableHeadButtons()
        {
            if (buttons != null)
                buttons.IsEnabled = false;
        }

        public static void enableHeadButtons()
        {
            if (buttons != null)
                buttons.IsEnabled = true;
        }
       

        //DEMO LOGIKA
        private void Demo_Click(object sender, RoutedEventArgs e)
        {
            //Page currentPage = Main.Content as Page;
            //Page home = new Home();

            /*demoMode = true;
            Page currentPage = Main.Content as Page;
            if (Main.Content.Equals(new Account()))
            {
                Console.WriteLine("ds");
            }*/

            //Home home = new Home();
            /*if (Main.Content.Equals(home))
            {

                Home.bt.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
                typeof(Button).GetMethod("set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(Home.bt, new object[] { true });
                //typeof(Button).GetMethod("set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(Home.bt, new object[] { false });
            }*/
            


            //LOGIKA RADI ZATO STO JE MEDS STATIC I TAMO GDE SE POZIVA MEDS USTV SE SAMO POZOVE TO  OPET!
            //URADITI OVAKO JOS NESTO
            /*
            if (Main.Content.Equals(meds))
            {
                home.demoText.Visibility = Visibility.Visible;
                home.demoBorder.BorderBrush = Brushes.Red;
               
                    taskDel();


                


            }
            demoValList.stopp = demoAddDrug.stopp = demoValList2.stopp = demoSend.stopp = false;*/


        }

        public async void taskDel()
        {
            await Task.Delay(2000);
            //Home.bt.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //typeof(Button).GetMethod("set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(Home.bt, new object[] { true });
            //typeof(Button).GetMethod("set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(Home.bt, new object[] { false });
            Main.NavigationService.Navigate(demoMeds);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
            {
                home.demoText.Visibility = Visibility.Hidden;
                home.demoBorder.BorderBrush = Brushes.Transparent;
            }
            await Task.Delay(4000);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                Main.NavigationService.Navigate(demoValList);

            await Task.Delay(4000);
            if (demoValList.stopp != true && demoAddDrug.stopp!=true && demoValList2.stopp != true && demoSend.stopp != true)
                Main.NavigationService.Navigate(demoAddDrug);

            //string s = "Brufen";
            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugName.Text = "B";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugName.Text = "BR";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugName.Text = "BRU";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugName.Text = "BRUF";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugName.Text = "BRUFE";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugName.Text = "BRUFEN";
                await Task.Delay(500);


            //SIFRA
            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugCode.Text = "1";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugCode.Text = "12";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugCode.Text = "123";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugCode.Text = "1234";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugCode.Text = "12345";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugCode.Text = "123456";
                await Task.Delay(500);

            //SASTOJCI
            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "a";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "an";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "ana";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anas";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anast";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastr";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastro";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol,";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol, ";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol, l";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol, li";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol, liz";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol, lizi";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.ingredientsBox.Text = "anastrozol, lizin";
                await Task.Delay(500);


            //ROK
            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugEndDateBox.Text = "0";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugEndDateBox.Text = "01";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugEndDateBox.Text = "01-";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugEndDateBox.Text = "01-20";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugEndDateBox.Text = "01-202";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugEndDateBox.Text = "01-2021";
                await Task.Delay(500);

            //KOLICINA

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugAmountBox.Text = "1";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugAmountBox.Text = "10";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.drugAmountBox.Text = "100";
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoAddDrug.addBorder.BorderBrush = Brushes.Red;
                await Task.Delay(2000);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
            {
                Main.NavigationService.Navigate(demoValList2);
                demoAddDrug.addBorder.BorderBrush = Brushes.LightGray;
                await Task.Delay(1000);
            }

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoValList2.sendBorder.BorderBrush = Brushes.Red;
                await Task.Delay(2000);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
            {
                Main.NavigationService.Navigate(demoSend);
                demoValList2.sendBorder.BorderBrush = Brushes.LightGray;
                demoSend.doctorComboBox.Text = "P";
                await Task.Delay(1000);
            }

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
            {
                demoSend.doctorComboBox.Text = "Petar Markovic";
                await Task.Delay(1000);

            }

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
                demoSend.sendBorder.BorderBrush = Brushes.Red;
                await Task.Delay(500);

            if (demoValList.stopp != true && demoAddDrug.stopp != true && demoValList2.stopp != true && demoSend.stopp != true)
            {
                //Main.NavigationService.Navigate(meds);
                demoSend.sendBorder.BorderBrush = Brushes.LightGray;
            }

            
        }
    }
}
