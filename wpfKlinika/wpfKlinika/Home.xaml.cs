﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{

    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    
    public partial class Home : Page
    {
        private static ObservableCollection<Model2.Room> _rooms = new ObservableCollection<Model2.Room>();
        public static ObservableCollection<Model2.Room> Room
        {
            get { return _rooms; }
            set { _rooms = value; }
        }

        private static ObservableCollection<Model2.Smene> _smene = new ObservableCollection<Model2.Smene>();
        public static ObservableCollection<Model2.Smene> Smene
        {
            get { return _smene; }
            set { _smene = value; }
        }

        public static Button bt = null;

        static Home()
        {
            
            
                List<String> radnici = new List<string>();
                radnici.Add("Marko Markovic");
                radnici.Add("Petar Petrovic");
                radnici.Add("Zika Simic");

            List<DateTime> reno = new List<DateTime>();
            reno.Add(new DateTime(2020, 6, 17));
            reno.Add(new DateTime(2020, 6, 4));
            reno.Add(new DateTime(2020, 6, 3));
            reno.Add(new DateTime(2020, 6, 2));
            reno.Add(new DateTime(2020, 6, 1));


            List<DateTime> reno1 = new List<DateTime>();
            reno1.Add(new DateTime(2020, 6, 17));
            reno1.Add(new DateTime(2020, 6, 1));
            reno1.Add(new DateTime(2020, 6, 9));
            reno1.Add(new DateTime(2020, 6, 18));
            reno1.Add(new DateTime(2020, 6, 1));

            List<DateTime> reno2 = new List<DateTime>();
            reno2.Add(new DateTime(2020, 6, 17));
            reno2.Add(new DateTime(2020, 6, 21));
            reno2.Add(new DateTime(2020, 6, 4));
            reno2.Add(new DateTime(2020, 6, 11));
            reno2.Add(new DateTime(2020, 6, 1));

            List<DateTime> reno3 = new List<DateTime>();
            reno3.Add(new DateTime(2020, 6, 11));
            reno3.Add(new DateTime(2020, 6, 2));
            reno3.Add(new DateTime(2020, 6, 4));
            reno3.Add(new DateTime(2020, 6, 14));
            reno3.Add(new DateTime(2020, 6, 1));


            Room.Add(new Model2.Room { RoomName = "200 B", Floor = "1", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE" , Renovation = reno});
                Room.Add(new Model2.Room { RoomName = "102 A", Floor = "2", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE", Renovation = reno1 });
                Room.Add(new Model2.Room { RoomName = "179", Floor = "3", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE" , Renovation = reno3 });
                Room.Add(new Model2.Room { RoomName = "500 A", Floor = "4", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE", Renovation = reno1 });
                Room.Add(new Model2.Room { RoomName = "450", Floor = "2", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE", Renovation = reno2 });
                Room.Add(new Model2.Room { RoomName = "200", Floor = "1", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE", Renovation = reno3 });
                Room.Add(new Model2.Room { RoomName = "80", Floor = "6", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE" , Renovation = reno1 });
                Room.Add(new Model2.Room { RoomName = "A1", Floor = "6", Workers = radnici, Info = "SOBA ZA HITNE SLUCAJEVE", Renovation = reno });

            
                Smene.Add(new Model2.Smene { Time = "00:00-08:00" });
                Smene.Add(new Model2.Smene { Time = "08:00-16:00" });
                Smene.Add(new Model2.Smene { Time = "16:00-00:00" });
               
            

        }

        public Home()
        { 
          
            InitializeComponent();
            this.DataContext = this;
            bt = this.equipment_Button;
            //MainWindow.disableDemoButton();

         

        }

        private void RateApp_Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri("RateApp.xaml", UriKind.RelativeOrAbsolute));
            
        }

        private void Meds_Button_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService nav = NavigationService.GetNavigationService(this);
            //nav.Navigate(new Uri("Medicine.xaml", UriKind.RelativeOrAbsolute));
            this.NavigationService.Navigate(new Medicine());
        }

        private void Workers_Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri("WorkersCategories.xaml", UriKind.RelativeOrAbsolute));
        }

  

        private void Room_Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            //nav.Navigate(new Uri("Rooms.xaml", UriKind.RelativeOrAbsolute));
            nav.Navigate(new Uri("RoomType.xaml", UriKind.RelativeOrAbsolute));
            //renovation_Button.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //typeof(Button).GetMethod("set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(renovation_Button, new object[] { true });
            //typeof(Button).GetMethod("set_IsPressed", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(renovation_Button, new object[] { false });
        }

        private void Renovation_Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri("RenovationNew.xaml", UriKind.RelativeOrAbsolute));
        }

        private void Equipment_Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri("Equipment.xaml", UriKind.RelativeOrAbsolute));
           
        }
    }
}
