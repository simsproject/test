﻿using Controller;
using Model.Rooms;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RenovationNew.xaml
    /// </summary>
    public partial class RenovationNew : Page
    {
        public static List<Renovation> renovations = new List<Renovation>();
        private static ObservableCollection<Room> _docs = new ObservableCollection<Room>();
        public static ObservableCollection<Room> Roomm
        {
            get { return _docs; }
            set { value = _docs; }

        }
        public static List<Renovation> n = new List<Renovation>();
        public static List<string> roomsNames = new List<string>();







        public RenovationNew()
        {
            InitializeComponent();
            this.DataContext = this;

           
            IEnumerable<Room> n = MainWindow.roomController.GetAll();

            this.fromDate.BlackoutDates.AddDatesInPast();
            this.endDate.BlackoutDates.AddDatesInPast();


            //Room room = null;
            foreach (Room rom in n)
            {
                roomsNames.Add(rom.Name);
            }

            this.roomCombo.ItemsSource = roomsNames;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
            IEnumerable<Room> nn = MainWindow.roomController.GetAll();
            
            string roomName = this.roomCombo.SelectedItem as string;
           
            foreach (Room r in nn)
            {
                if(r.Name.Equals(roomName))
                {
                   
                    n = MainWindow.renovationController.GetRenovationsForRoom(r);
                    this.renovationsGrid.ItemsSource = n;
                    break;

                    
                    //nasli smo tu sobu
                    //sad trazimo sva renoviranja za tu sobu

                }
            }



        }

        private void RenovationsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Renovation reno = this.renovationsGrid.SelectedItem as Renovation;
            if (reno.BeginDate.CompareTo(DateTime.Now.Date) > 0)
            {
                this.removeRenovationBorder.Visibility = Visibility.Visible;
            }

        }

        private void ConfirmTime_Click(object sender, RoutedEventArgs e)
        {
            Renovation reno = this.renovationsGrid.SelectedItem as Renovation;


           
            MainWindow.renovationController.Delete(reno);
            string roomName = this.roomCombo.SelectedItem as string;


            
            


            IEnumerable<Renovation> nn = MainWindow.renovationController.GetRenovationsForRoom(MainWindow.roomController.GetByID(roomName));
            this.renovationsGrid.ItemsSource = nn;
            this.removeRenovationBorder.Visibility = Visibility.Collapsed;
        }

        private void CancelTime_Click(object sender, RoutedEventArgs e)
        {
            this.removeRenovationBorder.Visibility = Visibility.Collapsed;
        }

        private void ConfirmAddRenovation_Click(object sender, RoutedEventArgs e)
        {

            string roomName = this.roomCombo.SelectedItem as string;

            if (this.fromDate.Text.Equals("") || this.endDate.Text.Equals("") || this.opis.Text.Equals(""))
            {
                MessageBox.Show("Morate popuniti sva polja!");
            }
            else if ((this.fromDate.SelectedDate.Value.CompareTo(this.endDate.SelectedDate.Value) > 0))
            {
                MessageBox.Show("Pogresno odabrani datumi!");
            }
            else
            {

                foreach (Room r in MainWindow.roomController.GetAll())
                {
                    if (r.Name.Equals(roomName))
                    {
                        Renovation rr = new Renovation(this.fromDate.SelectedDate.Value, this.endDate.SelectedDate.Value, this.opis.Text, 0, r);
                        MainWindow.renovationController.Create(rr);
                        break;
                    }
                }

                //repaint table

                IEnumerable<Renovation> nn = MainWindow.renovationController.GetRenovationsForRoom(MainWindow.roomController.GetByID(roomName));
                this.renovationsGrid.ItemsSource = nn;


                this.addRenovationBorder.Visibility = Visibility.Collapsed;
            }
        }

        private void CancelAddRenovation_Click(object sender, RoutedEventArgs e)
        {
            this.addRenovationBorder.Visibility = Visibility.Collapsed;
        }

        private void AddRenoButton_Click(object sender, RoutedEventArgs e)
        {
            this.addRenovationBorder.Visibility = Visibility.Visible;

            

            
            
        }
    }
}
