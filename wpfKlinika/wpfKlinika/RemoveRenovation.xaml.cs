﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RemoveRenovation.xaml
    /// </summary>
    public partial class RemoveRenovation : UserControl
    {
        //private Model.Room currentRoom;
        public static  Boolean removeR;
        public static Boolean removeC;
        public RemoveRenovation()
        {
            InitializeComponent();
            //removeR = false;
            //this.currentRoom = c;
        }

        private void CancelDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            removeC = true;
            this.Visibility = Visibility.Collapsed;
            
        }

        private void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            removeR = true;
            this.Visibility = Visibility.Collapsed;
        }
    }
}
