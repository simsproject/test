﻿
using Controller;
using Model.Drugs;
using Model.Rooms;
using Model.Users;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for DrugsForValidation.xaml
    /// </summary>
    public partial class DrugsForValidation : Page
    {
        public static Model2.ValidationList valList = null;
        public static Doctor selectedDoctor = null;
        private static ObservableCollection<Drug> _meds = new ObservableCollection<Drug>();
        public static ObservableCollection<Drug> ValidationList
        {
            get { return _meds; }
            set { value = _meds; }

        }
        public static List<string> doctorsNames = new List<string>();
        public Doctor d = null;
        static DrugsForValidation()
        {

            

           
            IEnumerable<Drug> n = MainWindow.drugController.GetAll();

            //Room room = null;
            foreach (Drug drug in n)
            {

                
                if (drug.State.ToString().Equals("waiting"))
                {
                    ValidationList.Add(drug);
                }

            }
        }


        public DrugsForValidation(Doctor doc)
        {
           
            InitializeComponent();
            this.DataContext = this;
            this.d = doc;

            


            /*foreach (Doctor doc in nn)
            {
                doctorsNames.Add(doc.FirstName + " " + doc.LastName);
            }*/

            this.doctorCombo.ItemsSource = MainWindow.doctorController.GetAll().ToList();
            


            if (doc != null)
            {
                foreach (Doctor ddd in doctorCombo.Items)
                {
                    if (ddd.PersonalID.Equals(doc.PersonalID))
                    {
                        doctorCombo.SelectedItem = ddd as Doctor;
                        break;
                    }
                }
            }



        }
        private void MedsMoreButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AddMedValidationButton_Click(object sender, RoutedEventArgs e)
        {
            DrugMore drugMore = new DrugMore(null);
            drugMore.redDate.Visibility = Visibility.Visible;
            drugMore.drugCode.IsReadOnly = false;
            drugMore.drugName.IsReadOnly = false;
            this.NavigationService.Navigate(drugMore);
        }

        private void SendValidationListButton_Click(object sender, RoutedEventArgs e)
        {
            if(ValidationList.Count != 0)
            {
               
                IEnumerable<Drug> n = MainWindow.drugController.GetAll();

                //Room room = null;
                foreach (Drug drug in n)
                {
                    if (drug.State.ToString().Equals("waiting"))
                    {
                        drug.State = DrugState.sentForValidation;
                        MainWindow.drugController.Update(drug);
                       
                    }

                }
                ValidationList.Clear();


                this.NavigationService.Navigate(new Medicine());
            }

           
            
        }

        private void BackValidationListButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Medicine());

        }

      

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            /*
            string search = searchBox.Text.ToLower();
            string searchText = null;
            if (search.Contains(" "))
            {
                string[] sr = search.Split(null);
                searchText = sr[0];
            }
            else
            {
                searchText = search;
            }



            var filtered = (ValidationList.Where((d => (d.Name.ToLower().StartsWith(searchText)) || (d.Code.ToLower().StartsWith(searchText)))));


            medicineGrid.ItemsSource = filtered;
            */
        }

        private void DoctorCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


            this.addMedValidationButton.IsEnabled = true;
            
            selectedDoctor = this.doctorCombo.SelectedItem as Doctor;
            // this.doctorCombo.Text = selectedDoctor.FirstName + " " + selectedDoctor.LastName;
            doctorCombo.SelectedItem = selectedDoctor;
            
        }
    }
}
