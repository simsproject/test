﻿using Controller;
using Model.Drugs;
using Model.Users;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for DrugMore.xaml
    /// </summary>
    public partial class DrugMore : Page
    {
        public Drug d = null;
        private const string dateTime = "dd.MM.yyyy.";
        public DrugMore(Drug med)
        {
            InitializeComponent();
            this.d = med;

         /*   this.cancelAddDrugButton.IsEnabled = true;
            this.cancelEditDrugButton.IsEnabled = true;
            this.deleteDrugButton.IsEnabled = true;
            this.editDrugButton.IsEnabled = true;
            this.confirmAddDrugButton.IsEnabled = true;
            this.confirmEditDrugButton.IsEnabled = true;
            this.drugBackButton.IsEnabled = true;*/
        }

        private void CancelAddDrugButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new DrugsForValidation(DrugsForValidation.selectedDoctor));
        }

        private void DrugBackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Medicine());
        }

        public async void taskDel()
        {
            await Task.Delay(5000);
            this.cancelEditDrugButton.IsEnabled = true;
            this.confirmEditDrugButton.IsEnabled = true;
            this.cancelAddDrugButton.IsEnabled = true;
            this.deleteDrugButton.IsEnabled = true;
            this.confirmAddDrugButton.IsEnabled = true;
            this.drugBackButton.IsEnabled = true;
            this.editDrugButton.IsEnabled = true;

        }

        private void DeleteDrugButton_Click(object sender, RoutedEventArgs e)
        {
            this.DrugRemoveMessage.Visibility = Visibility.Visible;
            this.cancelAddDrugButton.IsEnabled = false;
            this.cancelEditDrugButton.IsEnabled = false;
            this.deleteDrugButton.IsEnabled = false;
            this.editDrugButton.IsEnabled = false;
            this.confirmAddDrugButton.IsEnabled = false;
            this.confirmEditDrugButton.IsEnabled = false;
            this.drugBackButton.IsEnabled = false;
            this.taskDel();
            

        }

        private void ConfirmAddDrugButton_Click(object sender, RoutedEventArgs e)
        {
            //VALIDACIJA I PROVERA
            //DA LI SU SVA POLJA POPUNJENA
            // DA LI JE SIFRA LEKA JEDINSTVENA
            //DA LI JE KOLICINA SAMO U BROJEVIMA
            //FORMAT DATUMA?

            codeBorder.BorderBrush = Brushes.LightGray;
            redCode.Visibility = Visibility.Hidden;
            Boolean code = true;
            
            Boolean amount = false;
            Boolean name = false;
           // Boolean code = false;
            Boolean ingredients = false;
            Boolean date = false;

            //PROVERA ZA KOLICINU
            if (drugAmountBox.Text.Length == 0)
            {
                amountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "morate popuniti polje";
                amount = false;
            }
            else if (!Regex.IsMatch(drugAmountBox.Text, "^[0-9]+$"))
            {
                amountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = false;
            }
            else if(drugAmountBox.Text.Equals("0"))
            {
                amountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "kolicina ne sme biti 0";
                amount = false;
            }
            else
            {
                amountBorder.BorderBrush = Brushes.LightGray;
                redAmount.Visibility = Visibility.Hidden;
                //redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = true;
            }


            //NAME PROVERA SAMO DA NIJE PRAZNO POLJE
            if (drugName.Text.Length == 0)
            {

                nameBorder.BorderBrush = Brushes.PaleVioletRed;
                redName.Visibility = Visibility.Visible;
                redName.Text = "morate popuniti polje";

            }
         
            else
            {
                nameBorder.BorderBrush = Brushes.LightGray;
                redName.Visibility = Visibility.Hidden;
                name = true;
            }

            //INGREDIENTS PROVERA SAMO DA NIJE PRAZNO POLJE
            if (ingredientsBox.Text.Length == 0)
            {

                ingredientsBorder.BorderBrush = Brushes.PaleVioletRed;
                redIngredients.Visibility = Visibility.Visible;
                redIngredients.Text = "morate popuniti polje";

            }

            else
            {
                ingredientsBorder.BorderBrush = Brushes.LightGray;
                redIngredients.Visibility = Visibility.Hidden;
                ingredients = true;
            }


            //DATUM DA NIJE PRAZAN I FORMAT
           if (drugEndDateBox.Text.Length == 0)
            {

                dateBorder.BorderBrush = Brushes.PaleVioletRed;
                redDate.Visibility = Visibility.Visible;
                redDate.Foreground = Brushes.PaleVioletRed;
                redDate.Text = "morate popuniti polje";
                date = false;

            }
            
            else
            {
                dateBorder.BorderBrush = Brushes.LightGray;
                redDate.Visibility = Visibility.Hidden;
                date = true;
            }

            //CODE MORA BITI JEDINSTVEN
            if (drugCode.Text.Length == 0)
            {
                codeBorder.BorderBrush = Brushes.PaleVioletRed;
                redCode.Visibility = Visibility.Visible;
                redCode.Text = "morate popuniti polje";
                code = false;
            }

            else if (drugCode.Text.Length == 6)
            { var room = MainWindow.drugController.GetAll();
                foreach (Drug dd in room)
                {


                    if (dd.SerialID.Equals(this.drugCode.Text))
                    {
                        codeBorder.BorderBrush = Brushes.PaleVioletRed;
                        redCode.Visibility = Visibility.Visible;
                        redCode.Text = "vec postoji lek sa ovom sifrom";
                        code = false;

                    }



                }
            

               /* foreach (Model.ValidationList dd in DrugsForValidation.ValidationList)
                {

                    if (dd.Code.Equals(this.drugCode.Text))
                    {
                        codeBorder.BorderBrush = Brushes.PaleVioletRed;
                        redCode.Visibility = Visibility.Visible;
                        redCode.Text = "vec postoji lek sa ovom sifrom";
                        code = false;

                    }


                }*/
            }
            else if(drugCode.Text.Length < 6 || drugCode.Text.Length > 6)
            {
                codeBorder.BorderBrush = Brushes.PaleVioletRed;
                redCode.Visibility = Visibility.Visible;
                redCode.Text = "sifra leka mora da sadrzi 6 karaktera";
                code = false;
            }
            /*else
            {
                codeBorder.BorderBrush = Brushes.LightGray;
                redCode.Visibility = Visibility.Hidden;
               // redCode.Text = "vec postoji ovakav korisnik";
                code = true;
            }*/

            //&& (date == true)
            if ((code == true) && (name == true) && (amount == true)  && (ingredients == true) &&(date == true))
            {
               

                        string drugName = this.drugName.Text;
                        string serialID = this.drugCode.Text;
                        string ingradients = this.ingredientsBox.Text;
                        int drugAmount = int.Parse(this.drugAmountBox.Text);
                        DateTime expirationDate = this.drugEndDateBox.SelectedDate.Value;
                        string description = this.ingredientsBox.Text;
                        string instruction = this.ingredientsBox.Text;


                        Drug dg = new Drug(serialID, drugName, expirationDate, drugAmount, ingradients, description, instruction, DrugState.waiting, DrugsForValidation.selectedDoctor);

                        MainWindow.drugController.Create(dg);


                        DrugsForValidation.ValidationList.Add(dg);


                        this.NavigationService.Navigate(new DrugsForValidation(DrugsForValidation.selectedDoctor));
                      
                    
                
                

                //Drug dg = new Drug("","",DateTime.Parse("dd.MM.yyyy."),0,"","","",DrugState.waiting);
               
            }


        }

        private void ConfirmEditDrugButton_Click(object sender, RoutedEventArgs e)
        {
            //VALIDACIJA I PROVERA DA LI JE SIFRA LEKA BROJ 
            Boolean amount = false;

            if(drugAmountBox.Text.Length == 0)
            {
                amountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "morate popuniti polje";
                amount = false;
            }
            else if (!Regex.IsMatch(drugAmountBox.Text, "^[0-9]+$"))
            {
                amountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = false;
            }
            else
            {
                amountBorder.BorderBrush = Brushes.LightGray;
                redAmount.Visibility = Visibility.Hidden;
                //redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = true;
            }

            if(amount == true)
            {
                this.drugAmountBox.IsReadOnly = true;
                this.drugBackButton.Visibility = Visibility.Visible;
                this.deleteDrugButton.Visibility = Visibility.Visible;
                this.editDrugButton.Visibility = Visibility.Visible;

                this.cancelEditDrugButton.Visibility = Visibility.Hidden;
                this.confirmEditDrugButton.Visibility = Visibility.Hidden;

                var item = Medicine.meds;
                

                if(item != null)
                {
                    item.Name = drugName.Text;
                    item.SerialID = drugCode.Text;
                    item.Amount = int.Parse(drugAmountBox.Text);
                    item.ExpirationDate =  this.drugEndDateBox.SelectedDate.Value;
                item.Ingradients = ingredientsBox.Text;

              
                MainWindow.drugController.Update(item);

            }
           }


        }

        private void CancelEditDrugButton_Click(object sender, RoutedEventArgs e)
        {

            this.drugName.IsReadOnly = this.drugCode.IsReadOnly  = this.ingredientsBox.IsReadOnly= true;
            this.drugEndDateBox.IsEnabled = false;
            this.drugAmountBox.IsReadOnly = true;

            this.drugBackButton.Visibility = Visibility.Visible;
            this.deleteDrugButton.Visibility = Visibility.Visible;
            this.editDrugButton.Visibility = Visibility.Visible;

            this.confirmEditDrugButton.Visibility = Visibility.Hidden;
            this.cancelEditDrugButton.Visibility = Visibility.Hidden;
        }

        private void EditDrugButton_Click(object sender, RoutedEventArgs e)
        {
            this.drugName.IsReadOnly = this.drugCode.IsReadOnly  =  true;
            this.drugEndDateBox.IsEnabled = false;
            this.drugAmountBox.IsReadOnly = false;

            this.drugBackButton.Visibility = Visibility.Hidden;
            this.deleteDrugButton.Visibility = Visibility.Hidden;
            this.editDrugButton.Visibility = Visibility.Hidden;

            this.confirmEditDrugButton.Visibility = Visibility.Visible;
            this.cancelEditDrugButton.Visibility = Visibility.Visible;
        }
    }
}
