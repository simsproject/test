﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for LogOutMessage.xaml
    /// </summary>
    public partial class LogOutMessage : UserControl
    {
         
        public LogOutMessage()
        {
            InitializeComponent();
            
            
        }

        private void ConfirmLogout_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;

            MainWindow.enableHeadButtons();

            NavigationService nav = NavigationService.GetNavigationService(this);
            //nav.Navigate(new Uri("Login.xaml", UriKind.RelativeOrAbsolute));

            nav.Navigate(new Login());
            

        }

        private void CancelLogout_Button_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            //nav.Navigate(new Uri("Login.xaml", UriKind.RelativeOrAbsolute));

            nav.Navigate(new Account());


        }
    }
}
