﻿using Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wpfKlinika.Model2;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for UserWorkingPeriodSecretary.xaml
    /// </summary>
    public partial class UserWorkingPeriodSecretary : Page
    {
        public static List<WorkingPeriod> Periods = new List<WorkingPeriod>();
        public static WorkingPeriod selectedWP = null;
        public Model.Users.Secretary d = null;


        public UserWorkingPeriodSecretary(Model.Users.Secretary doc)
        {
            InitializeComponent();
            this.DataContext = this;
            this.d = doc;

            Periods.Clear();
            foreach (WorkingPeriod wp in d.WorkingPeriod)
            {
                if (!Periods.Contains(wp))
                {
                    Periods.Add(wp);
                }
            }

            this.datesGrid.ItemsSource = Periods;


            if (MainWindow.secretaryController.GetByID(d.PersonalID) == null)
            {
                this.addUser.Visibility = Visibility.Visible;


                //this.addForExistingUser.Visibility = Visibility.Hidden;
            }
            else
            {

                this.addUser.Visibility = Visibility.Hidden;
                //this.addForExistingUser.Visibility = Visibility.Visible;

            }

        }

        private void DatesGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            selectedWP = datesGrid.SelectedItem as WorkingPeriod;

            if (selectedWP.Begin.CompareTo(DateTime.Now.Date) > 0)
            {
                UserTimeRoomSecretary us = new UserTimeRoomSecretary(d);

                us.dateFromBox.SelectedDate = selectedWP.Begin;
                us.monBox.SelectedDate = selectedWP.End;

                us.fromComboBoxBox.IsEnabled = false;
                us.dateFromBox.IsEnabled = false;
                us.monBox.IsEnabled = false;
                us.fromCombo.IsEnabled = false;
                us.endTimeComboBoxBox.IsEnabled = false;
                us.ponCheck.IsEnabled = false;
                us.utCheck.IsEnabled = false;
                us.srCheck.IsEnabled = false;
                us.cetCheck.IsEnabled = false;
                us.petCheck.IsEnabled = false;
                us.subCheck.IsEnabled = false;
                us.nedCheck.IsEnabled = false;
                us.addNewDaysTime.IsEnabled = false;



                //us.editConfirmButton.Visibility = Visibility.Visible;
                us.cancleEditTimeRoomButton.Visibility = Visibility.Hidden;
                us.editConfirmButton.Visibility = Visibility.Hidden;
                us.editTimeRoomButton.Visibility = Visibility.Hidden;

                us.editTimeRoomButton.Visibility = Visibility.Hidden;
                us.addFinishButton.Visibility = Visibility.Hidden;
                us.addForExistingUser.Visibility = Visibility.Hidden;
                // us.backFromViewUserTimeRoom.Visibility = Visibility.Hidden;
                //us.backTimeRoomFromAddUser.Visibility = Visibility.Hidden;




                //Console.WriteLine("aaaaaaaaaaaa");
                if (UserTimeRoomSecretary.Dt.Count == 0)
                {


                    for (var dt = us.dateFromBox.SelectedDate.Value.Date; dt <= us.monBox.SelectedDate.Value.Date; dt = dt.AddDays(1))
                    {
                        UserTimeRoom.zadatiDatumi.Add(dt);
                    }
                    foreach (DateTime zadati in UserTimeRoomSecretary.zadatiDatumi)
                    {
                        //string zd = zadati.ToString("dd.MM.yyyy.");

                        //Vremena.Add(zadati);
                        bool find = false;
                        foreach (WorkingTime wt in UserWorkingPeriodSecretary.selectedWP.WorkingTime)
                        {

                            if (zadati.ToString("dd.MM.yyyy.").Equals(wt.Date.ToString("dd.MM.yyyy.")))
                            {
                                UserTimeRoom.Dt.Add(new WorkingT { Dates = zadati, Times = UserTimeRoom.Times, StartTime = wt.BeginTime.ToString("HH:mm"), EndTime = wt.EndTime.ToString("HH:mm") });
                                find = true;
                                break;
                            }

                        }

                        if (!find)
                        {

                            UserTimeRoomSecretary.Dt.Add(new WorkingT { Dates = zadati, Times = UserTimeRoomSecretary.Times, StartTime = "", EndTime = "" });

                        }




                    }






                }








                this.datesGrid.ItemsSource = UserTimeRoomSecretary.Dt;

                this.NavigationService.Navigate(us);
            }

        }

        private void AddPeriod_Click(object sender, RoutedEventArgs e)
        {
            selectedWP = datesGrid.SelectedItem as WorkingPeriod;
            UserTimeRoomSecretary us = new UserTimeRoomSecretary(d);

            this.datesGrid.ItemsSource = UserTimeRoomSecretary.Dt;
            us.editConfirmButton.Visibility = Visibility.Hidden;
            us.cancleEditTimeRoomButton.Visibility = Visibility.Hidden;
            us.editConfirmButton.Visibility = Visibility.Hidden;

            us.editTimeRoomButton.Visibility = Visibility.Hidden;
            //us.addFinishButton.Visibility = Visibility.Visible;
            //us.addForExistingUser.Visibility = Visibility.Hidden;
            //  us.backFromViewUserTimeRoom.Visibility = Visibility.Hidden;
            // us.backTimeRoomFromAddUser.Visibility = Visibility.Hidden;

            us.fromComboBoxBox.IsEnabled = true;
            us.dateFromBox.IsEnabled = true;
            us.monBox.IsEnabled = true;
            us.fromCombo.IsEnabled = true;
            us.endTimeComboBoxBox.IsEnabled = true;
            us.ponCheck.IsEnabled = true;
            us.utCheck.IsEnabled = true;
            us.srCheck.IsEnabled = true;
            us.cetCheck.IsEnabled = true;
            us.petCheck.IsEnabled = true;
            us.subCheck.IsEnabled = true;
            us.nedCheck.IsEnabled = true;
            us.addNewDaysTime.IsEnabled = true;


            this.NavigationService.Navigate(us);
        }

        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.secretaryController.Create(d);
            MainWindow.userController.Create(d);
            this.NavigationService.Navigate(new WorkersSecretary());
        }
    }
}
