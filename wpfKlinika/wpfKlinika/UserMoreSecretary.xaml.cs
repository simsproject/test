﻿using Controller;
using Microsoft.Win32;
using Model.Rooms;
using Model.Users;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for UserMoreSecretary.xaml
    /// </summary>
    public partial class UserMoreSecretary : Page
    {
        public static UserTimeRoomSecretary userTimeRoom;
        public static UserWorkingPeriodSecretary userWP;







        public Secretary d = null;


        public UserMoreSecretary(Secretary doc)
        {
            InitializeComponent();
            this.DataContext = this;
            this.d = doc;

            var adr = MainWindow.adressController.GetAll();
            this.doctorAdressBox.ItemsSource = adr;



            var rooms = MainWindow.officeController.GetAll();
            this.roomBox.ItemsSource = rooms;


            //this.doctorNameBox = WorkersDoctors.nameSelected.;
            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorPhoneBox.IsReadOnly = this.doctorUserNameBox.IsReadOnly = this.doctorJmbgBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = true;
            this.doctorPassBox.IsEnabled = false;
            this.doctorUserNameBox.IsReadOnly = true;
            //this.doctorTitleComboBox.IsEnabled = false;
            // this.doctorCityComboBox.IsEnabled = false;
            //this.doctorCountryComboBox.IsEnabled = false;
            this.genderComboBox.IsEnabled = false;
            this.dateUser.IsEnabled = false;
            this.doctorAdressBox.IsEnabled = false;
            this.roomBox.IsEnabled = false;
            this.doctorPhoneBox.IsEnabled = false;
            //this.loadImageButton.IsEnabled = false;
            //this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = false;


            //Doctors = WorkersDoctors.Doctors;

        }

        private void populateFields(Model2.Secretary doctor)
        {
            // Funkcija koja pop
            //nameField.text = doctor.Name;
            doctorNameBox.Text = doctor.Name;
            doctorLastNameBox.Text = doctor.LastName;
        }

        private void UserTimeRoomButton_Click(object sender, RoutedEventArgs e)
        {
            //KAD VEC POSTOJI USER
            //userTR = new UserTimeRoom();
            //Model.Doctors d = new Model.Doctors { Name = this.doctorNameBox.Text, LastName = this.doctorLastNameBox.Text, Title = this.doctorTitleComboBox.Text, Room = "100", UserName = this.doctorUserNameBox.Text, Password = this.doctorPassBox.Password, Jmbg = this.doctorJmbgBox.Text, Email = this.doctorEmailBox.Text, Address = this.doctorAdressBox.Text, Image = this.imgDynamic.Source, City = this.doctorCityComboBox.Text, Country = this.doctorCountryComboBox.Text, Phone = this.doctorPhoneBox.Text };

            userTimeRoom = new UserTimeRoomSecretary(d);
            userWP = new UserWorkingPeriodSecretary(d);
            //userTimeRoom.secretaryRoomBox.IsEnabled = userTimeRoom.nedBox.IsEnabled = userTimeRoom.petBox.IsEnabled = userTimeRoom.subBox.IsEnabled = userTimeRoom.cetBox.IsEnabled = userTimeRoom.monBox.IsEnabled = userTimeRoom.sreBox.IsEnabled = userTimeRoom.utoBox.IsEnabled = false;
            userTimeRoom.editTimeRoomButton.Visibility = Visibility.Visible;
            //userWP.addUser.Visibility = Visibility.Hidden;
            //userTimeRoom.backFromViewUserTimeRoom.Visibility = Visibility.Visible;

            this.NavigationService.Navigate(userWP);
        }

        private void EditUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorPhoneBox.IsReadOnly = this.doctorUserNameBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = false;

            this.doctorUserNameBox.IsReadOnly = true;
            this.doctorJmbgBox.IsEnabled = false;
            this.doctorPassBox.IsEnabled = true;
            // this.doctorTitleComboBox.IsEnabled = true;
            //this.doctorCityComboBox.IsEnabled = true;
            // this.doctorCountryComboBox.IsEnabled = true;
            this.genderComboBox.IsEnabled = true;
            this.dateUser.IsEnabled = true;
            this.doctorAdressBox.IsEnabled = true;
            this.roomBox.IsEnabled = true;
            this.doctorPhoneBox.IsEnabled = true;


            // this.loadImageButton.IsEnabled = true;
            //this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = true;

            this.backMoreUserButton.Visibility = Visibility.Hidden;
            this.deleteUserButton.Visibility = Visibility.Hidden;
            this.userTimeRoomButton.Visibility = Visibility.Hidden;
            this.editUserButton.Visibility = Visibility.Hidden;

            this.cancelEditUserButton.Visibility = Visibility.Visible;
            this.confirmEditUserButton.Visibility = Visibility.Visible;
        }

        public async void taskDel()
        {
            await Task.Delay(5000);
            this.editUserButton.IsEnabled = true;
            this.deleteUserButton.IsEnabled = true;

            this.backMoreUserButton.IsEnabled = true;
            this.addTimeRoomButton.IsEnabled = true;


        }


        private void DeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.UserRemoveMessageSecretary.Visibility = Visibility.Visible;
            this.editUserButton.IsEnabled = false;
            this.deleteUserButton.IsEnabled = false;

            this.backMoreUserButton.IsEnabled = false;
            this.addTimeRoomButton.IsEnabled = false;
            this.taskDel();

        }

        private void BackMoreUserButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersSecretary());
        }

        private void CancelAddUserButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersSecretary());


            //
            // WorkersDoctors.Doctors.Add(new Model.Doctors { Name = "MIROSALV", LastName = "Markovic", Title = "Specijalista", Room = "102A", UserName = "petMark", Password = "********", Jmbg = "112233", Email = "petMarkovic@gmail.com", Address = "Bulevar Oslobodjenja 5, Novi Sad, Srbija" });
        }

        public void AddTimeRoomButton_Click(object sender, RoutedEventArgs e)
        {


            //kad tek pravimo usera
            //bool postoji = true;
            //JMBG


            //userNameBorder.BorderBrush = Brushes.LightGray;
            //redUserName.Visibility = Visibility.Hidden;
            Boolean user = true;



            jmbgBorder.BorderBrush = Brushes.LightGray;
            redJmbg.Visibility = Visibility.Hidden;
            Boolean jmbg = true;

            Boolean name = false;
            Boolean last = false;
            Boolean email = false;

            Boolean address = false;
            Boolean pass = false;
           // Boolean titles = false;
            Boolean phone = false;
            Boolean room = false;
            Boolean rodjenje = false;
            Boolean gend = false;
            var users = MainWindow.userController.GetAll();
            string greska = "GRESKE:\n";

            if (doctorJmbgBox.Text.Length != 0)
            {
                if (!Regex.IsMatch(doctorJmbgBox.Text, "[A-Za-z0-9]{13}"))
                {
                    jmbgBorder.BorderBrush = Brushes.PaleVioletRed;
                    // redJmbg.Visibility = Visibility.Visible;
                    // redJmbg.Text = "jmbg sme da sadrzi samo 6 brojeva";
                    greska += "\n pogresan format jmbg";
                    jmbg = false;

                }
                //izmeniti broj brojeva
                else if (doctorJmbgBox.Text.Length != 13)
                {
                    jmbgBorder.BorderBrush = Brushes.PaleVioletRed;
                    // redJmbg.Visibility = Visibility.Visible;
                    // redJmbg.Text = "jmbg sme da sadrzi samo 6 brojeva";
                    greska += "\n pogresan format jmbg";
                    jmbg = false;

                }

                else if (doctorJmbgBox.Text.Length == 13)
                {

                    foreach (User dd in users)
                    {

                        if (dd.PersonalID.Equals(this.doctorJmbgBox.Text))
                        {
                            greska += "\n vec postoji  korisnik sa tim jmbg";
                            jmbg = false;

                        }


                    }


                }



                else if (doctorJmbgBox.Text.Length == 13 && Regex.IsMatch(doctorJmbgBox.Text, "[A-Za-z0-9]{13}"))
                {
                    //MessageBox.Show("pogresan format jmbg");
                    jmbg = true;

                }


            }
            else
            {
                greska += "\n morate popuniti jmbg polje";
                jmbg = false;
            }










            if (doctorUserNameBox.Text.Length != 0)
            {
                foreach (User dd in users)
                {
                    if (dd.UserName.Equals(this.doctorUserNameBox.Text))
                    {
                        greska += "\n los format korisnickog imena";
                    }

                }

            }



            if (doctorUserNameBox.Text.Length == 0)
            {

                greska += "\n los format korisnickog imena";

            }

            //USER NAME
            if (doctorUserNameBox.Text.Length != 0)
            {
                if (!Regex.IsMatch(doctorUserNameBox.Text, "[A-Za-z0-9]{6,100}"))
                {
                    greska += "\n los format korisnickog imena";
                    user = false;

                }
                else if (doctorUserNameBox.Text.Length >= 30)
                {
                    greska += "\n los format korisnickog imena";
                    user = false;
                }
                //izmeniti broj brojeva
                else if (doctorUserNameBox.Text.Length < 6)
                {
                    greska += "\n los format korisnickog imena";
                    user = false;

                }




                else if (doctorUserNameBox.Text.Length >= 6)
                {
                    foreach (User dd in users)
                    {

                        if (dd.UserName.Equals(this.doctorUserNameBox.Text))
                        {
                            greska += "\n los format korisnickog imena";
                            user = false;

                        }




                    }
                }




            }


            else
            {
                greska += "\n los format korisnickog imena";
            }











            //LOZINKA
            if (doctorPassBox.Text.Length == 0)
            {

                greska += "\n los format lozinke";

            }
            else if (doctorPassBox.Text.Length < 4)
            {
                greska += "\n los format lozinke";
            }
            else
            {

                pass = true;
            }


            //EMAIL 
            /*if (doctorEmailBox.Text.Length == 0)
            {

                emailBorder.BorderBrush = Brushes.PaleVioletRed;
                redEmail.Visibility = Visibility.Visible;
                redEmail.Text = "morate popuniti polje";

            }*/
            if ((doctorEmailBox.Text.Length != 0) && (!Regex.IsMatch(doctorEmailBox.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")))
            {

                greska += "\n los format email";
            }
            else if (doctorEmailBox.Text.Length == 0)
            {
                greska += "\n los format email";
            }
            else
            {

                email = true;
            }





            //TREBA DODATI ZA SOBU!

            if (roomBox.Text.Equals(""))
            {
                greska += "\n los format sobe";
            }
            else
            {
                room = true;
            }

            //TREBA DODATI ZA SOBU!

            if (genderComboBox.Text.Equals(""))
            {
                greska += "\n los format pola";
            }
            else
            {
                gend = true;
            }


            if (dateUser.Text.Equals(""))
            {
                greska += "\n los format datuma rodjenja";
            }
            else
            {
                rodjenje = true;
            }


            if (doctorAdressBox.Text.Equals(""))
            {
                greska += "\n los format adrese";
            }
            else
            {
                address = true;
            }



            //IME DA MORA BITI POPUNJENO
            if (doctorNameBox.Text.Length == 0)
            {

                greska += "\n los format imena";

            }
            //ime mora poceti sa velikim slovom i bez brojeva!
            else if (!Regex.IsMatch(doctorNameBox.Text, "[A-Z][a-z]{1,20}"))
            {
                greska += "\n los format imena";
            }
            else
            {

                name = true;
            }

            //PREZIME DA MORA BITI POPUNJENO
            if (doctorLastNameBox.Text.Length == 0)
            {

                greska += "\n los format prezimena";

            }
            else if (!Regex.IsMatch(doctorLastNameBox.Text, "[A-Z][a-z]{1,20}"))
            {
                greska += "\n los format prezimena";
            }
            else
            {

                last = true;
            }

            //TELEFON

            if (doctorPhoneBox.Text.Length == 0)
            {

                greska += "\n los format telefona";

            }
            else if (!Regex.IsMatch(doctorPhoneBox.Text, "[+]?[0-9]{9,15}"))
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else if (doctorPhoneBox.Text.Length > 15)
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else if (doctorPhoneBox.Text.Length < 9)
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }

            else if (Regex.IsMatch(doctorPhoneBox.Text, "[a-zA-Z]"))
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else
            {

                phone = true;
            }




            if ((user == true) && (room == true) && (gend == true) && (rodjenje == true) && (pass == true) && (phone == true) && (name == true) && (jmbg == true) && (last == true) && (address == true) && (email == true))
            {




                if (d != null)
                {

                    
                    d.FirstName = this.doctorNameBox.Text;
                    d.LastName = this.doctorLastNameBox.Text;
                    //item.Room = this.doctorRoomBox.Text;
                    //item.Title = this.doctorTitleBox.Text;
                    d.Password = this.doctorPassBox.Text;
                    d.UserName = this.doctorUserNameBox.Text;
                    d.Email = this.doctorEmailBox.Text;
                    //d.Address.Name = this.doctorAdressBox.Text;
                    d.PersonalID = this.doctorJmbgBox.Text;
                    //tit = this.doctorTitleComboBox.Text;
                    d.PhoneNumber = this.doctorPhoneBox.Text;
                    d.Address = this.doctorAdressBox.SelectedItem as Address;
                    // d.Address.City = this.doctorCityComboBox.SelectedItem as City;
                    // d.Address.City.Country = this.doctorCountryComboBox.SelectedItem as Country;
                    d.Office = this.roomBox.SelectedItem as Office;




                    if (this.genderComboBox.Text.Equals("Female"))
                    {
                        d.Gender = Model.Users.Gender.Female;
                    }
                    else
                    {
                        d.Gender = Model.Users.Gender.Male;
                    }
                    d.DateOfBirth = this.dateUser.SelectedDate.Value;

                    // this.NavigationService.Navigate(new UserTimeRoom(doc));
                    UserTimeRoomSecretary rr = new UserTimeRoomSecretary(d);
                    //rr.secretaryRoomBox.IsEnabled = rr.nedBox.IsEnabled = rr.petBox.IsEnabled = rr.subBox.IsEnabled = rr.cetBox.IsEnabled = rr.monBox.IsEnabled = rr.sreBox.IsEnabled = rr.utoBox.IsEnabled = true;
                    //rr.addFinishButton.Visibility = Visibility.Visible;
                    //rr.backTimeRoomFromAddUser.Visibility = Visibility.Visible;


                    UserWorkingPeriodSecretary wp = new UserWorkingPeriodSecretary(d);
                    this.NavigationService.Navigate(wp);
                }
            }
            else
            {
                MessageBox.Show(greska);
            }

        }

        private void CancelEditUserButton_Click(object sender, RoutedEventArgs e)
        {







            this.NavigationService.Navigate(new WorkersDoctors());

        }



        private void LoadImageButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Uri fileUri = new Uri(openFileDialog.FileName);
                // imgDynamic.Source = new BitmapImage(fileUri);
            }
        }

        private void DoctorCountryComboBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void DoctorCityComboBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void DoctorTitleComboBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void DoctorRoomComboBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void DoctorCountryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DoctorTitleComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void DoctorRoomComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ConfirmEditUserButton_Click(object sender, RoutedEventArgs e)
        {

            //kad tek pravimo usera
            //bool postoji = true;
            //JMBG


            //userNameBorder.BorderBrush = Brushes.LightGray;
            //redUserName.Visibility = Visibility.Hidden;
            //Boolean user = true;



            //jmbgBorder.BorderBrush = Brushes.LightGray;
            //redJmbg.Visibility = Visibility.Hidden;
            //Boolean jmbg = true;

            Boolean name = false;
            Boolean last = false;
            Boolean email = false;

            Boolean address = false;
            Boolean pass = false;
           // Boolean titles = false;
            Boolean phone = false;
            Boolean room = false;
            Boolean rodjenje = false;
            Boolean gend = false;
            var users = MainWindow.userController.GetAll();
            string greska = "GRESKE:\n";

            
            //LOZINKA
            if (doctorPassBox.Text.Length == 0)
            {

                greska += "\n los format lozinke";

            }
            else if (doctorPassBox.Text.Length < 4)
            {
                greska += "\n los format lozinke";
            }
            else
            {

                pass = true;
            }


            //EMAIL 
            /*if (doctorEmailBox.Text.Length == 0)
            {

                emailBorder.BorderBrush = Brushes.PaleVioletRed;
                redEmail.Visibility = Visibility.Visible;
                redEmail.Text = "morate popuniti polje";

            }*/
            if ((doctorEmailBox.Text.Length != 0) && (!Regex.IsMatch(doctorEmailBox.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")))
            {

                greska += "\n los format email";
            }
            else if (doctorEmailBox.Text.Length == 0)
            {
                greska += "\n los format email";
            }
            else
            {

                email = true;
            }


           


            //TREBA DODATI ZA SOBU!

            if (roomBox.Text.Equals(""))
            {
                greska += "\n los format sobe";
            }
            else
            {
                room = true;
            }

            //TREBA DODATI ZA SOBU!

            if (genderComboBox.Text.Equals(""))
            {
                greska += "\n los format pola";
            }
            else
            {
                gend = true;
            }


            if (dateUser.Text.Equals(""))
            {
                greska += "\n los format datuma rodjenja";
            }
            else
            {
                rodjenje = true;
            }


            if (doctorAdressBox.Text.Equals(""))
            {
                greska += "\n los format adrese";
            }
            else
            {
                address = true;
            }



            //IME DA MORA BITI POPUNJENO
            if (doctorNameBox.Text.Length == 0)
            {

                greska += "\n los format imena";

            }
            //ime mora poceti sa velikim slovom i bez brojeva!
            else if (!Regex.IsMatch(doctorNameBox.Text, "[A-Z][a-z]{1,20}"))
            {
                greska += "\n los format imena";
            }
            else
            {

                name = true;
            }

            //PREZIME DA MORA BITI POPUNJENO
            if (doctorLastNameBox.Text.Length == 0)
            {

                greska += "\n los format prezimena";

            }
            else if (!Regex.IsMatch(doctorLastNameBox.Text, "[A-Z][a-z]{1,20}"))
            {
                greska += "\n los format prezimena";
            }
            else
            {

                last = true;
            }

            //TELEFON

            if (doctorPhoneBox.Text.Length == 0)
            {

                greska += "\n los format telefona";

            }
            else if (!Regex.IsMatch(doctorPhoneBox.Text, "[+]?[0-9]{9,15}"))
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else if (doctorPhoneBox.Text.Length > 15)
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else if (doctorPhoneBox.Text.Length < 9)
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }

            else if (Regex.IsMatch(doctorPhoneBox.Text, "[a-zA-Z]"))
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else
            {

                phone = true;
            }




            if ((room == true) && (gend == true) && (rodjenje == true) && (pass == true) && (phone == true) && (name == true)  && (last == true)  && (address == true) && (email == true))
            {
                if (d != null)
                {
                    d.FirstName = this.doctorNameBox.Text;
                    d.LastName = this.doctorLastNameBox.Text;
                    d.PersonalID = this.doctorJmbgBox.Text;
                    d.PhoneNumber = this.doctorPhoneBox.Text;
                    d.UserName = this.doctorUserNameBox.Text;
                    d.Password = this.doctorPassBox.Text;
                    d.Email = this.doctorEmailBox.Text;

                   





                    //d.Address.City.Country.Name = doctorCountryComboBox.Text;
                    //d.Address.City.Name = doctorCityComboBox.Text;

                    d.Address = this.doctorAdressBox.SelectedItem as Address;
                    // d.Address.City = this.doctorCityComboBox.SelectedItem as City;
                    // d.Address.City.Country = this.doctorCountryComboBox.SelectedItem as Country;
                    d.Office = this.roomBox.SelectedItem as Office;
                    //MedicalRoom mr = MainWindow.medicalRoomController.GetByID(d.MedicalRoom.Name);



                    if (this.genderComboBox.Text.Equals("Female"))
                    {
                        d.Gender = Model.Users.Gender.Female;
                    }
                    else
                    {
                        d.Gender = Model.Users.Gender.Male;
                    }
                    d.DateOfBirth = this.dateUser.SelectedDate.Value;




                    MainWindow.secretaryController.Update(d);
                    MainWindow.userController.Update(d);

                    //medical room, gender, address


                    this.NavigationService.Navigate(new WorkersSecretary());
                }

            }
            else
            {
                MessageBox.Show(greska);
            }
        }
    }
}
