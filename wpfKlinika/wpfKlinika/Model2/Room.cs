﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace wpfKlinika.Model2
{
    public class Room : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private string _roomName;
        private string _floor;
        private List<string> _workers;
        private string _info;

        private List<DateTime> _renovation;


        public string RoomName
        {
            get
            {
                return _roomName;
            }
            set
            {
                if (value != _roomName)
                {
                    _roomName = value;
                    OnPropertyChanged("PROSTORIJA");
                }
            }
        }

        public List<DateTime> Renovation
        {
            get
            {
                return _renovation;
            }
            set
            {
                if (value != _renovation)
                {
                    _renovation = value;
                    OnPropertyChanged("RENOVIRANJA");
                }
            }
        }

        public string Info
        {
            get
            {
                return _info;
            }
            set
            {
                if (value != _roomName)
                {
                    _info = value;
                    OnPropertyChanged("INFO");
                }
            }
        }

        public string Floor
        {
            get
            {
                return _floor;
            }
            set
            {
                if (value != _floor)
                {
                    _floor = value;
                    OnPropertyChanged("SPRAT");
                }
            }
        }

        public List<string> Workers
        {
            get
            {
                return _workers;
            }
            set
            {
                if (value != _workers)
                {
                    _workers = value;
                    OnPropertyChanged("ZAPOSLENI");
                }
            }
        }




    }
}
