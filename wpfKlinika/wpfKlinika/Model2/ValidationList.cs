﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpfKlinika.Model2
{
    public class ValidationList : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        private string _name;
        private string _code;
        private string _quantity;
        private string _ingredients;
        private string _exparation;



        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    OnPropertyChanged("NAZIV");
                }
            }
        }

        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (value != _code)
                {
                    _code = value;
                    OnPropertyChanged("SIFRA");
                }
            }
        }

        public string Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                if (value != _quantity)
                {
                    _quantity = value;
                    OnPropertyChanged("KOLICINA");
                }
            }
        }

        public string Ingredients
        {
            get
            {
                return _ingredients;
            }
            set
            {
                if (value != _ingredients)
                {
                    _ingredients = value;
                    OnPropertyChanged("SASTOJCI");
                }
            }
        }

        public string Exparation
        {
            get
            {
                return _exparation;
            }
            set
            {
                if (value != _exparation)
                {
                    _exparation = value;
                    OnPropertyChanged("ROK");
                }
            }
        }
    }
}
