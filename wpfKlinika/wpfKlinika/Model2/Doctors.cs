﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace wpfKlinika.Model2
{
    public class Doctors : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private string _name;
        private string _lastName;
        private string _title;
        private string _room;
        private string _userName;
        private string _password;
        private string _eMail;
        private string _jmbg;
        private string _address;
        private string _city;
        private string _country;
        private string _phone;


        private string _pon;
        private string _ut;
        private string _sre;
        private string _cet;
        private string _pet;
        private string _sub;
        private string _ned;

        private System.Windows.Media.ImageSource _image;

        public string Pon
        {
            get
            {
                return _pon;
            }
            set
            {
                if (value != _pon)
                {
                    _pon = value;
                    OnPropertyChanged("PON");
                }
            }
        }
        public string Uto
        {
            get
            {
                return _ut;
            }
            set
            {
                if (value != _ut)
                {
                    _ut = value;
                    OnPropertyChanged("UTO");
                }
            }
        }
        public string Sre
        {
            get
            {
                return _sre;
            }
            set
            {
                if (value != _sre)
                {
                    _sre = value;
                    OnPropertyChanged("SRE");
                }
            }
        }

        public string Cet
        {
            get
            {
                return _cet;
            }
            set
            {
                if (value != _cet)
                {
                    _cet = value;
                    OnPropertyChanged("CET");
                }
            }
        }

        public string Pet
        {
            get
            {
                return _pet;
            }
            set
            {
                if (value != _pet)
                {
                    _pet = value;
                    OnPropertyChanged("PET");
                }
            }
        }

        public string Sub
        {
            get
            {
                return _sub;
            }
            set
            {
                if (value != _sub)
                {
                    _sub = value;
                    OnPropertyChanged("SUB");
                }
            }
        }
        public string Ned
        {
            get
            {
                return _ned;
            }
            set
            {
                if (value != _ned)
                {
                    _ned = value;
                    OnPropertyChanged("NED");
                }
            }
        }


        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    OnPropertyChanged("IME");
                }
            }
        }

        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                if (value != _phone)
                {
                    _phone = value;
                    OnPropertyChanged("TELEFON");
                }
            }
        }

        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value != _city)
                {
                    _city = value;
                    OnPropertyChanged("GRAD");
                }
            }
        }

        public string Country
        {
            get
            {
                return _country;
            }
            set
            {
                if (value != _country)
                {
                    _country = value;
                    OnPropertyChanged("ZEMLJA");
                }
            }
        }


        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                if (value != _lastName)
                {
                    _lastName = value;
                    OnPropertyChanged("PREZIME");
                }
            }
        }

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (value != _title)
                {
                    _title = value;
                    OnPropertyChanged("TITULA");
                }
            }
        }

        public string Room
        {
            get
            {
                return _room;
            }
            set
            {
                if (value != _room)
                {
                    _room = value;
                    OnPropertyChanged("PROSTORIJA");
                }
            }
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                if (value != _userName)
                {
                    _userName = value;
                    OnPropertyChanged("KORISNICKO IME");
                }
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                if (value != _password)
                {
                    _password = value;
                    OnPropertyChanged("LOZINKA");
                }
            }
        }

        public string Email
        {
            get
            {
                return _eMail;
            }
            set
            {
                if (value != _eMail)
                {
                    _eMail = value;
                    OnPropertyChanged("EMAIL");
                }
            }
        }

        public string Jmbg
        {
            get
            {
                return _jmbg;
            }
            set
            {
                if (value != _jmbg)
                {
                    _jmbg = value;
                    OnPropertyChanged("JMBG");
                }
            }
        }

        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                if (value != _address)
                {
                    _address = value;
                    OnPropertyChanged("ADRESA");
                }
            }
        }
        public System.Windows.Media.ImageSource Image
        {
            get
            {
                return _image;
            }
            set
            {
                if (value != _image)
                {
                    _image = value;
                    OnPropertyChanged("FOTO");
                }
            }
        }

    }
}
