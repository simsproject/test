﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpfKlinika.Model2
{
    public class WorkingT : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private List<String> _times;
        public List<String> Times
        {
            get
            {
                return _times;
            }
            set
            {
                if (value != _times)
                {
                    _times = value;
                    OnPropertyChanged("VREMENA");
                }
            }
        }


        private DateTime _dates;
        public DateTime Dates
        {
            get
            {
                return _dates;
            }
            set
            {
                if (value != _dates)
                {
                    _dates = value;
                    OnPropertyChanged("DATUMI");
                }
            }
        }

        private string _startTime;
        public string StartTime
        {
            get
            {
                return _startTime;
            }
            set
            {
                if (value != _startTime)
                {
                    _startTime = value;
                    OnPropertyChanged("OD");
                }
            }
        }

        private string _endTime;
        public string EndTime
        {
            get
            {
                return _endTime;
            }
            set
            {
                if (value != _endTime)
                {
                    _endTime = value;
                    OnPropertyChanged("DO");
                }
            }
        }
    }
}

