﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace wpfKlinika.Model2
{
    public class EquipmentM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private string _nameE;
        private string _codeE;
        private string _quantityE;
        private string _roomE;
        private string _kind;
        private string _descript;
        private System.Windows.Media.ImageSource _image;


        public string NameE
        {
            get
            {
                return _nameE;
            }
            set
            {
                if (value != _nameE)
                {
                    _nameE = value;
                    OnPropertyChanged("NAZIV");
                }
            }
        }

        public string Descript
        {
            get
            {
                return _descript;
            }
            set
            {
                if (value != _descript)
                {
                    _descript = value;
                    OnPropertyChanged("OPIS");
                }
            }
        }

        public string Kind
        {
            get
            {
                return _kind;
            }
            set
            {
                if (value != _kind)
                {
                    _kind = value;
                    OnPropertyChanged("TIP");
                }
            }
        }


        public string CodeE
        {
            get
            {
                return _codeE;
            }
            set
            {
                if (value != _codeE)
                {
                    _codeE = value;
                    OnPropertyChanged("SIFRA");
                }
            }
        }

        public string QuantityE
        {
            get
            {
                return _quantityE;
            }
            set
            {
                if (value != _quantityE)
                {
                    _quantityE = value;
                    OnPropertyChanged("KOLICINA");
                }
            }
        }

        public string RoomE
        {
            get
            {
                return _roomE;
            }
            set
            {
                if (value != _roomE)
                {
                    _roomE = value;
                    OnPropertyChanged("PROSTORIJA");
                }
            }
        }

        public System.Windows.Media.ImageSource Image
        {
            get
            {
                return _image;
            }
            set
            {
                if (value != _image)
                {
                    _image = value;
                    OnPropertyChanged("FOTO");
                }
            }
        }




    }
}
