﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for WorkersCategories.xaml
    /// </summary>
    public partial class WorkersCategories : Page
    {
        public static WorkersDoctors workerDoc = new WorkersDoctors();
        public static WorkersSecretary workerSec = new WorkersSecretary();
        public WorkersCategories()
        {
            InitializeComponent();

        }

        private void DoctorCategoryButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(workerDoc);
            //NavigationService nav = NavigationService.GetNavigationService(this);
            //nav.Navigate(new Uri("WorkersDoctors.xaml", UriKind.RelativeOrAbsolute));
        }

        private void SecretaryButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(workerSec);
        }
    }
}
