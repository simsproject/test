﻿using Model.Users;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        //public static Button bt = null;

        public static User userName = null;

        public Login()
        {
            InitializeComponent();
            MainWindow.hideHeadButtons();
            //MainWindow.disableDemoButton();
        }

        /*public static void enableDemo()
        {
            MainWindow.enableDemoButton();
        }*/

        private void ConfirmLogin_Click(object sender, RoutedEventArgs e)
        {

            //VALIDACIJA
            /*if (this.userNameBox.Text.Equals("anaPet") && this.passBox.Password.Equals("pera"))
            {
                this.NavigationService.Navigate(MainWindow.home);
                MainWindow.displayHeadButtons();
            }
            else
            {
                this.redLogin.Visibility = Visibility.Visible;
            }*/
            IEnumerable<User> us = MainWindow.userController.GetAll();
             userName = MainWindow.userController.GetByID(this.userNameBox.Text);
            
            if (userName != null)
            {
                
                if(!userName.Password.Equals(this.passBox.Password))
                {
                    this.redLogin.Visibility = Visibility.Visible;
                }
                else
                {
                    this.NavigationService.Navigate(MainWindow.home);
                    MainWindow.displayHeadButtons();
                }
            }
            else
            {
                this.redLogin.Visibility = Visibility.Visible;
            }


            //bt = this.ConfirmLogin;


        }
    }
}
