﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RemoveRoomOfficeMessage.xaml
    /// </summary>
    public partial class RemoveRoomOfficeMessage : UserControl
    {
        public RemoveRoomOfficeMessage()
        {
            InitializeComponent();
        }
        private void CancelDeleteRoomButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void ConfirmDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            //Medicine.Meds.Remove(Medicine.meds);
            MainWindow.roomController.Delete(RoomOffice.rooms);
            MainWindow.officeController.Delete(RoomOffice.rooms);
            NavigationService nav = NavigationService.GetNavigationService(this);
            this.Visibility = Visibility.Hidden;
            nav.Navigate(new RoomOffice());
        }
    }
}
