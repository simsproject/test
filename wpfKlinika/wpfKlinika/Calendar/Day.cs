﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpfKlinika.Calendar
{
    public class Day : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private DateTime date;
        private bool isTargetMonth;
        private bool isToday;
        public bool hasRenovations;
        //public Boolean isScheduledForRenovation;      <---- OOOOOO POGLEDAJ ME 
        // Ovde dodaj sva polja koja ti trebaju za konkretan dan (tipa kod mene List<Procedure> za taj dan.)
        // Day se poziva i kreira u Calendar.BuildCalendar() funkciji

        public bool IsToday
        {
            get { return isToday; }
            set
            {
                isToday = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IsToday"));
            }
        }

        public bool HasRenovations
        {
            get { return hasRenovations; }
            set
            {
                hasRenovations = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("HasRenovations"));
            }
        }

        public bool IsTargetMonth
        {
            get { return isTargetMonth; }
            set
            {
                isTargetMonth = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IsTargetMonth"));
            }
        }

        /*public bool HasAppointments
        {
            get { return hasAppointments; }
            set
            {
                hasAppointments = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("hasAppointments"));
            }
        }*/

        public DateTime Date
        {
            get { return date; }
            set
            {
                date = value;
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Date"));
            }
        }
    }
}
