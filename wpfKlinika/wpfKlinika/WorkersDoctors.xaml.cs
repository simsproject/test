﻿using Controller;
using Model.Users;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for WorkersDoctors.xaml
    /// </summary>
    public partial class WorkersDoctors : Page
    {
        //public static Object nameSelected;
        public static Doctor workers = null;
        public static UserMore user;

        //public static IEnumerable<Model.Doctors> filtered = new ObservableCollection<Model.Doctors>();
        //public static UserMore userAdd = new UserMore();
       


        
        
        
      


        public WorkersDoctors()
        {
            InitializeComponent();
            //this.showMoreBorder.Visibility = Visibility.Hidden;

            IEnumerable<Doctor> n = MainWindow.doctorController.GetAll();

            this.DataContext = this;
            this.doctorsGrid.ItemsSource = n;


            





        }

       
       

        

        private void DoctorMoreButton_Click(object sender, RoutedEventArgs e)
        {
            

             workers = this.doctorsGrid.SelectedItem as Doctor;
            var vv = MainWindow.doctorController.GetByID(workers.PersonalID);

            user = new UserMore(vv);

            user.doctorNameBox.Text = workers.FirstName;
            user.doctorLastNameBox.Text = workers.LastName;
            user.doctorJmbgBox.Text = workers.PersonalID;
            user.doctorEmailBox.Text = workers.Email;
            user.doctorPassBox.Text = workers.Password;
            user.doctorUserNameBox.Text = workers.UserName;
            //user.doctorAdressBox.Text = workers.Address.Name;
           // user.doctorRoomBox.Text = workers.Room;
            //user.doctorTitleBox.Text = workers.Title;

            foreach(Address address in user.doctorAdressBox.Items)
            {
                if(address.Id== workers.Address.Id)
                {
                    user.doctorAdressBox.SelectedItem = address;
                    break;
                }
            }


            user.doctorTitleComboBox.Text = workers.Title.ToString();
            //user.doctorRoomComboBox.Text = workers.Room;
            user.doctorPhoneBox.Text = workers.PhoneNumber;

            user.doctorTitleComboBox.SelectedItem = workers.Title;

            user.roomBox.Text = workers.MedicalRoom.Name;

            //user.doctorCityComboBox.Text = workers.Address.City.Name;
            //user.doctorCountryComboBox.Text = workers.Address.City.Country.Name;
            user.genderComboBox.Text = workers.Gender.ToString();
            user.dateUser.SelectedDate = workers.DateOfBirth; 
            //user.imgDynamic.Source = workers.Image;
            //user.radioDoctor.IsChecked = true;
           
            this.NavigationService.Navigate(user);


        }

        private void BackDoctors_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersCategories());
        }



        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            Doctor d = null;
            WorkingPeriod wp = MainWindow.workingPeriodController.GetByID(1);
            WorkingPeriod[] wpp = new WorkingPeriod[1];
            wpp[0] = wp;
           d = new Doctor(Model.Users.Title.DoctorOfGeneralPractice,wpp,null,"","",DateTime.Parse("01.01.2020."),"",Model.Users.Gender.Female,"","",null,"","");
           //d = new Doctor(Model.Users.Title.DoctorOfGeneralPractice,1,null,"","",DateTime.Parse("01.01.2020."),"",Model.Users.Gender.Female,"","",null,"","");
            UserMore userAdd = new UserMore(d);

           userAdd.backMoreUserButton.Visibility = Visibility.Hidden;
            userAdd.deleteUserButton.Visibility = Visibility.Hidden;
            userAdd.userTimeRoomButton.Visibility = Visibility.Hidden;
            userAdd.editUserButton.Visibility = Visibility.Hidden;

            userAdd.addTimeRoomButton.Visibility = Visibility.Visible;
            userAdd.cancelAddUserButton.Visibility = Visibility.Visible;

            //userAdd.radioDoctor.IsEnabled = userAdd.radioSecretary.IsEnabled = true;
            userAdd.doctorNameBox.IsReadOnly = userAdd.doctorLastNameBox.IsReadOnly = userAdd.doctorPhoneBox.IsReadOnly = userAdd.doctorUserNameBox.IsReadOnly = userAdd.doctorJmbgBox.IsReadOnly = userAdd.doctorAdressBox.IsReadOnly = userAdd.doctorEmailBox.IsReadOnly = false;
            
            userAdd.doctorTitleComboBox.IsEnabled = true;
            //userAdd.doctorCountryComboBox.IsEnabled = true;
            //userAdd.doctorCityComboBox.IsEnabled = true;
            userAdd.doctorPassBox.IsEnabled = true;
            userAdd.genderComboBox.IsEnabled = true;
            userAdd.dateUser.IsEnabled = true;
            userAdd.doctorAdressBox.IsEnabled = true;
            userAdd.roomBox.IsEnabled = true;
            userAdd.doctorPhoneBox.IsEnabled = true;
            userAdd.doctorUserNameBox.IsReadOnly = false;

            // userAdd.doctorPassBox.IsEnabled = true;
            this.NavigationService.Navigate(userAdd);
        }

        private void BackMoreUser_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersDoctors());
        }





      

        

       

    }
}