﻿using Model.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RoomMorePatient.xaml
    /// </summary>
    public partial class RoomMorePatient : Page
    {
        public static string s;
        public PatientRoom r = null;
        public RoomMorePatient(PatientRoom rom)
        {
            InitializeComponent();
            this.DataContext = this;
            this.r = rom;

            this.roomName.IsReadOnly = this.roomFloor.IsReadOnly = this.moreInfoBox.IsReadOnly = true;
        }

        private void EquipmentButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void CancelAddRoomButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RoomPatient());
        }

        private void ConfirmAddRoomButton_Click(object sender, RoutedEventArgs e)
        {
            nameBorder.BorderBrush = Brushes.LightGray;
            redName.Visibility = Visibility.Hidden;
            Boolean name = true;

            Boolean floor = false;

            //ROOM NAME PROVERA
            if (roomName.Text.Length == 0)
            {
                nameBorder.BorderBrush = Brushes.PaleVioletRed;
                redName.Visibility = Visibility.Visible;
                redName.Text = "morate popuniti polje";
                name = false;
            }
            else
            {
                var room = MainWindow.roomController.GetAll();
                foreach (Room dd in room)
                {
                    if (dd.Name.Equals(this.roomName.Text))
                    {
                        nameBorder.BorderBrush = Brushes.PaleVioletRed;
                        redName.Visibility = Visibility.Visible;
                        redName.Text = "vec postoji prostorija sa ovim nazivom";
                        name = false;
                    }
                }
            }

            if (roomFloor.Text.Length == 0)
            {
                floorBorder.BorderBrush = Brushes.PaleVioletRed;
                redFloor.Visibility = Visibility.Visible;
                redFloor.Text = "morate popuniti polje";
                floor = false;
            }
            else
            {
                floorBorder.BorderBrush = Brushes.LightGray;
                redFloor.Visibility = Visibility.Hidden;
                //redFloor.Text = "morate popuniti polje";
                floor = true;
            }

            if ((name == true) && (floor == true))
            {
                this.roomName.IsReadOnly = this.roomFloor.IsReadOnly = this.moreInfoBox.IsReadOnly = true;




                PatientRoom item = new PatientRoom("", 0);


                item.Name = roomName.Text;
                item.Floor = int.Parse(roomFloor.Text);


                Rooms.Roomm.Add(item);

                MainWindow.patientRoomController.Create(item);
                MainWindow.roomController.Create(item);


                this.NavigationService.Navigate(new RoomPatient());
            }
        }

        private void RoomBackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new RoomPatient());
        }

        public async void taskDel()
        {
            await Task.Delay(5000);
            this.roomBackButton.IsEnabled = true;
            this.editRoomButton.IsEnabled = true;

            this.deleteRoomButton.IsEnabled = true;


        }

        private void DeleteRoomButton_Click(object sender, RoutedEventArgs e)
        {
            this.RemovePatientRoomMessage.Visibility = Visibility.Visible;
            this.roomBackButton.IsEnabled = false;
            this.editRoomButton.IsEnabled = false;

            this.deleteRoomButton.IsEnabled = false;
            this.taskDel();
        }

        private void EditRoomButton_Click(object sender, RoutedEventArgs e)
        {
            this.roomFloor.IsReadOnly = this.moreInfoBox.IsReadOnly = false;
            this.roomName.IsReadOnly = true;
            this.roomBackButton.Visibility = Visibility.Hidden;
            this.deleteRoomButton.Visibility = Visibility.Hidden;
            this.editRoomButton.Visibility = Visibility.Hidden;

            this.confirmEditButton.Visibility = Visibility.Visible;
            this.cancelEditButton.Visibility = Visibility.Visible;


            s = this.roomName.Text;


        }

        private void ConfirmEditButton_Click(object sender, RoutedEventArgs e)
        {
            //validacija
            nameBorder.BorderBrush = Brushes.LightGray;
            redName.Visibility = Visibility.Hidden;
           // Boolean name = true;

            Boolean floor = false;

           
            if (roomFloor.Text.Length == 0)
            {
                floorBorder.BorderBrush = Brushes.PaleVioletRed;
                redFloor.Visibility = Visibility.Visible;
                redFloor.Text = "morate popuniti polje";
                floor = false;
            }
            else
            {
                floorBorder.BorderBrush = Brushes.LightGray;
                redFloor.Visibility = Visibility.Hidden;
                //redFloor.Text = "morate popuniti polje";
                floor = true;
            }
            if (floor == true)
            {

                this.roomFloor.IsReadOnly = this.roomName.IsReadOnly = this.moreInfoBox.IsReadOnly = true;

                this.roomBackButton.Visibility = Visibility.Visible;
                this.deleteRoomButton.Visibility = Visibility.Visible;
                this.editRoomButton.Visibility = Visibility.Visible;

                this.confirmEditButton.Visibility = Visibility.Hidden;
                this.cancelEditButton.Visibility = Visibility.Hidden;

                var item = r;
                if (item != null)
                {
                    item.Name = roomName.Text;
                    item.Floor = int.Parse(roomFloor.Text);

                    MainWindow.roomController.Update(item);
                    MainWindow.patientRoomController.Update(item);

                    //item.Info = moreInfoBox.Text;
                }

            }
        }

        private void CancelEditButton_Click(object sender, RoutedEventArgs e)
        {
            this.roomName.IsReadOnly = this.roomFloor.IsReadOnly = true;

            this.roomBackButton.Visibility = Visibility.Visible;
            this.deleteRoomButton.Visibility = Visibility.Visible;
            this.editRoomButton.Visibility = Visibility.Visible;

            this.confirmEditButton.Visibility = Visibility.Hidden;
            this.cancelEditButton.Visibility = Visibility.Hidden;
        }
    }
}
