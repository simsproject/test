﻿using Microsoft.Win32;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Model.Rooms;
using Repository;
using Service;
using Controller;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for InventoryMore.xaml
    /// </summary>
    public partial class InventoryMore : Page
    {
        public Inventory eq = new Inventory("",0,TypeOfInvetory.Stable,"",10,new Room("100",1));
        public List<String> Room { get; set; }
        private static ObservableCollection<Model2.InventoryType> _inventoryType = new ObservableCollection<Model2.InventoryType>();
        public static ObservableCollection<Model2.InventoryType> InventoryType
        {
            get { return _inventoryType; }
            set { _inventoryType = value; }
        }
       

        public InventoryMore(Inventory equip)
        {
           
            InitializeComponent();
            
            this.DataContext = this;

            if(InventoryType.Count == 0)
            {
                InventoryType.Add(new Model2.InventoryType { Name = "Staticka" });
                InventoryType.Add(new Model2.InventoryType { Name = "Potrosna" });
            }

            this.eq = equip;
            Room = new List<string>();
            IEnumerable<Room> rr = MainWindow.roomController.GetAll();
            foreach (Room r in rr)
            {
                Room.Add(r.Name);
            }
            this.inventoryRoomBox.ItemsSource = MainWindow.roomController.GetAll().ToList();
            //this.inventoryTypeBox.ItemsSource = MainWindow.inventoryController.GetAll().ToList();

            if (equip != null)
            {


                inventoryTypeBox.Text = equip.Type.ToString();



                foreach (Room ddd in inventoryRoomBox.Items)
                {

                    if (ddd.Name.Equals(equip.Room))
                    {
                        inventoryRoomBox.SelectedItem = ddd as Room;
                        break;
                    }

                }
            }
        }

       
        

        private void CancelAddInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Equipment());
        }

        private void InventoryBackButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Equipment());
        }


        public async void taskDel()
        {
            await Task.Delay(5000);
            this.cancelAddInventoryButton.IsEnabled = true;
            this.cancelEditInventoryButton.IsEnabled = true;
            this.deleteInventoryButton.IsEnabled = true;
            this.editInventoryButton.IsEnabled = true;
            this.confirmAddInventoryButton.IsEnabled = true;
            this.confirmEditInventoryButton.IsEnabled = true;
            this.inventoryBackButton.IsEnabled = true;

        }

        private void DeleteInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            this.InventoryRemoveMessage.Visibility = Visibility.Visible;
            this.cancelAddInventoryButton.IsEnabled = false;
            this.cancelEditInventoryButton.IsEnabled = false;
            this.deleteInventoryButton.IsEnabled = false;
            this.editInventoryButton.IsEnabled = false;
            this.confirmAddInventoryButton.IsEnabled = false;
            this.confirmEditInventoryButton.IsEnabled = false;
            this.inventoryBackButton.IsEnabled = false;
            this.taskDel();
        }

        private void ConfirmAddInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            //VALIDACIJA
            inventoryCodeBorder.BorderBrush = Brushes.LightGray;
            redCode.Visibility = Visibility.Hidden;
            Boolean code = true;
            Boolean name = false;
            Boolean amount = false;
            Boolean room = false;
            Boolean kind = false;


            //PROVERA ZA KOLICINU
            if (inventoryAmountBox.Text.Length == 0)
            {
                inventoryAmountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "morate popuniti polje";
                amount = false;
            }
            else if (!Regex.IsMatch(inventoryAmountBox.Text, "^[0-9]+$"))
            {
                inventoryAmountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = false;
            }
            else if (inventoryAmountBox.Text.Equals("0"))
            {
                inventoryAmountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "kolicina ne sme biti 0";
                amount = false;
            }
            else
            {
                inventoryAmountBorder.BorderBrush = Brushes.LightGray;
                redAmount.Visibility = Visibility.Hidden;
                //redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = true;
            }


            //NAME PROVERA SAMO DA NIJE PRAZNO POLJE
            if (inventoryNameBox.Text.Length == 0)
            {

                inventoryNameBorder.BorderBrush = Brushes.PaleVioletRed;
                redName.Visibility = Visibility.Visible;
                redName.Text = "morate popuniti polje";

            }

            else
            {
                inventoryNameBorder.BorderBrush = Brushes.LightGray;
                redName.Visibility = Visibility.Hidden;
                name = true;
            }

            //SOBA
            string str = inventoryRoomBox.Text;
            string noSpace = str.Replace(" ", "");
            if (noSpace.Equals(""))
            {
                inventoryRoomBorder.BorderBrush = Brushes.PaleVioletRed;
                redRoom.Visibility = Visibility.Visible;
                redRoom.Text = "morate popuniti polje";
            }
            /*else if (renovationComboBox.ItemsSource.Equals(renovationComboBox.Text))
            {
                MessageBox.Show("ok");
            }*/
            else if (inventoryRoomBox.SelectedIndex < 0)
            {
                inventoryRoomBorder.BorderBrush = Brushes.PaleVioletRed;
                redRoom.Visibility = Visibility.Visible;
                redRoom.Text = "morate popuniti polje";
            }
            else
            {
                inventoryRoomBorder.BorderBrush = Brushes.LightGray;
                redRoom.Visibility = Visibility.Hidden;
                room = true;
            }

            //TIP
            string str2 = inventoryTypeBox.Text;
            string noSpace2 = str2.Replace(" ", "");
            if (noSpace2.Equals(""))
            {
                typeInventoryBox.BorderBrush = Brushes.PaleVioletRed;
                redType.Visibility = Visibility.Visible;
                redType.Text = "morate popuniti polje";
            }
            /*else if (renovationComboBox.ItemsSource.Equals(renovationComboBox.Text))
            {
                MessageBox.Show("ok");
            }*/
            else if (inventoryTypeBox.SelectedIndex < 0)
            {
                typeInventoryBox.BorderBrush = Brushes.PaleVioletRed;
                redType.Visibility = Visibility.Visible;
                redType.Text = "morate popuniti polje";
            }
            else
            {
                typeInventoryBox.BorderBrush = Brushes.LightGray;
                redType.Visibility = Visibility.Hidden;
                kind = true;
            }

            //CODE MORA BITI JEDINSTVEN
            if (inventoryCodeBox.Text.Length == 0)
            {
                inventoryCodeBorder.BorderBrush = Brushes.PaleVioletRed;
                redCode.Visibility = Visibility.Visible;
                redCode.Text = "morate popuniti polje";
                code = false;
            }

            else if (inventoryCodeBox.Text.Length == 6)
            {
                var r = MainWindow.inventoryController.GetAll();
                foreach (Inventory dd in r)
                {
                    
                        if (this.inventoryCodeBox.Text == dd.SerialID.ToString())
                        {
                            inventoryCodeBorder.BorderBrush = Brushes.PaleVioletRed;
                            redCode.Visibility = Visibility.Visible;
                            redCode.Text = "vec postoji oprema sa ovom sifrom";
                            code = false;

                        }


                }


            }
            else if (inventoryCodeBox.Text.Length < 6 || inventoryCodeBox.Text.Length > 6)
            {
                inventoryCodeBorder.BorderBrush = Brushes.PaleVioletRed;
                redCode.Visibility = Visibility.Visible;
                redCode.Text = "sifra opreme mora da sadrzi 6 karaktera";
                code = false;
            }



            //////////////////////




            if (name == true && code == true && amount == true && room == true && kind == true)
            {
                Inventory inv = new Inventory("", 0, TypeOfInvetory.Stable, "", 10, new Room("100", 999));
                inv.Name = this.inventoryNameBox.Text;
                inv.SerialID = int.Parse(this.inventoryCodeBox.Text);
                //inv.Room.Name = this.inventoryRoomBox.Text;

                // inv.Kind = this.inventoryTypeBox.Text;
                inv.Quantity = int.Parse(this.inventoryAmountBox.Text);
                inv.Description = this.moreInfoBox.Text;
                //inv.Image = this.imgDynamic.Source;

                inv.Room = this.inventoryRoomBox.SelectedItem as Room;




                if (this.inventoryTypeBox.Text.Equals("Stable"))
                {
                    inv.Type = TypeOfInvetory.Stable;
                }
                else
                {
                    inv.Type = TypeOfInvetory.Consumable;
                }




                Equipment.EquipmentM.Add(inv);
                MainWindow.inventoryController.Create(inv);
                if (inv.Name.Equals("Krevet"))
                {
                    IEnumerable<PatientRoom> r = MainWindow.patientRoomController.GetAll();
                    foreach (PatientRoom rom in r)
                    {
                        if (rom.Name.Equals(inv.Room.Name))
                        {
                            Bed b = new Bed(int.Parse(inv.SerialID.ToString()), null, rom);
                            MainWindow.bedController.Create(b);
                            break;
                        }
                    }


                }
                this.NavigationService.Navigate(new Equipment());
            }   
        }

        private void ConfirmEditInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            //VALIDACIJA
            inventoryCodeBorder.BorderBrush = Brushes.LightGray;
            redCode.Visibility = Visibility.Hidden;
            Boolean code = true;
            Boolean name = false;
            Boolean amount = false;
            Boolean room = false;
            Boolean kind = false;


            //PROVERA ZA KOLICINU
            if (inventoryAmountBox.Text.Length == 0)
            {
                inventoryAmountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "morate popuniti polje";
                amount = false;
            }
            else if (!Regex.IsMatch(inventoryAmountBox.Text, "^[0-9]+$"))
            {
                inventoryAmountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = false;
            }
            else if (inventoryAmountBox.Text.Equals("0"))
            {
                inventoryAmountBorder.BorderBrush = Brushes.PaleVioletRed;
                redAmount.Visibility = Visibility.Visible;
                redAmount.Text = "kolicina ne sme biti 0";
                amount = false;
            }
            else
            {
                inventoryAmountBorder.BorderBrush = Brushes.LightGray;
                redAmount.Visibility = Visibility.Hidden;
                //redAmount.Text = "kolicina sme da sadrzi samo brojeve";
                amount = true;
            }


            //NAME PROVERA SAMO DA NIJE PRAZNO POLJE
            if (inventoryNameBox.Text.Length == 0)
            {

                inventoryNameBorder.BorderBrush = Brushes.PaleVioletRed;
                redName.Visibility = Visibility.Visible;
                redName.Text = "morate popuniti polje";

            }

            else
            {
                inventoryNameBorder.BorderBrush = Brushes.LightGray;
                redName.Visibility = Visibility.Hidden;
                name = true;
            }

            //SOBA
            string str = inventoryRoomBox.Text;
            string noSpace = str.Replace(" ", "");
            if (noSpace.Equals(""))
            {
                inventoryRoomBorder.BorderBrush = Brushes.PaleVioletRed;
                redRoom.Visibility = Visibility.Visible;
                redRoom.Text = "morate popuniti polje";
            }
            /*else if (renovationComboBox.ItemsSource.Equals(renovationComboBox.Text))
            {
                MessageBox.Show("ok");
            }*/
            else if (inventoryRoomBox.SelectedIndex < 0)
            {
                inventoryRoomBorder.BorderBrush = Brushes.PaleVioletRed;
                redRoom.Visibility = Visibility.Visible;
                redRoom.Text = "morate popuniti polje";
            }
            else
            {
                inventoryRoomBorder.BorderBrush = Brushes.LightGray;
                redRoom.Visibility = Visibility.Hidden;
                room = true;
            }

            //TIP
            string str2 = inventoryTypeBox.Text;
            string noSpace2 = str2.Replace(" ", "");
            if (noSpace2.Equals(""))
            {
                typeInventoryBox.BorderBrush = Brushes.PaleVioletRed;
                redType.Visibility = Visibility.Visible;
                redType.Text = "morate popuniti polje";
            }
            /*else if (renovationComboBox.ItemsSource.Equals(renovationComboBox.Text))
            {
                MessageBox.Show("ok");
            }*/
            else if (inventoryTypeBox.SelectedIndex < 0)
            {
                typeInventoryBox.BorderBrush = Brushes.PaleVioletRed;
                redType.Visibility = Visibility.Visible;
                redType.Text = "morate popuniti polje";
            }
            else
            {
                typeInventoryBox.BorderBrush = Brushes.LightGray;
                redType.Visibility = Visibility.Hidden;
                kind = true;
            }



           


            //////////////////////

            if ((name == true) && (amount == true) && (code == true) && (kind == true) && (room == true))
            {


                this.inventoryNameBox.IsReadOnly = this.inventoryCodeBox.IsReadOnly = this.inventoryAmountBox.IsReadOnly = this.inventoryRoomBox.IsReadOnly = this.moreInfoBox.IsReadOnly = true;
                //this.radioDynamicInventory.IsEnabled = this.radioStaticInvenotry.IsEnabled = false;
                //this.loadImageButton.IsEnabled = false;
                this.inventoryRoomBox.IsEnabled = this.inventoryTypeBox.IsEnabled = false;

                this.inventoryBackButton.Visibility = Visibility.Visible;
                this.deleteInventoryButton.Visibility = Visibility.Visible;
                this.editInventoryButton.Visibility = Visibility.Visible;

                this.confirmEditInventoryButton.Visibility = Visibility.Hidden;
                this.cancelEditInventoryButton.Visibility = Visibility.Hidden;

                var inv = Equipment.equipm;
                //Model.EquipmentM inv = new Model.EquipmentM();
                if (inv != null)
                {


                    inv.Name = this.inventoryNameBox.Text;
                    inv.SerialID = int.Parse(this.inventoryCodeBox.Text);
                    inv.Description = this.moreInfoBox.Text;
                    inv.Quantity = int.Parse(this.inventoryAmountBox.Text);
                    inv.Room = this.inventoryRoomBox.SelectedItem as Room;
                

                    

                    if(this.inventoryTypeBox.Text.Equals("Stable"))
                     {
                        inv.Type = TypeOfInvetory.Stable;
                     }
                    else
                    {
                        inv.Type = TypeOfInvetory.Consumable;
                    }

                    

                if (inv.Name.Equals("Krevet"))
                {
                    IEnumerable<PatientRoom> r = MainWindow.patientRoomController.GetAll();
                    foreach (PatientRoom rom in r)
                    {
                        if (rom.Name.Equals(inv.Room.Name))
                        {
                            Bed b = new Bed(int.Parse(inv.SerialID.ToString()), null, rom);
                            MainWindow.bedController.Update(b);
                            break;
                        }
                    }


                }


                MainWindow.inventoryController.Update(inv);

               }


            }
        }

        private void CancelEditInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            this.inventoryNameBox.IsReadOnly = this.inventoryCodeBox.IsReadOnly = this.inventoryAmountBox.IsReadOnly = this.inventoryRoomBox.IsReadOnly = this.moreInfoBox.IsReadOnly = true;
            //this.radioDynamicInventory.IsEnabled = this.radioStaticInvenotry.IsEnabled = false;
           // this.loadImageButton.IsEnabled = false;
            this.inventoryRoomBox.IsEnabled = this.inventoryTypeBox.IsEnabled = false;

            this.inventoryBackButton.Visibility = Visibility.Visible;
            this.deleteInventoryButton.Visibility = Visibility.Visible;
            this.editInventoryButton.Visibility = Visibility.Visible;

            this.confirmEditInventoryButton.Visibility = Visibility.Hidden;
            this.cancelEditInventoryButton.Visibility = Visibility.Hidden;
        }

        private void EditInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            this.inventoryCodeBox.IsReadOnly = true;
            this.inventoryNameBox.IsReadOnly  = this.inventoryAmountBox.IsReadOnly = this.inventoryRoomBox.IsReadOnly = this.moreInfoBox.IsReadOnly = false;
            //this.radioDynamicInventory.IsEnabled = this.radioStaticInvenotry.IsEnabled = true;
            // this.loadImageButton.IsEnabled = true;
            
            this.inventoryTypeBox.IsReadOnly = false;


            this.typeInventoryBox.IsEnabled = this.inventoryRoomBox.IsEnabled = true;

            this.inventoryBackButton.Visibility = Visibility.Hidden;
            this.deleteInventoryButton.Visibility = Visibility.Hidden;
            this.editInventoryButton.Visibility = Visibility.Hidden;

            this.confirmEditInventoryButton.Visibility = Visibility.Visible;
            this.cancelEditInventoryButton.Visibility = Visibility.Visible;
        }

        private void DooctorRoomBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void InventoryTypeBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void LoadImageButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Uri fileUri = new Uri(openFileDialog.FileName);
               // imgDynamic.Source = new BitmapImage(fileUri);
            }
        }
    }
}
