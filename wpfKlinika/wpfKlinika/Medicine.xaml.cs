﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Repository;
using Service;
using Controller;
using Model.Drugs;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for Medicine.xaml
    /// </summary>
    public partial class Medicine : Page
    {
        public static DrugMore addDrug;
        public static DrugMore drugMore;
        public static DrugsForValidation validationList;
        private const string dateTime = "dd.MM.yyyy.";

        public static Drug meds = null;

        private static ObservableCollection<Drug> _meds = new ObservableCollection<Drug>();
        public static ObservableCollection<Drug> Meds
        {
            get { return _meds; }
            set { value = _meds; }

        }
        
        /*static Medicine()
        {
                //MainWindow.demoButt.IsEnabled = true;
                 Meds = new ObservableCollection<Model.Meds>();
                Meds.Add(new Model.Meds { Name = "BRUFEN", Code = "1123AA", Quantity = "100", Exparation = "10/2020", Ingredients = "*************************" });
                Meds.Add(new Model.Meds { Name = "ASPIRIN", Code = "4455BB", Quantity = "200", Exparation = "05/2020", Ingredients = "*************************" });
                Meds.Add(new Model.Meds { Name = "KAFETIN", Code = "5555tv", Quantity = "300", Exparation = "11/2020", Ingredients = "*************************" });
                Meds.Add(new Model.Meds { Name = "LEK4", Code = "888833", Quantity = "400", Exparation = "10/2020", Ingredients = "*************************" });
                Meds.Add(new Model.Meds { Name = "LEK5", Code = "9999OP", Quantity = "500", Exparation = "11/2020", Ingredients = "*************************" });
                Meds.Add(new Model.Meds { Name = "LEK6", Code = "713AS4", Quantity = "600", Exparation = "12/2020", Ingredients = "*************************" });
                Meds.Add(new Model.Meds { Name = "LEK7", Code = "612332", Quantity = "700", Exparation = "09/2020", Ingredients = "*************************" });
            
        }*/

        static Medicine()
        {
            //PROVERA ZA DRUGS.CSV
            
            IEnumerable<Drug> n = MainWindow.drugController.GetAll();

            //Room room = null;
            foreach (Drug drug in n)
            {
                if (drug.State.ToString().Equals("valid"))
                {
                    Meds.Add(drug);
                }
            }
        }


        public Medicine()
        {
            InitializeComponent();
            //MainWindow.enableDemoButton();
            // MainWindow.demoMode = true;
            //Login.enableDemo();
            this.DataContext = this;


           


            //DemoValList.stopp = DemoAddDrug.stopp = DemoValList2.stopp = demoS.stopp = false;


        }

        private void SearchMeds_Button_Click(object sender, RoutedEventArgs e)
        {

        }


       

        private void MedsMoreButton_Click(object sender, RoutedEventArgs e)
        {
            meds = this.medicineGrid.SelectedItem as Drug;
            drugMore = new DrugMore(meds);

            drugMore.drugName.IsReadOnly = drugMore.drugCode.IsReadOnly  = drugMore.ingredientsBox.IsReadOnly  = true;
            drugMore.drugAmountBox.IsReadOnly = true;
            drugMore.drugEndDateBox.IsEnabled = false;

            drugMore.drugName.Text = meds.Name;
            drugMore.drugCode.Text = meds.SerialID;
            drugMore.drugEndDateBox.Text = meds.ExpirationDate.ToString(dateTime);
            drugMore.ingredientsBox.Text = meds.Ingradients;
            
            drugMore.drugAmountBox.Text = meds.Amount.ToString();

            drugMore.cancelAddDrugButton.Visibility = Visibility.Hidden;
            drugMore.confirmAddDrugButton.Visibility = Visibility.Hidden;

            drugMore.drugBackButton.Visibility = Visibility.Visible;
            drugMore.deleteDrugButton.Visibility = Visibility.Visible;
            drugMore.editDrugButton.Visibility = Visibility.Visible;


            //treba workers binding
            this.NavigationService.Navigate(drugMore);
        }

        private void ValidationListButton_Click(object sender, RoutedEventArgs e)
        {
            validationList = new DrugsForValidation(DrugsForValidation.selectedDoctor);
            this.NavigationService.Navigate(validationList);
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            /*
            string search = searchBox.Text.ToLower();
            string searchText = null;
            if (search.Contains(" "))
            {
                string[] sr = search.Split(null);
                searchText = sr[0];
            }
            else
            {
                searchText = search;
            }



            var filtered = (Meds.Where(d => (d.Name.ToLower().StartsWith(searchText)) || (d.Code.ToLower().StartsWith(searchText))));


            medicineGrid.ItemsSource = filtered;*/
        }
    }
}
