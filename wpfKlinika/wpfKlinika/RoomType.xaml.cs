﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RoomType.xaml
    /// </summary>
    public partial class RoomType : Page
    {
        public RoomType()
        {
            InitializeComponent();
        }

        private void DoctorRoomButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RoomDoctor());
        }

        private void StorageButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RoomStorage());
        }

        private void OfficeRoom_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RoomOffice());
        }

        private void PatientRoomButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RoomPatient());
        }
    }
}
