﻿using Controller;
using Model.Rooms;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for InventoryRemoveMessage.xaml
    /// </summary>
    public partial class InventoryRemoveMessage : UserControl
    {
        public InventoryRemoveMessage()
        {
            InitializeComponent();
        }

        private void CancelDeleteInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void ConfirmDeleteInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            // Medicine.Meds.Remove(Medicine.meds);
            if (Equipment.equipm.Name.Equals("Krevet"))
            {
                IEnumerable<PatientRoom> r = MainWindow.patientRoomController.GetAll();
                foreach (PatientRoom rom in r)
                {
                    if (rom.Name.Equals(Equipment.equipm.Room.Name))
                    {
                        Bed b = new Bed(int.Parse(Equipment.equipm.SerialID.ToString()), null, rom);
                        MainWindow.bedController.Delete(b);
                        break;
                    }
                }


            }

            MainWindow.inventoryController.Delete(Equipment.equipm);
            Equipment.EquipmentM.Remove(Equipment.equipm);



            NavigationService nav = NavigationService.GetNavigationService(this);
            this.Visibility = Visibility.Hidden;
            nav.Navigate(new Equipment());
        }
    }
}
