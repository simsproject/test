﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Users;
using Repository;
using Service;
using Controller;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for Account.xaml
    /// </summary>
    public partial class Account : Page
    {



        public Account()
        {

            InitializeComponent();
            this.DataContext = this;
            var adr = MainWindow.adressController.GetAll();
            this.doctorAdressBox.ItemsSource = adr;



           


            //this.doctorNameBox = WorkersDoctors.nameSelected.;
            this.doctorJmbgBox.IsEnabled = false;
            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = true;
            this.doctorPassBox.IsEnabled = false;
            //this.doctorTitleComboBox.IsEnabled = false;
            // this.doctorCityComboBox.IsEnabled = false;
            //this.doctorCountryComboBox.IsEnabled = false;
            this.genderComboBox.IsEnabled = false;
            this.dateUser.IsEnabled = false;
            this.doctorAdressBox.IsEnabled = false;
            //this.roomBox.IsEnabled = false;
            this.doctorPhoneBox.IsEnabled = false;
            //this.loadImageButton.IsEnabled = false;
            //this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = false;
            this.doctorJmbgBox.IsEnabled = false;
            this.doctorUserNameBox.IsEnabled = false;
            //IEnumerable<Manager> mm = MainWindow.managerController.GetAll();


            //Doctors = WorkersDoctors.Doctors;
            Manager workers = MainWindow.managerController.GetByID(Login.userName.PersonalID);
            this.doctorNameBox.Text = workers.FirstName;
            this.doctorLastNameBox.Text = workers.LastName;
            this.doctorJmbgBox.Text = workers.PersonalID;
            this.doctorEmailBox.Text = workers.Email;
            this.doctorPassBox.Text = workers.Password;
            this.doctorUserNameBox.Text = workers.UserName;
            //user.doctorAdressBox.Text = workers.Address.Name;
            // user.doctorRoomBox.Text = workers.Room;
            //user.doctorTitleBox.Text = workers.Title;

            foreach (Address address in this.doctorAdressBox.Items)
            {
                if (address.Id == workers.Address.Id)
                {
                    this.doctorAdressBox.SelectedItem = address;
                    break;
                }
            }


            //user.doctorTitleComboBox.Text = workers.Title.ToString();
            //user.doctorRoomComboBox.Text = workers.Room;
            this.doctorPhoneBox.Text = workers.PhoneNumber;

            //user.doctorTitleComboBox.SelectedItem = workers.Title;

            //user.roomBox.Text = workers.Office.Name;

            //user.doctorCityComboBox.Text = workers.Address.City.Name;
            //user.doctorCountryComboBox.Text = workers.Address.City.Country.Name;
            this.genderComboBox.Text = workers.Gender.ToString();
            this.dateUser.SelectedDate = workers.DateOfBirth;
        }

        private void Edit_Button_Click(object sender, RoutedEventArgs e)
        {
            //WHEN EDIT BUTTON IS CLICKED
            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorPhoneBox.IsReadOnly = this.doctorUserNameBox.IsReadOnly = this.doctorJmbgBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = false;
            this.doctorPassBox.IsEnabled = true;
            //this.doctorTitleComboBox.IsEnabled = false;
            // this.doctorCityComboBox.IsEnabled = false;
            //this.doctorCountryComboBox.IsEnabled = false;
            this.genderComboBox.IsEnabled = true;
            this.dateUser.IsEnabled = true;
            this.doctorAdressBox.IsEnabled = true;
            //this.roomBox.IsEnabled = true;
            this.doctorPhoneBox.IsEnabled = true;
            this.doctorJmbgBox.IsEnabled = false;
            this.doctorUserNameBox.IsEnabled = false;
            //this.doctorTitleComboBox.IsEnabled = true;

            this.edit_Button.Visibility = Visibility.Hidden;
            this.logout_Button.Visibility = Visibility.Hidden;

            this.confirmEdit_Button.Visibility = Visibility.Visible;
            this.cancelEdit_Button.Visibility = Visibility.Visible;

            Manager workers = MainWindow.managerController.GetByID(Login.userName.PersonalID);
            this.doctorNameBox.Text = workers.FirstName;
            this.doctorLastNameBox.Text = workers.LastName;
            this.doctorJmbgBox.Text = workers.PersonalID;
            this.doctorEmailBox.Text = workers.Email;
            this.doctorPassBox.Text = workers.Password;
            this.doctorUserNameBox.Text = workers.UserName;
            //user.doctorAdressBox.Text = workers.Address.Name;
            // user.doctorRoomBox.Text = workers.Room;
            //user.doctorTitleBox.Text = workers.Title;

            foreach (Address address in this.doctorAdressBox.Items)
            {
                if (address.Id == workers.Address.Id)
                {
                    this.doctorAdressBox.SelectedItem = address;
                    break;
                }
            }


            //user.doctorTitleComboBox.Text = workers.Title.ToString();
            //user.doctorRoomComboBox.Text = workers.Room;
            this.doctorPhoneBox.Text = workers.PhoneNumber;

            //user.doctorTitleComboBox.SelectedItem = workers.Title;

            //user.roomBox.Text = workers.Office.Name;

            //user.doctorCityComboBox.Text = workers.Address.City.Name;
            //user.doctorCountryComboBox.Text = workers.Address.City.Country.Name;
            this.genderComboBox.Text = workers.Gender.ToString();
            this.dateUser.SelectedDate = workers.DateOfBirth;

        }


        private void CancelEdit_Button_Click(object sender, RoutedEventArgs e)
        {
            //IF YOU CANCEL EDIT THE ORIGINAL VIEW OPENS(Navigation only workes once)

            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Account());
        }

        private void ConfirmEdit_Button_Click(object sender, RoutedEventArgs e)
        {
            Manager d = MainWindow.managerController.GetByID(this.doctorJmbgBox.Text);
            //kad tek pravimo usera
            //bool postoji = true;
            //JMBG


            //userNameBorder.BorderBrush = Brushes.LightGray;
            //redUserName.Visibility = Visibility.Hidden;
            //Boolean user = true;



            //jmbgBorder.BorderBrush = Brushes.LightGray;
            //redJmbg.Visibility = Visibility.Hidden;
            //Boolean jmbg = true;

            Boolean name = false;
            Boolean last = false;
            Boolean email = false;

            Boolean address = false;
            Boolean pass = false;
            // Boolean titles = false;
            Boolean phone = false;
            //Boolean room = false;
            Boolean rodjenje = false;
            Boolean gend = false;
            var users = MainWindow.userController.GetAll();
            string greska = "GRESKE:\n";

            

            //LOZINKA
            if (doctorPassBox.Text.Length == 0)
            {

                greska += "\n los format lozinke";

            }
            else if (doctorPassBox.Text.Length < 4)
            {
                greska += "\n los format lozinke";
            }
            else
            {

                pass = true;
            }


            //EMAIL 
            /*if (doctorEmailBox.Text.Length == 0)
            {

                emailBorder.BorderBrush = Brushes.PaleVioletRed;
                redEmail.Visibility = Visibility.Visible;
                redEmail.Text = "morate popuniti polje";

            }*/
            if ((doctorEmailBox.Text.Length != 0) && (!Regex.IsMatch(doctorEmailBox.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$")))
            {

                greska += "\n los format email";
            }
            else if (doctorEmailBox.Text.Length == 0)
            {
                greska += "\n los format email";
            }
            else
            {

                email = true;
            }





           

            //TREBA DODATI ZA SOBU!

            if (genderComboBox.Text.Equals(""))
            {
                greska += "\n los format pola";
            }
            else
            {
                gend = true;
            }


            if (dateUser.Text.Equals(""))
            {
                greska += "\n los format datuma rodjenja";
            }
            else
            {
                rodjenje = true;
            }


            if (doctorAdressBox.Text.Equals(""))
            {
                greska += "\n los format adrese";
            }
            else
            {
                address = true;
            }



            //IME DA MORA BITI POPUNJENO
            if (doctorNameBox.Text.Length == 0)
            {

                greska += "\n los format imena";

            }
            //ime mora poceti sa velikim slovom i bez brojeva!
            else if (!Regex.IsMatch(doctorNameBox.Text, "[A-Z][a-z]{1,20}"))
            {
                greska += "\n los format imena";
            }
            else
            {

                name = true;
            }

            //PREZIME DA MORA BITI POPUNJENO
            if (doctorLastNameBox.Text.Length == 0)
            {

                greska += "\n los format prezimena";

            }
            else if (!Regex.IsMatch(doctorLastNameBox.Text, "[A-Z][a-z]{1,20}"))
            {
                greska += "\n los format prezimena";
            }
            else
            {

                last = true;
            }

            //TELEFON

            if (doctorPhoneBox.Text.Length == 0)
            {

                greska += "\n los format telefona";

            }
            else if (!Regex.IsMatch(doctorPhoneBox.Text, "[+]?[0-9]{9,15}"))
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else if (doctorPhoneBox.Text.Length > 15)
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else if (doctorPhoneBox.Text.Length < 9)
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }

            else if (Regex.IsMatch(doctorPhoneBox.Text, "[a-zA-Z]"))
            {
                greska += "\n los format telefona (mora bar 9 brojeva)";
            }
            else
            {

                phone = true;
            }




            if ((gend == true) && (rodjenje == true) && (pass == true) && (phone == true) && (name == true)  && (last == true) && (address == true) && (email == true))
            {
                if (d != null)
                {
                    d.FirstName = this.doctorNameBox.Text;
                    d.LastName = this.doctorLastNameBox.Text;
                    d.PersonalID = this.doctorJmbgBox.Text;
                    d.PhoneNumber = this.doctorPhoneBox.Text;
                    d.UserName = this.doctorUserNameBox.Text;
                    d.Password = this.doctorPassBox.Text;
                    d.Email = this.doctorEmailBox.Text;







                    //d.Address.City.Country.Name = doctorCountryComboBox.Text;
                    //d.Address.City.Name = doctorCityComboBox.Text;

                    d.Address = this.doctorAdressBox.SelectedItem as Address;
                    // d.Address.City = this.doctorCityComboBox.SelectedItem as City;
                    // d.Address.City.Country = this.doctorCountryComboBox.SelectedItem as Country;
                    //d.Office = this.roomBox.SelectedItem as Office;
                    //MedicalRoom mr = MainWindow.medicalRoomController.GetByID(d.MedicalRoom.Name);



                    if (this.genderComboBox.Text.Equals("Female"))
                    {
                        d.Gender = Model.Users.Gender.Female;
                    }
                    else
                    {
                        d.Gender = Model.Users.Gender.Male;
                    }
                    d.DateOfBirth = this.dateUser.SelectedDate.Value;




                    MainWindow.managerController.Update(d);
                    MainWindow.userController.Update(d);

                    //medical room, gender, address


                    this.NavigationService.Navigate(new Account());
                }

            }
            else
            {
                MessageBox.Show(greska);
            }
        }







    
    
   
    
        private void Logout_Button_Click(object sender, RoutedEventArgs e)
        {
          
            MainWindow.disableHeadButtons();
            this.edit_Button.IsEnabled = false;
            this.logout_Button.IsEnabled = false;
            this.LogOutMessage.Visibility = Visibility.Visible;
            
        }

        private void LoadImageButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                Uri fileUri = new Uri(openFileDialog.FileName);
                //imgDynamic.Source = new BitmapImage(fileUri);
            }
        }

        private void GenderComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
