﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RateAppMessage.xaml
    /// </summary>
    public partial class RateAppMessage : UserControl
    {
        public RateAppMessage()
        {
            InitializeComponent();
        }

        private void BackToHomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.enableHeadButtons();

            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Home());
        }
    }
}
