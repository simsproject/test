﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for UserRemoveMessage.xaml
    /// </summary>
    public partial class UserRemoveMessage : UserControl
    {
        public UserRemoveMessage()
        {
            InitializeComponent();
        }

        private void CancelDeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
        }

        private void ConfirmDeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            //WorkersDoctors.Doctors.Remove(WorkersDoctors.workers);
            MainWindow.doctorController.Delete(WorkersDoctors.workers);
            MainWindow.userController.Delete(WorkersDoctors.workers);
            NavigationService nav = NavigationService.GetNavigationService(this);
            this.Visibility = Visibility.Hidden;
            nav.Navigate(new WorkersDoctors());
        }
    }
}
