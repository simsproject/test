﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wpfKlinika.Model2;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for DemoSendValList.xaml
    /// </summary>
    public partial class DemoSendValList : Page
    {
        public  Boolean stopp = false;
        public List<String> Doctors { get; set; }
        public DemoSendValList()
        {
            InitializeComponent();
            this.DataContext = this;

            Doctors = new List<String>();
            /*foreach (Doctors r in WorkersDoctors.Doctors)
            {
                Doctors.Add(r.Name + " " + r.LastName);
            }*/
        }
        private void CancelSendButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new DrugsForValidation(DrugsForValidation.selectedDoctor));
        }

        private void ConfirmSendButton_Click(object sender, RoutedEventArgs e)
        {
            //spisak uspesno poslat prvo
            string str = doctorComboBox.Text;
            string noSpace = str.Replace(" ", "");
            if (noSpace.Equals(""))
            {
                redBox.BorderBrush = Brushes.PaleVioletRed;
                redText.Text = "Odaberite doktora sa spiska";
                redText.Visibility = Visibility.Visible;
            }
            /*else if (renovationComboBox.ItemsSource.Equals(renovationComboBox.Text))
            {
                MessageBox.Show("ok");
            }*/
            else if (doctorComboBox.SelectedIndex < 0)
            {
                redBox.BorderBrush = Brushes.PaleVioletRed;
                redText.Text = "Odaberite doktora sa spiska";
                redText.Visibility = Visibility.Visible;
            }
            else
            {

                DrugsForValidation.ValidationList.Clear();
                OkSend.Visibility = Visibility.Visible;
            }



            //this.NavigationService.Navigate(new Medicine());
        }

        private void DoctorComboBox_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void StopDemo_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.stop = true;
            stopp = true;
            this.NavigationService.Navigate(new Medicine());
        }
    }
}
