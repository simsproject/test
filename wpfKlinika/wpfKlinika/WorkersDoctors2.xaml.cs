﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ComponentModel;
using System.Data;
using System.Drawing;



namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for WorkersDoctors.xaml
    /// </summary>
    public partial class WorkersDoctors2 : Page
    {
        //public static Object nameSelected;
        public static UserMore2 user = new UserMore2();
        public static UserMore2 userAdd = new UserMore2();

        //public static ObservableCollection<Model.Doctors> Doctors = new ObservableCollection<Model.Doctors>();

        /*public static ObservableCollection<Model.Doctors> Doctors
        {
            get;
            set;
        }*/

        public WorkersDoctors2()
        {
            InitializeComponent();
            //this.showMoreBorder.Visibility = Visibility.Hidden;



           /* this.DataContext = this;
            Doctors = new ObservableCollection<Model.Doctors>();
            Doctors.Add(new Model.Doctors { Name = "Petar", LastName = "Markovic", Title = "Specijalista", Room = "102A", UserName = "petMark", Password = "********", Jmbg = "112233", Email = "petMarkovic@gmail.com", Address = "Bulevar Oslobodjenja 5, Novi Sad, Srbija" });
            Doctors.Add(new Model.Doctors { Name = "Marko", LastName = "Matic", Title = "Opsta praksa", Room = "200B", UserName = "markoM", Password = "********", Jmbg = "554433", Email = "markoM@gmail.com", Address = "Jase Tomica 11, Novi Sad, Srbija" });
            Doctors.Add(new Model.Doctors { Name = "Luka", LastName = "Ivkovic", Title = "Specijalista", Room = "400", UserName = "ivkoL", Password = "********", Jmbg = "222222", Email = "ivkoL@gmail.com", Address = "Bore Prodanovica 15, Novi Sad, Srbija" });
            Doctors.Add(new Model.Doctors { Name = "Luka", LastName = "Petrovic", Title = "Specijalista", Room = "400", UserName = "petkoL", Password = "********", Jmbg = "222222", Email = "petko@gmail.com", Address = "Bore Prodanovica 15, Novi Sad, Srbija" });

            //Doctors.Add(user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox,, user.doctorNameBox, user.doctorNameBox);
            */
         }

        DataTable table = new DataTable();

        private void SearchDoctors_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.searchDoctors.Text.Equals("pretraga lekara..."))
            {
                this.searchDoctors.Text = "";

            }

        }

        private void SearchDoctors_MouseLeave(object sender, MouseEventArgs e)
        {
            if (this.searchDoctors.Text.Equals(""))
            {
                this.searchDoctors.Text = "pretraga lekara...";
            }
        }

        private void SearchDocButton_Click(object sender, RoutedEventArgs e)
        {
            //Doctors.Add(user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox, user.doctorNameBox,, user.doctorNameBox, user.doctorNameBox);
        }

        private void DoctorMoreButton_Click(object sender, RoutedEventArgs e)
        {
            //this.userMoreGrid.Visibility = Visibility.Visible;
            //this.showMoreBorder.Visibility = Visibility.Visible;

            //this.doctorNameBox.IsEnabled = this.doctorLastNameBox.IsEnabled = this.doctorTitleBox.IsEnabled = this.doctorUserNameBox.IsEnabled = this.doctorPassBox.IsEnabled = this.doctorJmbgBox.IsEnabled = this.doctorAdressBox.IsEnabled = this.doctorEmailBox.IsEnabled = this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = false;
            //this.backDoctors.Visibility = this.addUser.Visibility = Visibility.Hidden;
            //this.backMoreUser.Visibility = this.userTimeRoom.Visibility = this.deleteUser.Visibility = this.editUser.Visibility = Visibility.Visible;
            //nameSelected = this.doctorsGrid.SelectedItem;
            // NavigationService nav = NavigationService.GetNavigationService(this);
            // nav.Navigate(new UserMore());


           /* Model.Doctors workers = this.doctorsGrid.SelectedItem as Model.Doctors;

            user.doctorNameBox.Text = workers.Name;
            user.doctorLastNameBox.Text = workers.LastName;
            user.doctorJmbgBox.Text = workers.Jmbg;
            user.doctorEmailBox.Text = workers.Email;
            user.doctorPassBox.Text = workers.Password;
            user.doctorUserNameBox.Text = workers.UserName;
            user.doctorAdressBox.Text = workers.Address;
            user.doctorRoomBox.Text = workers.Room;
            user.doctorTitleBox.Text = workers.Title;
            user.radioDoctor.IsChecked = true;*/

            this.NavigationService.Navigate(user);




        }

        private void BackDoctors_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersCategories());
        }



        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            //UserMore user = new UserMore();

            userAdd.backMoreUserButton.Visibility = Visibility.Hidden;
            userAdd.deleteUserButton.Visibility = Visibility.Hidden;
            userAdd.userTimeRoomButton.Visibility = Visibility.Hidden;
            userAdd.editUserButton.Visibility = Visibility.Hidden;

            userAdd.addTimeRoomButton.Visibility = Visibility.Visible;
            userAdd.cancelAddUserButton.Visibility = Visibility.Visible;

            userAdd.radioDoctor.IsEnabled = userAdd.radioSecretary.IsEnabled = true;
            userAdd.doctorNameBox.IsReadOnly = userAdd.doctorLastNameBox.IsReadOnly = userAdd.doctorRoomBox.IsReadOnly = userAdd.doctorTitleBox.IsReadOnly = userAdd.doctorUserNameBox.IsReadOnly = userAdd.doctorPassBox.IsReadOnly = userAdd.doctorJmbgBox.IsReadOnly = userAdd.doctorAdressBox.IsReadOnly = userAdd.doctorEmailBox.IsReadOnly = false;

            this.NavigationService.Navigate(userAdd);
        }

        private void BackMoreUser_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersDoctors());
        }





        /*private void EditUser_Click(object sender, RoutedEventArgs e)
           {
               this.doctorNameBox.IsEnabled = this.doctorLastNameBox.IsEnabled = this.doctorTitleBox.IsEnabled = this.doctorUserNameBox.IsEnabled = this.doctorPassBox.IsEnabled = this.doctorJmbgBox.IsEnabled = this.doctorAdressBox.IsEnabled = this.doctorEmailBox.IsEnabled = this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = true;
           }*/

        private void SearchDoctors_KeyUp(object sender, KeyEventArgs e)
        {
            //UNOS MALIH I VELIKIH SLOVA PROVERRITI
            //pretraga po svemu
            //var filtered = Doctors.Where(Doctors => ( Doctors.Name.Contains(searchDoctors.Text) || Doctors.LastName.Contains(searchDoctors.Text)));  
            //pretraga po tituli?
            //startsWith umesto Contains

            //string searchText = searchDoctors.Text.ToLower();
            //var filtered = Doctors.Where(Doctors => (Doctors.Name.ToLower().StartsWith(searchText) || Doctors.LastName.ToLower().StartsWith(searchText) || Doctors.Title.ToLower().StartsWith(searchText) || Doctors.Room.ToLower().StartsWith(searchText)));
            //doctorsGrid.ItemsSource = filtered;






        }

        /* private void RadioDoctor_Click(object sender, RoutedEventArgs e)
         {

             this.radioSecretary.IsChecked = false;
         }

         private void RadioSecretary_Click(object sender, RoutedEventArgs e)
         {
             this.radioDoctor.IsChecked = false;
         }*/
    }
}
