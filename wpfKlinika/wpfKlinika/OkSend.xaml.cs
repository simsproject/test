﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for OkSend.xaml
    /// </summary>
    public partial class OkSend : UserControl
    {
        public OkSend()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void OkSendButton_Click(object sender, RoutedEventArgs e)
        {
            //this.NavigationService.Navigate(new Medicine());
            this.Visibility = Visibility.Hidden;
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri("Medicine.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}
