﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for Renovation2.xaml
    /// </summary>
    public partial class Renovation2 : Page
    {
        private List<DateTime> selectedDates = new List<DateTime>();
        private Model2.Room currentRoom;
        private int Month = DateTime.Now.Month; // Ovo koristimo da pamtimo u kom smo mesecu (Umesto Comboboxa jer je lepse)
        private int Year = DateTime.Now.Year;   // Isto ko mesec ali za godinu
        List<String> months = new List<String>
        {
            "January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December"
        };

        public Renovation2(Model2.Room room)
        {
            InitializeComponent();

            this.currentRoom = room;
            //calendarRenovation.BlackoutDates.AddDatesInPast();
            //calendarRenovation.BlackoutDates.Add(new CalendarDateRange(DateTime.Today, DateTime.Today.AddDays(1)));

            //__________________________________________________
            // ISPOD JE RUKOVANJE KALENDAROM

            RefreshCalendar();
            Calendar.Background = Brushes.White;        // Pozadina kalendara
        }

        private void RefreshCalendar()  // Ako nesto promenis, ovo refreshuje kalendar da prikaze novoizabrani datum
        {
            DateTime targetDate = new DateTime(Year, Month, 1);

            Calendar.BuildCalendar(targetDate, currentRoom);
        }

        private void BackRenovation_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new Uri("Renovation.xaml", UriKind.RelativeOrAbsolute));
        }

        private void PickRenovation_Click(object sender, RoutedEventArgs e)
        {
            if (selectedDates.Count == 0)
            {
                errorMessage.Visibility = Visibility.Visible;
                return;
            }
            else
            {
                errorMessage.Visibility = Visibility.Hidden;
                foreach (DateTime date in selectedDates)
                {
                    if (currentRoom.Renovation == null)
                    {
                        currentRoom.Renovation = new List<DateTime>();
                    }
                    if (!currentRoom.Renovation.Contains(date))
                    {
                        currentRoom.Renovation.Add(date);
                    }
                }

                selectedDates.Clear();
                RefreshCalendar();
            }
        }

        private void DockPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            StackPanel stackPanel = (sender as DockPanel).Children[0] as StackPanel;
            TextBlock date = stackPanel.Children[0] as TextBlock;

            // Fetch the date of the selected DockPanel
            int Day = Convert.ToInt32(date.Text);
            DateTime dateTime = new DateTime(Year, Month, Day);
            if (dateTime < DateTime.Now)
            {
                return; // Ako je datum u proslosti ne daj da se klikne
            }
            if (currentRoom.Renovation.Contains(dateTime))  // Ne moze da se klikne ako vec ima rezervacija taj dan
            {
                //ZNACI VEC JE ZAKAZANO OVDE UBACITI LOGIKU ZA OTKAZIVANJE
                //DA SE OTVORI DA LI STE SIGURNI
                /*MessageBoxResult result = MessageBox.Show("DA LI STE SIGURNI DA ZELITE DA UKLONITE RENOVIRANJE?","" ,MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
                switch(result)
                {
                    case MessageBoxResult.OK:
                        currentRoom.Renovation.Remove(dateTime);
                        break;
                    case MessageBoxResult.Cancel:
                        RefreshCalendar();
                        break;

                }*/
                this.RemoveRenovation.Visibility = Visibility.Visible;
                this.backRenovation.IsEnabled = false;
                this.pickRenovation.IsEnabled = false;
                
                    currentRoom.Renovation.Remove(dateTime);
                    this.taskDel();
                    

                
                


                RefreshCalendar();
                return;
            }
            if (selectedDates.Contains(dateTime))
            {
                (sender as DockPanel).Background = Brushes.White;
                selectedDates.Remove(dateTime);
            }
            else
            {
                (sender as DockPanel).Background = Brushes.PaleVioletRed;
                selectedDates.Add(dateTime);
            }
        }

        public async void taskDel()
        {
            await Task.Delay(5000);
            this.backRenovation.IsEnabled = true;
            this.pickRenovation.IsEnabled = true;

        }

            // HANDLOVANJE DUGMICA ZA LEVO DESNO KROZ MESECE
            private void previousButton_MouseEnter(object sender, MouseEventArgs e)
        {
            previousButton.Background = Brushes.WhiteSmoke;
        }

        private void previousButton_MouseLeave(object sender, MouseEventArgs e)
        {
            previousButton.Background = Brushes.White;
        }

        private void previousButton_Click(object sender, RoutedEventArgs e)
        {
            if (Month > 1)
                Month--;
            else
            {
                Month = 12;
                Year--;
            }
            monthName.Text = months[Month - 1] + " " + Year;
            RefreshCalendar();
            selectedDates.Clear();
            errorMessage.Visibility = Visibility.Hidden;
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            if (Month < 12)
                Month++;
            else
            {
                Month = 1;
                Year++;
            }
            monthName.Text = months[Month - 1] + " " + Year;
            RefreshCalendar();
            selectedDates.Clear();
            errorMessage.Visibility = Visibility.Hidden;
        }

        private void nextButton_MouseEnter(object sender, MouseEventArgs e)
        {
            nextButton.Background = Brushes.WhiteSmoke;
        }

        private void nextButton_MouseLeave(object sender, MouseEventArgs e)
        {
            nextButton.Background = Brushes.White;
        }
    }
}