﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for UserMore.xaml
    /// </summary>
    public partial class UserMore2 : Page

    {
        //UserTimeRoom userTimeRoom = new UserTimeRoom();
        public ObservableCollection<Model2.Doctors> Doctors
        {
            get;
            set;
        }
        public UserMore2()
        {
            InitializeComponent();

            this.DataContext = this;
            //this.doctorNameBox = WorkersDoctors.nameSelected.;
            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorRoomBox.IsReadOnly = this.doctorTitleBox.IsReadOnly = this.doctorUserNameBox.IsReadOnly = this.doctorPassBox.IsReadOnly = this.doctorJmbgBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = true;
            this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = false;

        }

        private void UserTimeRoomButton_Click(object sender, RoutedEventArgs e)
        {
            //KAD VEC POSTOJI USER
         /*   userTimeRoom.userMoreNameBox.Text = this.doctorNameBox.Text;
            userTimeRoom.userMoreLastNameBox.Text = this.doctorLastNameBox.Text;
            userTimeRoom.userMoreRoomBox.Text = this.doctorRoomBox.Text;

            userTimeRoom.userMoreNameBox.IsEnabled = userTimeRoom.userMoreLastNameBox.IsEnabled = userTimeRoom.userMoreRoomBox.IsEnabled = false;
            userTimeRoom.backFromViewUserTimeRoom.Visibility = Visibility.Visible;
            userTimeRoom.editTimeRoomButton.Visibility = Visibility.Visible;

            this.NavigationService.Navigate(userTimeRoom);*/
        }

        private void EditUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorRoomBox.IsReadOnly = this.doctorTitleBox.IsReadOnly = this.doctorUserNameBox.IsReadOnly = this.doctorPassBox.IsReadOnly = this.doctorJmbgBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = false;
            this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = true;

            this.backMoreUserButton.Visibility = Visibility.Hidden;
            this.deleteUserButton.Visibility = Visibility.Hidden;
            this.userTimeRoomButton.Visibility = Visibility.Hidden;
            this.editUserButton.Visibility = Visibility.Hidden;

            this.cancelEditUserButton.Visibility = Visibility.Visible;
            this.confirmEditUserButton.Visibility = Visibility.Visible;
        }

        private void DeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.UserRemoveMessage.Visibility = Visibility.Visible;
        }

        private void BackMoreUserButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersDoctors());
        }

        private void CancelAddUserButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService nav = NavigationService.GetNavigationService(this);
            nav.Navigate(new WorkersDoctors());


            //
            // WorkersDoctors.Doctors.Add(new Model.Doctors { Name = "MIROSALV", LastName = "Markovic", Title = "Specijalista", Room = "102A", UserName = "petMark", Password = "********", Jmbg = "112233", Email = "petMarkovic@gmail.com", Address = "Bulevar Oslobodjenja 5, Novi Sad, Srbija" });
        }

        private void AddTimeRoomButton_Click(object sender, RoutedEventArgs e)
        {
            //kad tek pravimo usera
          /*  userTimeRoom.userMoreNameBox.Text = this.doctorNameBox.Text;
            userTimeRoom.userMoreLastNameBox.Text = this.doctorLastNameBox.Text;
            userTimeRoom.userMoreRoomBox.Text = this.doctorRoomBox.Text;

            userTimeRoom.userMoreNameBox.IsEnabled = userTimeRoom.userMoreLastNameBox.IsEnabled = userTimeRoom.userMoreRoomBox.IsEnabled = false;
            userTimeRoom.backTimeRoomFromAddUser.Visibility = Visibility.Visible; //sad moze back
            userTimeRoom.addFinishButton.Visibility = Visibility.Visible;
            this.NavigationService.Navigate(userTimeRoom);*/
        }

        private void CancelEditUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorRoomBox.IsReadOnly = this.doctorTitleBox.IsReadOnly = this.doctorUserNameBox.IsReadOnly = this.doctorPassBox.IsReadOnly = this.doctorJmbgBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = true;
            this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = false;

            this.backMoreUserButton.Visibility = Visibility.Visible;
            this.deleteUserButton.Visibility = Visibility.Visible;
            this.userTimeRoomButton.Visibility = Visibility.Visible;
            this.editUserButton.Visibility = Visibility.Visible;

            this.cancelEditUserButton.Visibility = Visibility.Hidden;
            this.confirmEditUserButton.Visibility = Visibility.Hidden;
        }

        private void ConfirmEditUserButton_Click(object sender, RoutedEventArgs e)
        {

            this.doctorNameBox.IsReadOnly = this.doctorLastNameBox.IsReadOnly = this.doctorRoomBox.IsReadOnly = this.doctorTitleBox.IsReadOnly = this.doctorUserNameBox.IsReadOnly = this.doctorPassBox.IsReadOnly = this.doctorJmbgBox.IsReadOnly = this.doctorAdressBox.IsReadOnly = this.doctorEmailBox.IsReadOnly = true;
            this.radioDoctor.IsEnabled = this.radioSecretary.IsEnabled = false;

            this.backMoreUserButton.Visibility = Visibility.Visible;
            this.deleteUserButton.Visibility = Visibility.Visible;
            this.userTimeRoomButton.Visibility = Visibility.Visible;
            this.editUserButton.Visibility = Visibility.Visible;

            this.cancelEditUserButton.Visibility = Visibility.Hidden;
            this.confirmEditUserButton.Visibility = Visibility.Hidden;
        }

        private void RadioDoctor_Click(object sender, RoutedEventArgs e)
        {

            this.radioSecretary.IsChecked = false;
        }

        private void RadioSecretary_Click(object sender, RoutedEventArgs e)
        {
            this.radioDoctor.IsChecked = false;
        }
    }
}
