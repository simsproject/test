﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for DemoValList.xaml
    /// </summary>
    public partial class DemoValList : Page
    {
        public static Model2.ValidationList valList = null;
        public  Boolean stopp = false;
        private static ObservableCollection<Model2.ValidationList> _meds = new ObservableCollection<Model2.ValidationList>();
        public static ObservableCollection<Model2.ValidationList> ValidationList
        {
            get { return _meds; }
            set { value = _meds; }

        }
        static DemoValList()
        {
            ValidationList = new ObservableCollection<Model2.ValidationList>();


            ValidationList.Add(new Model2.ValidationList { Name = "LEK", Code = "555555", Quantity = "100", Exparation = "10-2020", Ingredients = "*************************" });
            

        }
        public DemoValList()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void MedsMoreButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AddMedValidationButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void SendValidationListButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void BackValidationListButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void SearchBox_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

        }

        private void SearchBox_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void StopDemo_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.stop = true;
            stopp = true;
            this.NavigationService.Navigate(new Medicine());
        }
    }
}
