﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wpfKlinika.Model2;

using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using System.ComponentModel;
using System.Drawing;
using Syncfusion.Pdf.Parsing;
using Syncfusion.Pdf.Grid;
using System.Data;
using Syncfusion.Pdf.Tables;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RoomReport.xaml
    /// </summary>
    public partial class RoomReport : Page
    {
        public List<String> Room { get; set; }
       // public Model.Room room;

        public RoomReport()
        {
            InitializeComponent();
           // this.room = rr;
            this.DataContext = this;
            this.
            Room = new List<string>();
            foreach (Room r in Home.Room)
            {
                Room.Add(r.RoomName);
            }
        }

        private void CancelReportButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Rooms());
        }

        private void ConfirmReportButton_Click(object sender, RoutedEventArgs e)
        {
            /*using (PdfDocument document = new PdfDocument())
            {
                PdfPage page = document.Pages.Add();
                PdfGraphics graphics = page.Graphics;
                PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);
                graphics.DrawString("Hello World!!!", font, PdfBrushes.Black, new PointF(0, 0));
                document.Save("Tamara.pdf");
                //PdfLoadedDocument loadedDocument = new PdfLoadedDocument("Output.pdf", true);
            }*/
            //Create a new PDF document

            if (this.roomComboBox.Text.Length == 0)
            {
                redText.Visibility = Visibility.Visible;
                //redBox.BorderBrush = Brushes.PaleVioletRed;
            }
            else
            {
                PdfDocument document = new PdfDocument();


                //Add a page

                PdfPage page = document.Pages.Add();


                PdfGraphics graphics = page.Graphics;
                PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);
                //"ZA PERIOD OD " + this.dateOneBox.Text +" DO " + this.dateTwoBox.Text
                graphics.DrawString("IZVESTAJ O ZAUZETOSTI ZA PROSTORIJU " + this.roomComboBox.Text, font, PdfBrushes.Black, new PointF(0, 0));
                graphics.DrawString("ZA PERIOD OD " + this.dateOneBox.Text + " DO " + this.dateTwoBox.Text, font, PdfBrushes.Black, new PointF(20, 20));
                //graphics.DrawString("ZA PERIOD OD " + this.dateOneBox.Text + " DO " + this.dateTwoBox.Text, font, PdfBrushes.Black, new PointF(20, 20));

                //Create a PdfGrid

                PdfGrid pdfGrid = new PdfGrid();

                //Create a DataTable

                DataTable dataTable = new DataTable();

                //Add columns to the DataTable
                dataTable.Columns.Add("DATUM");
                dataTable.Columns.Add("RENOVIRANJE");
                // dataTable.Columns.Add("OPIS");

                var zadatiDatumi = new List<DateTime>();
                /* int dt = DateTime.Compare(this.dateOneBox.SelectedDate.Value.Date, this.dateTwoBox.SelectedDate.Value.Date);
                 for (DateTime d = this.dateOneBox.SelectedDate.Value.Date; dt > 0; d.AddDays(1))
                 {
                     zadatiDatumi.Add(d);
                 }
                 */
                /* DateTime d = this.dateOneBox.SelectedDate.Value.Date;
                 while (DateTime.Compare(this.dateOneBox.SelectedDate.Value.Date, this.dateTwoBox.SelectedDate.Value.Date) <0)
                 {
                     zadatiDatumi.Add(d);
                     d.AddDays(1);
                 }*/
                for (var dt = this.dateOneBox.SelectedDate.Value.Date; dt <= this.dateTwoBox.SelectedDate.Value.Date; dt = dt.AddDays(1))
                {
                    zadatiDatumi.Add(dt);
                }





                List<DateTime> postojiRenovacija = new List<DateTime>();
                Model2.Room r = new Model2.Room();
                foreach (Model2.Room rr in Home.Room)
                {
                    if (this.roomComboBox.Text.Equals(rr.RoomName))
                    {
                        r = rr;
                        foreach (DateTime ren in r.Renovation)
                        {
                            int dd = DateTime.Compare(ren, this.dateOneBox.SelectedDate.Value.Date);
                            int dr = DateTime.Compare(ren, this.dateTwoBox.SelectedDate.Value.Date);
                            string datum = ren.ToString("dd/MM/yyyy");

                            if (dd >= 0 && dr <= 0)
                            {
                                //dataTable.Rows.Add(new object[] { datum, "renoviranje" });
                                postojiRenovacija.Add(ren);
                            }

                        }

                    }
                }


                foreach (DateTime zadati in zadatiDatumi)
                {
                    string zd = zadati.ToString("dd/MM/yyyy");
                    if (!postojiRenovacija.Contains(zadati))
                    {
                        dataTable.Rows.Add(new object[] { zd, "SLOBODNA" });
                    }
                    else
                    {
                        dataTable.Rows.Add(new object[] { zd, "ZAKAZANO RENOVIRANJE" });
                    }
                }



                //Add rows to the DataTable
                /*
                dataTable.Rows.Add(new object[] { "11-06", "RENOVIRANJE" });

                dataTable.Rows.Add(new object[] { "12-06", "SLOBODNA" });

                dataTable.Rows.Add(new object[] { "13-06", "RENOVIRANJE" });

                dataTable.Rows.Add(new object[] { "14-06", "SLOBODNA" });*/



                //Assign data source

                pdfGrid.DataSource = dataTable;

                //Apply built-in table style
                pdfGrid.ApplyBuiltinStyle(PdfGridBuiltinStyle.GridTable4Accent1);
                //Draw grid to the page of PDF document.
                pdfGrid.Draw(page, new PointF(50, 100));

                //Declare and define the grid style
                /*
                PdfGridStyle gridStyle = new PdfGridStyle();


                //Set cell padding, which specifies the space between border and content of the cell

                gridStyle.CellPadding = new PdfPaddings(2, 2, 2, 2);

                //Set cell spacing, which specifies the space between the adjacent cells

                gridStyle.CellSpacing = 2;

                //Enable to adjust PDF table row width based on the text length

                gridStyle.AllowHorizontalOverflow = true;



                pdfGrid.Style = gridStyle;



                pdfGrid.Draw(page, new PointF(200, 100));
                */

                document.Save("Zauzetost prostorije " + this.roomComboBox.Text + ".pdf");

                document.Close(true);
            }
        }
    }
}
