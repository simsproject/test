﻿using Controller;
using Model.Rooms;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for Equipment.xaml
    /// </summary>
    public partial class Equipment : Page
    {
        public static InventoryMore addInventory;
        public static InventoryMore inventoryMore;
        public static Inventory equipm = null;

        private static ObservableCollection<Inventory> _equipment = new ObservableCollection<Inventory>();
        public static ObservableCollection<Inventory> EquipmentM
        {
            get { return _equipment; }
            set { value = _equipment; }

        }

        static Equipment()
        {
           
            IEnumerable<Inventory> n = MainWindow.inventoryController.GetAll();


            //Room room = null;
            foreach (Inventory inv in n)
            {
                EquipmentM.Add(inv);
            }
        }

        /*static Equipment()
        {
            EquipmentM = new ObservableCollection<Model.EquipmentM>();

            EquipmentM.Add(new Model.EquipmentM { NameE = "STETOSKOP", CodeE = "1111aa", RoomE = "102 A", QuantityE = "200", Image = new BitmapImage(new Uri("Images/mDoctor.jpg", UriKind.Relative)), Kind="Staticka", Descript = "OBICNA MASINA" });
            EquipmentM.Add(new Model.EquipmentM { NameE = "KREVET", CodeE = "2003aa", RoomE = "200 B", QuantityE = "50", Image = new BitmapImage(new Uri("Images/mDoctor.jpg", UriKind.Relative)), Kind = "Staticka", Descript = "OBICNA MASINA" });
            EquipmentM.Add(new Model.EquipmentM { NameE = "MASINA1", CodeE = "202011", RoomE = "179", QuantityE = "20", Image = new BitmapImage(new Uri("Images/mDoctor.jpg", UriKind.Relative)), Kind = "Staticka", Descript = "OBICNA MASINA" });
            EquipmentM.Add(new Model.EquipmentM { NameE = "MASINA2", CodeE = "AA555TT", RoomE = "179", QuantityE = "100", Image = new BitmapImage(new Uri("Images/mDoctor.jpg", UriKind.Relative)), Kind = "Staticka", Descript = "OBICNA MASINA" });
            EquipmentM.Add(new Model.EquipmentM { NameE = "OPREMA1", CodeE = "101021", RoomE = "500", QuantityE = "100", Image = new BitmapImage(new Uri("Images/mDoctor.jpg", UriKind.Relative)), Kind = "Staticka", Descript = "OBICNA MASINA" });
            EquipmentM.Add(new Model.EquipmentM { NameE = "OPREMA3", CodeE = "2000PP", RoomE = "500", QuantityE = "300", Image = new BitmapImage(new Uri("Images/mDoctor.jpg", UriKind.Relative)), Kind = "Staticka", Descript = "OBICNA MASINA" });
        }*/

        public Equipment()
        {
            InitializeComponent();
            this.DataContext = this;
            
        }

        

        private void InventoryMoreButton_Click(object sender, RoutedEventArgs e)
        {
            equipm = this.equipmentGrid.SelectedItem as Inventory;
            inventoryMore = new InventoryMore(equipm);

            inventoryMore.inventoryNameBox.IsReadOnly = inventoryMore.inventoryCodeBox.IsReadOnly = inventoryMore.inventoryAmountBox.IsReadOnly = inventoryMore.inventoryRoomBox.IsReadOnly = inventoryMore.moreInfoBox.IsReadOnly =  true;
            //inventoryMore.radioDynamicInventory.IsEnabled = inventoryMore.radioStaticInvenotry.IsEnabled = false;
            inventoryMore.typeInventoryBox.IsEnabled = inventoryMore.inventoryRoomBox.IsEnabled = false;
            //inventoryMore.loadImageButton.IsEnabled = false;

            inventoryMore.inventoryNameBox.Text = equipm.Name;
            inventoryMore.inventoryCodeBox.Text = equipm.SerialID.ToString();
            inventoryMore.inventoryAmountBox.Text = equipm.Quantity.ToString();

            foreach(Room rr in inventoryMore.inventoryRoomBox.Items)
            {
                if(rr.Name.Equals(equipm.Room.Name))
                {
                    inventoryMore.inventoryRoomBox.SelectedItem = rr;
                    break;
                }
            }
            
            
            
            inventoryMore.moreInfoBox.Text = equipm.Description;

            
            inventoryMore.inventoryTypeBox.SelectedItem = equipm.Type;
          


            //inventoryMore.inventoryTypeBox.Text = equipm.Kind;
            //inventoryMore.moreInfoBox.Text = equipm.Descript;

            inventoryMore.cancelAddInventoryButton.Visibility = Visibility.Hidden;
            inventoryMore.confirmAddInventoryButton.Visibility = Visibility.Hidden;
           
            inventoryMore.inventoryBackButton.Visibility = Visibility.Visible;
            inventoryMore.deleteInventoryButton.Visibility = Visibility.Visible;
            inventoryMore.editInventoryButton.Visibility = Visibility.Visible;


            //treba workers binding
            this.NavigationService.Navigate(inventoryMore);

        }

        private void AddInventoryButton_Click(object sender, RoutedEventArgs e)
        {
            Inventory equ = new Inventory("", 0, TypeOfInvetory.Stable, "", 10, new Room("100", 1));
            InventoryMore eq = new InventoryMore(equ);
            eq.inventoryNameBox.IsReadOnly = false;
            eq.inventoryCodeBox.IsReadOnly = false;
            eq.inventoryAmountBox.IsReadOnly = false;
            eq.inventoryRoomBox.IsReadOnly = false;
            eq.moreInfoBox.IsReadOnly = false;
            //addInventory = new InventoryMore();
            this.NavigationService.Navigate(eq);
        }

        private void SearchEBox_KeyUp(object sender, KeyEventArgs e)
        {
           /* string search = searchEBox.Text.ToLower();
            string searchText = null;
            if (search.Contains(" "))
            {
                string[] sr = search.Split(null);
                searchText = sr[0];
            }
            else
            {
                searchText = search;
            }



            var filtered = (EquipmentM.Where((d => (d.NameE.ToLower().StartsWith(searchText)) || (d.CodeE.ToLower().StartsWith(searchText)))));


            equipmentGrid.ItemsSource = filtered;*/
        }
    }
}
