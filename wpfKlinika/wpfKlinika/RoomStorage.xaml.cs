﻿using Model.Rooms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfKlinika
{
    /// <summary>
    /// Interaction logic for RoomStorage.xaml
    /// </summary>
    public partial class RoomStorage : Page
    {
        public static RoomStorageMore roomMore;

        public static Storage rooms = null;

        private static ObservableCollection<Room> _docs = new ObservableCollection<Room>();
        public static ObservableCollection<Room> Roomm
        {
            get { return _docs; }
            set { value = _docs; }

        }








        /* static Rooms()
         {
             List<String> radnici1 = new List<string>();
             radnici1.Add("Iva Jankovic");
             radnici1.Add("Petar Markovic");

             List<String> radnici2 = new List<string>();
             radnici2.Add("Olga Petrovic");
             radnici2.Add("Maja Matic");

             List<String> radnici3 = new List<string>();
             radnici3.Add("Marko Ivkovic");
             //radnici3.Add("Petar Markovic");

             List<String> radnici4 = new List<string>();
             radnici4.Add("Ivana Petrovic");
             //radnici4.Add("Petar Markovic");



             Room = new ObservableCollection<Model.Room>();
             Room.Add(new Model.Room { RoomName = "102 A", Floor = "1", Workers = radnici1, Info = "SOBA ZA HITNE SLUCAJEVE" });
             Room.Add(new Model.Room { RoomName = "200 B", Floor = "2", Workers = radnici2, Info = "SOBA ZA HITNE SLUCAJEVE" });
             Room.Add(new Model.Room { RoomName = "179", Floor = "1", Workers = radnici3, Info = "SOBA ZA HITNE SLUCAJEVE" });
             Room.Add(new Model.Room { RoomName = "500 A", Floor = "5", Workers = radnici4, Info = "SOBA ZA HITNE SLUCAJEVE" });
         }*/

        static RoomStorage()
        {
            //PROVERA DA LI DOCTOR ROOM RADI -> medicalRoom


            IEnumerable<Storage> n = MainWindow.storageController.GetAll();


            //Room room = null;
            foreach (Room rom in n)
            {
                Roomm.Add(rom);
            }
        }



        public RoomStorage()
        {

            InitializeComponent();
            this.DataContext = this;
            this.roomsGrid.ItemsSource = MainWindow.storageController.GetAll();
            //PROVERA DA LI ROOM RADI
            /*var roomRepository = new RoomRepository();
            var roomService = new RoomService(roomRepository);

            RoomController m = RoomController.GetInstance(roomService);
            IEnumerable<Room> n = m.GetAll();

            //Room room = null;
            foreach (Room rom in n)
            {
                Roomm.Add(rom);
            }*/



        }

        private void SearchRooms_Button_Click(object sender, RoutedEventArgs e)
        {

        }





        private void RoomReport_Button_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new RoomStorage());
        }

        private void AddRoom_Button_Click(object sender, RoutedEventArgs e)
        {
            Storage r = null;
            r = new Storage( "", 0);
            RoomStorageMore addRoom = new RoomStorageMore(r);
            addRoom.roomFloor.IsReadOnly = false;
            addRoom.roomName.IsReadOnly = false;
            //addRoom.workersBox1.IsEnabled = true;
            //addRoom.workersBox2.IsEnabled = true;
            //addRoom.workersBox3.IsEnabled = true;


            this.NavigationService.Navigate(addRoom);
        }

        private void RoomMoreButton_Click(object sender, RoutedEventArgs e)
        {
            rooms = this.roomsGrid.SelectedItem as Storage;
            roomMore = new RoomStorageMore(rooms);

            roomMore.roomName.IsReadOnly = roomMore.roomFloor.IsReadOnly = true;
            

            roomMore.roomName.Text = rooms.Name;
            roomMore.roomFloor.Text = rooms.Floor.ToString();
         


            roomMore.cancelAddRoomButton.Visibility = Visibility.Hidden;
            roomMore.confirmAddRoomButton.Visibility = Visibility.Hidden;

            roomMore.roomBackButton.Visibility = Visibility.Visible;
            roomMore.deleteRoomButton.Visibility = Visibility.Visible;
            roomMore.editRoomButton.Visibility = Visibility.Visible;


            //treba workers binding
            this.NavigationService.Navigate(roomMore);

        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            /*
            string search = searchBox.Text.ToLower();
            string searchText = null;
            if (search.Contains(" "))
            {
                string[] sr = search.Split(null);
                searchText = sr[0];
            }
            else
            {
                searchText = search;
            }



            var filtered = (Room.Where(d => (d.RoomName.ToLower().StartsWith(searchText)) || (d.Floor.ToLower().StartsWith(searchText))));


            roomsGrid.ItemsSource = filtered;*/
        }
    }
}
