﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Appointment;
using Model.MedicalRecords;
using Model.Users;
using pokusaj1000.medicalRecord.medicalReport;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for instructionToSpecialist.xaml
    /// </summary>
    /// 

    

    public partial class instructionToSpecialist : Page
    {
        public static List<Doctor> sp;
        //public static List<Appointment> ap;

        public instructionToSpecialist()
        {
            InitializeComponent();
            List<Doctor> specialists = MainWindow.doctorController.GetDoctorByTitle(Model.Users.Title.DoctorSpecialist).Where(Doctor =>(!Doctor.PersonalID.Equals(MainWindow.doctor.PersonalID))).ToList();
            this.specialisti.ItemsSource = specialists;

            this.datum.IsEnabled = false;
            this.termini.IsEnabled = false;
            this.imeIprezime.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
            this.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;

        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void nazadNaPisanjeIzvestaja(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalRecord.medicalReport.medicalReportNew());
        }

        private void promenaSpecijalista(object sender, SelectionChangedEventArgs e)
        {
            this.datum.IsEnabled = true;
            this.datum.SelectedDate = null;
            this.termini.ItemsSource = null;
            this.datum.BlackoutDates.AddDatesInPast();
        }

        private void promenaDatuma(object sender, SelectionChangedEventArgs e)
        {
            if (DateTime.Compare(Convert.ToDateTime(this.datum.SelectedDate),DateTime.Now.Date) < 0) {
                this.termini.IsEnabled = false;
                return;
            }

            this.termini.IsEnabled = true;

            Doctor doctor = specialisti.SelectedItem as Doctor;
            DateTime dateTime = this.datum.SelectedDate.Value;
            List<Appointment> allAppointmentsForDoctor = MainWindow.appointmentController.GetAppointmentsForDoctorByDate(doctor, this.datum.SelectedDate.Value.Date);
            WorkingTime workingTime = MainWindow.workingTimeController.GetWorkingTimeForDoctorByDate(doctor, this.datum.SelectedDate.Value);

            if (workingTime == null) {
                this.termini.ItemsSource = null;
                return;
            }
            WorkingTimePomocni workingTime1 = new WorkingTimePomocni(new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.BeginTime.Hour, workingTime.BeginTime.Minute, workingTime.BeginTime.Second)
                , new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.EndTime.Hour, workingTime.EndTime.Minute, workingTime.EndTime.Second));

            List<WorkingTimePomocni> workingTimes = workingTimesForDay(workingTime1);
           
            removeAllScheduleWorkingTime(allAppointmentsForDoctor, workingTimes);

            this.termini.ItemsSource = workingTimes;
        }

        private void uputiKodSpecijaliste(object sender, RoutedEventArgs e)
        {
            bool uputi = true;
            if (this.datum.Text.Equals(""))
            {
                this.datum.BorderThickness = new Thickness(3);
                uputi = false;
            }
            else {
                this.datum.BorderThickness = new Thickness(1);
            }
            if (this.specialisti.Text.Equals(""))
            {
                Style style = this.FindResource("komboBox1") as Style;
                this.specialisti.Style = style;
                uputi = false;
            }
            else {
                Style style = this.FindResource("komboBox2") as Style;
                this.specialisti.Style = style;
            }
            if (this.termini.Text.Equals("")) {
                Style style = this.FindResource("komboBox1") as Style;
                this.termini.Style = style;
                uputi = false;
            }
            else
            {
                Style style = this.FindResource("komboBox2") as Style;
                this.termini.Style = style;
            }
            if (this.uputKodSpecijaliste.Text.Equals(""))
            {
                this.uputKodSpecijaliste.BorderThickness = new Thickness(3);
                uputi = false;
            }
            else {
                this.uputKodSpecijaliste.BorderThickness = new Thickness(1);
            }
            

            if (uputi) {

                WorkingTimePomocni workingTimeSelected = this.termini.SelectedItem as WorkingTimePomocni;
                Patient patient = MainWindow.patient;
                Doctor doctor = this.specialisti.SelectedItem as Doctor;
                Appointment appointment = new Appointment(workingTimeSelected.OdSati, workingTimeSelected.DoSati, AppointmentType.regularAp, 0,patient, doctor, doctor.MedicalRoom);
                MainWindow.appointmentController.Create(appointment);
                SpecialistTreatment specialistTreatment = new SpecialistTreatment(0,this.uputKodSpecijaliste.Text,appointment,MainWindow.medicalRecord);
                MainWindow.specialistTreatmentController.Create(specialistTreatment);

                this.NavigationService.Navigate(new medicalReportNew());
            }

           
        }

        private void specialisti_KeyUp(object sender, KeyEventArgs e)
        {
            var filtrered = sp.Where(Doctor => (Doctor.FirstName.ToUpper().Contains(specialisti.Text.ToUpper())));
            this.specialisti.ItemsSource = filtrered;
        }

        private void termini_KeyUp(object sender, KeyEventArgs e)
        {
           /* var filtrered = ap.Where(Appointment => (Appointment.vreme.ToUpper().Contains(termini.Text.ToUpper())));
            this.termini.ItemsSource = filtrered;*/
        }



        public class WorkingTimePomocni
        {
            private DateTime odSati;
            private DateTime doSati;


            public DateTime OdSati { get => odSati; set => odSati = value; }
            public DateTime DoSati { get => doSati; set => doSati = value; }

            public WorkingTimePomocni(DateTime odSati, DateTime doSati)
            {
                this.OdSati = odSati;
                this.DoSati = doSati;
            }

            public override string ToString()
            {
                return OdSati.ToString("HH:mm") + "-" + DoSati.ToString("HH:mm");
            }

        }

        void removeAllScheduleWorkingTime(List<Model.Appointment.Appointment> appointmentsForDoctor, List<WorkingTimePomocni> returnValue)
        {
            WorkingTimePomocni helpVariable;
            foreach (Model.Appointment.Appointment appointment in appointmentsForDoctor)
            {
                helpVariable = new WorkingTimePomocni(appointment.StartDate, appointment.FinishDate);
                if (DateTime.Compare(helpVariable.OdSati, appointment.StartDate) == 0 && DateTime.Compare(helpVariable.DoSati, appointment.FinishDate) == 0) removeFromList(returnValue, helpVariable);
            }
        }

        bool removeFromList(List<WorkingTimePomocni> listOfWorkingTimesForDay, WorkingTimePomocni workingTimeWhoNeedRemove)
        {
            foreach (WorkingTimePomocni workingTimeForDay in listOfWorkingTimesForDay)
            {
                if (workingTimeWhoNeedRemove.OdSati == workingTimeForDay.OdSati && workingTimeWhoNeedRemove.DoSati == workingTimeForDay.DoSati) return listOfWorkingTimesForDay.Remove(workingTimeForDay);
            }
            return false;
        }

        List<WorkingTimePomocni> workingTimesForDay(WorkingTimePomocni workingTime)
        { // vraca mi listu potencijalnih termina za odredjeni datum --
            List<WorkingTimePomocni> returnValue = new List<WorkingTimePomocni>();
            DateTime begin = workingTime.OdSati;
            DateTime end = workingTime.DoSati;
            for (DateTime date = begin; date < end; date = date.AddMinutes(15))
            {
                WorkingTimePomocni helpVariable = new WorkingTimePomocni(date, date.AddMinutes(15));
                returnValue.Add(helpVariable);
                helpVariable = null;
            }
            return returnValue;

        }


    }
}
