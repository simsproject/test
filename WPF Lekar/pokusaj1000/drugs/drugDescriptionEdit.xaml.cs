﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokusaj1000.drugs
{
    /// <summary>
    /// Interaction logic for drugDescriptionEdit.xaml
    /// </summary>
    public partial class drugDescriptionEdit : Page
    {
        public drugDescriptionEdit()
        {
            InitializeComponent();
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void potvrda(object sender, RoutedEventArgs e)
        {

            MainWindow.drugDescription.sastav.Text = this.sastav.Text;
            MainWindow.drugDescription.upotreba.Text = this.upotreba.Text;
            MainWindow.drugDescription.dejstvo.Text = this.dejstvo.Text;
          
            this.NavigationService.Navigate(MainWindow.drugDescription);
        }

        private void odustani(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.drugDescription);
        }

        private void nazadNaLek(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.drugDescription);
        }


    }
}
