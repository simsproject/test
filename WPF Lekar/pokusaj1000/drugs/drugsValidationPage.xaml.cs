﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Drugs;

namespace pokusaj1000.drugs
{
    /// <summary>
    /// Interaction logic for drugsValidationPage.xaml
    /// </summary>
    public partial class drugsValidationPage : Page
    {
        public drugsValidationPage()
        {
            InitializeComponent();
            dataGridLekovi.ItemsSource = MainWindow.drugController.GetAllSentValidationDrugsForDoctor(MainWindow.doctor);

        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void opisLeka(object sender, RoutedEventArgs e)
        {
            Drug drug = this.dataGridLekovi.SelectedItem as Drug;


                    MainWindow.drugDescription.sastav.Text = drug.Ingradients;
                    MainWindow.drugDescription.dejstvo.Text = drug.Description;
                    MainWindow.drugDescription.upotreba.Text = drug.Instruction;
                    MainWindow.drugDescription.sifra.Content = drug.SerialID;
                    MainWindow.drugDescription.nazivLeka.Content = drug.Name;

                    this.NavigationService.Navigate(MainWindow.drugDescription);
         

        }

 
    }

}
