﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Drugs;

namespace pokusaj1000.drugs
{
    /// <summary>
    /// Interaction logic for drugDescription.xaml
    /// </summary>
    public partial class drugDescription : Page
    {
        public drugDescription()
        {
            InitializeComponent();
        }


        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }


        private void izmenaLeka(object sender, RoutedEventArgs e)
        {
            drugDescriptionEdit drugDescriptionEdit = new drugDescriptionEdit();

            drugDescriptionEdit.sastav.Text = this.sastav.Text;
            drugDescriptionEdit.dejstvo.Text = this.dejstvo.Text;
            drugDescriptionEdit.upotreba.Text = this.upotreba.Text;
            drugDescriptionEdit.sifra.Content = this.sifra.Content;
            drugDescriptionEdit.nazivLeka.Content = "Izmena "  + this.nazivLeka.Content;
            this.NavigationService.Navigate(drugDescriptionEdit);
        }

        private void nazadNaLekove(object sender, RoutedEventArgs e)
        {
            
            this.NavigationService.Navigate(new drugsValidationPage());
        }

        private void validacija(object sender, RoutedEventArgs e)
        {
            bool validno = true;
            if (this.sastav.Text.Equals("") || this.dejstvo.Text.Equals("") || this.upotreba.Text.Equals(""))
            {
                this.border.Visibility = Visibility.Visible;
                validno = false;
                this.back.IsEnabled = false;
                this.filpro.IsEnabled = false;
                this.kuca.IsEnabled = false;
                this.mess.IsEnabled = false;
                this.validacijaEnable.IsEnabled = false;
                this.izmeneEnabele.IsEnabled = false;
                this.obavestenje.IsEnabled = false;
            }
            else {
                this.border.Visibility = Visibility.Hidden;
            }

            if (validno)
            {
                List<Drug> drugs = MainWindow.drugController.GetAllSentValidationDrugsForDoctor(MainWindow.doctor);
                foreach (Drug d in drugs)
                {
                    if (d.SerialID.Equals(this.sifra.Content))
                    {
                        d.Ingradients = this.sastav.Text;
                        d.Instruction = this.upotreba.Text;
                        d.Description = this.dejstvo.Text;
                        d.State = DrugState.valid;
                        MainWindow.drugController.Update(d);
                        break;
                    }
                }
                this.NavigationService.Navigate(new drugsValidationPage());
            }
        }

        private void dugme_Click(object sender, RoutedEventArgs e)
        {
            this.back.IsEnabled = true;
            this.filpro.IsEnabled = true;
            this.kuca.IsEnabled = true;
            this.mess.IsEnabled = true;
            this.validacijaEnable.IsEnabled = true;
            this.izmeneEnabele.IsEnabled = true;
            this.obavestenje.IsEnabled = true;
            this.border.Visibility = Visibility.Hidden;
        }
    }
}
