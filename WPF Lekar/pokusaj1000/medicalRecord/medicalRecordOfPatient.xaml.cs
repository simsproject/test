﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using pokusaj1000.medicalRecord.medicalReport;

namespace pokusaj1000.medicalRecord
{
    /// <summary>
    /// Interaction logic for medicalRecordOfPatient.xaml
    /// </summary>
    public partial class medicalRecordOfPatient : Page
    {
        public medicalRecordOfPatient()
        {
            InitializeComponent();

        }


        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void viseInfoZdravstveniKarton(object sender, RoutedEventArgs e)
        {
            medicalRecordOfPatientMoreInfo medicalRecordOfPatientMoreInfo = new medicalRecordOfPatientMoreInfo();

            medicalRecordOfPatientMoreInfo.jmbg.Content = this.jmbg.Content;
            medicalRecordOfPatientMoreInfo.imeIprezime.Content = this.imeIprezime.Content;
            medicalRecordOfPatientMoreInfo.mesto.Content = this.mesto.Content;
            medicalRecordOfPatientMoreInfo.ulica.Content = this.ulica.Content;
            medicalRecordOfPatientMoreInfo.datum.Content = this.datum.Content;
            medicalRecordOfPatientMoreInfo.telefon.Content = this.telefon.Content;
            medicalRecordOfPatientMoreInfo.brZK.Content = this.brZdravstvenogKartona.Content;
            medicalRecordOfPatientMoreInfo.Zenski.IsChecked = this.Zenski.IsChecked;
            medicalRecordOfPatientMoreInfo.Muski.IsChecked = this.Muski.IsChecked;
            medicalRecordOfPatientMoreInfo.nazad.Visibility = this.nazad.Visibility;
            medicalRecordOfPatientMoreInfo.noviIzvestaj.Visibility = this.noviIzvestaj.Visibility;

            this.NavigationService.Navigate(medicalRecordOfPatientMoreInfo);
        }



        private void pisanjeIzvestaja(object sender, RoutedEventArgs e)
        {
            medicalReportNew medicalReport = new medicalReportNew();
            medicalReport.brZk.Content = this.brZdravstvenogKartona.Content;
            medicalReport.imeIprezime.Content = this.imeIprezime.Content;
            this.NavigationService.Navigate(medicalReport);
        }

        private void nazadNaPretraguPacijenta(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new searchPatient());
        }
    }
}
