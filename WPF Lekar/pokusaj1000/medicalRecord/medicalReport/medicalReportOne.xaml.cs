﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokusaj1000.medicalRecord.medicalReport
{
    /// <summary>
    /// Interaction logic for medicalReportOne.xaml
    /// </summary>
    public partial class medicalReportOne : Page
    {
        public medicalReportOne()
        {
            InitializeComponent();
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void nazadNaSveIzvestaje(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalReportsAllOfPatient());
        }
    }
}
