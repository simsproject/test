﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Appointment;

namespace pokusaj1000.medicalRecord.medicalReport
{
    /// <summary>
    /// Interaction logic for medicalReportNewMoreInfo.xaml
    /// </summary>
    public partial class medicalReportNewMoreInfo : Page
    {
        public medicalReportNewMoreInfo()
        {
            InitializeComponent();
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void izvestaj(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalRecord.medicalReport.medicalReportNew());
        }

        private void zakazivanjePregleda(object sender, MouseButtonEventArgs e)
        {
            MainWindow.vrstaPregleda = AppointmentType.controlAp;
            this.NavigationService.Navigate(new scheduleAppointment());
        }

        private void prepisivanjeRecepta(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new rewriteRecipe());
        }

        private void uputKodSpecijaliste(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new instructionToSpecialist());
        }

        private void uputNaBolnickoLecenje(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new instructionToHospitalTreatment());
        }

        private void zakzaivanjeOperacije(object sender, MouseButtonEventArgs e)
        {
            if (MainWindow.profilePage.zvanje.Content.Equals("DoctorSpecialist")) {
                MainWindow.vrstaPregleda = AppointmentType.operation;
                this.NavigationService.Navigate(new scheduleAppointment());
            }
        }
    }
}
