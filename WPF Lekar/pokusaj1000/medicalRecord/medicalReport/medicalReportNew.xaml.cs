﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;

namespace pokusaj1000.medicalRecord.medicalReport
{
    /// <summary>
    /// Interaction logic for medicalReportNew.xaml
    /// </summary>
    public partial class medicalReportNew : Page
    {
        public medicalReportNew()
        {
            InitializeComponent();
            this.imeIprezime.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;
            this.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;
            this.brZk.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;


        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void izvestajViseInfo(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalRecord.medicalReport.medicalReportNewMoreInfo());
        }

        private void nazadNaZdravstveniKarton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.medicalRecordOfPatient);
        }

        private void pisaneIzvestaja(object sender, RoutedEventArgs e)
        {
            bool ispravnaForma = true;
            if (this.nalazImisljenje.Text.Equals(""))
            {
                this.nalazImisljenje.BorderThickness = new Thickness(3);
                this.obaveznoPolje.Visibility = Visibility.Visible;
                ispravnaForma = false;
            }
            else {
                this.nalazImisljenje.BorderThickness = new Thickness(1);
                this.obaveznoPolje.Visibility = Visibility.Hidden;
            }

            if (ispravnaForma)
            {
                DateTime danas = DateTime.Now.Date;
                string vrsta = "";
                switch (MainWindow.vrstaPregleda.ToString()) {
                    case "operation":
                        vrsta = "Operacija";break;
                    case "controlAp":
                        vrsta = "Kontrola"; break;
                    default:
                        vrsta = "Ponovni pregled"; break;
                }

                DoctorReport doctorReport = new DoctorReport(vrsta, danas, this.nalazImisljenje.Text, this.dijagnoza.Text, this.anamneza.Text, 0, MainWindow.medicalRecordController.GetByID(long.Parse(this.brZk.Content.ToString())), MainWindow.doctor);
                MainWindow.doctorReportController.Create(doctorReport);
                this.NavigationService.Navigate(new medicalReportsAllOfPatient());
            }
        }
    }
}
