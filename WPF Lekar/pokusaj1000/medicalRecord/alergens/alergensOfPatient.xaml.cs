﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller;
using Model.MedicalRecords;
using Repository;
using Service;

namespace pokusaj1000.medicalRecord.alergens
{
    /// <summary>
    /// Interaction logic for alergensOfPatient.xaml
    /// </summary>
    public partial class alergensOfPatient : Page
    {
        public alergensOfPatient()
        {
            InitializeComponent();
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
            this.dodavanje.Visibility = Visibility.Visible;
           
           
            if (MainWindow.medicalRecordOfPatient.noviIzvestaj.Visibility == Visibility.Hidden) {
                this.nazivAlergena.IsReadOnly = true;
                this.nazivAlergena.IsEnabled = false;
            }
            
            this.alergeniLista.ItemsSource = MainWindow.medicalRecordController.GetAllergens(long.Parse(this.brZK.Content.ToString()));
            this.nazivAlergena.ItemsSource = MainWindow.allergensController.GetAll();
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }


        private void dodavanjeAlergena(object sender, RoutedEventArgs e)
        {

            List<Allergens> alergens = MainWindow.medicalRecordController.GetAllergens(long.Parse(this.brZK.Content.ToString()));
            if (nazivAlergena.SelectedItem == null || alergens.Where(Allergens => (Allergens.Name.Equals((nazivAlergena.SelectedItem as Allergens).Name))).FirstOrDefault() != null) { 
                return;
            }

            MainWindow.medicalRecordController.AddAllergenToMedicalRecord((this.nazivAlergena.SelectedItem as Allergens), long.Parse(this.brZK.Content.ToString()));
            this.alergeniLista.ItemsSource = MainWindow.medicalRecordController.GetAllergens(long.Parse(this.brZK.Content.ToString()));


        }

        private void nazadNaZdravstveniKarton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.medicalRecordOfPatient);
        }
    }
}
