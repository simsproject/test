﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokusaj1000.medicalRecord
{
    /// <summary>
    /// Interaction logic for medicalRecordOfPatientMoreInfo.xaml
    /// </summary>
    public partial class medicalRecordOfPatientMoreInfo : Page
    {
        public medicalRecordOfPatientMoreInfo()
        {
            InitializeComponent();
        }


        private void zdravstveniKarton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.medicalRecordOfPatient);
        }

        private void listaSvihIzvestaja(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new medicalReportsAllOfPatient());
        }

        private void uvidUIstorijuBolesti(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new medicalHistoryReportsAll());
        }

        private void alergeni(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new medicalRecord.alergens.alergensOfPatient());
        }
    }
}
