﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using pokusaj1000.modelTest;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for homePageMoreInfo.xaml
    /// </summary>
    public partial class homePageMoreInfo : Page
    {
        public homePageMoreInfo()
        {
            InitializeComponent();
            dataGridTermini.ItemsSource = MainWindow.homePage.dataGridTermini.ItemsSource;
        }

        private void validacijaLekova(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new drugs.drugsValidationPage());
        }

        private void pretragaPacijenta(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new searchPatient());
        }

        private void nazadNaHomePage(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }
    }
}
