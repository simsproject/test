﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Appointment;
using Model.Users;
using pokusaj1000.medicalRecord.medicalReport;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for scheduleAppointment.xaml
    /// </summary>
    public partial class scheduleAppointment : Page
    {
        public scheduleAppointment()
        {
            InitializeComponent();
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
            this.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;
            this.imeIperezime.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;
            this.dataGridTermini.ItemsSource = null;
            this.datum.SelectedDate = null;
            this.datum.BlackoutDates.AddDatesInPast();

        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void nazadNaPisanjeIzvestaja(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalRecord.medicalReport.medicalReportNew());
        }

        private void zakazivanjeTermina(object sender, RoutedEventArgs e)
        {

             if (this.dataGridTermini.SelectedItem == null || (this.dataGridTermini.SelectedItem as Appointment).Patient != null) 
             {
                            return;
             }
                        
             Patient patient = MainWindow.patient;
            
             Doctor doctor = MainWindow.doctor;


             Appointment appointment = this.dataGridTermini.SelectedItem as Appointment;
             appointment.Patient = patient;
             MainWindow.appointmentController.Create(appointment);
            
             this.NavigationService.Navigate(new medicalReportNew());


        }

        private void promenaDatuma(object sender, SelectionChangedEventArgs e)
        {

            Doctor doctor = MainWindow.doctor;
            DateTime dateTime = this.datum.SelectedDate.Value;
           
            List<Appointment> allAppointmentsForDoctor = MainWindow.appointmentController.GetAppointmentsForDoctorByDate(doctor, this.datum.SelectedDate.Value.Date);
            WorkingTime workingTime = MainWindow.workingTimeController.GetWorkingTimeForDoctorByDate(doctor, this.datum.SelectedDate.Value);

            if (workingTime == null)
            {
                this.dataGridTermini.ItemsSource = null;
                return;
            }
            WorkingTimePomocni workingTime1 = new WorkingTimePomocni(new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.BeginTime.Hour, workingTime.BeginTime.Minute, workingTime.BeginTime.Second)
                , new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.EndTime.Hour, workingTime.EndTime.Minute, workingTime.EndTime.Second));

            List<WorkingTimePomocni> workingTimes = workingTimesForDay(workingTime1);
            List<Appointment> sviAppointmenti = new List<Appointment>();

            foreach (WorkingTimePomocni wtp in workingTimes)
            {
                Appointment appointment = new Appointment(wtp.OdSati, wtp.DoSati, MainWindow.vrstaPregleda, 0, null, doctor, doctor.MedicalRoom);
                sviAppointmenti.Add(appointment);
            }

            foreach (Appointment a in allAppointmentsForDoctor) {
                foreach (Appointment appointment in sviAppointmenti) {
                    if (a.StartDate.ToString().Equals(appointment.StartDate.ToString())) {
                        appointment.Patient = a.Patient;
                     
                        break;
                    }
                }
            }
            this.dataGridTermini.ItemsSource = sviAppointmenti;


        }


        public class WorkingTimePomocni
        {
            private DateTime odSati;
            private DateTime doSati;


            public DateTime OdSati { get => odSati; set => odSati = value; }
            public DateTime DoSati { get => doSati; set => doSati = value; }

            public WorkingTimePomocni(DateTime odSati, DateTime doSati)
            {
                this.OdSati = odSati;
                this.DoSati = doSati;
            }

            public override string ToString()
            {
                return OdSati.ToString("HH:mm") + "-" + DoSati.ToString("HH:mm");
            }

        }

    
        List<WorkingTimePomocni> workingTimesForDay(WorkingTimePomocni workingTime)
        { // vraca mi listu potencijalnih termina za odredjeni datum --
            List<WorkingTimePomocni> returnValue = new List<WorkingTimePomocni>();
            DateTime begin = workingTime.OdSati;
            DateTime end = workingTime.DoSati;
            for (DateTime date = begin; date < end; date = date.AddMinutes(15))
            {
                WorkingTimePomocni helpVariable = new WorkingTimePomocni(date, date.AddMinutes(15));
                returnValue.Add(helpVariable);
                helpVariable = null;
            }
            return returnValue;

        }


    }
}
