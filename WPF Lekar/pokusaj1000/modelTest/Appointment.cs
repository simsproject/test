﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
   
    public class Appointment
    {
        public enum AppointmentType { Operacija, Pregled, Kontrola };
        public String vreme { get; set; }
        public AppointmentType type { get; set; }
        public Patient patient { get; set; }

        public Doctor doctor { get; set; }

        public DateTime datum { get; set; }
        public Appointment(String vreme,  AppointmentType type, Patient patient, Doctor doctor,DateTime datum) {
            this.vreme = vreme;
            this.type = type;
            this.patient = patient;
            this.doctor = doctor;
            this.datum = datum;
        }

        public override string ToString()
        {
            return this.vreme;
        }


    }



}
