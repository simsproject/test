﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class MedicalHistory
    {
        public long brZk { get; set; }
        public String tip { get; set; }

        public int id { get; set; }

        public String datumPocetka { get; set; }
        public String datumKraja { get; set; }

        public String uzrokHospitalizacije { get; set; }
        public String uzrokSmrti {get;set;}

        public String razlogOtpusta { get; set; }

        public MedicalHistory(long brZk,String tip,String datumPocetka,String datumKraja,int id,String uzrokHospitalizacije, String uzrokSmrti, String razlogOtpusta) {
            this.brZk = brZk;
            this.tip = tip;
            this.datumPocetka = datumPocetka;
            this.datumKraja = datumKraja;
            this.id = id;
            this.uzrokHospitalizacije = uzrokHospitalizacije;
            this.uzrokSmrti = uzrokSmrti;
            this.razlogOtpusta = razlogOtpusta;
        }


    }
}
