﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class Patient
    {
        public enum Gender { MALE, FEMALE };

        public String firstName { get; set; }
        public String lastName { get; set; }
        public DateTime dateOfBirth { get; set; }
        public String personalID { get; set; }
        public Gender gender { get; set; }
        public String phoneNumber { get; set; }
        public String email { get; set; }
        public String address { get; set; }
        public String city { get; set; }

        public long brZdravstvenogKartona { get; set; }


        public Patient(String firstName, String lastName, DateTime dateOfBirth, String personalID, Gender gender, String phoneNumber, String email, String address, String city, long brojZK) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.dateOfBirth = dateOfBirth;
            this.personalID = personalID;
            this.gender = gender;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.address = address;
            this.city = city;
            this.brZdravstvenogKartona = brojZK;
        }

        public Patient() {
            this.firstName = "//";
            this.lastName = "//";
            this.dateOfBirth = new DateTime();
            this.personalID = "";
            this.gender = Gender.FEMALE;
            this.phoneNumber = "";
            this.email = "";
            this.address = "";
            this.city = "";
            this.brZdravstvenogKartona = -1;
        }

 
 
    }
}
