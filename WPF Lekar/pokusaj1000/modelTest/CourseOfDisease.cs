﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class CourseOfDisease
    {
        public String datum { get; set; }
        public String tokBolesti { get; set; }

        public String terapija { get; set; }

        public int idIstorijeBolesti { get; set; }

        public CourseOfDisease(String datum, String tokBolesti, String terapija,int idIstorijeBolesti) {
            this.datum = datum;
            this.tokBolesti = tokBolesti;
            this.terapija = terapija;
            this.idIstorijeBolesti = idIstorijeBolesti;
        }
    }
}
