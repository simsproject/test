﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class MedicalReport
    {
        public String nalazImisljenje { get; set; }
        public String dijagnoza { get; set; }
        public String anamneza { get; set; }

        public long brZK { get; set; }

        public Appointment.AppointmentType vrsta { get; set; }

        public String vreme { get; set; }

        public MedicalReport(String nalazImisljenje, String dijagnoza, String anamneza,long brZK,Appointment.AppointmentType vrsta,String vreme) {
            this.nalazImisljenje = nalazImisljenje;
            this.dijagnoza = dijagnoza;
            this.anamneza = anamneza;
            this.brZK = brZK;
            this.vrsta = vrsta;
            this.vreme = vreme;
        }

        public MedicalReport(long brZK) {
            this.brZK = brZK;
        }
    }
}
