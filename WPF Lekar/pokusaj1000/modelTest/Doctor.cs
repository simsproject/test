﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
   
    public class Doctor
    {
        public enum Title { Specijalista, Opsta_praksa};
        public String firstName { get; set; }
        public String lastName { get; set; }
        public String personalID { get; set; }
        public String phoneNumber { get; set; }
        public String email { get; set; }
        public String  address { get; set; }
        public String userName { get; set; }
        public String password { get; set; }

        public Title title { get; set; }

        public String zvanje { get; set; }
        public String bio { get; set; }



        public Doctor(String firstName, String lastName, String personalID, String phoneNumber, String email , String address,String userName,String password,Title title,String zvanje,String bio) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.personalID = personalID;
            this.phoneNumber = phoneNumber;
            this.email = email;
            this.address = address;
            this.userName = userName;
            this.password = password;
            this.title = title;
            this.bio = bio;
            this.zvanje = zvanje;
        }
        public override string ToString()
        {
            return this.firstName;
        }

 
    }
}
