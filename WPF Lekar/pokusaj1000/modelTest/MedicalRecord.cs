﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class MedicalRecord
    {
        public List<Alergens> alergens { get; set; }
        public Patient Patient { get; set; }
        public long brZK { get; set; }

        public MedicalRecord(List<Alergens> alergens,Patient patient,long brZk) {
            this.alergens = alergens;
            this.brZK = brZk;
            this.Patient = patient;
        }

    }
}
