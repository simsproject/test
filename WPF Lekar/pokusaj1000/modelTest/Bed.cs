﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class Bed
    {
        public bool zauzet { get;  set; }

        public String broj { get; set; }

        public String nazivSobe { get; set; }
        public Bed(String broj,String nazivSobe) {
            this.broj = broj;
            this.nazivSobe = nazivSobe;
            this.zauzet = false;
        }

        public override string ToString()
        {
            return this.broj;
        }


    }
}
