﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class Drug
    {
        public String naziv { get; set; }
        public String sifra { get; set; }
        public String sastav { get; set; }
        public String dejstvo { get; set; }
        public String upotreba { get; set; }

        public Drug(String naziv, String sifra, String sastav, String dejstvo, String upotreba) {
            this.naziv = naziv;
            this.sifra = sifra;
            this.sastav = sastav;
            this.dejstvo = dejstvo;
            this.upotreba = upotreba;
        }

    }
}
