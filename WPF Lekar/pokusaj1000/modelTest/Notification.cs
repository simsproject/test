﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pokusaj1000.modelTest
{
    public class Notification
    {

        public string naziv { get; set; }


        public Notification(String naziv)
        {
            this.naziv = naziv;
        }

        public override string ToString()
        {
            return this.naziv;
        }
    }
}
