﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for medicalHistoryTideOfDiseaseAdd.xaml
    /// </summary>
    public partial class medicalHistoryTideOfDiseaseAdd : Page
    {
        public medicalHistoryTideOfDiseaseAdd()
        {
            InitializeComponent();
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
            this.datum.Content = DateTime.Now.ToString("dd.MM.yyyy.");
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void nazadNaListuTokovaBolesti(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalHistoryTideOfDisease());
        }

        private void dodavanjeTokaBolesti(object sender, RoutedEventArgs e)
        {
            bool okPodaci = true;

            if (this.tokBolesti.Text.Equals(""))
            {
                this.tokBolesti.BorderThickness = new Thickness(3);
                okPodaci = false;
            }
            else {
                this.tokBolesti.BorderThickness = new Thickness(1);
            }


            if (this.terapija.Text.Equals(""))
            {
                this.terapija.BorderThickness = new Thickness(3);
                okPodaci = false;
            }
            else
            {
                this.terapija.BorderThickness = new Thickness(1);
            }

            if (okPodaci)
            {
                CourseOfDisease courseOfDisease = new CourseOfDisease(DateTime.Now.Date,this.terapija.Text,this.tokBolesti.Text,0,MainWindow.medicalHistoryController.GetByID(long.Parse(MainWindow.idIstorijeBolseti.ToString())));
                MainWindow.courseOfDieaseController.Create(courseOfDisease);

                this.NavigationService.Navigate(new medicalHistoryTideOfDisease());
            }
        }
    }
}
