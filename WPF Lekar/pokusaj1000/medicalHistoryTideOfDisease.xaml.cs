﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for medicalHistoryTideOfDisease.xaml
    /// </summary>
    public partial class medicalHistoryTideOfDisease : Page
    {
        public medicalHistoryTideOfDisease()
        {
            InitializeComponent();
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
            List<CourseOfDisease> courseOfdisease = new List<CourseOfDisease>();
            foreach (CourseOfDisease cs in MainWindow.courseOfDieaseController.GetAll()) {
                if (cs.MedicalHistory.SerialID.ToString().Equals(MainWindow.idIstorijeBolseti.ToString())) {
                    courseOfdisease.Add(cs);
                }
            }
            if (!MainWindow.medicalHistoryController.GetByID(long.Parse(MainWindow.idIstorijeBolseti.ToString())).DateEnd.Equals("na lecenju"))
            {
                this.dodavanjeToka.Visibility = Visibility.Hidden;
            }


            /* foreach (MedicalHistory mh in MainWindow.medicalHistories) {
                 if (mh.id == MainWindow.idIstorijeBolseti) {
                     if (!mh.datumKraja.Equals("na lecenju")) {
                         this.dodavanjeToka.Visibility = Visibility.Hidden;
                     }
                     break;
                 }
             }*/

            this.dataGridPregledi.ItemsSource = MainWindow.courseOfDieaseController.GetAllCourseOfDiseasesForMedicalHistory(MainWindow.medicalHistoryController.GetByID(long.Parse(MainWindow.idIstorijeBolseti.ToString())));
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void dodajTokBolesti(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalHistoryTideOfDiseaseAdd());
        }

        private void nazadNaIstorijuBolesti(object sender, RoutedEventArgs e)
        {
            if (MainWindow.medicalHistoryController.GetByID(long.Parse(MainWindow.idIstorijeBolseti.ToString())).DateEnd.Equals("na lecenju"))
            {
                this.NavigationService.Navigate(new medicalHistoryReportNotHospitalizationYet());
            }
            else {
                this.NavigationService.Navigate(new medicalHistoryReportOne());
            }
           
        }
    }
}
