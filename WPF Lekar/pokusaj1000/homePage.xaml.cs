﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Appointment;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for homePage.xaml
    /// </summary>
    public partial class homePage : Page
    {


        public homePage()
        {
            InitializeComponent();
            this.dataGridTermini.ItemsSource = MainWindow.appointmentController.GetAppointmentsForDoctorByDate(MainWindow.doctor,DateTime.Now.Date);

        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void medicalRecord(object sender, RoutedEventArgs e)
        {


            Appointment appointment = this.dataGridTermini.SelectedItem as Appointment;

            if (DateTime.Compare(DateTime.Now,appointment.FinishDate) > 0) {
                MessageBox.Show("Ne mozete pisati izvestaj za pacienta koji je vec bio na pregledu");
                return;
            }

            if (DateTime.Compare(appointment.StartDate, DateTime.Now) > 0) {
                MessageBox.Show("Ne mozete pisati izvestaj pre pocetka zakazanog termina");
                return;
            }
            
            MainWindow.medicalRecord = appointment.Patient.MedicalRecord;
            MainWindow.trenutnoVreme = appointment.StartDate.ToString("HH:mm");
            MainWindow.vrstaPregleda = appointment.Type;
            MainWindow.patient = appointment.Patient;

            MainWindow.medicalRecordOfPatient.jmbg.Content = appointment.Patient.PersonalID;
            MainWindow.medicalRecordOfPatient.imeIprezime.Content = appointment.Patient.FirstName + " " + appointment.Patient.LastName;
            MainWindow.medicalRecordOfPatient.datum.Content = appointment.Patient.DateOfBirth.ToString("dd.MM.yyyy.");
            MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content = appointment.Patient.MedicalRecord.SerialID.ToString();
            MainWindow.medicalRecordOfPatient.telefon.Content = appointment.Patient.PhoneNumber;
            MainWindow.medicalRecordOfPatient.mesto.Content = appointment.Patient.Address.City.Name;
            MainWindow.medicalRecordOfPatient.ulica.Content = appointment.Patient.Address.Name + " " + appointment.Patient.Address.PostalCode;

            if (appointment.Patient.Gender.ToString().Equals("Female")) {
                MainWindow.medicalRecordOfPatient.Zenski.IsChecked = true;
                MainWindow.medicalRecordOfPatient.Muski.IsChecked = false;
            }
            else {
                MainWindow.medicalRecordOfPatient.Zenski.IsChecked = false;
                MainWindow.medicalRecordOfPatient.Muski.IsChecked = true;
            }

            MainWindow.medicalRecordOfPatient.nazad.Visibility = Visibility.Hidden;
            MainWindow.medicalRecordOfPatient.noviIzvestaj.Visibility = Visibility.Visible;

            this.NavigationService.Navigate(MainWindow.medicalRecordOfPatient);
        }

        private void viseMogucnosti(object sender, RoutedEventArgs e)
        {
            homePageMoreInfo homePageMoreInfo = new homePageMoreInfo();
            this.NavigationService.Navigate(homePageMoreInfo);
        }

        private void odjavaPovratneInfo(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new homePageDotsVerical());
        }
    }



}
