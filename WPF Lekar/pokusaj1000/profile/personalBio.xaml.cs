﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Users;

namespace pokusaj1000.profile
{
    /// <summary>
    /// Interaction logic for personalBio.xaml
    /// </summary>
    public partial class personalBio : Page
    {
        public personalBio()
        {
            InitializeComponent();
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void potvrdi(object sender, RoutedEventArgs e)
        {
            List<Doctor> doctors = MainWindow.doctorController.GetAll().ToList();
            foreach (Doctor d in doctors) {
                if (d.UserName.Equals(MainWindow.profilePage.korisnickoIme.Content)) {
                    //d.bio = this.biografija.Text;
                    break;
                }
            }
            MainWindow.profilePage.bio.Text = this.biografija.Text;
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void odustani(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void nazadNaProfil(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }
    }
}
