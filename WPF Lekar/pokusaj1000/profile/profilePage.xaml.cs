﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Users;

namespace pokusaj1000.profile
{
    /// <summary>
    /// Interaction logic for profilePage.xaml
    /// </summary>
    public partial class profilePage : Page
    {
        public profilePage()
        {
            InitializeComponent();
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void licni_podaci(object sender, RoutedEventArgs e)
        {
            personalInfo personalInfo = new personalInfo();
            personalInfo.ime.Text = this.ime.Content.ToString();
            personalInfo.prezime.Text = this.prezime.Content.ToString();
            personalInfo.jmbg.Text = this.jmbg.Content.ToString();
            personalInfo.email.Text = this.email.Content.ToString();
           
            personalInfo.kontaktTel.Text = this.tel.Content.ToString();
            long j = 0;
            foreach (Address c in MainWindow.adressController.GetAll())
            {
                if (c.City.Name.Equals(MainWindow.doctor.Address.City.Name))
                {
                    if (c.Name.Equals(MainWindow.doctor.Address.Name))
                    {
                        break;
                    }
                    j++;
                }
            }
            MainWindow.indexAdrese = j;
            long i = 0;
            foreach (City c in MainWindow.cityController.GetAll())
            {
                if (c.Name.Equals(MainWindow.doctor.Address.City.Name))
                {
                    break;
                }
                i++;
            }

            personalInfo.Cmb.SelectedIndex = int.Parse(i.ToString());
            personalInfo.adresa.SelectedIndex = int.Parse(j.ToString());

            this.NavigationService.Navigate(personalInfo);

        }

        private void profil_korIme_lozinka(object sender, RoutedEventArgs e)
        {           
            personalUsernameAndPassword personalUsernameAndPassword = new personalUsernameAndPassword();
            personalUsernameAndPassword.korisnickoime.Text = this.korisnickoIme.Content.ToString();
            personalUsernameAndPassword.nova_lozinka.Password = this.lozinka.Content.ToString();
            this.NavigationService.Navigate(personalUsernameAndPassword);
        }

        private void profilBiografija(object sender, RoutedEventArgs e)
        {
            personalBio personalBio = new personalBio();
            personalBio.biografija.Text = this.bio.Text;
            this.NavigationService.Navigate(personalBio);
        }
    }
}
