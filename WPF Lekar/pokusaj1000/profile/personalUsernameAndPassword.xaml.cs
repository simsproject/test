﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Users;

namespace pokusaj1000.profile
{
    /// <summary>
    /// Interaction logic for personalUsernameAndPassword.xaml
    /// </summary>
    public partial class personalUsernameAndPassword : Page
    {
        public personalUsernameAndPassword()
        {
            InitializeComponent();
        }
        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }


        private void potvrdi(object sender, RoutedEventArgs e)
        {
            bool ispravnoKorisnickoIme = true;
            bool ispravnaLozinka = true;
            bool korisnickoImeDuzina = true;


            if (this.nova_lozinka.Password.Equals("") || this.nova_lozinka.Password.Length < 6)
            {
                this.nova_lozinka.BorderThickness = new Thickness(3);
                ispravnaLozinka = false;
            }
            else {
                this.nova_lozinka.BorderThickness = new Thickness(1);
            }

            if (ispravnaLozinka && ispravnoKorisnickoIme && korisnickoImeDuzina) {

                Doctor doctor = MainWindow.doctor;
               
                doctor.UserName = this.korisnickoime.Text;
                if (!doctor.Password.Equals(this.nova_lozinka.Password))
                {
                    doctor.Password = this.nova_lozinka.Password;
                }

                MainWindow.doctorController.Update(doctor);
                MainWindow.userController.Update(doctor);
                MainWindow.doctor = doctor;
                        
                    
                
                MainWindow.profilePage.korisnickoIme.Content = this.korisnickoime.Text;
                StringBuilder sb = new StringBuilder();
                foreach (Char c in this.nova_lozinka.Password)
                {
                    sb.Append("*");
                }
                MainWindow.profilePage.lozinka.Content = sb.ToString();
                this.NavigationService.Navigate(MainWindow.profilePage);
            }

            if (!ispravnoKorisnickoIme) {
                this.porukaZaKorisnikoIme.Visibility = Visibility.Visible;
                this.porukaZaNovuLozinku.Visibility = Visibility.Hidden;
                this.porukaZaKorisnikoImePrazno.Visibility = Visibility.Hidden;
            } else if (!ispravnaLozinka) {
                this.porukaZaKorisnikoIme.Visibility = Visibility.Hidden;
                this.porukaZaNovuLozinku.Visibility = Visibility.Visible;
                this.porukaZaKorisnikoImePrazno.Visibility = Visibility.Hidden;
            } else if (!korisnickoImeDuzina) {
                this.porukaZaKorisnikoIme.Visibility = Visibility.Hidden;
                this.porukaZaNovuLozinku.Visibility = Visibility.Hidden;
                this.porukaZaKorisnikoImePrazno.Visibility = Visibility.Visible;
            }

        }
        private void odustani(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void nazadNaProfil(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }


    }
}
