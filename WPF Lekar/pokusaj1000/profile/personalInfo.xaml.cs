﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Users;

namespace pokusaj1000.profile
{
    /// <summary>
    /// Interaction logic for personalInfo.xaml
    /// </summary>
    public partial class personalInfo : Page
    {
        public personalInfo()
        {
            InitializeComponent();
            this.Cmb.ItemsSource = MainWindow.cityController.GetAll().ToList();
            this.adresa.ItemsSource = MainWindow.adressController.GetAll().ToList();

        }

        //menjanje prozora 
        #region

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }
        #endregion


        private void odustani(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }
        private void potvrdi(object sender, RoutedEventArgs e)
        {
            bool ispravniPodaci = true;

            if (this.ime.Text.Equals(""))
            {
                this.ime.BorderThickness = new Thickness(3);
                ispravniPodaci = false;
            }
            else {
                this.ime.BorderThickness = new Thickness(1);

            }
            if (this.prezime.Text.Equals(""))
            {
                this.prezime.BorderThickness = new Thickness(3);
                ispravniPodaci = false;
            }
            else
            {
                this.prezime.BorderThickness = new Thickness(1);

            }
            if (this.adresa.Text.Equals(""))
            {
                this.adresa.BorderThickness = new Thickness(3);
                ispravniPodaci = false;
            }
            else
            {
                this.adresa.BorderThickness = new Thickness(1);

            }
            if (this.Cmb.Text.Equals(""))
            {
                Style style = this.FindResource("komboBox1") as Style;        
                this.Cmb.Style = style;
                ispravniPodaci = false;
            }
            else
            {
                Style style = this.FindResource("komboBox2") as Style;
                this.Cmb.Style = style;

            }

            Regex rx = new Regex(@"[0-9]{3}/[0-9]{3}-[0-9]{4}");


            if (!rx.IsMatch(this.kontaktTel.Text) && !kontaktTel.Text.Equals("")) {
                this.kontaktTel.BorderThickness = new Thickness(3);
                ispravniPodaci = false;
            }
            else
            {
                this.kontaktTel.BorderThickness = new Thickness(1);

            }

            if (!this.email.Text.Equals(""))
            {
                try
                {
                    MailAddress m = new MailAddress(this.email.Text);
                    this.email.BorderThickness = new Thickness(1);

                }
                catch (FormatException)
                {
                    this.email.BorderThickness = new Thickness(3);
                    ispravniPodaci = false;
                }
            }
            else {
                this.email.BorderThickness = new Thickness(1);
            }



            if (!ispravniPodaci) {
                this.border.Visibility = Visibility.Visible;
                this.nazad.IsEnabled = false;
                this.dugme1.IsEnabled = false;
                this.dugme2.IsEnabled = false;
                this.kuca.IsEnabled = false;
                this.filpro.IsEnabled = false;
                this.zvono.IsEnabled = false;
                this.mess.IsEnabled = false;

                this.prezime.IsEnabled = false;
                this.jmbg.IsEnabled = false;
                this.kontaktTel.IsEnabled = false;
                this.email.IsEnabled = false;
                this.adresa.IsEnabled = false;
                this.Cmb.IsEnabled = false;

                return;
            }

           
            Doctor doctor = MainWindow.doctor;


            doctor.FirstName = this.ime.Text;
            doctor.LastName = this.prezime.Text;
            doctor.PersonalID = this.jmbg.Text;
            doctor.PhoneNumber = this.kontaktTel.Text;
            doctor.Email = this.email.Text;
            doctor.Address = this.adresa.SelectedItem as Address;

            MainWindow.doctor = doctor;
            MainWindow.doctorController.Update(doctor);
            MainWindow.userController.Update(doctor);
                    
                
            

            MainWindow.profilePage.ime.Content = this.ime.Text;
            MainWindow.profilePage.prezime.Content = this.prezime.Text;
            MainWindow.profilePage.jmbg.Content = this.jmbg.Text;
            MainWindow.profilePage.tel.Content = this.kontaktTel.Text;
            MainWindow.profilePage.email.Content = this.email.Text;
            MainWindow.profilePage.adresa.Content = this.adresa.Text + "," + this.Cmb.Text;

            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void nazadNaProfil(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void Adresa(object sender, KeyEventArgs e)
        {
            var filtrered = MainWindow.adressController.GetAll().Where(Address => (Address.Name.ToUpper().Contains(this.adresa.Text.ToUpper())));
            this.adresa.ItemsSource = filtrered;

        }

        private void grad(object sender, KeyEventArgs e)
        {
            var filtrered = MainWindow.cityController.GetAll().Where(City => (City.Name.ToUpper().Contains(Cmb.Text.ToUpper())));
            this.Cmb.ItemsSource = filtrered;

        }

        private void dugmeUredu(object sender, RoutedEventArgs e)
        {
            this.border.Visibility = Visibility.Hidden;
            this.nazad.IsEnabled = true;
            this.dugme1.IsEnabled = true;
            this.dugme2.IsEnabled = true;
            this.kuca.IsEnabled = true;
            this.filpro.IsEnabled = true;
            this.zvono.IsEnabled = true;
            this.mess.IsEnabled = true;

            this.prezime.IsEnabled = true;
            this.jmbg.IsEnabled = true;
            this.kontaktTel.IsEnabled = true;
            this.email.IsEnabled = true;
            this.adresa.IsEnabled = true;
            this.Cmb.IsEnabled = true;
        }

        private void specialniKarakteri(object sender, KeyEventArgs e)
        {
            Regex regex = new Regex(@"(?i)^[a-z’'()/.,\s-]+$");

            if (!regex.IsMatch(e.Key.ToString()) ) {
                e.Handled = true;
            }
        }

        private void adresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<Address> addrese = new List<Address>();
            List<Address> sveAdrese = MainWindow.adressController.GetAll().ToList();
            foreach (Address ad in sveAdrese) {
                if (ad.City.Name.Equals((this.Cmb.SelectedItem as City).Name)) {
                    addrese.Add(ad);
                }
            }
            this.adresa.ItemsSource = addrese;
        }
    }


}
