﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Other;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for notificationPage.xaml
    /// </summary>
    public partial class notificationPage : Page
    {
        public notificationPage()
        {
            InitializeComponent();
            List<Notification> notificationsForDoctor = MainWindow.notificationController.GetNotificationForUser(MainWindow.doctor);
            this.dataGridObavestenja.ItemsSource = notificationsForDoctor;
        }
        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }
    }
}
