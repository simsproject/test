﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Users;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for logInPage.xaml
    /// </summary>
    public partial class logInPage : Page
    {
        List<Doctor> doctors = new List<Doctor>();


        public logInPage()
        {

            InitializeComponent();
            
        }

        private void prijava(object sender, RoutedEventArgs e)
        {
            bool uspesno = false;
           
            List<Doctor> doctors = MainWindow.doctorController.GetAll().ToList();


            List<Address> addresses = MainWindow.adressController.GetAll().ToList();
            List<City> cities = MainWindow.cityController.GetAll().ToList();
           
            foreach (Doctor d in doctors) {
                if (d.UserName.Equals(this.korisnickoIme.Text)) {
                    if (d.Password.Equals(this.lozinka.Password)) {
                        MainWindow.doctor = d;
                        MainWindow.homePage = new homePage();
                        uspesno = true;
                        MainWindow.profilePage.zvanje.Content = d.Title.ToString();
                        MainWindow.profilePage.korisnickoIme.Content = d.UserName;
                        StringBuilder sb = new StringBuilder();
                        foreach (Char c in d.Password)
                        {
                            sb.Append("*");
                        } 
                        MainWindow.profilePage.lozinka.Content = sb.ToString();
                        MainWindow.profilePage.ime.Content = d.FirstName;
                        MainWindow.profilePage.prezime.Content = d.LastName;
                        MainWindow.profilePage.jmbg.Content = d.PersonalID;
                        MainWindow.profilePage.email.Content = d.Email;
                        MainWindow.profilePage.adresa.Content = d.Address.Name + " " +  d.Address.PostalCode + "," + d.Address.City.Name;
                        long j = 0;
                        
                        foreach (Address c in addresses)
                        {
                            if (c.City.Name.Equals(d.Address.City.Name)) {
                                if (c.Name.Equals(d.Address.Name))
                                {
                                    break;
                                }
                                j++;
                            }
                        }
                        MainWindow.indexAdrese = j;
                        long i = 0;
                        
                        foreach (City c in cities) {
                            if (c.Name.Equals(d.Address.City.Name))
                            {                           
                                break;
                            }
                            i++;
                        }
                        MainWindow.indexGrada = i;
                        MainWindow.profilePage.tel.Content = d.PhoneNumber;
                        MainWindow.profilePage.bio.Text = "";


                        this.NavigationService.Navigate(MainWindow.homePage);
                    }
                }
            }

            if (!uspesno) {
                this.greska.Visibility = Visibility.Visible;
            }

        }
    }
}
