﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;
using Model.Rooms;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for medicalHistoryDismiss.xaml
    /// </summary>
    public partial class medicalHistoryDismiss : Page
    {
        public medicalHistoryDismiss()
        {
            InitializeComponent();

            foreach (MedicalHistory mh in MainWindow.medicalHistoryController.GetAllMedicalHistoriesForPatient(MainWindow.medicalRecord))
            {
                if (mh.SerialID.ToString().Equals(MainWindow.idIstorijeBolseti.ToString()))
                {
                    this.datumPrijema.Content = mh.DateBegin.ToString("dd.MM.yyyy.");
                    this.uzrokHospitalizacije.Text = mh.ReasonOfHospitalization;
                    break;
                }
            }

            DateTime trenutnoVreme = DateTime.Now;
            String vreme = trenutnoVreme.ToString("dd.MM.yyyy.");
            this.datumotpusta.Content = vreme;            
            this.imeIprezime.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;
            this.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;

           
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void nazadNaIstorijuBolesti(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalHistoryReportNotHospitalizationYet());
        }

        private void potvrda(object sender, RoutedEventArgs e)
        {
            bool okPodaci = true;

            if (this.razlogOtpusta.Text.Equals(""))
            {
                okPodaci = false;
                this.razlogOtpusta.BorderThickness = new Thickness(3);
            }
            else {
                this.razlogOtpusta.BorderThickness = new Thickness(1);
            }

            if (this.uzrokHospitalizacije.Text.Equals(""))
            {
                okPodaci = false;
                this.uzrokHospitalizacije.BorderThickness = new Thickness(3);
            }
            else
            {
                this.uzrokHospitalizacije.BorderThickness = new Thickness(1);
            }

            if (okPodaci)
            {
                MedicalHistory mh = MainWindow.medicalHistoryController.GetByID(long.Parse(MainWindow.idIstorijeBolseti.ToString()));
               
                mh.ReasonOfRelase = this.razlogOtpusta.Text;
                mh.ReasonOfHospitalization = this.uzrokHospitalizacije.Text;
                mh.ReasonOfDeath = this.uzrokSmrti.Text;
                mh.DateEnd = this.datumotpusta.Content.ToString();

                MainWindow.medicalHistoryController.Update(mh);
                Bed bed = null;
                foreach (Bed b in MainWindow.bedController.GetAll()) {
                    if (b.Patient != null)
                    {
                        if (b.Patient.PersonalID.Equals(this.jmbg.Content.ToString()))
                        {
                            bed = b;
                            break;
                        }
                    }
                }

                bed.Patient = null;
                MainWindow.bedController.Update(bed);

 
                this.NavigationService.Navigate(new medicalHistoryReportsAll());
            }
        }
    }
}
