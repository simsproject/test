﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;
using Model.Rooms;
using pokusaj1000.medicalRecord.medicalReport;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for instructionToHospitalTreatment.xaml
    /// </summary>
    public partial class instructionToHospitalTreatment : Page
    {
        public static String Soba;
        public static String Krevet;
        public instructionToHospitalTreatment()
        {
            InitializeComponent();
            this.imeIprezime.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;
            this.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;
            this.datum.DisplayDate = DateTime.Now.Date;
            this.datum.BlackoutDates.AddDatesInPast();
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;

            this.soba.ItemsSource = MainWindow.patientRoomController.GetAll();
            this.soba.SelectedIndex = 0;
           

        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void nazadNaPisanjeIzvestaja(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalRecord.medicalReport.medicalReportNew());
        }

   

        private void soba_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.soba.SelectedItem != null)
            {
                this.krevet.ItemsSource = MainWindow.bedController.GetFreeBeds((this.soba.SelectedItem as PatientRoom));
            }          
        }

        private void potvrdaUputaNaBolLecenje(object sender, RoutedEventArgs e)
        {
            bool podaciOk = true;

            if (this.soba.Text.Equals(""))
            {
                Style style = this.FindResource("komboBox1") as Style;
                this.soba.Style = style;
                podaciOk = false;
            }
            else {
                Style style = this.FindResource("komboBox2") as Style;
                this.soba.Style = style;
            }

            if (this.krevet.Text.Equals(""))
            {
                Style style = this.FindResource("komboBox1") as Style;
                this.krevet.Style = style;
                podaciOk = false;
            }
            else
            {
                Style style = this.FindResource("komboBox2") as Style;
                this.krevet.Style = style;
            }

            if (this.uput.Text.Equals(""))
            {
                this.uput.BorderThickness = new Thickness(3);
                podaciOk = false;
            }
            else {
                this.uput.BorderThickness = new Thickness(1);
            }

            if (this.datum.Text.Equals(""))
            {
                this.datum.BorderThickness = new Thickness(3);
                podaciOk = false;
            }
            else
            {
                this.uput.BorderThickness = new Thickness(1);
            }


            if (podaciOk)
            {
                

                if (!this.uput.Text.Equals("") && !this.krevet.SelectedItem.ToString().Equals("") && !this.soba.SelectedItem.ToString().Equals(""))
                {

                    HospitalTreatment hospitalTreatment = new HospitalTreatment(this.datum.SelectedDate.Value,DateTime.Now,this.uput.Text,0,(this.soba.SelectedItem as PatientRoom),MainWindow.medicalRecord);
                    MainWindow.HospitalTreatmentController.Create(hospitalTreatment);
                    Bed bed = this.krevet.SelectedItem as Bed;
                    bed.Patient = MainWindow.patientController.GetByID(this.jmbg.Content.ToString());
                    MainWindow.bedController.Update(bed);

                    this.NavigationService.Navigate(new medicalReportNew());
                }
            }
            
        }

        private void soba_KeyUp(object sender, KeyEventArgs e)
        {
           // var filtrered = MainWindow.rooms.Where(Soba => (Soba.naziv.ToUpper().Contains(soba.Text.ToUpper())));
           // this.soba.ItemsSource = filtrered;

        }

        private void krevet_KeyUp(object sender, KeyEventArgs e)
        {
           // var filtrered = MainWindow.beds.Where(Soba => (Soba.broj.ToUpper().Contains(krevet.Text.ToUpper())));
           // this.krevet.ItemsSource = filtrered;
        }


    }
}
