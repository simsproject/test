﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Controller;
using Model.Rooms;
using pokusaj1000.modelTest;
using Repository;
using Service;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {

      
        public static AllergensController allergensController = AllergensController.GetInstance();
        public static MedicalRecordController medicalRecordController = MedicalRecordController.GetInstance();
        public static BedController bedController = BedController.GetInstance();
        public static PatientRoomController patientRoomController = PatientRoomController.GetInstance();
        public static MedicalHistoryController medicalHistoryController = MedicalHistoryController.GetInstance();
        public static CourseOfDieaseController courseOfDieaseController = CourseOfDieaseController.GetInstance();
        public static DoctorReportController doctorReportController = DoctorReportController.GetInstance();
        public static DoctorController doctorController = DoctorController.GetInstance();
        public static CityController cityController = CityController.GetInstance();
        public static AdressController adressController = AdressController.GetInstance();
        public static AppointmentController appointmentController = AppointmentController.GetInstance();
        public static DrugController drugController = DrugController.GetInstance();
        public static PatientController patientController = PatientController.GetInstance();
        public static RecipeController recipeController = RecipeController.GetInstance();
        public static QuestionnaireController questionnaireController = QuestionnaireController.GetInstance();
        public static QuestionController questionController = QuestionController.GetInstance();
        public static UserController userController = UserController.GetInstance();
        public static HospitalTreatmentController HospitalTreatmentController = HospitalTreatmentController.GetInstance();
        public static NotificationController notificationController = NotificationController.GetInstance();
        public static FeedbackController feedbackController = FeedbackController.GetInstance();
        public static WorkingPeriodController workingPeriodController = WorkingPeriodController.GetInstance();
        public static WorkingTimeController workingTimeController = WorkingTimeController.GetInstance();
        public static SpecialistTreatmentController specialistTreatmentController = SpecialistTreatmentController.GetInstance();



        public static long indexAdrese;
        public static long indexGrada = 0;
        public static Model.Users.Doctor doctor;
        public static Model.MedicalRecords.MedicalRecord medicalRecord;
        public static Model.MedicalRecords.MedicalHistory medicalHistory;
        public static Model.Users.Patient patient;



        public static String trenutnoVreme;
        public static Model.Appointment.AppointmentType vrstaPregleda;
        public static int idIstorijeBolseti;

        public static profile.profilePage profilePage = new profile.profilePage();
        public static homePage homePage;
        public static notificationPage notificationPage;
        public static messagePage messagePage;


        public static drugs.drugDescription drugDescription = new drugs.drugDescription();
        public static medicalRecord.medicalRecordOfPatient medicalRecordOfPatient = new medicalRecord.medicalRecordOfPatient();



        public MainWindow()
        {
            InitializeComponent();
            Main.NavigationService.Navigate(new logInPage());
        }


    }



}
