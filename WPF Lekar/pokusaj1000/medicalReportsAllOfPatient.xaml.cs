﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for medicalInstructions.xaml
    /// </summary>
    public partial class medicalReportsAllOfPatient : Page
    {

        public medicalReportsAllOfPatient()
        {
            InitializeComponent();
           /* List<DoctorReport> medicalReports = new List<DoctorReport>();
            foreach (DoctorReport mr in MainWindow.doctorReportController.GetAll()) {
                if (mr.MedicalRecord.SerialID.ToString().Equals(MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content.ToString())) {
                    medicalReports.Add(mr);                  
                }
            }*/
            this.brZk.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
            dataGridPregledi.ItemsSource = MainWindow.doctorReportController.GetAllDoctorReportForPatient(MainWindow.medicalRecord);

        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void dataGridPregledi_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DoctorReport doctorReport = this.dataGridPregledi.SelectedItem as DoctorReport;
            medicalRecord.medicalReport.medicalReportOne medicalReportOne = new medicalRecord.medicalReport.medicalReportOne();

            medicalReportOne.anamneza.Text = doctorReport.Anamnesis;
            medicalReportOne.dijagnoza.Text = doctorReport.DiagnosisAndTeraphy;
            medicalReportOne.nalazImisljenje.Text = doctorReport.DoctorOpinion;
            medicalReportOne.brZK.Content = doctorReport.MedicalRecord.SerialID.ToString();
            medicalReportOne.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;
            medicalReportOne.imeIprezime.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;


            this.NavigationService.Navigate(medicalReportOne);
        }

        private void nazadNaZdravstveniKarton(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.medicalRecordOfPatient);
        }
    }

}
