﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Users;
using Model.Appointment;

using pokusaj1000.medicalRecord;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for searchPatient.xaml
    /// </summary>
    public partial class searchPatient : Page
    {
        public searchPatient()
        {
            InitializeComponent();
            List<Appointment> appointments = MainWindow.appointmentController.GetAll().ToList();
            this.DataGridPacijenti.ItemsSource = appointments.Where(Appointment =>(Appointment.Doctor.PersonalID.Equals(MainWindow.doctor.PersonalID)));
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }


        private void generisanjeIzvestaja(object sender, RoutedEventArgs e)
        {
            if ((this.DataGridPacijenti.SelectedItem as Patient) != null)
            {

                genericPDFfile pDFfile = new genericPDFfile();
                Patient patient = this.DataGridPacijenti.SelectedItem as Patient;

                pDFfile.brZK.Content = patient.MedicalRecord.SerialID.ToString();
                pDFfile.imeIprezime.Content = patient.FirstName + " " + patient.LastName;
                pDFfile.jmbg.Content = patient.PersonalID;

                this.NavigationService.Navigate(pDFfile);
            }
            else {
                this.generisanjePoruka.Visibility = Visibility.Visible;
            }
        }

        private void medicalRecord(object sender, RoutedEventArgs e)
        {
            Appointment appointment = this.DataGridPacijenti.SelectedItem as Appointment;
            MainWindow.medicalRecord = appointment.Patient.MedicalRecord;
            MainWindow.medicalRecordOfPatient.jmbg.Content = appointment.Patient.PersonalID;
            MainWindow.medicalRecordOfPatient.imeIprezime.Content = appointment.Patient.FirstName + " " + appointment.Patient.LastName;
            MainWindow.medicalRecordOfPatient.mesto.Content = appointment.Patient.Address.City.Name;
            MainWindow.medicalRecordOfPatient.ulica.Content = appointment.Patient.Address.Name + " " + appointment.Patient.Address.PostalCode;
            MainWindow.medicalRecordOfPatient.datum.Content = appointment.Patient.DateOfBirth.ToString("dd.MM.yyyy.");
            MainWindow.medicalRecordOfPatient.telefon.Content = appointment.Patient.PhoneNumber;
            MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content = appointment.Patient.MedicalRecord.SerialID.ToString();

            if (appointment.Patient.Gender.ToString().Equals("Female"))
            {
                MainWindow.medicalRecordOfPatient.Zenski.IsChecked = true;
                MainWindow.medicalRecordOfPatient.Muski.IsChecked = false;
            }
            else {
                MainWindow.medicalRecordOfPatient.Zenski.IsChecked = false;
                MainWindow.medicalRecordOfPatient.Muski.IsChecked = true;
            }

            MainWindow.medicalRecordOfPatient.nazad.Visibility = Visibility.Visible;
            MainWindow.medicalRecordOfPatient.noviIzvestaj.Visibility = Visibility.Hidden;

            this.NavigationService.Navigate(MainWindow.medicalRecordOfPatient);
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
           /* List<Appointment> appointments = MainWindow.appointmentController.GetAll().ToList();
            var filtrered = appointments.Where(Appointment => (Appointment.Doctor.PersonalID.Equals(MainWindow.doctor.PersonalID))).ToList().Where(Appointment => (Appointment.Patient.FirstName.ToUpper().Contains(search.Text.ToUpper()) || Appointment.Patient.LastName.ToUpper().Contains(search.Text.ToUpper()) || Appointment.Patient.MedicalRecord.SerialID.ToString().ToUpper().Contains(search.Text.ToUpper())));
            this.DataGridPacijenti.ItemsSource = filtrered;*/
        }
    }
}
