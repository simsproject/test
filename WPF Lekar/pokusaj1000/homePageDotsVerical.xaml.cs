﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using pokusaj1000.modelTest;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for homePageDotsVerical.xaml
    /// </summary>
    public partial class homePageDotsVerical : Page
    {
        public homePageDotsVerical()
        {
            InitializeComponent();
            dataGridTermini.ItemsSource = MainWindow.homePage.dataGridTermini.ItemsSource;
        }


        private void logOut(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new logInPage());
        }

        private void feedback(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new feedbackPage());
        }

        private void nazadNaHomePage(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void tutorijal(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new tutorialPage());
        }
    }
}
