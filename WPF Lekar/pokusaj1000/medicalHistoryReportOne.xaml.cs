﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for medicalHistoryReportOne.xaml
    /// </summary>
    public partial class medicalHistoryReportOne : Page
    {
        public medicalHistoryReportOne()
        {
            InitializeComponent();

            foreach (MedicalHistory mh in MainWindow.medicalHistoryController.GetAllMedicalHistoriesForPatient(MainWindow.medicalRecord))
            {
                if (mh.SerialID.ToString().Equals(MainWindow.idIstorijeBolseti.ToString()))
                {
                    this.datumPrijema.Content = mh.DateBegin.ToString("dd.MM.yyyy.");
                    this.datumotpusta.Content = mh.DateEnd;
                    this.uzrokHospitalizacije.Text = mh.ReasonOfHospitalization;
                    this.uzrokSmrti.Text = mh.ReasonOfDeath;
                    this.razlogOtpusta.Content = mh.ReasonOfRelase;
                    break;
                }
            }
            this.imeIprezime.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;
            this.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void tokBolesti(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalHistoryTideOfDisease());
        }

        private void nazadnaListuIstorijaBolesti(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalHistoryReportsAll());
        }
    }
}
