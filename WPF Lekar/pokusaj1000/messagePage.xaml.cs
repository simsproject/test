﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.Other;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for messagePage.xaml
    /// </summary>
    public partial class messagePage : Page
    {
        public messagePage()
        {
            InitializeComponent();
            List<Questionnaire> questionnaires = MainWindow.questionnaireController.GetQuestionnairesForDoctor(MainWindow.doctor);
            List<Question> questions = MainWindow.questionController.GetAll().ToList();
            double ocenaStrucnosti = 0;
            double ocenaLjubaznosti = 0;

            int i = 0;
            int j = 0;
            foreach (Questionnaire questionnaire in questionnaires) {
                foreach (Question question in questions) {
                    if (question.Questionnaire.SerialID == questionnaire.SerialID) {
                        if (question.Questions.Equals("1"))
                        {
                            ocenaStrucnosti += double.Parse(question.Answer);
                            j++;
                        }
                        else {
                            ocenaLjubaznosti += double.Parse(question.Answer);
                            i++;
                        }
                    }
                }
            }
            this.ljubaznost.Content = ocenaLjubaznosti / (double)i;
            this.strucnost.Content = ocenaStrucnosti / (double)j;

            this.dataGridAnketa.ItemsSource = questionnaires;
        }
        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }
    }
}
