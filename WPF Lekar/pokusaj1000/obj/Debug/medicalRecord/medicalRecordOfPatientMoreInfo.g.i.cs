﻿#pragma checksum "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "6B82CF932A6F02ED3B9EFA981B111DBD5063028FB1F7350558C5DDDB679D315A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using pokusaj1000;


namespace pokusaj1000.medicalRecord {
    
    
    /// <summary>
    /// medicalRecordOfPatientMoreInfo
    /// </summary>
    public partial class medicalRecordOfPatientMoreInfo : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 61 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button nazad;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label brZK;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label jmbg;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label imeIprezime;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label datum;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label mesto;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ulica;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label telefon;
        
        #line default
        #line hidden
        
        
        #line 127 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Zenski;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Muski;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button noviIzvestaj;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/pokusaj1000;component/medicalrecord/medicalrecordofpatientmoreinfo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.nazad = ((System.Windows.Controls.Button)(target));
            return;
            case 2:
            
            #line 65 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.zdravstveniKarton);
            
            #line default
            #line hidden
            return;
            case 3:
            this.brZK = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.jmbg = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.imeIprezime = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.datum = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.mesto = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.ulica = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.telefon = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.Zenski = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 11:
            this.Muski = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 12:
            
            #line 139 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
            ((System.Windows.Controls.StackPanel)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.listaSvihIzvestaja);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 146 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
            ((System.Windows.Controls.StackPanel)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.uvidUIstorijuBolesti);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 153 "..\..\..\medicalRecord\medicalRecordOfPatientMoreInfo.xaml"
            ((System.Windows.Controls.StackPanel)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.alergeni);
            
            #line default
            #line hidden
            return;
            case 15:
            this.noviIzvestaj = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

