﻿#pragma checksum "..\..\profilePage.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "781F023D2ED3B0E3D2D5165DE8E0B1C5B2E4A15B8365A8E5ECDFFA21A57E706B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using pokusaj1000;


namespace pokusaj1000 {
    
    
    /// <summary>
    /// profilePage
    /// </summary>
    public partial class profilePage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 75 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ime;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label prezime;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label JMBG;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label email;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label adresa;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label kontakt_telefon;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox biografija;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label korisnicko_ime;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\profilePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lozinka;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/pokusaj1000;component/profilepage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\profilePage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 30 "..\..\profilePage.xaml"
            ((MaterialDesignThemes.Wpf.PackIcon)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.home);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 37 "..\..\profilePage.xaml"
            ((MaterialDesignThemes.Wpf.PackIcon)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.profile);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 44 "..\..\profilePage.xaml"
            ((MaterialDesignThemes.Wpf.PackIcon)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.message);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 52 "..\..\profilePage.xaml"
            ((MaterialDesignThemes.Wpf.PackIcon)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.notification);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 64 "..\..\profilePage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.licni_podaci);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ime = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.prezime = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.JMBG = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.email = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.adresa = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.kontakt_telefon = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            
            #line 88 "..\..\profilePage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.profilBiografija);
            
            #line default
            #line hidden
            return;
            case 13:
            this.biografija = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            
            #line 107 "..\..\profilePage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.profil_korIme_lozinka);
            
            #line default
            #line hidden
            return;
            case 15:
            this.korisnicko_ime = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.lozinka = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

