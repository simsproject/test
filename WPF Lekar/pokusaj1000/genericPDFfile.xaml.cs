﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using iTextSharp.text;
using iTextSharp.text.pdf;
using pokusaj1000.modelTest;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for genericPDFfile.xaml
    /// </summary>
    public partial class genericPDFfile : Page
    {
        public genericPDFfile()
        {
            InitializeComponent();
        }



        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.messagePage);
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.notificationPage);
        }

        private void generisiIzvestaj(object sender, RoutedEventArgs e)
        {
            bool generisi = true;
            if (this.doVreme.Text.Equals("") || this.odVreme.Text.Equals(""))
            {
                this.greska.Visibility = Visibility.Visible;
                generisi = false;
            }
            else {
                this.greska.Visibility = Visibility.Hidden;
                generisi = true;
            }

            if (generisi)
            {
                try
                {
                    DataTable dtbl = MakeDataTable();
                    ExportDataTableToPdf(dtbl, @"C:\Users\NEMANJA\Desktop\HCI\project\lekar\pokusaj1000\test.pdf", "Izvestaj o pregledu stanju pacijenta za vremenski period od " + this.odVreme.SelectedDate.Value.ToString("MM/dd/yyyy") + " do " + this.doVreme.SelectedDate.Value.ToString("MM/dd/yyyy"));
                    this.NavigationService.Navigate(MainWindow.homePage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error Message");
                }
            }

        }


        DataTable MakeDataTable()
        {
            DataTable friend = new DataTable();


            friend.Columns.Add("Datum pregleda");
            friend.Columns.Add("Vrsta pregleda");
            friend.Columns.Add("Nalaz i misljenje Lekara");

            CultureInfo provider = CultureInfo.InvariantCulture;
           // DateTime datum;
           /* foreach (MedicalReport rp in MainWindow.medicalReports) {
                if (rp.brZK.ToString().Equals(this.brZK.Content.ToString())) {
                     datum = DateTime.ParseExact(rp.vreme, "dd/MM/yyyy", provider);
                        if (datum >= this.odVreme.SelectedDate.Value && datum <= this.doVreme.SelectedDate.Value) {
                        friend.Rows.Add(rp.vreme, rp.vrsta, rp.nalazImisljenje);
                        }
                }
            }*/



            return friend;
        }


        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font fntHead = new Font(bfntHead, 16, 1, BaseColor.BLACK);
            Paragraph prgHeading = new Paragraph();
            prgHeading.Alignment = Element.ALIGN_CENTER;
            prgHeading.Add(new Chunk(strHeader.ToUpper(), fntHead));
            document.Add(prgHeading);

            Paragraph prgAuthor = new Paragraph();
            BaseFont btnAuthor = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font fntAuthor = new Font(btnAuthor,14, 2, BaseColor.DARK_GRAY);
            prgAuthor.Alignment = Element.ALIGN_RIGHT;
            prgAuthor.Add(new Chunk("Izabrani lekar : " + MainWindow.profilePage.prezime.Content.ToString() + " " + MainWindow.profilePage.ime.Content.ToString(), fntAuthor));
            prgAuthor.Add(new Chunk("\nPacijent : " + this.imeIprezime.Content.ToString(), fntAuthor));
            prgAuthor.Add(new Chunk("\nDatum izdavanja izvestaja : " + DateTime.Now.ToString("MM/dd/yyyy  hh:mm:ss"), fntAuthor));

            document.Add(prgAuthor);

            Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            document.Add(p);

            document.Add(new Chunk("\n", fntHead));

            PdfPTable table = new PdfPTable(dtblTable.Columns.Count);

            BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Font fntColumnHeader = new Font(btnColumnHeader, 10, 1, BaseColor.WHITE);
            for (int i = 0; i < dtblTable.Columns.Count; i++)
            {
                PdfPCell cell = new PdfPCell();
                cell.BackgroundColor = BaseColor.GRAY;
                cell.AddElement(new Chunk(dtblTable.Columns[i].ColumnName.ToUpper(), fntColumnHeader));
                table.AddCell(cell);
            }

            for (int i = 0; i < dtblTable.Rows.Count; i++)
            {
                for (int j = 0; j < dtblTable.Columns.Count; j++)
                {
                    table.AddCell(dtblTable.Rows[i][j].ToString());
                }
            }

            document.Add(table);
            document.Close();
            writer.Close();
            fs.Close();
        }


        private void nazadNaPretragu(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new searchPatient());
        }
    }
}
