﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Model.MedicalRecords;
using pokusaj1000.medicalRecord.medicalReport;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for rewriteRecipe.xaml
    /// </summary>
    public partial class rewriteRecipe : Page
    {
        public rewriteRecipe()
        {
            InitializeComponent();
            this.brZK.Content = MainWindow.medicalRecordOfPatient.brZdravstvenogKartona.Content;
            this.jmbg.Content = MainWindow.medicalRecordOfPatient.jmbg.Content;
            this.imeIprezimePacijenta.Content = MainWindow.medicalRecordOfPatient.imeIprezime.Content;
            this.datum.Content = DateTime.Now.ToString("dd.MM.yyyy.");
            this.imeIprezimeDoktora.Content = MainWindow.profilePage.ime.Content.ToString() + " " + MainWindow.profilePage.ime.Content.ToString();

        }

        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new notificationPage());
        }

        private void nazadNaPisanjeIzvestaja(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new medicalRecord.medicalReport.medicalReportNew());
        }

        private void potvrdaPrepisivanjaRecepta(object sender, RoutedEventArgs e)
        {
            bool potvrdi = true;
            if (this.sifraLeka.Text.Equals(""))
            {
                potvrdi = false;
                this.sifraLeka.BorderThickness = new Thickness(3);
            }
            else
            {
                this.sifraLeka.BorderThickness = new Thickness(1);
            }

            if (this.recept.Text.Equals(""))
            {
                potvrdi = false;
                this.recept.BorderThickness = new Thickness(3);
            }
            else {
                this.recept.BorderThickness = new Thickness(1);
            }
            if (this.kolicina.Text.Equals(""))
            {
                potvrdi = false;
                this.kolicina.BorderThickness = new Thickness(3);
            }
            else {
                if (this.kolicina.Text.Length > 2)
                {
                    potvrdi = false;
                    this.kolicina.BorderThickness = new Thickness(3);
                }
                else {
                    this.kolicina.BorderThickness = new Thickness(1);
                }
            }

            if (potvrdi) {
                MainWindow.recipeController.Create(new Recipe(DateTime.Now.Date,this.kolicina.Text,int.Parse(this.sifraLeka.Text),this.recept.Text,1,MainWindow.doctor,MainWindow.medicalRecord));
                this.NavigationService.Navigate(new medicalReportNew());
            }
        }



        private void kolicina_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Regex regex = new Regex(@"[\d]");

            if (!regex.IsMatch( e.Key.ToString()) || this.kolicina.Text.Length == 1)
            {
                e.Handled = true;
            }

        }
    }
}
