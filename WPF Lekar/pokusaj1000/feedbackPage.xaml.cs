﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace pokusaj1000
{
    /// <summary>
    /// Interaction logic for feedbackPage.xaml
    /// </summary>
    public partial class feedbackPage : Page
    {
        public feedbackPage()
        {
            InitializeComponent();
        }



        private void home(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }

        private void profile(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.profilePage);
        }

        private void message(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new messagePage());
        }

        private void notification(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.notificationPage);
        }

        private void potvrdi(object sender, RoutedEventArgs e)
        {
            bool cekirano = false;
            int ocena = -1;
            if (this.i.IsChecked.Value) {
                cekirano = true;
                ocena = 1;
            }
            if (this.ii.IsChecked.Value)
            {
                cekirano = true;
                ocena = 2;
            }
            if (this.iiii.IsChecked.Value)
            {
                cekirano = true;
                ocena = 3;
            }
            if (this.iiii.IsChecked.Value)
            {
                cekirano = true;
                ocena = 4;
            }
            if (this.iiiii.IsChecked.Value)
            {
                cekirano = true;
                ocena = 5;
            }


            if (cekirano)
            {
                MainWindow.feedbackController.Create(new Model.Users.Feedback(ocena,this.komentar.Text,MainWindow.doctor));
                this.NavigationService.Navigate(MainWindow.homePage);
            }
            else {
                this.greska.Visibility = Visibility.Visible;
            }
        }

        private void odustani(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(MainWindow.homePage);
        }
    }
}
