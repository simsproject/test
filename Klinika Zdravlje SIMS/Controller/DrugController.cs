// File:    DrugController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:10:54 PM
// Purpose: Definition of Class DrugController

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Drugs;
using Model.Users;
using Service;
using Service.DrugsPackageService;

namespace Controller
{
	public class DrugController : Controller.DrugsPackageController.IDrugController
	{
		private static DrugController instance;
		public IDrugService iDrugService;

		public static DrugController GetInstance()
		{
			if (instance == null)
			{
				instance = new DrugController();
			}
			return instance;
		}

		public DrugController()
		{
			this.iDrugService = DrugService.GetInstance();
		}

		public List<Drug> GetAllWaitingDrugs()
			=> iDrugService.GetAllWaitingDrugs();

		public List<Drug> GetAllValidDrugs()
			=> iDrugService.GetAllValidDrugs();

		public void UpdateWaitingDrugs(List<Drug> listOfWaitingDrugs)
			=> iDrugService.UpdateWaitingDrugs(listOfWaitingDrugs);

		public IEnumerable<Drug> GetAll()
			=> iDrugService.GetAll();

        public void Update(Drug entity)
            => iDrugService.Update(entity);

        public void Delete(Drug entiity)
            => iDrugService.Delete(entiity);

        public Drug Create(Drug entity)
            => iDrugService.Create(entity);

        public Drug GetByID(string id)
		    => iDrugService.GetByID(id);

		public List<Drug> GetAllSentValidationDrugs()
			=> iDrugService.GetAllSentValidationDrugs();

		public List<Drug> GetAllSentValidationDrugsForDoctor(Doctor doctor)
			=> iDrugService.GetAllSentValidationDrugsForDoctor(doctor);
	}
}