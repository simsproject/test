// File:    WorkingTimeController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:30:42 PM
// Purpose: Definition of Class WorkingTimeController

using System;
using System.Collections.Generic;
using Controller.UserPackageController;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class WorkingTimeController : IWorkingTimeController
   {
      private static WorkingTimeController instance;
		public IWorkingTimeService iWorkingTimeService;

		public static WorkingTimeController GetInstance()
      {
			if (instance == null)
			{
				instance = new WorkingTimeController();
			}
			return instance;
		}

		public WorkingTimeController()
		{
			this.iWorkingTimeService = WorkingTimeService.GetInstance();
		}


        public WorkingTime GetWorkingTimeForDoctorByDate(Doctor doctor, DateTime date)
        => iWorkingTimeService.GetWorkingTimeForDoctorByDate(doctor, date);

  
      public IEnumerable<WorkingTime> GetAll()
		=> iWorkingTimeService.GetAll();

	    public void Update(WorkingTime entity)
       => iWorkingTimeService.Update(entity);

        public void Delete(WorkingTime entiity)
       => iWorkingTimeService.Delete(entiity);

        public WorkingTime Create(WorkingTime entity)
      => iWorkingTimeService.Create(entity);

        public WorkingTime GetByID(long id)
		=> iWorkingTimeService.GetByID(id);


	}
}