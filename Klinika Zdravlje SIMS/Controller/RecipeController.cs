// File:    RecipeController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:13:26 PM
// Purpose: Definition of Class RecipeController

using System;
using System.Collections.Generic;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
   public class RecipeController : Controller.MedicalRecordPackageController.IRecipeController
   {
      private static RecipeController instance;

        public RecipeController()
        {
            this.iRecipeService = RecipeService.GetInstance();
        }

        public static RecipeController GetInstance()
      {
            if (instance == null)
            {
                instance = new RecipeController();
            }
            return instance;
      }

        public List<Recipe> GetAllRecipesForPatient(MedicalRecord medicalRecord)
        => iRecipeService.GetAllRecipesForPatient(medicalRecord);

        public Recipe Create(Recipe entity)
        => iRecipeService.Create(entity);

        public Recipe GetByID(long id)
        => iRecipeService.GetByID(id);

        public IEnumerable<Recipe> GetAll()
        => iRecipeService.GetAll();

        public Service.MedicalRecordPackageService.IRecipeService iRecipeService;
   
   }
}