// File:    QuestionController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:23:29 PM
// Purpose: Definition of Class QuestionController

using System;
using System.Collections.Generic;
using Controller.OtherPackageController;
using Model.Other;
using Service;
using Service.OtherPackageService;

namespace Controller
{
   public class QuestionController : Controller.OtherPackageController.IQuestionController
   {
      private static QuestionController instance;
      
      public static QuestionController GetInstance()
      {
            if (instance == null)
            {
                instance = new QuestionController();
            }

            return instance;
      }

        public IEnumerable<Question> GetAll()
        => iQuestionService.GetAll();

        public Question Create(Question entity)
        => iQuestionService.Create(entity);

        public Question GetByID(long id)
        => iQuestionService.GetByID(id);

        public QuestionController()
        {
            this.iQuestionService = QuestionService.GetInstance();
        }

        public Service.OtherPackageService.IQuestionService iQuestionService;
   
   }
}