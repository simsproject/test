// File:    MedicalRoomController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:28:57 PM
// Purpose: Definition of Class MedicalRoomController

using System;
using System.Collections.Generic;
using Model.MedicalRecords;
using Model.Rooms;
using Model.Users;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class MedicalRoomController : Controller.RoomsPackageController.IMedicalRoomController
   {
      private static MedicalRoomController instance;
      private readonly IMedicalRoomService iMedicalRoomService;

        public MedicalRoomController()
        {
            this.iMedicalRoomService = MedicalRoomService.GetInstance();
        }

        public static MedicalRoomController GetInstance()
        {
            if (instance == null) {
                instance = new MedicalRoomController();
            }
            return instance;
        }

        public IEnumerable<MedicalRoom> GetAll()
        => iMedicalRoomService.GetAll();

        public void Update(MedicalRoom entity)
        => iMedicalRoomService.Update(entity);

        public void Delete(MedicalRoom entiity)
        => iMedicalRoomService.Delete(entiity);

        public MedicalRoom Create(MedicalRoom entity)
        => iMedicalRoomService.Create(entity);

        public MedicalRoom GetByID(string id)
        => iMedicalRoomService.GetByID(id);
    }
}