// File:    DoctorReportController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:13:25 PM
// Purpose: Definition of Class DoctorReportController

using System;
using System.Collections.Generic;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
   public class DoctorReportController : Controller.MedicalRecordPackageController.IDoctorReportController
   {
      private static DoctorReportController instance;
        public IDoctorReportService iDoctorReportService;

        public DoctorReportController()
        {
            this.iDoctorReportService = DoctorReportService.GetInstance();
        }

        public static DoctorReportController GetInstance()
        {
            if (instance == null)
            {
                instance = new DoctorReportController();
            }
            return instance;
        }

        public List<DoctorReport> GetAllDoctorReportForPatient(MedicalRecord medicalRecord)
            => iDoctorReportService.GetAllDoctorReportForPatient(medicalRecord);

        public DoctorReport Create(DoctorReport entity)
            => iDoctorReportService.Create(entity);

        public DoctorReport GetByID(long id)
            => iDoctorReportService.GetByID(id);

        public IEnumerable<DoctorReport> GetAll()
            => iDoctorReportService.GetAll();

        
   
   }
}