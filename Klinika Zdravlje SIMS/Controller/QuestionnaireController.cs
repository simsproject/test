// File:    QuestionaireController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:23:28 PM
// Purpose: Definition of Class QuestionaireController

using System;
using System.Collections.Generic;
using Model.Other;
using Model.Users;
using Service;
using Service.OtherPackageService;

namespace Controller
{
   public class QuestionnaireController : Controller.OtherPackageController.IQuestionnaireController
   {
      private static QuestionnaireController instance;
      
      public static QuestionnaireController GetInstance()
      {
         if (instance == null)
            {
                instance = new QuestionnaireController();  
            }
            return instance;
      }

        public QuestionnaireController()
        {
            this.iQuestionnaireService = QuestionnaireService.GetInstance();
        }

        public List<Questionnaire> GetQuestionnairesForDoctor(Doctor doctor)
            => iQuestionnaireService.GetQuestionnairesForDoctor(doctor);

        public IEnumerable<Questionnaire> GetAll()
        => iQuestionnaireService.GetAll();

        public Questionnaire GetByID(long id)
        => iQuestionnaireService.GetByID(id);

        public Questionnaire Create(Questionnaire entity)
        => iQuestionnaireService.Create(entity);

        public Service.OtherPackageService.IQuestionnaireService iQuestionnaireService;
   
   }
}