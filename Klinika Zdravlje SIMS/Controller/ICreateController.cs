// File:    ICreateController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 12:54:43 AM
// Purpose: Definition of Interface ICreateController

using System;

namespace Controller
{
   public interface ICreateController<E>
   {
      E Create(E entity);
   
   }
}