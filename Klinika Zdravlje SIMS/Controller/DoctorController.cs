// File:    DoctorController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:32:09 PM
// Purpose: Definition of Class DoctorController

using System;
using System.Collections.Generic;
using Controller.UserPackageController;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class DoctorController : IDoctorController
   {
		  private static DoctorController instance;
		  public IDoctorService iDoctorService;

		  public DoctorController()
		  {
               this.iDoctorService = DoctorService.GetInstance();
          }

        public static DoctorController GetInstance()
        {
            if (instance == null) {
                instance = new DoctorController();
            }
            return instance;
        }

        public List<Doctor> GetDoctorByTitle(Title title)
        => iDoctorService.GetDoctorByTitle(title);

        public IEnumerable<Doctor> GetAll()
            => iDoctorService.GetAll();

		  public void Update(Doctor entity)
				=> iDoctorService.Update(entity);

		  public void Delete(Doctor entiity)
				=> iDoctorService.Delete(entiity);

	  	  public Doctor Create(Doctor entity)
				=> iDoctorService.Create(entity);

        public Doctor GetByID(string id)
            => iDoctorService.GetByID(id);

        public Dictionary<Doctor, WorkingTime> FilterDoctorsByWorkingTime(DateTime date)
            => iDoctorService.FilterDoctorsByWorkingTime(date);



    }
}