// File:    OfficeController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:28:58 PM
// Purpose: Definition of Class OfficeController

using System;
using System.Collections.Generic;
using Controller.RoomsPackageController;
using Model.Rooms;
using Model.Users;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class OfficeController : IOfficeController
   {
			private static OfficeController instance;
			public IOfficeService iOfficeService;


			public OfficeController()
			{
				this.iOfficeService = OfficeService.GetInstance();
			}

			public static OfficeController GetInstance()
			{
				if (instance == null)
				{
					instance = new OfficeController();
				}
				return instance;
			}

		public IEnumerable<Office> GetAll()
		=> iOfficeService.GetAll();

        public void Update(Office entity)
        => iOfficeService.Update(entity);

        public void Delete(Office entiity)
        => iOfficeService.Delete(entiity);

		public Office Create(Office entity)
		=> iOfficeService.Create(entity);

		public Office GetByID(string id)
		=> iOfficeService.GetByID(id);

		
   
   }
}