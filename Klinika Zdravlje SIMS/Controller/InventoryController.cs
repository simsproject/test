// File:    InventoryController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:28:55 PM
// Purpose: Definition of Class InventoryController

using System;
using System.Collections.Generic;
using Model.Rooms;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class InventoryController : Controller.RoomsPackageController.IInventoryController
   {
      private static InventoryController instance;

        public IInventoryService iInventoryService;

        public static InventoryController GetInstance()
        {
            if (instance == null)
            {
                instance = new InventoryController();
            }
            return instance;
        }

        public InventoryController()
        {
            this.iInventoryService = InventoryService.GetInstance();
        }


        public IEnumerable<Inventory> GetAll()
            => iInventoryService.GetAll();

        public void Update(Inventory entity)
             => iInventoryService.Update(entity);

        public void Delete(Inventory entiity)
            => iInventoryService.Delete(entiity);

        public Inventory Create(Inventory entity)
            => iInventoryService.Create(entity);

        public Inventory GetByID(long id)
            => iInventoryService.GetByID(id);



    }
}