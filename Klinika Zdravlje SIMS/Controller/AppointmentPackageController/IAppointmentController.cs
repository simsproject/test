// File:    IAppointmentController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 1:26:42 AM
// Purpose: Definition of Interface IAppointmentController

using Model.Appointment;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.AppointmentPackageController
{
   public interface IAppointmentController : Controller.IController<Appointment>, Controller.IGetByIDController<Appointment, long>
   {      
      List<Appointment> GetAllScheduleAppointmenstForDoctor(Doctor doctor);
            
      List<Appointment> GetAppointmentByDate(DateTime date);

      List<Appointment> GetAppointmentsForPatient(Patient patient);

      List<Appointment> GetAppointmentsForPatientByDate(Patient patient, DateTime dateTime);

      List<Appointment> GetAppointmentsForDoctorByDate(Doctor doctor, DateTime dateTime);

    }
}