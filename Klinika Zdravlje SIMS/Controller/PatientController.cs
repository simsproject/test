// File:    PatientController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:33:39 PM
// Purpose: Definition of Class PatientController

using System;
using System.Collections.Generic;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class PatientController : Controller.UserPackageController.IPatientService
   {
      private static PatientController instance;
      
      public static PatientController GetInstance()
      {
            if (instance == null)
            {
                instance = new PatientController();
            }
            return instance;
      }

        public PatientController()
        {
            this.iPatientService = PatientService.GetInstance();
        }


        public IEnumerable<Patient> GetAll()
        => iPatientService.GetAll();
        

        public void Update(Patient entity)
        => iPatientService.Update(entity);

        public void Delete(Patient entiity)
        => iPatientService.Delete(entiity);

        public Patient Create(Patient entity)
        => iPatientService.Create(entity);

        public Patient GetByID(string id)
        =>iPatientService.GetByID(id);
        

        public Service.UserPackageService.IPatientService iPatientService;
   
   }
}