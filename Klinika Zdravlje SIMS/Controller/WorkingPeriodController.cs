// File:    WorkingPeriodController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:32:11 PM
// Purpose: Definition of Class WorkingPeriodController

using System;
using System.Collections.Generic;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class WorkingPeriodController : Controller.UserPackageController.IWorkingPeriodController
   {
        private static WorkingPeriodController instance;
	    public IWorkingPeriodService iWorkingPeriodService;

		public static WorkingPeriodController GetInstance()
        {
			if (instance == null)
			{
				instance = new WorkingPeriodController();
			}
			return instance;
		}

		public WorkingPeriodController()
		{
			this.iWorkingPeriodService = WorkingPeriodService.GetInstance();
		}


      public IEnumerable<WorkingPeriod> GetAll()
			=> iWorkingPeriodService.GetAll();

	 public void Update(WorkingPeriod entity)
       => iWorkingPeriodService.Update(entity);

        public void Delete(WorkingPeriod entiity)
       => iWorkingPeriodService.Delete(entiity);

        public WorkingPeriod Create(WorkingPeriod entity)
        => iWorkingPeriodService.Create(entity);

      public WorkingPeriod GetByID(long id)
		=> iWorkingPeriodService.GetByID(id);

        public WorkingPeriod WorkingPeriodForDoctorByDate(Doctor doctor, DateTime date)
            => iWorkingPeriodService.WorkingPeriodForDoctorByDate(doctor, date);

        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        => iWorkingPeriodService.EachDay(from, thru);
    }
}