// File:    IUpdateController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 12:54:45 AM
// Purpose: Definition of Interface IUpdateController

using System;

namespace Controller
{
   public interface IUpdateController<E>
   {
      void Update(E entity);
   
   }
}