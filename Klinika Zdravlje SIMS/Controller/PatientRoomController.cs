// File:    PatientRoomController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:28:59 PM
// Purpose: Definition of Class PatientRoomController

using System;
using System.Collections.Generic;
using Model.Rooms;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class PatientRoomController : Controller.RoomsPackageController.IPatientRoomController
   {
        private static PatientRoomController instance;
      
        public static PatientRoomController GetInstance()
        {
            if (instance == null)
            {
                instance = new PatientRoomController();
            }
            return instance;
        }

        public PatientRoomController()
        {
            this.iPatientRoomService = PatientRoomService.GetInstance();
        }

        public IEnumerable<PatientRoom> GetAll()
        => iPatientRoomService.GetAll();


        public void Update(PatientRoom entity)
        => iPatientRoomService.Update(entity);

        public void Delete(PatientRoom entiity)
        => iPatientRoomService.Delete(entiity);

        public PatientRoom Create(PatientRoom entity)
        => iPatientRoomService.Create(entity);

        public PatientRoom GetByID(string id)
        => iPatientRoomService.GetByID(id);

        public Service.RoomsPackageService.IPatientRoomService iPatientRoomService;
   
   }
}