// File:    SecretaryController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:33:40 PM
// Purpose: Definition of Class SecretaryController

using System;
using System.Collections.Generic;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class SecretaryController : Controller.UserPackageController.ISecretaryController
   {
		private static SecretaryController instance;
		public ISecretaryService iSecretaryService;

		public SecretaryController()
		{
			this.iSecretaryService = SecretaryService.GetInstance();
		}

		public static SecretaryController GetInstance()
		{
			if (instance == null)
			{
				instance = new SecretaryController();
			}
			return instance;
		}

		public IEnumerable<Secretary> GetAll()
		=> iSecretaryService.GetAll();

		public void Update(Secretary entity)
		  => iSecretaryService.Update(entity);

		public void Delete(Secretary entiity)
		  => iSecretaryService.Delete(entiity);

		public Secretary Create(Secretary entity)
		  => iSecretaryService.Create(entity);

		public Secretary GetByID(string id)
		=> iSecretaryService.GetByID(id);

   }
}