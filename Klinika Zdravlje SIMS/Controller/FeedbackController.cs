// File:    FeedbackController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:29:01 PM
// Purpose: Definition of Class FeedbackController

using System;
using System.Collections.Generic;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class FeedbackController : Controller.UserPackageController.IFeedbackController
   {
        private static FeedbackController instance;
        public IFeedbackService iFeedbackService;

        public FeedbackController()
        {
            this.iFeedbackService = FeedbackService.GetInstance();
        }

        public static FeedbackController GetInstance()
      {
            if (instance == null) {
                instance = new FeedbackController();
            }
            return instance;
      }

        public IEnumerable<Feedback> GetAll()
            => iFeedbackService.GetAll();

        public Feedback Create(Feedback entity)
            => iFeedbackService.Create(entity);

        
   
   }
}