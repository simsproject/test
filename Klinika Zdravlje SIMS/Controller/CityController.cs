// File:    CityController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:32:05 PM
// Purpose: Definition of Class CityController

using System;
using System.Collections.Generic;
using Controller.UserPackageController;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class CityController : ICityController
   {
     public ICityService iCityService;
     private static CityController instance;

     public CityController()
     {
            iCityService = CityService.GetInstance();
     }

     public static CityController GetInstance()
     {
            if (instance == null)
            {
                instance = new CityController();
            }
            return instance;
     }

     public IEnumerable<City> GetAll()
			=> iCityService.GetAll();

    public City GetByID(long id) 
			=> iCityService.GetByID(id);




    }
}