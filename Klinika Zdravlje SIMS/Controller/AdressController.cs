// File:    AdressController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:32:03 PM
// Purpose: Definition of Class AdressController

using System;
using System.Collections.Generic;
using Controller.UserPackageController;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class AdressController : IAddressController
    {
        private static AdressController instance;
		  public IAddressService iAddressService;

		  public AdressController()
        {
            this.iAddressService = AddressService.GetInstance();
        }

        public static AdressController GetInstance()
        {
            if (instance == null)
            {
                instance = new AdressController();
            }
            return instance;
        }

        public IEnumerable<Address> GetAll()
            => iAddressService.GetAll();
        

        public Address GetByID(long id)
            => iAddressService.GetByID(id);
        



    }
}