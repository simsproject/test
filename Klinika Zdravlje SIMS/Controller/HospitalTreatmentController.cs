// File:    HospitalTreatmentController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:13:26 PM
// Purpose: Definition of Class HospitalTreatmentController

using System;
using System.Collections.Generic;
using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
   public class HospitalTreatmentController : Controller.MedicalRecordPackageController.IHospitalTreatmentController
   {
        private static HospitalTreatmentController instance;
        public IHospitalTreatmentService iHospitalTreatmentService;

        public HospitalTreatmentController()
        {
            this.iHospitalTreatmentService = HospitalTreatmentService.GetInstance();
        }

        public static HospitalTreatmentController GetInstance()
      {
            if (instance == null)
            {
                instance = new HospitalTreatmentController();
            }
            return instance;
        }

        public List<HospitalTreatment> GetAllHospitalTreatmentsForPatient(MedicalRecord medicalRecord)
        => iHospitalTreatmentService.GetAllHospitalTreatmentsForPatient(medicalRecord);

        public HospitalTreatment Create(HospitalTreatment entity)
             => iHospitalTreatmentService.Create(entity);

        public IEnumerable<HospitalTreatment> GetAll()
            => iHospitalTreatmentService.GetAll();

        public HospitalTreatment GetByID(long id)
            => iHospitalTreatmentService.GetByID(id);



    }
}