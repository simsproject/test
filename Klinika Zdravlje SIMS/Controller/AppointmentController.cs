// File:    AppointmentController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:10:53 PM
// Purpose: Definition of Class AppointmentController

using Controller.AppointmentPackageController;
using Model.Appointment;
using Model.Users;
using Service;
using Service.AppointmentPackageService;
using System;
using System.Collections.Generic;

namespace Controller
{
   public class AppointmentController : IAppointmentController
   {
        private static AppointmentController instance;
		  public IAppointmentService iAppointmentService;

			public static AppointmentController GetInstance()
			{
					if (instance == null)
					{
						 instance = new AppointmentController();
					}
					return instance;
			}

        public AppointmentController()
        {
            this.iAppointmentService = AppointmentService.GetInstance();
        }

        public List<Appointment> GetAllScheduleAppointmenstForDoctor(Doctor doctor)
            => iAppointmentService.GetAllScheduleAppointmenstForDoctor(doctor);

		public List<Appointment> GetAppointmentByDate(DateTime date)
		 => iAppointmentService.GetAppointmentByDate(date);

        public IEnumerable<Appointment> GetAll()
            => iAppointmentService.GetAll();


		  public void Update(Appointment entity)
				=> iAppointmentService.Update(entity);


		  public void Delete(Appointment entiity)
			   => iAppointmentService.Delete(entiity);


		  public Appointment Create(Appointment entity)
				=> iAppointmentService.Create(entity);


        public Appointment GetByID(long id) 
            => iAppointmentService.GetByID(id);



        public List<Appointment> GetAppointmentsForDoctorByDate(Doctor doctor, DateTime dateTime)
        => iAppointmentService.GetAppointmentsForDoctorByDate(doctor, dateTime);

        public List<Appointment> GetAppointmentsForPatient(Patient patient)
        => iAppointmentService.GetAppointmentsForPatient(patient);

        public List<Appointment> GetAppointmentsForPatientByDate(Patient patient, DateTime dateTime)
               => iAppointmentService.GetAppointmentsForPatientByDate(patient, dateTime);

    }
}