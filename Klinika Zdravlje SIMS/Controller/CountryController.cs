// File:    CountryController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:32:06 PM
// Purpose: Definition of Class CountryController

using System;
using System.Collections.Generic;
using Controller.UserPackageController;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class CountryController : ICountryController
    {
        private static CountryController instance;
		  public ICountryService iCountryService;

		  public CountryController()
		  {
					this.iCountryService = CountryService.GetInstance();
		  }

        public static CountryController GetInstance()
        {
            if (instance == null)
            {
                instance = new CountryController();
            }
            return instance;
        }

        public IEnumerable<Country> GetAll()
				=> iCountryService.GetAll();
        

        public Country GetByID(string id)
            => iCountryService.GetByID(id);
        

 

    }
}