// File:    MedicalRecordController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:49:43 PM
// Purpose: Definition of Class MedicalRecordController

using System;
using System.Collections.Generic;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
   public class MedicalRecordController : Controller.MedicalRecordPackageController.IMedicalRecordController
   {
      private static MedicalRecordController instance;

        public MedicalRecordController()
        {
            this.iMedicalRecordService = MedicalRecordService.GetInstance();
        }

        public static MedicalRecordController GetInstance( )
        {
            if (instance == null)
            {
                instance = new MedicalRecordController();
            }
            return instance;
        }

        public List<Allergens> GetAllergens(long id)
          => iMedicalRecordService.GetAllergens(id);

        public void AddAllergenToMedicalRecord(Allergens allergen,long id)
          => iMedicalRecordService.AddAllergenToMedicalRecord(allergen, id);

        public void Update(MedicalRecord entity)
        => iMedicalRecordService.Update(entity);

        public MedicalRecord GetByID(long id)
        => iMedicalRecordService.GetByID(id);
        
        

        public IEnumerable<MedicalRecord> GetAll()
        =>iMedicalRecordService.GetAll();

        public MedicalRecord Create(MedicalRecord entity)
        => iMedicalRecordService.Create(entity);

        public Service.MedicalRecordPackageService.IMedicalRecordService iMedicalRecordService;
   
   }
}