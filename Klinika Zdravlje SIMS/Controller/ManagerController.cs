// File:    ManagerController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:30:41 PM
// Purpose: Definition of Class ManagerController

using System;
using System.Collections.Generic;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class ManagerController : Controller.UserPackageController.IManagerController
   {
        public  IManagerService iManagerService;
        private static ManagerController instance;

        public ManagerController()
        {
            iManagerService = ManagerService.GetInstance();
        }

     
      
        public static ManagerController GetInstance()
        {
            if (instance == null)
            {
                instance = new ManagerController();
            }
            return instance;
        }

       

		public void Update(Manager entity)
            => iManagerService.Update(entity);

        public IEnumerable<Manager> GetAll()
		    => iManagerService.GetAll();

        public Manager GetByID(string id)
            => iManagerService.GetByID(id);
    }

}