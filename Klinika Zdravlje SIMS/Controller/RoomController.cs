// File:    RoomController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:30:38 PM
// Purpose: Definition of Class RoomController

using System;
using System.Collections.Generic;
using Model.Rooms;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class RoomController : Controller.RoomsPackageController.IRoomController
   {
			private static RoomController instance;
			private readonly IRoomService iRoomService;

			public RoomController()
			{
				this.iRoomService = RoomService.GetInstance();
			}

			public static RoomController GetInstance()
			{
				if (instance == null) {
						instance = new RoomController();
				}
				return instance;
			}

        public IEnumerable<Room> GetAll()
            => iRoomService.GetAll();

        public void Update(Room entity)
            => iRoomService.Update(entity);

        public void Delete(Room entiity)
            => iRoomService.Delete(entiity);

		  public Room Create(Room entity)
			 => iRoomService.Create(entity);

        public Room GetByID(string id)
            => iRoomService.GetByID(id);

   
   }
}