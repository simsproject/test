// File:    IAllergensController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:03:12 PM
// Purpose: Definition of Interface IAllergensController

using Model.MedicalRecords;
using System;

namespace Controller.MedicalRecordPackageController
{
   public interface IAllergensController : Controller.ICreateController<Allergens>, Controller.IGetAllController<Allergens>, Controller.IGetByIDController<Allergens, String>
   {
   }
}