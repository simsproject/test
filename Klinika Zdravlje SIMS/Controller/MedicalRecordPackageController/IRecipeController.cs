// File:    IRecipeController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:06:33 PM
// Purpose: Definition of Interface IRecipeController

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Controller.MedicalRecordPackageController
{
   public interface IRecipeController : Controller.ICreateController<Recipe>, Controller.IGetByIDController<Recipe, long>, Controller.IGetAllController<Recipe>
   {
      List<Recipe> GetAllRecipesForPatient(MedicalRecord medicalRecord);
   
   }
}