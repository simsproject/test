// File:    IHospitalTreatmentController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:03:37 PM
// Purpose: Definition of Interface IHospitalTreatmentController

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Controller.MedicalRecordPackageController
{
   public interface IHospitalTreatmentController : Controller.ICreateController<HospitalTreatment>, Controller.IGetAllController<HospitalTreatment>, Controller.IGetByIDController<HospitalTreatment, long>
   {
      List<HospitalTreatment> GetAllHospitalTreatmentsForPatient(MedicalRecord medicalRecord);
   
   }

}