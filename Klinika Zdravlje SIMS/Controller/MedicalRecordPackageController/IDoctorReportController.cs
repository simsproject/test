// File:    IDoctorReportController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:03:24 PM
// Purpose: Definition of Interface IDoctorReportController

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Controller.MedicalRecordPackageController
{
   public interface IDoctorReportController : Controller.ICreateController<DoctorReport>, Controller.IGetByIDController<DoctorReport, long>, Controller.IGetAllController<DoctorReport>
   {
      List<DoctorReport> GetAllDoctorReportForPatient(MedicalRecord medicalRecord);
   
   }
}