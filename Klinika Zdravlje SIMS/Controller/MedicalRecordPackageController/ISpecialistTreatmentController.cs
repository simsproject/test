// File:    ISpecialistTreatmentController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:04:10 PM
// Purpose: Definition of Interface ISpecialistTreatmentController

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Controller.MedicalRecordPackageController
{
   public interface ISpecialistTreatmentController : Controller.ICreateController<SpecialistTreatment>, Controller.IGetByIDController<SpecialistTreatment, long>, Controller.IGetAllController<SpecialistTreatment>
   {
      List<SpecialistTreatment> GetAllSpecialistTreatmentsForPatient(MedicalRecord medicalRecord);
   
   }
}