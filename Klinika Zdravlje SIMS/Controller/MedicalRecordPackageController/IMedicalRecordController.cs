// File:    IMedicalRecordController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:00:02 PM
// Purpose: Definition of Interface IMedicalRecordController

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Controller.MedicalRecordPackageController
{
   public interface IMedicalRecordController : Controller.IUpdateController<MedicalRecord>, Controller.ICreateController<MedicalRecord>, Controller.IGetByIDController<MedicalRecord, long>, Controller.IGetAllController<MedicalRecord>
   {
      List<Allergens> GetAllergens(long id);
      
      void AddAllergenToMedicalRecord(Allergens allergen,long id);
   
   }
}