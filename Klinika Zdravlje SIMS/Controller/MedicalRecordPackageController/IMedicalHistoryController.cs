// File:    IMedicalHistoryController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:00:15 PM
// Purpose: Definition of Interface IMedicalHistoryController

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Controller.MedicalRecordPackageController
{
   public interface IMedicalHistoryController : Controller.IUpdateController<MedicalHistory>, Controller.ICreateController<MedicalHistory>, Controller.IGetByIDController<MedicalHistory, long>, Controller.IGetAllController<MedicalHistory>
   {
      List<MedicalHistory> GetAllMedicalHistoriesForPatient(MedicalRecord medicalRecord);
   
   }
}