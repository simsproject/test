// File:    ICourseOfDiseaseController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 6:00:26 PM
// Purpose: Definition of Interface ICourseOfDiseaseController

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Controller.MedicalRecordPackageController
{
   public interface ICourseOfDiseaseController : Controller.ICreateController<CourseOfDisease>, Controller.IGetAllController<CourseOfDisease>, Controller.IGetByIDController<CourseOfDisease, long>
   {
      List<CourseOfDisease> GetAllCourseOfDiseasesForMedicalHistory(MedicalHistory medicalHistory);
   
   }
}