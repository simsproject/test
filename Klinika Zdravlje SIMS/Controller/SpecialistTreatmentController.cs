// File:    SpecialistTreatmentController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:13:28 PM
// Purpose: Definition of Class SpecialistTreatmentController

using System;
using System.Collections.Generic;
using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
   public class SpecialistTreatmentController : Controller.MedicalRecordPackageController.ISpecialistTreatmentController
   {
      private static SpecialistTreatmentController instance;
		public ISpecialistTreatmentService iSpecialistTreatmentService;

		public SpecialistTreatmentController()
		{
			this.iSpecialistTreatmentService = SpecialistTreatmentService.GetInstance();
		}

		public static SpecialistTreatmentController GetInstance()
      {
			if (instance == null)
			{
				instance = new SpecialistTreatmentController();
			}
			return instance;
		}

		public List<SpecialistTreatment> GetAllSpecialistTreatmentsForPatient(MedicalRecord medicalRecord)
		=> iSpecialistTreatmentService.GetAllSpecialistTreatmentsForPatient(medicalRecord);

		public SpecialistTreatment Create(SpecialistTreatment entity)
			=> iSpecialistTreatmentService.Create(entity);

			public SpecialistTreatment GetByID(long id)
			 => iSpecialistTreatmentService.GetByID(id);

		  public IEnumerable<SpecialistTreatment> GetAll()
		    => iSpecialistTreatmentService.GetAll(); 


   
   }
}