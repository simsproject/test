// File:    IPatientService.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:00:43 PM
// Purpose: Definition of Interface IPatientService

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface IPatientService : Controller.IController<Patient>, Controller.IGetByIDController<Patient, String>
   {
   
   }
}