// File:    IAddressController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:03:46 PM
// Purpose: Definition of Interface IAddressController

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface IAddressController : Controller.IGetAllController<Address>, Controller.IGetByIDController<Address, long>
   {
   }
}