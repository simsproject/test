// File:    IWorkingTimeController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:04:33 PM
// Purpose: Definition of Interface IWorkingTimeController

using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.UserPackageController
{
   public interface IWorkingTimeController : Controller.IController<WorkingTime>,IGetByIDController<WorkingTime,long>
   {    
      WorkingTime GetWorkingTimeForDoctorByDate(Doctor doctor, DateTime date);

   }
}