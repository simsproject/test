// File:    IManagerController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:01:02 PM
// Purpose: Definition of Interface IManagerController

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface IManagerController : Controller.IUpdateController<Manager>, Controller.IGetAllController<Manager>,IGetByIDController<Manager,string>
   {
   }
}