// File:    IUserController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:01:06 PM
// Purpose: Definition of Interface IUserController

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface IUserController : Controller.IController<User>, Controller.IGetByIDController<User, String>
   {
   }
}