// File:    ISecretaryController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:01:03 PM
// Purpose: Definition of Interface ISecretaryController

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface ISecretaryController : Controller.IController<Secretary>, Controller.IGetByIDController<Secretary, String>
   {
   }
}