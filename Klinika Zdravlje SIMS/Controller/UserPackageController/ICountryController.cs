// File:    ICountryController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:03:45 PM
// Purpose: Definition of Interface ICountryController

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface ICountryController : Controller.IGetAllController<Country>, Controller.IGetByIDController<Country,string>
   {
   }
}