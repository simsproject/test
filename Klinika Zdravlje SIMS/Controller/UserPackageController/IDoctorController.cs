// File:    IDoctorController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:00:53 PM
// Purpose: Definition of Interface IDoctorController

using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.UserPackageController
{
   public interface IDoctorController : Controller.IController<Doctor>, Controller.IGetByIDController<Doctor, String>
   {
      List<Doctor> GetDoctorByTitle(Model.Users.Title title);
      Dictionary<Doctor, WorkingTime> FilterDoctorsByWorkingTime(DateTime date);


   }
}