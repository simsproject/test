// File:    IFeedbackController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:04:21 PM
// Purpose: Definition of Interface IFeedbackController

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface IFeedbackController : Controller.IGetAllController<Feedback>, Controller.ICreateController<Feedback>
   {
   }
}