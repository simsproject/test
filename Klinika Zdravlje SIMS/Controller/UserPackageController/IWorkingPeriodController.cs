// File:    IWorkingPeriodController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:04:34 PM
// Purpose: Definition of Interface IWorkingPeriodController

using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.UserPackageController
{
   public interface IWorkingPeriodController : Controller.IController<WorkingPeriod>,IGetByIDController<WorkingPeriod,long>
   {
      
      WorkingPeriod WorkingPeriodForDoctorByDate(Doctor doctor, DateTime date);
      IEnumerable<DateTime> EachDay(DateTime from, DateTime thru);
   }
}