// File:    ICityController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 10:03:45 PM
// Purpose: Definition of Interface ICityController

using Model.Users;
using System;

namespace Controller.UserPackageController
{
   public interface ICityController : Controller.IGetAllController<City>, Controller.IGetByIDController<City, long>
   {
   }
}