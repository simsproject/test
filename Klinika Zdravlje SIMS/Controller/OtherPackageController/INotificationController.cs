// File:    INotificationController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 4:32:57 PM
// Purpose: Definition of Interface INotificationController

using Model.Other;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.OtherPackageController
{
   public interface INotificationController : Controller.IGetAllController<Notification>, Controller.ICreateController<Notification>
   {
      List<Notification> GetNotificationForUser(User user);

   
   }
}