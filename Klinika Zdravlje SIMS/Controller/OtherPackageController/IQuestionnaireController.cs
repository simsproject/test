// File:    IQuestionaireController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 4:33:44 PM
// Purpose: Definition of Interface IQuestionaireController

using Model.Other;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.OtherPackageController
{
   public interface IQuestionnaireController : Controller.IGetAllController<Questionnaire>, Controller.IGetByIDController<Questionnaire, long>, Controller.ICreateController<Questionnaire>
   {
      List<Questionnaire> GetQuestionnairesForDoctor(Doctor doctor);
   
   }
}