// File:    IBlogController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 4:32:56 PM
// Purpose: Definition of Interface IBlogController

using Model.Other;
using System;

namespace Controller.OtherPackageController
{
   public interface IBlogController : Controller.IGetByIDController<Blog, long>, Controller.IController<Blog>
   {
   }
}