// File:    IQuestionController.cs
// Author:  Korisnik
// Created: Monday, May 25, 2020 4:32:57 PM
// Purpose: Definition of Interface IQuestionController

using Model.Other;
using System;

namespace Controller.OtherPackageController
{
   public interface IQuestionController : Controller.IGetAllController<Question>, Controller.ICreateController<Question>, Controller.IGetByIDController<Question,long>
   { 
   }
}