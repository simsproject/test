// File:    MedicalHistoryController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:48:13 PM
// Purpose: Definition of Class MedicalHistoryController

using System;
using System.Collections.Generic;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
   public class MedicalHistoryController : Controller.MedicalRecordPackageController.IMedicalHistoryController
   {
      private static MedicalHistoryController instance;

        public MedicalHistoryController()
        {
            this.iMedicalHistoryService = MedicalHistoryService.GetInstance();
        }

        public static MedicalHistoryController GetInstance()
        {
            if (instance == null) {
                instance = new MedicalHistoryController();
            }
            return instance;
        }

        public List<MedicalHistory> GetAllMedicalHistoriesForPatient(MedicalRecord medicalRecord)
            => iMedicalHistoryService.GetAllMedicalHistoriesForPatient(medicalRecord);

        public void Update(MedicalHistory entity)
        => iMedicalHistoryService.Update(entity);

        public MedicalHistory Create(MedicalHistory entity)
        => iMedicalHistoryService.Create(entity);

        public MedicalHistory GetByID(long id)
        => iMedicalHistoryService.GetByID(id);

        public IEnumerable<MedicalHistory> GetAll()
        => iMedicalHistoryService.GetAll();

        public IMedicalHistoryService iMedicalHistoryService;
   
   }
}