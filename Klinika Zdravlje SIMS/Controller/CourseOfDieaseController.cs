// File:    CourseOfDieaseController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:13:24 PM
// Purpose: Definition of Class CourseOfDieaseController

using System;
using System.Collections.Generic;
using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
   public class CourseOfDieaseController : ICourseOfDiseaseController
   {
      private static CourseOfDieaseController instance;
		public ICourseOfDiseaseService iCourseOfDiseaseService;

		public CourseOfDieaseController()
      {
            this.iCourseOfDiseaseService = CourseOfDiseaseSevice.GetInstance();
      }

      public static CourseOfDieaseController GetInstance()
      {
            if (instance == null) {
                instance = new CourseOfDieaseController();
            }
            return instance;
      }

        public List<CourseOfDisease> GetAllCourseOfDiseasesForMedicalHistory(MedicalHistory medicalHistory)
          => iCourseOfDiseaseService.GetAllCourseOfDiseasesForMedicalHistory(medicalHistory);

       public CourseOfDisease Create(CourseOfDisease entity)
            => iCourseOfDiseaseService.Create(entity);

       public IEnumerable<CourseOfDisease> GetAll()
            => iCourseOfDiseaseService.GetAll();

       public CourseOfDisease GetByID(long id)
            => iCourseOfDiseaseService.GetByID(id);

 
   
   }
}