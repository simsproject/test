// File:    IStorageController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:29:37 PM
// Purpose: Definition of Interface IStorageController

using Model.Rooms;
using System;

namespace Controller.RoomsPackageController
{
   public interface IStorageController : Controller.IController<Storage>, Controller.IGetByIDController<Storage, String>
   {
   }
}