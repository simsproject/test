// File:    IOfficeController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:29:26 PM
// Purpose: Definition of Interface IOfficeController

using Model.Rooms;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.RoomsPackageController
{
   public interface IOfficeController : IController<Office>,IGetByIDController<Office,string>
   {  
   }
}