// File:    IRoomController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:30:00 PM
// Purpose: Definition of Interface IRoomController

using Model.Rooms;
using System;
using System.Collections.Generic;

namespace Controller.RoomsPackageController
{
   public interface IRoomController : Controller.IController<Room>, Controller.IGetByIDController<Room, String>
   { 
   
   }
}