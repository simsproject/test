// File:    IMedicalRoomController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:28:57 PM
// Purpose: Definition of Interface IMedicalRoomController

using Model.MedicalRecords;
using Model.Rooms;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.RoomsPackageController
{
   public interface IMedicalRoomController : Controller.IController<MedicalRoom>,IGetByIDController<MedicalRoom,string>
   {
   }
}