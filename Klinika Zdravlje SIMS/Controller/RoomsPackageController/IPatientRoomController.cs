// File:    IPatientRoomController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:30:01 PM
// Purpose: Definition of Interface IPatientRoomController

using Model.Rooms;
using System;

namespace Controller.RoomsPackageController
{
   public interface IPatientRoomController : Controller.IController<PatientRoom>, Controller.IGetByIDController<PatientRoom,string>
   {
   
   }
}