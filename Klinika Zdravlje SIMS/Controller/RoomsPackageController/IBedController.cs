// File:    IBedController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:31:00 PM
// Purpose: Definition of Interface IBedController

using Model.Rooms;
using System;
using System.Collections.Generic;

namespace Controller.RoomsPackageController
{
   public interface IBedController : Controller.IGetByIDController<Bed, int>, Controller.IController<Bed>
   {   
      List<Bed> GetFreeBeds(Room room);
   
   }
}