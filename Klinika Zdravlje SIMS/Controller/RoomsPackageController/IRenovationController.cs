// File:    IRenovationController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:31:02 PM
// Purpose: Definition of Interface IRenovationController

using Model.Rooms;
using System;
using System.Collections.Generic;

namespace Controller.RoomsPackageController
{
   public interface IRenovationController : Controller.IDeleteController<Renovation>, Controller.ICreateController<Renovation>, Controller.IGetAllController<Renovation>, Controller.IGetByIDController<Renovation, long>
   {
        List<Renovation> GetRenovationsForRoom(Room room);
    }
}