// File:    IInventoryController.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 7:30:17 PM
// Purpose: Definition of Interface IInventoryController

using Model.Rooms;
using System;

namespace Controller.RoomsPackageController
{
   public interface IInventoryController : Controller.IController<Inventory>, Controller.IGetByIDController<Inventory, long>
   {
   
   }
}