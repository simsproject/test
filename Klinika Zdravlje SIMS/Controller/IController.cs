// File:    IController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 12:54:42 AM
// Purpose: Definition of Interface IController

using System;

namespace Controller
{
   public interface IController<E> : IGetAllController<E>, IUpdateController<E>, IDeleteController<E>, ICreateController<E>
   {
   }
}