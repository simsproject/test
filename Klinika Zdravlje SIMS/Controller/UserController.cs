// File:    UserController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:33:42 PM
// Purpose: Definition of Class UserController

using System;
using System.Collections.Generic;
using Model.Users;
using Service;
using Service.UserPackageService;

namespace Controller
{
   public class UserController : Controller.UserPackageController.IUserController
   {
      private static UserController instance;
      public IUserService iUserService;

        public UserController()
        {
            iUserService = UserService.GetInstance();
        }

        public static UserController GetInstance()
      {
            if (instance == null)
            {
                instance = new UserController();
            }
            return instance;
        }

        public IEnumerable<User> GetAll()
            => iUserService.GetAll();

        public void Update(User entity)
            => iUserService.Update(entity);

        public void Delete(User entiity)
            => iUserService.Delete(entiity);

        public User Create(User entity)
            => iUserService.Create(entity);

        public User GetByID(string id)
            => iUserService.GetByID(id);


    }
}