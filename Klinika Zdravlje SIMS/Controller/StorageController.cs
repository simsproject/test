// File:    StorageController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:30:40 PM
// Purpose: Definition of Class StorageController

using System;
using System.Collections.Generic;
using Model.Rooms;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class StorageController : Controller.RoomsPackageController.IStorageController
   {
      private static StorageController instance;
      public IStorageService iStorageService;

        public StorageController()
        {
            this.iStorageService = StorageService.GetInstance();
        }

        public static StorageController GetInstance()
        {
            if (instance == null)
            {
                instance = new StorageController();
            }
            return instance;
        }

        public Storage Create(Storage entity)
            => iStorageService.Create(entity);

        public void Delete(Storage entiity)
            => iStorageService.Delete(entiity);

        public IEnumerable<Storage> GetAll()
        => iStorageService.GetAll();

        

        public Storage GetByID(string id)
        => iStorageService.GetByID(id);

        public void Update(Storage entity)
            => iStorageService.Update(entity);
    }
}