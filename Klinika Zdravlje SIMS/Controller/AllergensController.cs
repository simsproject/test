// File:    AllergensController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:13:23 PM
// Purpose: Definition of Class AllergensController

using System;
using System.Collections.Generic;
using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using Service;
using Service.MedicalRecordPackageService;

namespace Controller
{
    public class AllergensController : IAllergensController
    {
        private static AllergensController instance;
        private readonly IAllergensService iAllergensService;

        public AllergensController()
        {
            this.iAllergensService = AllergensService.GetInstance();
        }

        public static AllergensController GetInstance()
        {
            if (instance == null)
            {
                instance = new AllergensController();
            }
            return instance;
        }


		  public Allergens Create(Allergens entity)
				=> iAllergensService.Create(entity);

        public IEnumerable<Allergens> GetAll()
            => iAllergensService.GetAll();

        public Allergens GetByID(string id)
            => iAllergensService.GetByID(id);


    }
}