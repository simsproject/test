// File:    IGetByIDController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 12:56:26 AM
// Purpose: Definition of Interface IGetByIDController

using System;

namespace Controller
{
   public interface IGetByIDController<E,ID>
   {
      E GetByID(ID id);
   
   }
}