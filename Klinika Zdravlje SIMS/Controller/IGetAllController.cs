// File:    IGetAllController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 12:54:46 AM
// Purpose: Definition of Interface IGetAllController

using System;
using System.Collections.Generic;

namespace Controller
{
   public interface IGetAllController<E>
   {
      IEnumerable<E> GetAll();
   
   }
}