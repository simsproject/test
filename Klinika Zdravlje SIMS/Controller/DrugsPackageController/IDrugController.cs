// File:    IDrugController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 1:52:41 AM
// Purpose: Definition of Interface IDrugController

using Model.Drugs;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Controller.DrugsPackageController
{
   public interface IDrugController : Controller.IController<Drug>, Controller.IGetByIDController<Drug,String>
   {
      List<Drug> GetAllWaitingDrugs();
      
      List<Drug> GetAllValidDrugs();
      
      void UpdateWaitingDrugs(List<Drug> listOfWaitingDrugs);

      List<Drug> GetAllSentValidationDrugs();

      List<Drug> GetAllSentValidationDrugsForDoctor(Doctor doctor);


    }
}