// File:    RenovationController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:30:37 PM
// Purpose: Definition of Class RenovationController

using System;
using System.Collections.Generic;
using Model.Rooms;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class RenovationController : Controller.RoomsPackageController.IRenovationController
   {
        private static RenovationController instance;
        public IRenovationService iRenovationService;

        public RenovationController()
        {
            iRenovationService = RenovationService.GetInstance();
        }

        public static RenovationController GetInstance()
        {
            if (instance == null)
            {
                instance = new RenovationController();
            }
            return instance;
        }

        public void Delete(Renovation entiity)
        => iRenovationService.Delete(entiity);

        public Renovation Create(Renovation entity)
        => iRenovationService.Create(entity);

        public IEnumerable<Renovation> GetAll()
        => iRenovationService.GetAll();

        public Renovation GetByID(long id)
        => iRenovationService.GetByID(id);

        public List<Renovation> GetRenovationsForRoom(Room room)
        => iRenovationService.GetRenovationsForRoom(room);

    }
}