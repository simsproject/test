// File:    NotificationController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:23:27 PM
// Purpose: Definition of Class NotificationController

using System;
using System.Collections.Generic;
using Model.Other;
using Model.Users;
using Service;
using Service.OtherPackageService;

namespace Controller
{
   public class NotificationController : Controller.OtherPackageController.INotificationController
   {
      private static NotificationController instance;

        public NotificationController()
        {
            this.iNotificationService = NotificationService.GetInstance();
        }

        public static NotificationController GetInstance()
      {
            if (instance == null) {
                instance = new NotificationController();
            }
            return instance;
      }

        public List<Notification> GetNotificationForUser(User user)
             => iNotificationService.GetNotificationForUser(user);


        public IEnumerable<Notification> GetAll()
            => iNotificationService.GetAll();

        public Notification Create(Notification entity)
            => iNotificationService.Create(entity);

        public Service.OtherPackageService.INotificationService iNotificationService;
   
   }
}