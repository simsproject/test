// File:    IDeleteController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 12:54:44 AM
// Purpose: Definition of Interface IDeleteController

using System;

namespace Controller
{
   public interface IDeleteController<E>
   {
      void Delete(E entiity);
   
   }
}