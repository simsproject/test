// File:    BedController.cs
// Author:  tomic
// Created: Tuesday, May 26, 2020 3:23:34 PM
// Purpose: Definition of Class BedController

using System;
using System.Collections.Generic;
using Controller.RoomsPackageController;
using Model.Rooms;
using Service;
using Service.RoomsPackageService;

namespace Controller
{
   public class BedController : IBedController
   {
      private static BedController instance;
		public IBedService iBedService;

		public static BedController GetInstance()
      {
         if (instance == null)
            {
                instance = new BedController();
            }
            return instance;
      }

        public BedController()
        {
            this.iBedService = BedService.GetInstance();
        }

        public List<Bed> GetFreeBeds(Room room)
            => iBedService.GetFreeBeds(room);

		   public Bed GetByID(int id)
					=> iBedService.GetByID(id);
        

         public IEnumerable<Bed> GetAll()
					=> iBedService.GetAll();


			public void Update(Bed entity)
					=> iBedService.Update(entity);

			public void Delete(Bed entiity)
					=> iBedService.Delete(entiity);

			public Bed Create(Bed entity)
					=> iBedService.Create(entity);


   
   }
}