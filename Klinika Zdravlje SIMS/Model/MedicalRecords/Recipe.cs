// File:    Recipe.cs
// Author:  Korisnik
// Created: Tuesday, April 21, 2020 10:04:42 AM
// Purpose: Definition of Class Recipe

using System;
using Model.Users;

namespace Model.MedicalRecords
{
   public class Recipe
   {
      private DateTime dateOfCreate;
      private String amount;
      private int drugID;
      private String description;
      private long serialID;

        private Model.Users.Doctor doctor;
        private MedicalRecord medicalRecord;

        public DateTime DateOfCreate { get => dateOfCreate; set => dateOfCreate = value; }
        public string Amount { get => amount; set => amount = value; }
        public int DrugID { get => drugID; set => drugID = value; }
        public string Description { get => description; set => description = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public Doctor Doctor { get => doctor; set => doctor = value; }
        public MedicalRecord MedicalRecord { get => medicalRecord; set => medicalRecord = value; }

        public Recipe(DateTime dateOfCreate, string amount, int drugID, string description, long serialID, Doctor doctor, MedicalRecord medicalRecord)
        {
            this.DateOfCreate = dateOfCreate;
            this.Amount = amount;
            this.DrugID = drugID;
            this.Description = description;
            this.SerialID = serialID;
            this.Doctor = doctor;
            this.MedicalRecord = medicalRecord;
        }
    }
}