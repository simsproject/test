// File:    MedicalRecord.cs
// Author:  Korisnik
// Created: Thursday, April 16, 2020 8:19:47 PM
// Purpose: Definition of Class MedicalRecord

using System;
using System.Collections.Generic;
using Model.Users;

namespace Model.MedicalRecords
{
   public class MedicalRecord
   {
      private long serialID;

        
        public System.Collections.Generic.List<Allergens> allergens;

        public long SerialID { get => serialID; set => serialID = value; }
        
        public MedicalRecord(long serialID, List<Allergens> allergens)
        {
            this.SerialID = serialID;
            
            this.allergens = allergens;
        }



        /// <summary>
        /// Property for collection of Allergens
        /// </summary>
        /// <pdGenerated>Default opposite class collection property</pdGenerated>
        public System.Collections.Generic.List<Allergens> Allergens
      {
         get
         {
            if (allergens == null)
               allergens = new System.Collections.Generic.List<Allergens>();
            return allergens;
         }
         set
         {
            RemoveAllAllergens();
            if (value != null)
            {
               foreach (Allergens oAllergens in value)
                  AddAllergens(oAllergens);
            }
         }
      }



        /// <summary>
        /// Add a new Allergens in the collection
        /// </summary>
        /// <pdGenerated>Default Add</pdGenerated>
        public void AddAllergens(Allergens newAllergens)
      {
         if (newAllergens == null)
            return;
         if (this.allergens == null)
            this.allergens = new System.Collections.Generic.List<Allergens>();
         if (!this.allergens.Contains(newAllergens))
            this.allergens.Add(newAllergens);
      }
      
      /// <summary>
      /// Remove an existing Allergens from the collection
      /// </summary>
      /// <pdGenerated>Default Remove</pdGenerated>
      public void RemoveAllergens(Allergens oldAllergens)
      {
         if (oldAllergens == null)
            return;
         if (this.allergens != null)
            if (this.allergens.Contains(oldAllergens))
               this.allergens.Remove(oldAllergens);
      }
      
      /// <summary>
      /// Remove all instances of Allergens from the collection
      /// </summary>
      /// <pdGenerated>Default removeAll</pdGenerated>
      public void RemoveAllAllergens()
      {
         if (allergens != null)
            allergens.Clear();
      }
   
   }
}