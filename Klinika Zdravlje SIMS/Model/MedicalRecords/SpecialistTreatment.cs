// File:    SpecialistTreatment.cs
// Author:  Korisnik
// Created: Friday, April 17, 2020 1:57:04 PM
// Purpose: Definition of Class SpecialistTreatment

using System;

namespace Model.MedicalRecords
{
   public class SpecialistTreatment
   {
		private long serialID;
		private String note;
      private Model.Appointment.Appointment appointment;
      private MedicalRecord medicalRecord;

      public string Note { get => note; set => note = value; }
      public long SerialID { get => serialID; set => serialID = value; }
      public Appointment.Appointment Appointment { get => appointment; set => appointment = value; }
      public MedicalRecord MedicalRecord { get => medicalRecord; set => medicalRecord = value; }

      public SpecialistTreatment(long serialID,string note, Appointment.Appointment appointment, MedicalRecord medicalRecord)
      {
         this.Note = note;
         this.SerialID = serialID;
         this.Appointment = appointment;
         this.MedicalRecord = medicalRecord;
      }
    }
}