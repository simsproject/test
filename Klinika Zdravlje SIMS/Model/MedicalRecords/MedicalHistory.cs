// File:    MedicalHistory.cs
// Author:  Korisnik
// Created: Friday, April 17, 2020 1:42:54 PM
// Purpose: Definition of Class MedicalHistory

using System;

namespace Model.MedicalRecords
{
   public class MedicalHistory
   {
      private String description;
      private DateTime dateBegin;
      private String dateEnd;
      private String reasonOfHospitalization;
      private String reasonOfDeath;
      private String reasonOfRelase;
      private long serialID;

        private MedicalRecord medicalRecord;

        public string Description { get => description; set => description = value; }
        public DateTime DateBegin { get => dateBegin; set => dateBegin = value; }
        public String DateEnd { get => dateEnd; set => dateEnd = value; }
        public string ReasonOfHospitalization { get => reasonOfHospitalization; set => reasonOfHospitalization = value; }
        public string ReasonOfDeath { get => reasonOfDeath; set => reasonOfDeath = value; }
        public string ReasonOfRelase { get => reasonOfRelase; set => reasonOfRelase = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public MedicalRecord MedicalRecord { get => medicalRecord; set => medicalRecord = value; }

        public MedicalHistory(string description, DateTime dateBegin, string dateEnd, string reasonOfHospitalization, string reasonOfDeath, string reasonOfRelase, long serialID, MedicalRecord medicalRecord)
        {
            this.Description = description;
            this.DateBegin = dateBegin;
            this.DateEnd = dateEnd;
            this.ReasonOfHospitalization = reasonOfHospitalization;
            this.ReasonOfDeath = reasonOfDeath;
            this.ReasonOfRelase = reasonOfRelase;
            this.SerialID = serialID;
            this.MedicalRecord = medicalRecord;
        }
    }
}