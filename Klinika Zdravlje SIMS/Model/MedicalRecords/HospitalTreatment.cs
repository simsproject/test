// File:    HospitalTreatment.cs
// Author:  Korisnik
// Created: Friday, April 17, 2020 1:57:06 PM
// Purpose: Definition of Class HospitalTreatment

using System;
using Model.Rooms;

namespace Model.MedicalRecords
{
   public class HospitalTreatment
   {
      private DateTime startDate;
      private DateTime endDate;
      private String note;
      private long serialID;

        private Model.Rooms.PatientRoom patientRoom;
        private MedicalRecord medicalRecord;

        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }
        public string Note { get => note; set => note = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public PatientRoom PatientRoom { get => patientRoom; set => patientRoom = value; }
        public MedicalRecord MedicalRecord { get => medicalRecord; set => medicalRecord = value; }

        public HospitalTreatment(DateTime startDate, DateTime endDate, string note, long serialID, PatientRoom patientRoom, MedicalRecord medicalRecord)
        {
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Note = note;
            this.SerialID = serialID;
            this.PatientRoom = patientRoom;
            this.MedicalRecord = medicalRecord;
        }
    }
}