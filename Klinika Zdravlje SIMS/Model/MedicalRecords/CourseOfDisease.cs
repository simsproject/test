// File:    CourseOfDisease.cs
// Author:  Korisnik
// Created: Tuesday, April 21, 2020 10:51:08 AM
// Purpose: Definition of Class CourseOfDisease

using System;

namespace Model.MedicalRecords
{
   public class CourseOfDisease
   {
      private DateTime date;
      private String therapy;
      private String note;
      private long serialID;

        private MedicalHistory medicalHistory;

        public CourseOfDisease(DateTime date, string therapy, string note, long serialID, MedicalHistory medicalHistory)
        {
            this.Date = date;
            this.Therapy = therapy;
            this.Note = note;
            this.SerialID = serialID;
            this.MedicalHistory = medicalHistory;
        }

        public DateTime Date { get => date; set => date = value; }
        public string Therapy { get => therapy; set => therapy = value; }
        public string Note { get => note; set => note = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public MedicalHistory MedicalHistory { get => medicalHistory; set => medicalHistory = value; }
    }
}