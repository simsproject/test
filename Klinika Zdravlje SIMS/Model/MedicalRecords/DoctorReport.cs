// File:    DoctorReport.cs
// Author:  Korisnik
// Created: Thursday, April 16, 2020 8:27:51 PM
// Purpose: Definition of Class DoctorReport

using Model.Users;
using System;

namespace Model.MedicalRecords
{
   public class DoctorReport
   {
      private String name;
      private DateTime date;
      private String doctorOpinion;
      private String diagnosisAndTeraphy;
      private String anamnesis;
      private long serialID;
        private Doctor doctor;

        private MedicalRecord medicalRecord;

        public string Name { get => name; set => name = value; }
        public DateTime Date { get => date; set => date = value; }
        public string DoctorOpinion { get => doctorOpinion; set => doctorOpinion = value; }
        public string DiagnosisAndTeraphy { get => diagnosisAndTeraphy; set => diagnosisAndTeraphy = value; }
        public string Anamnesis { get => anamnesis; set => anamnesis = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public MedicalRecord MedicalRecord { get => medicalRecord; set => medicalRecord = value; }
        public Doctor Doctor { get => doctor; set => doctor = value; }

        public DoctorReport(string name, DateTime date, string doctorOpinion, string diagnosisAndTeraphy, string anamnesis, long serialID, MedicalRecord medicalRecord,Doctor doctor)
        {
            this.Name = name;
            this.Date = date;
            this.DoctorOpinion = doctorOpinion;
            this.DiagnosisAndTeraphy = diagnosisAndTeraphy;
            this.Anamnesis = anamnesis;
            this.SerialID = serialID;
            this.MedicalRecord = medicalRecord;
            this.Doctor = doctor;
        }
    }
}