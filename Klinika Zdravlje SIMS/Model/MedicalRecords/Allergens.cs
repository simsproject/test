// File:    Allergens.cs
// Author:  Korisnik
// Created: Thursday, April 16, 2020 9:41:22 PM
// Purpose: Definition of Class Allergens

using System;

namespace Model.MedicalRecords
{
   public class Allergens
   {
      private String name;

        public Allergens(string name)
        {
            this.Name = name;
        }

        public override string ToString()
        {
            return this.name;
        }

        public string Name { get => name; set => name = value; }
    }
}