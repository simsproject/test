// File:    Question.cs
// Author:  Korisnik
// Created: Tuesday, April 21, 2020 11:34:12 AM
// Purpose: Definition of Class Question

using System;

namespace Model.Other
{
   public class Question
   {
        private Questionnaire questionnaire;
      private String questions;
      private String answer;
        private long id;


        public string Questions { get => questions; set => questions = value; }
        public string Answer { get => answer; set => answer = value; }
        public Questionnaire Questionnaire { get => questionnaire; set => questionnaire = value; }
        public long Id { get => id; set => id = value; }

        public Question(string question, string answer, Questionnaire questionnaire, long id)
        {
            this.Questions = question;
            this.Answer = answer;
            this.Questionnaire = questionnaire;
            this.Id = id;
        }

    }
}