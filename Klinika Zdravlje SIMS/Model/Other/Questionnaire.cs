// File:    Questionnaire.cs
// Author:  Korisnik
// Created: Monday, April 20, 2020 5:26:30 PM
// Purpose: Definition of Class Questionnaire

using Model.Users;
using System;
using System.Collections.Generic;

namespace Model.Other
{
   public class Questionnaire
   {
      private String comment;
      private String name;
      private long serialID;
        private Doctor doctor;

        private Model.Users.Patient patient;

        public string Comment { get => comment; set => comment = value; }
        public string Name { get => name; set => name = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public Patient Patient { get => patient; set => patient = value; }
        public Doctor Doctor { get => doctor; set => doctor = value; }

        public Questionnaire(string comment, string name, long serialID, Patient patient, Doctor doctor)
        {
            this.Comment = comment;
            this.Name = name;
            this.SerialID = serialID;
            this.Patient = patient;
            this.Doctor = doctor;
        }


        /// <summary>
        /// Property for collection of Question
        /// </summary>
        /// <pdGenerated>Default opposite class collection property</pdGenerated>
        
   
   }
}