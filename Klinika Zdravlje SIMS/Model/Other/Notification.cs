// File:    Notification.cs
// Author:  Korisnik
// Created: Monday, April 20, 2020 3:37:43 PM
// Purpose: Definition of Class Notification

using Model.Users;
using System;
using System.Collections.Generic;

namespace Model.Other
{
   public class Notification
   {
      private String text;
      private DateTime date;

        private User user;

        public string Text { get => text; set => text = value; }
        public DateTime Date { get => date; set => date = value; }
        public User User { get => user; set => user = value; }

        public Notification(string text, DateTime date, User user)
        {
            this.Text = text;
            this.Date = date;
            this.User = user;
        }



   }
}