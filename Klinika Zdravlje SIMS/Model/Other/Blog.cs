// File:    Blog.cs
// Author:  Korisnik
// Created: Monday, April 20, 2020 3:37:43 PM
// Purpose: Definition of Class Blog

using System;

namespace Model.Other
{
   public class Blog
   {
      private String title;
      private DateTime dateOfCreate;
      private String createdBy;
      private String content;
      private long serialID;

        public string Title { get => title; set => title = value; }
        public DateTime DateOfCreate { get => dateOfCreate; set => dateOfCreate = value; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        public string Content { get => content; set => content = value; }
        public long SerialID { get => serialID; set => serialID = value; }

        public Blog(string title, DateTime dateOfCreate, string createdBy, string content, long serialID)
        {
            this.Title = title;
            this.DateOfCreate = dateOfCreate;
            this.CreatedBy = createdBy;
            this.Content = content;
            this.SerialID = serialID;
        }


    }
}