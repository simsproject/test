/***********************************************************************
 * Module:  Manager.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Manager
 ***********************************************************************/

using System;

namespace Model.Users
{
   public class Manager : User
   {

        public Manager(string firstName, string lastName, DateTime dateOfBirth, string personalID, Gender gender, string phoneNumber, string email, Address address, string userName, string password) : base(firstName, lastName, dateOfBirth, personalID, gender, phoneNumber, email, address, userName, password)
        {
            
        }
    }
}