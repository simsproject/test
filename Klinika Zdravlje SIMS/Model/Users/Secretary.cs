/***********************************************************************
 * Module:  Secretary.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Secretary
 ***********************************************************************/

using Model.Rooms;
using System;

namespace Model.Users
{
   public class Secretary : User
   {
        private WorkingPeriod[] workingPeriod;
        private Office office;

        public Secretary(string firstName, string lastName, DateTime dateOfBirth, string personalID, Gender gender, string phoneNumber, string email, Address address, string userName, string password, WorkingPeriod[] workingPeriod, Office office) : base(firstName, lastName, dateOfBirth, personalID, gender, phoneNumber, email, address, userName, password)
        {
            this.WorkingPeriod = workingPeriod;
            this.Office = office;
        }

        public WorkingPeriod[] WorkingPeriod { get => workingPeriod; set => workingPeriod = value; }
        public Office Office { get => office; set => office = value; }

    }
}