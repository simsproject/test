/***********************************************************************
 * Module:  Gender.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Gender
 ***********************************************************************/

using System;

namespace Model.Users
{
   public enum Gender
   {
      Male,
      Female
   }
}