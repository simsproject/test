/***********************************************************************
 * Module:  City.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class City
 ***********************************************************************/

using System;

namespace Model.Users
{
   public class City
   {
        private String name;
        private long postalCode;

        private Country country;

        public City(string name, long postalCode, Country country)
        {
            this.Name = name;
            this.PostalCode = postalCode;
            this.Country = country;
        }



        /// <summary>
        /// Property for Country
        /// </summary>
        /// <pdGenerated>Default opposite class property</pdGenerated>


        public string Name { get => name; set => name = value; }
        public long PostalCode { get => postalCode; set => postalCode = value; }
        public Country Country { get => country; set => country = value; }

    }
}