// File:    WorkingPeriod.cs
// Author:  Korisnik
// Created: Monday, April 20, 2020 9:25:24 AM
// Purpose: Definition of Class WorkingPeriod

using System;
using System.Collections.Generic;

namespace Model.Users
{
   public class WorkingPeriod
   {
		  private long serialID;
        private DateTime begin;
        private DateTime end;
		  private List<WorkingTime> workingTime;

		public WorkingPeriod(long serialID,DateTime begin, DateTime end, List<WorkingTime> workingTime)
        {
				this.SerialID = serialID;
            this.Begin = begin;
            this.End = end;
            this.WorkingTime = workingTime;
        }



     

        /// <summary>
        /// Property for collection of WorkingTime
        /// </summary>
        /// <pdGenerated>Default opposite class collection property</pdGenerated>


        public DateTime Begin { get => begin; set => begin = value; }
        public DateTime End { get => end; set => end = value; }
        public List<WorkingTime> WorkingTime { get => workingTime; set => workingTime = value; }
		  public long SerialID { get => serialID; set => serialID = value; }

		/// <summary>
		/// Add a new WorkingTime in the collection
		/// </summary>
		/// <pdGenerated>Default Add</pdGenerated>
		public void AddWorkingTime(WorkingTime newWorkingTime)
        {
            if (newWorkingTime == null)
                return;
            if (this.WorkingTime == null)
                this.WorkingTime = new System.Collections.Generic.List<WorkingTime>();
            if (!this.WorkingTime.Contains(newWorkingTime))
                this.WorkingTime.Add(newWorkingTime);
        }

        /// <summary>
        /// Remove an existing WorkingTime from the collection
        /// </summary>
        /// <pdGenerated>Default Remove</pdGenerated>
        public void RemoveWorkingTime(WorkingTime oldWorkingTime)
        {
            if (oldWorkingTime == null)
                return;
            if (this.WorkingTime != null)
                if (this.WorkingTime.Contains(oldWorkingTime))
                    this.WorkingTime.Remove(oldWorkingTime);
        }

        /// <summary>
        /// Remove all instances of WorkingTime from the collection
        /// </summary>
        /// <pdGenerated>Default removeAll</pdGenerated>
        public void RemoveAllWorkingTime()
        {
            if (WorkingTime != null)
                WorkingTime.Clear();
        }


    }
}