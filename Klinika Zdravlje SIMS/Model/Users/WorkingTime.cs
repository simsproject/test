// File:    WorkingTime.cs
// Author:  tomic
// Created: Tuesday, April 21, 2020 5:18:45 PM
// Purpose: Definition of Class WorkingTime

using System;
using System.Collections.Generic;

namespace Model.Users
{
   public class WorkingTime
   {
		long serialID;
        private DateTime beginTime;
        private DateTime endTime;
        private DateTime date;
           

		public WorkingTime(long serialID, DateTime beginTime, DateTime endTime, DateTime date)
        {
				this.SerialID = serialID;
            this.BeginTime = beginTime;
            this.EndTime = endTime;
            this.Date = date;
        }

	  	  public long SerialID { get => serialID; set => serialID = value; }
		  public DateTime BeginTime { get => beginTime; set => beginTime = value; }
          public DateTime EndTime { get => endTime; set => endTime = value; }
          public DateTime Date { get => date; set => date = value; }
       
    }
}