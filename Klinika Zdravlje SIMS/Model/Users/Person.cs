/***********************************************************************
 * Module:  Person.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Person
 ***********************************************************************/

using System;

namespace Model.Users
{
   public class Person
   {
        private String firstName;
        private String lastName;
        private DateTime dateOfBirth;
        private String personalID;
        private Gender gender;
        private String phoneNumber;
        private String email;

        private Address address;

        public Person(string firstName, string lastName, DateTime dateOfBirth, string personalID, Gender gender, string phoneNumber, string email, Address address)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.DateOfBirth = dateOfBirth;
            this.PersonalID = personalID;
            this.Gender = gender;
            this.PhoneNumber = phoneNumber;
            this.Email = email;
            this.Address = address;
        }

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public DateTime DateOfBirth { get => dateOfBirth; set => dateOfBirth = value; }
        public string PersonalID { get => personalID; set => personalID = value; }
        public Gender Gender { get => gender; set => gender = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Email { get => email; set => email = value; }
        public Address Address { get => address; set => address = value; }

    }
}