/***********************************************************************
 * Module:  Country.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Country
 ***********************************************************************/

using System;

namespace Model.Users
{
    public class Country
    {
        private String name;
        private String code;

        public Country(string name, string code)
        {
            this.name = name;
            this.code = code;
        }

        public string Name { get => name; set => name = value; }
        public string Code { get => code; set => code = value; }
    }
}