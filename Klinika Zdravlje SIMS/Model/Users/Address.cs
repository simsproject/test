/***********************************************************************
 * Module:  City.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class City
 ***********************************************************************/

using System;

namespace Model.Users
{
   public class Address
   {
        private long _id;
        private String _name;
        private String _postalCode;

        private City _city;

        public Address(string name, string postalCode, City city, long id)
        {
            Name = name;
            PostalCode = postalCode;
            City = city;
            Id = id;
        }

        public override string ToString()
        {
            return this.Name + " " + this.PostalCode;
        }

        public string Name { get => _name; set => _name = value; }
        public string PostalCode { get => _postalCode; set => _postalCode = value; }
        public City City { get => _city; set => _city = value; }
        public long Id { get => _id; set => _id = value; }
    }
}