/***********************************************************************
 * Module:  User.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class User
 ***********************************************************************/

using System;

namespace Model.Users
{
   public class User : Person
   {
        private String _userName;
        private String _password;

        public User(string firstName, string lastName, DateTime dateOfBirth, string personalID, Gender gender, string phoneNumber, string email, Address address, string userName, string password) : base(firstName, lastName, dateOfBirth, personalID, gender, phoneNumber, email, address)
        {
            _userName = userName;
            _password = password;
        }

        public string UserName { get => _userName; set => _userName = value; }
        public string Password { get => _password; set => _password = value; }
    }
}