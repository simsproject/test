/***********************************************************************
 * Module:  Patient.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Patient
 ***********************************************************************/

using Model.MedicalRecords;
using System;

namespace Model.Users
{
   public class Patient : User
   {
        private Boolean isGuest;

        private MedicalRecord medicalRecord;

        public Patient(string firstName, string lastName, DateTime dateOfBirth, string personalID, Gender gender, string phoneNumber, string email, Address address, string userName, string password, bool isGuest, MedicalRecord medicalRecord) : base(firstName, lastName, dateOfBirth, personalID, gender, phoneNumber, email, address, userName, password)
        {
            this.IsGuest = isGuest;
            this.MedicalRecord = medicalRecord;
        }

        public bool IsGuest { get => isGuest; set => isGuest = value; }
        public MedicalRecord MedicalRecord { get => medicalRecord; set => medicalRecord = value; }
    }
}