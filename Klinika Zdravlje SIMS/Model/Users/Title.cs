/***********************************************************************
 * Module:  Gender.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Gender
 ***********************************************************************/

using System;

namespace Model.Users
{
   public enum Title
   {
      DoctorOfGeneralPractice,
      DoctorSpecialist
   }
}