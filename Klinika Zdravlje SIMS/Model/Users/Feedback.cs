// File:    Feedback.cs
// Author:  Korisnik
// Created: Monday, April 20, 2020 2:56:47 PM
// Purpose: Definition of Class Feedback

using Model.Other;
using System;
using System.Collections.Generic;

namespace Model.Users
{
   public class Feedback
   {
        private int rate;
        private String comment;

        private User user;


        public Feedback(int rate, string comment, User user)
        {
            this.Rate = rate;
            this.Comment = comment;
            this.User = user;
        }


        public int Rate { get => rate; set => rate = value; }
        public string Comment { get => comment; set => comment = value; }
        public User User { get => user; set => user = value; }



    }
}