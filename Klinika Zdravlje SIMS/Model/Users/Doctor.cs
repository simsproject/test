/***********************************************************************
 * Module:  Doctor.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Doctor
 ***********************************************************************/

using Model.Rooms;
using System;

namespace Model.Users
{
   public class Doctor : User
   {
        private Title title;

        private WorkingPeriod[] workingPeriod;
        private MedicalRoom medicalRoom;

        public Doctor(Title title, WorkingPeriod[] workingPeriod, MedicalRoom medicalRoom, string firstName, string lastName, DateTime dateOfBirth, string personalID, Gender gender, string phoneNumber, string email, Address address, string userName, string password) : base(firstName, lastName, dateOfBirth, personalID, gender, phoneNumber, email, address, userName, password)
        {
            this.Title = title;
            this.WorkingPeriod = workingPeriod;
            this.MedicalRoom = medicalRoom;
        }
        public override string ToString()
        {
            return this.FirstName + " " + this.LastName;
        }
        public Title Title { get => title; set => title = value; }
        public WorkingPeriod[] WorkingPeriod { get => workingPeriod; set => workingPeriod = value; }
        public MedicalRoom MedicalRoom { get => medicalRoom; set => medicalRoom = value; }

    }
}