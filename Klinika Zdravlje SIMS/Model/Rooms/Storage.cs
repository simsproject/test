// File:    Storage.cs
// Author:  Korisnik
// Created: Monday, April 20, 2020 11:17:17 AM
// Purpose: Definition of Class Storage

using System;
using System.Collections.Generic;

namespace Model.Rooms
{
   public class Storage : Room
   {
        public Storage(string name,int floor) : base(name,floor) { 
        
        }
   }
}