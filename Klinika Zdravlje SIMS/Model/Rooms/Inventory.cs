/***********************************************************************
 * Module:  Inventory.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Inventory
 ***********************************************************************/

using System;

namespace Model.Rooms
{
   public class Inventory
   {
      private String name;
      private int quantity;
      private TypeOfInvetory type;
      private String description;
      private long serialID;

        private Room room;

        public string Name { get => name; set => name = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public TypeOfInvetory Type { get => type; set => type = value; }
        public string Description { get => description; set => description = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public Room Room { get => room; set => room = value; }

        public Inventory(string name, int quantity, TypeOfInvetory type, string description, long serialID, Room room)
        {
            this.Name = name;
            this.Quantity = quantity;
            this.Type = type;
            this.Description = description;
            this.SerialID = serialID;
            this.Room = room;
        }

    }
}