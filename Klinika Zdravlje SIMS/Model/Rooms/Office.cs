/***********************************************************************
 * Module:  Office.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Office
 ***********************************************************************/

using System;
using System.Collections.Generic;

namespace Model.Rooms
{
   public class Office : Room
   {

        public Office(string name, int floor) : base(name, floor) { 
            
        }
   }
}