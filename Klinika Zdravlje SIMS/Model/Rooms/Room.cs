/***********************************************************************
 * Module:  Room.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Room
 ***********************************************************************/

using System;
using System.Collections.Generic;

namespace Model.Rooms
{
   public class Room
   {
      private String name;
      private int floor;

        public string Name { get => name; set => name = value; }
        public int Floor { get => floor; set => floor = value; }

        public Room(string name, int floor)
        {
            this.Name = name;
            this.Floor = floor;
        }
        public override string ToString()
        {
            return this.name;
        }


    }
}