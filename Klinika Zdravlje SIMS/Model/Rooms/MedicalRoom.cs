/***********************************************************************
 * Module:  MedicalRoom.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class MedicalRoom
 ***********************************************************************/

using System;
using System.Collections.Generic;

namespace Model.Rooms
{
   public class MedicalRoom : Room
   {
      private TypeOfMedicalRoom type;

        public MedicalRoom(TypeOfMedicalRoom type,string name, int floor) : base(name, floor)
        {
            this.Type = type;
        }

        public TypeOfMedicalRoom Type { get => type; set => type = value; }
    }
}