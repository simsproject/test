/***********************************************************************
 * Module:  PatientRoom.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class PatientRoom
 ***********************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;

namespace Model.Rooms
{
   public class PatientRoom : Room
   {

        public PatientRoom(string name, int floor) : base(name, floor){

        }



        /// <summary>
        /// Property for collection of Bed
        /// </summary>
        /// <pdGenerated>Default opposite class collection property</pdGenerated>
        
   
   }
}