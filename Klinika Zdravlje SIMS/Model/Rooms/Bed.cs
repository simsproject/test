/***********************************************************************
 * Module:  Bed.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Bed
 ***********************************************************************/

using System;
using Model.Users;

namespace Model.Rooms
{
   public class Bed
   {
      private int serialID;

        private Model.Users.Patient patient;
        public PatientRoom patientRoom;

        public int SerialID { get => serialID; set => serialID = value; }
        public Patient Patient { get => patient; set => patient = value; }

        public Bed(int serialID, Patient patient, PatientRoom patientRoom)
        {
            this.SerialID = serialID;
            this.Patient = patient;
            this.patientRoom = patientRoom;
        }
        public override string ToString()
        {
            return this.serialID.ToString();
        }


        /// <summary>
        /// Property for PatientRoom
        /// </summary>
        /// <pdGenerated>Default opposite class property</pdGenerated>
        public PatientRoom PatientRoom
      {
         get
         {
            return patientRoom;
         }
         set
         {
            if (this.patientRoom == null || !this.patientRoom.Equals(value))
            {
               if (this.patientRoom != null)
               {
                  this.patientRoom = null;
                
               }
               if (value != null)
               {
                  this.patientRoom = value;
               }
            }
         }
      }


    }
}