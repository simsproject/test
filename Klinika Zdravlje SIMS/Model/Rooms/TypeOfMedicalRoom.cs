/***********************************************************************
 * Module:  Gender.cs
 * Author:  Korisnik
 * Purpose: Definition of the Class Gender
 ***********************************************************************/

using System;

namespace Model.Rooms
{
   public enum TypeOfMedicalRoom
   {
      OperationRoom,
      ExaminationRoom
   }
}