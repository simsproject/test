// File:    Renovation.cs
// Author:  Korisnik
// Created: Monday, April 20, 2020 11:28:59 AM
// Purpose: Definition of Class Renovation

using System;

namespace Model.Rooms
{
   public class Renovation
   {
      private DateTime beginDate;
      private DateTime endDate;
      private String description;
      private long serialID;
      
        
        public DateTime BeginDate { get => beginDate; set => beginDate = value; }
        public DateTime EndDate { get => endDate; set => endDate = value; }
        public string Description { get => description; set => description = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public Room Room;

        public Renovation(DateTime beginDate, DateTime endDate, string description, long serialID, Room room)
        {
            this.BeginDate = beginDate;
            this.EndDate = endDate;
            this.Description = description;
            this.SerialID = serialID;
            this.Room = room;
        }
    }
}