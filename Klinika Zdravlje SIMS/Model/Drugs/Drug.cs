// File:    Drug.cs
// Author:  Korisnik
// Created: Friday, April 17, 2020 5:27:18 PM
// Purpose: Definition of Class Drug

using Model.Users;
using System;

namespace Model.Drugs
{
   public class Drug
   {
		private String serialID;
		private String name;
      private DateTime expirationDate;
      private int amount;
      private String ingradients;
      private String description;
      private String instruction;
      private DrugState state;
       private Doctor doctor;

        public Drug(string serialID,string name, DateTime expirationDate, int amount, string ingradients, string description, string instruction, DrugState state, Doctor doctor)
        {

			this.SerialID = serialID;
			this.Name = name;
            this.ExpirationDate = expirationDate;
            this.Amount = amount;
            this.Ingradients = ingradients;
            this.Description = description;
            this.Instruction = instruction;
            this.State = state;
            this.Doc = doctor;
        }

	  	public string SerialID { get => serialID; set => serialID = value; }
		public string Name { get => name; set => name = value; }
        public DateTime ExpirationDate { get => expirationDate; set => expirationDate = value; }
        public int Amount { get => amount; set => amount = value; }
        public string Ingradients { get => ingradients; set => ingradients = value; }
        public string Description { get => description; set => description = value; }
        public string Instruction { get => instruction; set => instruction = value; }
        public DrugState State { get => state; set => state = value; }
        public Doctor Doc { get => doctor; set => doctor = value; }
    }
}