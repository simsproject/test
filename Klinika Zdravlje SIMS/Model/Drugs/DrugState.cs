// File:    DrugState.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 6:57:10 PM
// Purpose: Definition of Enum DrugState

using System;

namespace Model.Drugs
{
   public enum DrugState
   {
      sentForValidation,
      valid,
      waiting
   }
}