// File:    Appointment.cs
// Author:  Korisnik
// Created: Friday, April 17, 2020 3:54:44 PM
// Purpose: Definition of Class Appointment

using System;
using Model.Rooms;
using Model.Users;

namespace Model.Appointment
{
   public class Appointment
   {
      private DateTime startDate;
      private DateTime finishDate;
      private AppointmentType type;
      private long serialID;

        private Model.Users.Patient patient;
        private Model.Users.Doctor doctor;
        private Model.Rooms.MedicalRoom room;

        public DateTime StartDate { get => startDate; set => startDate = value; }
        public DateTime FinishDate { get => finishDate; set => finishDate = value; }
        public AppointmentType Type { get => type; set => type = value; }
        public long SerialID { get => serialID; set => serialID = value; }
        public Patient Patient { get => patient; set => patient = value; }
        public Doctor Doctor { get => doctor; set => doctor = value; }
        public MedicalRoom Room { get => room; set => room = value; }

        public Appointment(DateTime startDate, DateTime finishDate, AppointmentType type, long serialID, Patient patient, Doctor doctor, MedicalRoom room)
        {
            this.StartDate = startDate;
            this.FinishDate = finishDate;
            this.Type = type;
            this.SerialID = serialID;
            this.Patient = patient;
            this.Doctor = doctor;
            this.Room = room;
        }


    }
}