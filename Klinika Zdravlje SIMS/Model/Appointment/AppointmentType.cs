// File:    AppointmentType.cs
// Author:  Korisnik
// Created: Friday, April 17, 2020 4:27:41 PM
// Purpose: Definition of Enum AppointmentType

using System;

namespace Model.Appointment
{
   public enum AppointmentType
   {
      operation,
      regularAp,
      controlAp
   }
}