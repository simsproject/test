// File:    ICreateRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 9:13:45 PM
// Purpose: Definition of Interface ICreateRepository

using System;

namespace Repository.Abstract
{
   public interface ICreateRepository<E>
   {
      E Create(E entity);
   
   }
}