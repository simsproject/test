// File:    IMedicalRoomRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:38:23 PM
// Purpose: Definition of Interface IMedicalRoomRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IMedicalRoomRepository : Repository.Abstract.IRepository<MedicalRoom, String>,IGetByIDRepository<MedicalRoom,string>
   {
   }
}