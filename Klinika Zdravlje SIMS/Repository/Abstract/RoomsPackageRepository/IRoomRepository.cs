// File:    IRoomRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:38:25 PM
// Purpose: Definition of Interface IRoomRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IRoomRepository : Repository.Abstract.IRepository<Room, String>, Repository.Abstract.IGetByIDRepository<Room, String>
   {
   }
}