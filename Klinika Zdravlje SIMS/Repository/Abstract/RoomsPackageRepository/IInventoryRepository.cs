// File:    IInventoryRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:54:25 PM
// Purpose: Definition of Interface IInventoryRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IInventoryRepository : Repository.Abstract.IRepository<Inventory, long>, Repository.Abstract.IGetByIDRepository<Inventory, long>
   {
   }
}