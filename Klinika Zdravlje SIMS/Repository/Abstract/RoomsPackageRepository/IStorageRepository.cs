// File:    IStorageRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:38:21 PM
// Purpose: Definition of Interface IStorageRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IStorageRepository : Repository.Abstract.IRepository<Storage, String>, IGetByIDRepository<Storage, string>
    {
   }
}