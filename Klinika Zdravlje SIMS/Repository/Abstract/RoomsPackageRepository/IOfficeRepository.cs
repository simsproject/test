// File:    IOfficeRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:38:24 PM
// Purpose: Definition of Interface IOfficeRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IOfficeRepository : Repository.Abstract.IRepository<Office, String>,IGetByIDRepository<Office,string>
   {
   }
}