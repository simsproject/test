// File:    IPatientRoomRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 10:11:08 PM
// Purpose: Definition of Interface IPatientRoomRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IPatientRoomRepository : Repository.Abstract.IRepository<PatientRoom, String>, Repository.Abstract.IGetByIDRepository<PatientRoom,string>
   {
   }
}