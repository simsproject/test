// File:    IBedRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:51:52 PM
// Purpose: Definition of Interface IBedRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IBedRepository : Repository.Abstract.IRepository<Bed, int>, Repository.Abstract.IGetByIDRepository<Bed,int>
   {
   }
}