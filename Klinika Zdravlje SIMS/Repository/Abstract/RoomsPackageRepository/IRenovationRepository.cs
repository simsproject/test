// File:    IRenovationRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:54:24 PM
// Purpose: Definition of Interface IRenovationRepository

using Model.Rooms;
using System;

namespace Repository.Abstract.RoomsPackageRepository
{
   public interface IRenovationRepository : Repository.Abstract.ICreateRepository<Renovation>, Repository.Abstract.IDeleteRepository<Renovation>, Repository.Abstract.IGetAllRepository<Renovation>, Repository.Abstract.IGetByIDRepository<Renovation, long>
   {
   }
}