// File:    IUpdateRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 9:13:47 PM
// Purpose: Definition of Interface IUpdateRepository

using System;

namespace Repository.Abstract
{
   public interface IUpdateRepository<E>
   {
      void Update(E entity);
   
   }
}