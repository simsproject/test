// File:    IRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 9:07:49 PM
// Purpose: Definition of Interface IRepository

using System;

namespace Repository.Abstract
{
   public interface IRepository<E,ID> : IGetAllRepository<E>, ICreateRepository<E>, IDeleteRepository<E>, IUpdateRepository<E>
   {
   }
}