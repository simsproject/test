// File:    IDrugRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:50:10 PM
// Purpose: Definition of Interface IDrugRepository

using Model.Drugs;
using System;

namespace Repository.Abstract.DrugsPackageRepository
{
   public interface IDrugRepository : Repository.Abstract.IRepository<Drug, String>, Repository.Abstract.IGetByIDRepository<Drug, String>
    {
   }
}