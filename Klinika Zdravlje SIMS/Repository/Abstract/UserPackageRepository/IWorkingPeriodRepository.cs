// File:    IWorkingPeriodRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 9:05:06 PM
// Purpose: Definition of Interface IWorkingPeriodRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IWorkingPeriodRepository : Repository.Abstract.IRepository<WorkingPeriod,String>,IGetByIDRepository<WorkingPeriod,long>
   {
   }
}