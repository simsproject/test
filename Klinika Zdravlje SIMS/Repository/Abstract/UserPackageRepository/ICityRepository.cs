// File:    ICityRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:58:18 PM
// Purpose: Definition of Interface ICityRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
    public interface ICityRepository : Repository.Abstract.IGetAllRepository<City>, Repository.Abstract.IGetByIDRepository<City, long>
    {
    }
}