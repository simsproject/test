// File:    IPatientRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 10:07:42 PM
// Purpose: Definition of Interface IPatientRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IPatientRepository : Repository.Abstract.IRepository<Patient,String>, Repository.Abstract.IGetByIDRepository<Patient,String>
   {
   }
}