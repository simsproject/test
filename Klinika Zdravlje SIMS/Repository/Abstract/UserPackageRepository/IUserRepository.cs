// File:    IUserRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 10:11:07 PM
// Purpose: Definition of Interface IUserRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IUserRepository : Repository.Abstract.IRepository<User,String>, Repository.Abstract.IGetByIDRepository<User,String>
   {
   }
}