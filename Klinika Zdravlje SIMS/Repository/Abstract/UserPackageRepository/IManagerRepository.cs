// File:    IManagerRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 9:33:44 PM
// Purpose: Definition of Interface IManagerRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IManagerRepository : Repository.Abstract.IUpdateRepository<Manager>, Repository.Abstract.IGetAllRepository<Manager>, IGetByIDRepository<Manager,string>
   {
   }
}