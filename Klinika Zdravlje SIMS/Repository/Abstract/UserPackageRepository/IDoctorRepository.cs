// File:    IDoctorRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 9:33:26 PM
// Purpose: Definition of Interface IDoctorRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IDoctorRepository : Repository.Abstract.IRepository<Doctor, String>, Repository.Abstract.IGetByIDRepository<Doctor, String>
   {
   }
}