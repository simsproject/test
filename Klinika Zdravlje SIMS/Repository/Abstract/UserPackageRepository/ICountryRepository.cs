// File:    ICountryRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:58:18 PM
// Purpose: Definition of Interface ICountryRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface ICountryRepository : Repository.Abstract.IGetAllRepository<Country>, Repository.Abstract.IGetByIDRepository<Country, string>
   {
   }
}