// File:    IWorkingTimeRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 9:06:02 PM
// Purpose: Definition of Interface IWorkingTimeRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IWorkingTimeRepository : IRepository<WorkingTime,String>,IGetByIDRepository<WorkingTime,long>
   {
   }
}