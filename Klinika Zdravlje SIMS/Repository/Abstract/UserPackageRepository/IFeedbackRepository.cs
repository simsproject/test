// File:    IFeedbackRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:58:16 PM
// Purpose: Definition of Interface IFeedbackRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IFeedbackRepository : Repository.Abstract.IGetAllRepository<Feedback>, Repository.Abstract.ICreateRepository<Feedback>
   {
   }
}