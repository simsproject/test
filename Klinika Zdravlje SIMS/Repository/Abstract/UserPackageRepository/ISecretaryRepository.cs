// File:    ISecretaryRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 10:07:44 PM
// Purpose: Definition of Interface ISecretaryRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface ISecretaryRepository : Repository.Abstract.IRepository<Secretary,String>, Repository.Abstract.IGetByIDRepository<Secretary,String>
   {
   }
}