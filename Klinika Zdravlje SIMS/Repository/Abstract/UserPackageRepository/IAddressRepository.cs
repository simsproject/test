// File:    IAddressRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:58:17 PM
// Purpose: Definition of Interface IAddressRepository

using Model.Users;
using System;

namespace Repository.Abstract.UserPackageRepository
{
   public interface IAddressRepository : Repository.Abstract.IGetAllRepository<Address>, Repository.Abstract.IGetByIDRepository<Address,long>
   {
   }
}