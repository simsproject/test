// File:    IDeleteRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 9:13:46 PM
// Purpose: Definition of Interface IDeleteRepository

using System;

namespace Repository.Abstract
{
   public interface IDeleteRepository<E>
   {
      void Delete(E entity);
   
   }
}