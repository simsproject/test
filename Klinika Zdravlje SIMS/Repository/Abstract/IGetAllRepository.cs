// File:    IGetAllRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 9:13:44 PM
// Purpose: Definition of Interface IGetAllRepository

using System;
using System.Collections.Generic;

namespace Repository.Abstract
{
   public interface IGetAllRepository<E>
   {
      IEnumerable<E> GetAll();
   
   }
}