// File:    ICourseOfDiseaseRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:50:11 PM
// Purpose: Definition of Interface ICourseOfDiseaseRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface ICourseOfDiseaseRepository : Repository.Abstract.ICreateRepository<CourseOfDisease>, Repository.Abstract.IGetAllRepository<CourseOfDisease>, Repository.Abstract.IGetByIDRepository<CourseOfDisease, long>
   {
   }
}