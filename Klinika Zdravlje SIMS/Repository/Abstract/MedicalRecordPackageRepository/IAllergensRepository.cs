// File:    IAllergensRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:42:38 PM
// Purpose: Definition of Interface IAllergensRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface IAllergensRepository : Repository.Abstract.ICreateRepository<Allergens>, Repository.Abstract.IGetAllRepository<Allergens>, Repository.Abstract.IGetByIDRepository<Allergens, String>
   {
   }
}