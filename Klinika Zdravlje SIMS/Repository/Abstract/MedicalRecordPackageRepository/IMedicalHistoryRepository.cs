// File:    IMedicalHistoryRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:45:14 PM
// Purpose: Definition of Interface IMedicalHistoryRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface IMedicalHistoryRepository : Repository.Abstract.IUpdateRepository<MedicalHistory>, Repository.Abstract.ICreateRepository<MedicalHistory>, Repository.Abstract.IGetAllRepository<MedicalHistory>, Repository.Abstract.IGetByIDRepository<MedicalHistory, long>
   {
   }
}