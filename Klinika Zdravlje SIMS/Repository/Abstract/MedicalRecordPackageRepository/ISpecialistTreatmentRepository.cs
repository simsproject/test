// File:    ISpecialistTreatmentRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:50:10 PM
// Purpose: Definition of Interface ISpecialistTreatmentRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface ISpecialistTreatmentRepository : Repository.Abstract.IGetAllRepository<SpecialistTreatment>, Repository.Abstract.IGetByIDRepository<SpecialistTreatment, long>, Repository.Abstract.ICreateRepository<SpecialistTreatment>
   {
   }
}