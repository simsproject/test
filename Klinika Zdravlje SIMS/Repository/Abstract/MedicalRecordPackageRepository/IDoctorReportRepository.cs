// File:    IDoctorReportRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:50:12 PM
// Purpose: Definition of Interface IDoctorReportRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface IDoctorReportRepository : Repository.Abstract.ICreateRepository<DoctorReport>, Repository.Abstract.IGetAllRepository<DoctorReport>, Repository.Abstract.IGetByIDRepository<DoctorReport, long>
   {
   }
}