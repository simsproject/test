// File:    IHospitalTreatmentRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:50:11 PM
// Purpose: Definition of Interface IHospitalTreatmentRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface IHospitalTreatmentRepository : Repository.Abstract.ICreateRepository<HospitalTreatment>, Repository.Abstract.IGetByIDRepository<HospitalTreatment, long>, Repository.Abstract.IGetAllRepository<HospitalTreatment>
   {
   }
}