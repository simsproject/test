// File:    IRecipeRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:50:11 PM
// Purpose: Definition of Interface IRecipeRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface IRecipeRepository : Repository.Abstract.ICreateRepository<Recipe>, Repository.Abstract.IGetAllRepository<Recipe>, Repository.Abstract.IGetByIDRepository<Recipe, long>
   {
   }
}