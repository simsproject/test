// File:    IMedicalRecordRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 7:42:36 PM
// Purpose: Definition of Interface IMedicalRecordRepository

using Model.MedicalRecords;
using System;

namespace Repository.Abstract.MedicalRecordPackageRepository
{
   public interface IMedicalRecordRepository : Repository.Abstract.IUpdateRepository<MedicalRecord>, Repository.Abstract.IGetByIDRepository<MedicalRecord, long>, Repository.Abstract.IGetAllRepository<MedicalRecord>, Repository.Abstract.ICreateRepository<MedicalRecord>
   {
   }
}