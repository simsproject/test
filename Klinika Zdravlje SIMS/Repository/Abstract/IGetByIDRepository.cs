// File:    IGetByIDRepository.cs
// Author:  Korisnik
// Created: Tuesday, May 26, 2020 11:55:15 AM
// Purpose: Definition of Interface IGetByIDRepository

using System;

namespace Repository.Abstract
{
   public interface IGetByIDRepository<E,ID>
   {
      E GetByID(ID id);
   
   }
}