// File:    IAppointmentRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 10:11:06 PM
// Purpose: Definition of Interface IAppointmentRepository

using Model.Appointment;
using System;

namespace Repository.Abstract.AppointmentPackageRepository
{
   public interface IAppointmentRepository : Repository.Abstract.IRepository<Appointment,long>, Repository.Abstract.IGetByIDRepository<Appointment,long>
   {
   }
}