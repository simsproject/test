// File:    INotificationRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:51:52 PM
// Purpose: Definition of Interface INotificationRepository

using Model.Other;
using System;

namespace Repository.Abstract.OtherPackageRepository
{
   public interface INotificationRepository : Repository.Abstract.IGetAllRepository<Notification>, Repository.Abstract.ICreateRepository<Notification>
   {
   }
}