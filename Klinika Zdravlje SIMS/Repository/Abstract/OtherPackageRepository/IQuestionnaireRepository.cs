// File:    IQuestionaireRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:51:51 PM
// Purpose: Definition of Interface IQuestionaireRepository

using Model.Other;
using System;

namespace Repository.Abstract.OtherPackageRepository
{
   public interface IQuestionnaireRepository : Repository.Abstract.IGetAllRepository<Questionnaire>, Repository.Abstract.ICreateRepository<Questionnaire>, Repository.Abstract.IGetByIDRepository<Questionnaire, long>
   {
   }
}