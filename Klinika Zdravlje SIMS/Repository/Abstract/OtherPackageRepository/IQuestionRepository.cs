// File:    IQuestionRepository.cs
// Author:  Korisnik
// Created: Friday, May 22, 2020 10:11:07 PM
// Purpose: Definition of Interface IQuestionRepository

using Model.Other;
using System;

namespace Repository.Abstract.OtherPackageRepository
{
   public interface IQuestionRepository : Repository.Abstract.IGetAllRepository<Question>, Repository.Abstract.ICreateRepository<Question>,Repository.Abstract.IGetByIDRepository<Question, long>
   {
   }
}