// File:    IBlogRepository.cs
// Author:  Korisnik
// Created: Saturday, May 23, 2020 8:51:51 PM
// Purpose: Definition of Interface IBlogRepository

using Model.Other;
using System;

namespace Repository.Abstract.OtherPackageRepository
{
   public interface IBlogRepository : Repository.Abstract.IRepository<Blog, long>, Repository.Abstract.IGetByIDRepository<Blog, long>
   {
   }
}