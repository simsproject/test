// File:    BedRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:54:13 PM
// Purpose: Definition of Class BedRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;
using Repository.Abstract.RoomsPackageRepository;
using Repository.Csv;
using Service.RoomsPackageService;

namespace Repository
{
   public class BedRepository : IBedRepository
   {
      private String filePath= "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\bed.csv";
      private GenericCSVFile<Bed> genericCSVRepository;
      
      public GenericCSVFileFactory<Bed> genericCSVFileFactory;

        private static BedRepository instance;
        public static BedRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new BedRepository();
            }
            return instance;
        }
        public BedRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Bed>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);

        }

        public IEnumerable<Bed> GetAll()
        => genericCSVRepository.Load(filePath);

        public Bed Create(Bed entity)
        {
				
				genericCSVRepository.AppendToFile(filePath, entity);
				return entity;
		  }

        public void Delete(Bed entity)
        {
				var listOfEntites = genericCSVRepository.Load(filePath);
				var entityToRemove = listOfEntites.SingleOrDefault(Bed => GetByID(Bed.SerialID).SerialID == entity.SerialID);

				if (entityToRemove != null)
				{
					listOfEntites.Remove(entityToRemove);
					genericCSVRepository.SaveAll(filePath, listOfEntites);

				}
		  }

        public void Update(Bed entity)
        {
				var listOfEntites = genericCSVRepository.Load(filePath);
				listOfEntites[listOfEntites.FindIndex(Bed => (Bed.SerialID == entity.SerialID))] = entity;
				genericCSVRepository.SaveAll(filePath, listOfEntites);
	     }

        public Bed GetByID(int id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}