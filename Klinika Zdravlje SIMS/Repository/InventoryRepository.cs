// File:    InventoryRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:54:14 PM
// Purpose: Definition of Class InventoryRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;

namespace Repository
{
   public class InventoryRepository : Repository.Abstract.RoomsPackageRepository.IInventoryRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\inventory.csv";
      private Repository.Csv.GenericCSVFile<Inventory> genericCSVRepository;
      
      public GenericCSVFileFactory<Inventory> genericCSVFileFactory;


        private static InventoryRepository instance;
        public static InventoryRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new InventoryRepository();
            }
            return instance;
        }

        public InventoryRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Inventory>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<Inventory> GetAll()
         => genericCSVRepository.Load(filePath);

        public Inventory Create(Inventory entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(Inventory entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Inventory => GetByID(Inventory.SerialID).SerialID == entity.SerialID);

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(Inventory entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(Inventory => (Inventory.SerialID == entity.SerialID))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public Inventory GetByID(long id)
         => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}