// File:    CourseOfDiseaseRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:36:53 PM
// Purpose: Definition of Class CourseOfDiseaseRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Repository.Abstract.MedicalRecordPackageRepository;
using Repository.Csv;

namespace Repository
{
   public class CourseOfDiseaseRepository : ICourseOfDiseaseRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\courseOfDisesase.csv";
      private GenericCSVFile<CourseOfDisease> genericCSVRepository;
      
      public GenericCSVFileFactory<CourseOfDisease> genericCSVFileFactory;

        private static CourseOfDiseaseRepository instance;
        public static CourseOfDiseaseRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new CourseOfDiseaseRepository();
            }
            return instance;
        }

        public CourseOfDiseaseRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<CourseOfDisease>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public CourseOfDisease Create(CourseOfDisease entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public IEnumerable<CourseOfDisease> GetAll()
            => genericCSVRepository.Load(filePath);

        public CourseOfDisease GetByID(long id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}