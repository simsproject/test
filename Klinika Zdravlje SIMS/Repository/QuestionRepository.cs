// File:    QuestionRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:48:52 PM
// Purpose: Definition of Class QuestionRepository

using Model.Other;
using Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
   public class QuestionRepository : Repository.Abstract.OtherPackageRepository.IQuestionRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\questions.csv";
      private Repository.Csv.GenericCSVFile<Question> genericCSVRepository;
      
      public GenericCSVFileFactory<Question> genericCSVFileFactory;

        private static QuestionRepository instance;
        public static QuestionRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new QuestionRepository();
            }
            return instance;
        }

        public QuestionRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Question>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

       

        public Question Create(Question entity)
        {
            entity.Id = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().Id + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public Question GetByID(long id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Id == id);

        public IEnumerable<Question> GetAll()
        => genericCSVRepository.Load(filePath);
    }
}