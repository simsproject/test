// File:    OfficeRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:57:30 PM
// Purpose: Definition of Class OfficeRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;
using Repository.Csv;

namespace Repository
{
   public class OfficeRepository : Repository.Abstract.RoomsPackageRepository.IOfficeRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\office.csv";
      private GenericCSVFile<Office> genericCSVRepository;
      
      public GenericCSVFileFactory<Office> genericCSVFileFactory;

        private static OfficeRepository instance;
        public static OfficeRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new OfficeRepository();
            }
            return instance;
        }

        public OfficeRepository()
		{
			genericCSVFileFactory = new GenericCSVFileFactory<Office>();
			genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
		}

		public IEnumerable<Office> GetAll()
		 => genericCSVRepository.Load(filePath);

        public Office Create(Office entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(Office entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Office => GetByID(Office.Name).Name.Equals(entity.Name));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(Office entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(Office => (Office.Name.Equals(entity.Name)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

	  	  public Office GetByID(string id)
            => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Name.Equals(id));


    }
}