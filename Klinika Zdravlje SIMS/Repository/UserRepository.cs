// File:    UserRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:55 PM
// Purpose: Definition of Class UserRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository.Csv;

namespace Repository
{
   public class UserRepository : Repository.Abstract.UserPackageRepository.IUserRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\user.csv";
      private GenericCSVFile<User> genericCSVRepository;
      public GenericCSVFileFactory<User> genericCSVFileFactory;

        private static UserRepository instance;
        public static UserRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new UserRepository();
            }
            return instance;
        }


        public UserRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<User>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<User> GetAll()
            => genericCSVRepository.Load(filePath);

        public User Create(User entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(User entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(User => GetByID(User.UserName).UserName.Equals(entity.UserName));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(User entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(User => (User.UserName.Equals(entity.UserName)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public User GetByID(string id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.UserName.Equals(id));
    }
}