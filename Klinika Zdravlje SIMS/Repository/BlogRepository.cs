// File:    BlogRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:48:54 PM
// Purpose: Definition of Class BlogRepository

using System;
using System.Collections.Generic;
using Model.Other;
using Repository.Abstract.OtherPackageRepository;
using Repository.Csv;

namespace Repository
{
   public class BlogRepository : IBlogRepository
   {
      private String filePath;
      private GenericCSVFile<Blog> genericCSVRepository;
      
      public GenericCSVFileFactory<Blog> genericCSVFileFactory;

        public IEnumerable<Blog> GetAll()
        {
            throw new NotImplementedException();
        }

        public Blog Create(Blog entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Blog entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Blog entity)
        {
            throw new NotImplementedException();
        }

        public Blog GetByID(long id)
        {
            throw new NotImplementedException();
        }
    }
}