// File:    MedicalRoomRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:54:14 PM
// Purpose: Definition of Class MedicalRoomRepository

using Model.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
   public class MedicalRoomRepository : Repository.Abstract.RoomsPackageRepository.IMedicalRoomRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\medicalRooms.csv";
      private Repository.Csv.GenericCSVFile<MedicalRoom> genericCSVRepository;
      
      public GenericCSVFileFactory<MedicalRoom> genericCSVFileFactory;

        private static MedicalRoomRepository instance;
        public static MedicalRoomRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new MedicalRoomRepository();
            }
            return instance;
        }

        public MedicalRoomRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<MedicalRoom>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<MedicalRoom> GetAll()
            => genericCSVRepository.Load(filePath);

        public MedicalRoom Create(MedicalRoom entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }
        public void Delete(MedicalRoom entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(MedicalRoom => GetByID(MedicalRoom.Name).Name.Equals(entity.Name));

            if(entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }

            


        }

        public void Update(MedicalRoom entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(MedicalRoom => (MedicalRoom.Name.Equals(entity.Name)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public MedicalRoom GetByID(string id)
          => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Name.Equals(id));
    }
}