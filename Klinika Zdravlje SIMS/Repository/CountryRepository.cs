// File:    CountryRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:47 PM
// Purpose: Definition of Class CountryRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository.Abstract.UserPackageRepository;
using Repository.Csv;

namespace Repository
{
   public class CountryRepository : ICountryRepository
    { 
   private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\country.csv";
    private GenericCSVFile<Country> genericCSVRepository;

    public GenericCSVFileFactory<Country> genericCSVFileFactory;



        private static CountryRepository instance;
        public static CountryRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new CountryRepository();
            }
            return instance;
        }
        public CountryRepository()
    {
        genericCSVFileFactory = new GenericCSVFileFactory<Country>();
        genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
    }

    public IEnumerable<Country> GetAll()
        => genericCSVRepository.Load(filePath);

    public Country GetByID(string id)
    => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Code.Equals(id));

    }
}