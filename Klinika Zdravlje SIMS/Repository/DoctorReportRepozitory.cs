// File:    DoctorReportRepozitory.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:46:21 PM
// Purpose: Definition of Class DoctorReportRepozitory

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Repository.Abstract.MedicalRecordPackageRepository;
using Repository.Csv;

namespace Repository
{
   public class DoctorReportRepozitory : IDoctorReportRepository
   {
        private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\doctorReport.csv";
      private GenericCSVFile<DoctorReport> genericCSVRepository;
      
      public GenericCSVFileFactory<DoctorReport> genericCSVFileFactory;

        private static DoctorReportRepozitory instance;
        public static DoctorReportRepozitory GetInstance()
        {
            if (instance == null)
            {
                instance = new DoctorReportRepozitory();
            }
            return instance;
        }


        public DoctorReportRepozitory()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<DoctorReport>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public DoctorReport Create(DoctorReport entity)
        {
				entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
				genericCSVRepository.AppendToFile(filePath, entity);
				return entity;
		  }

        public IEnumerable<DoctorReport> GetAll()
				=> genericCSVRepository.Load(filePath);

        public DoctorReport GetByID(long id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}