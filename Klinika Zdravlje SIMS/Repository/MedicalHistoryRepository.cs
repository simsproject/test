// File:    MedicalHistoryRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:42:55 PM
// Purpose: Definition of Class MedicalHistoryRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;

namespace Repository
{
   public class MedicalHistoryRepository : Repository.Abstract.MedicalRecordPackageRepository.IMedicalHistoryRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\medicalHistory.csv";
      private Repository.Csv.GenericCSVFile<MedicalHistory> genericCSVRepository;
      
      public GenericCSVFileFactory<MedicalHistory> genericCSVFileFactory;

        private static MedicalHistoryRepository instance;
        public static MedicalHistoryRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new MedicalHistoryRepository();
            }
            return instance;
        }

        public MedicalHistoryRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<MedicalHistory>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public void Update(MedicalHistory entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(MedicalHistory => (MedicalHistory.SerialID == entity.SerialID))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public MedicalHistory Create(MedicalHistory entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public IEnumerable<MedicalHistory> GetAll()
        => genericCSVRepository.Load(filePath);

        public MedicalHistory GetByID(long id)
       => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}