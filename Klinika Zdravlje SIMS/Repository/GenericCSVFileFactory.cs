// File:    GenericCSVFileFactory.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:22:32 AM
// Purpose: Definition of Class GenericCSVFileFactory

using KlinikaZdravljeCD.Repository.Csv;
using Repository.Csv;
using System;
using System.Collections.Generic;

namespace Repository
{
    public class GenericCSVFileFactory<E>
    {
        
      
        public GenericCSVFile<E> GetGenericCSVFIle(String filePath)
      {
        
            switch (filePath)
            {
                

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\manager.csv":

                    return (GenericCSVFile<E>)new ManagerCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\address.csv":
                    return (GenericCSVFile<E>)new AddressCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\city.csv":
                    return (GenericCSVFile<E>)new CityCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\country.csv":
                    return (GenericCSVFile<E>)new CountryCSVRepository();
               
                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\allergens.csv":
                    return (GenericCSVFile<E>)new AllergensCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\medicalRecord.csv":
                    return (GenericCSVFile<E>)new MedicalRecordCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\rooms.csv":
                    return (GenericCSVFile<E>)new RoomCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\inventory.csv":
                    return (GenericCSVFile<E>)new InventoryCSVRepository();

			    case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\secretary.csv":
				    return (GenericCSVFile<E>)new SecretaryCSVRepository();

			    case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\workingTime.csv":
			   	    return (GenericCSVFile<E>)new WorkingTimeCSVRepository();

			    case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\workingPeriod.csv":
			        return (GenericCSVFile<E>)new WorkingPeriodCSVRepository();

			    case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\office.csv":
				    return (GenericCSVFile<E>)new OfficeCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\bed.csv":
                    return (GenericCSVFile<E>)new BedCSVRepository();
                
                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\patientRooms.csv":
                    return (GenericCSVFile<E>)new PatientRoomCSVRepository();
                
                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\patients.csv":
                    return (GenericCSVFile<E>)new PatientCSVRepository();


                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\appointment.csv":
                    return (GenericCSVFile<E>)new AppointmentCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\doctor.csv":
                    return (GenericCSVFile<E>)new DoctorCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\medicalRooms.csv":
                    return (GenericCSVFile<E>)new MedicalRoomCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\drugs.csv":
                    return (GenericCSVFile<E>)new DrugCSVRepository();
                
                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\medicalHistory.csv":
                    return (GenericCSVFile<E>)new MedicalHistoryCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\recipe.csv":
                    return (GenericCSVFile<E>)new RecipeCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\specialistTreatment.csv":
                    return (GenericCSVFile<E>)new SpecialistTreatmentCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\courseOfDisesase.csv":
                    return (GenericCSVFile<E>)new CourseOfDiseaseCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\doctorReport.csv":
                    return (GenericCSVFile<E>)new DoctorReportCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\user.csv":
                    return (GenericCSVFile<E>)new UserCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\hospitalTreatment.csv":
                    return (GenericCSVFile<E>)new HospitalTreatmentCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\questions.csv":
                    return (GenericCSVFile<E>)new QuestionCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\questionnaires.csv":
                    return (GenericCSVFile<E>)new QuestionnaireCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\feedback.csv":
                    return (GenericCSVFile<E>)new FeedbackCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\renovation.csv":
                    return (GenericCSVFile<E>)new RenovationCSVRepository();
               
                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\notification.csv":
                    return (GenericCSVFile<E>)new NotificationCSVRepository();

                case "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\storage.csv":
                    return (GenericCSVFile<E>)new StorageCSVRepository();

            }
            return (GenericCSVFile<E>)new ManagerCSVRepository();
        }


	}
}