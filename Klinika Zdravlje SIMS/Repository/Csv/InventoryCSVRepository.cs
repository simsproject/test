// File:    InventoryCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:17 AM
// Purpose: Definition of Class InventoryCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class InventoryCSVRepository : GenericCSVFile<Inventory>
	{
		private readonly InventoryCSVConverter _converter;

		public InventoryCSVRepository()
		{
			_converter = new InventoryCSVConverter();
		}

		public void AppendToFile(string path, Inventory entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<Inventory> Load(string path)
		    => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Inventory> entities)
		    => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}