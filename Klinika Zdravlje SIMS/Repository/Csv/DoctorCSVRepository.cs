// File:    DoctorCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:15 AM
// Purpose: Definition of Class DoctorCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class DoctorCSVRepository : GenericCSVFile<Doctor>
	{
		private readonly DoctorCSVConverter _converter;

		public DoctorCSVRepository()
		{
			_converter = new DoctorCSVConverter();
		}

		public void AppendToFile(string path, Doctor entity)
			 => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Doctor> Load(string path)
			 => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Doctor> entities)
			 => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}