﻿using Model.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class StorageCSVConverter : ICSVConverter<Storage>
    {
        private const string CSV_DELIMITER = ",";
        private readonly string _delimiter;

        public StorageCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Storage ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Storage(
                tokens[0],
                int.Parse(tokens[1])
                               );
        }

        public string ConverterEntityToFormat(Storage entity)
        {
            string[] words = { entity.Name, entity.Floor.ToString() };
            return string.Join(_delimiter, words); 
        }
    }
}
