﻿using Model.Appointment;
using Model.Rooms;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class AppointmentCSVConverter : ICSVConverter<Appointment>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy. HH:mm";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public AppointmentCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public Appointment ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Appointment(
                DateTime.Parse(tokens[0]),
                DateTime.Parse(tokens[1]),
                (AppointmentType)Enum.Parse(typeof(AppointmentType), tokens[2], true),
                long.Parse(tokens[3]),
                GetPatient(tokens[4]),
                GetDoctor(tokens[5]),
                GetMedicalRoom(tokens[6])
                );
        }

        public Patient GetPatient(string patientID)
        {
            PatientRepository patientRepository = PatientRepository.GetInstance();
            return patientRepository.GetByID(patientID);
        }

        public Doctor GetDoctor(string doctorID)
        {
            DoctorRepository doctorRepository = DoctorRepository.GetInstance();
            return doctorRepository.GetByID(doctorID);
        }

        public MedicalRoom GetMedicalRoom(string medicalRoomID)
        {
            MedicalRoomRepository medicalRoomRepository = MedicalRoomRepository.GetInstance();
            return medicalRoomRepository.GetByID(medicalRoomID);
        }


        public string ConverterEntityToFormat(Appointment entity)
        {
            
            string[] words = { entity.StartDate.ToString(_datetimeFormat), entity.FinishDate.ToString(_datetimeFormat), entity.Type.ToString(), entity.SerialID.ToString(), entity.Patient.PersonalID, entity.Doctor.PersonalID, entity.Room.Name };
            return string.Join(_delimiter, words);
        }
    }
}
