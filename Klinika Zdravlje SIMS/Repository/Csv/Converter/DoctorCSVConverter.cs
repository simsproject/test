﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Rooms;
using Model.Users;
using Repository;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class DoctorCSVConverter : ICSVConverter<Doctor>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public DoctorCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public Doctor ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray()); 
            return new Doctor(
                tokens[0].Equals("DoctorSpecialist") ? Title.DoctorSpecialist : Title.DoctorOfGeneralPractice
                , GetWorkingPeriods(tokens[1])
                ,GetMedicalRoom(tokens[2])
                , tokens[3]
                ,tokens[4]
                ,DateTime.Parse(tokens[5])
                ,tokens[6]
                ,tokens[7].Equals("Female") ? Gender.Female : Gender.Male
                ,tokens[8]
                ,tokens[9]
                ,GetAddress(tokens[10])
                ,tokens[11]
                ,tokens[12]
                );
        }

        public MedicalRoom GetMedicalRoom(string medicalRoomID) {
            MedicalRoomRepository medicalRoomRepository = MedicalRoomRepository.GetInstance();
            return medicalRoomRepository.GetByID(medicalRoomID);
        }

        public Address GetAddress(string addresID) {
            AddressRepository addressRepository = AddressRepository.GetInstance();
            return addressRepository.GetByID(long.Parse(addresID));
        }

        public WorkingPeriod[] GetWorkingPeriods(String arrayOfWorkingPeriods) {
            WorkingPeriodRepository workingPeriodRepository = WorkingPeriodRepository.GetInstance();
            WorkingPeriod[] workingPeriods = new WorkingPeriod[arrayOfWorkingPeriods.Split('-').Length];
            for (int i = 0; i < arrayOfWorkingPeriods.Split('-').Length; i++) {
                workingPeriods[i] = workingPeriodRepository.GetByID(long.Parse(arrayOfWorkingPeriods.Split('-')[i]));
            }
            return workingPeriods;
        }

        public string ConverterEntityToFormat(Doctor entity)
        {
            string[] words = {entity.Title.ToString(), WorkingPeriodStringFormat(entity),entity.MedicalRoom.Name,entity.FirstName,entity.LastName,entity.DateOfBirth.ToString(_datetimeFormat),entity.PersonalID,entity.Gender.ToString(),entity.PhoneNumber,entity.Email,entity.Address.Id.ToString(),entity.UserName,entity.Password};
            return string.Join(_delimiter, words);
        }

        public string WorkingPeriodStringFormat(Doctor entity) {
            string workingPeriod = "";
            foreach (WorkingPeriod wp in entity.WorkingPeriod)
                workingPeriod += wp.SerialID.ToString() + "-";
            return workingPeriod.Remove(workingPeriod.Length - 1, 1);
        }
    }
}
