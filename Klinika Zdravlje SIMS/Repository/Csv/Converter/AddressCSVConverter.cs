﻿using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class AddressCSVConverter : ICSVConverter<Address>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public AddressCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Address ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Address(
                tokens[0],
                tokens[1],
                GetCity(tokens[2]),
                long.Parse(tokens[3]));
        }

        public City GetCity(string cityID) {
            CityRepository cityRepository = CityRepository.GetInstance();
            return cityRepository.GetByID(long.Parse(cityID));
        }


        public string ConverterEntityToFormat(Address entity)
        {
            string[] words = { entity.Name, entity.PostalCode, entity.City.PostalCode.ToString(), entity.Id.ToString() };
            return string.Join(_delimiter, words);

        }
    }
}
