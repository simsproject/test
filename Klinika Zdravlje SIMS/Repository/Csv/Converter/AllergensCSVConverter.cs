﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.MedicalRecords;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class AllergensCSVConverter : ICSVConverter<Allergens>
    {
        private const string CSV_DELIMITER = ",";
        private readonly string _delimiter;


        public AllergensCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Allergens ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Allergens(tokens[0]);
        }
        public string ConverterEntityToFormat(Allergens entity)
        {
            string[] words = { entity.Name };
            return string.Join(_delimiter, words); 
        }
    }
}
