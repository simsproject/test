﻿using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Converter
{
	class SecretaryCSVConverter : ICSVConverter<Secretary>
	{
		private const string CSV_DELIMITER = ",";
		private const string DATETIME_FORMAT = "dd.MM.yyyy.";
		private readonly string _delimiter;
		private readonly string _datetimeFormat;

		public SecretaryCSVConverter()
		{
			_delimiter = CSV_DELIMITER;
			_datetimeFormat = DATETIME_FORMAT;
		}

		public Secretary ConverterCSVFormatToEntity(string entityCSVFormat)
		{
			string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
			return new Secretary(
				tokens[0],
				tokens[1],
				DateTime.Parse(tokens[2]),
				tokens[3],
				(Gender)Enum.Parse(typeof(Gender), tokens[4], true),
				tokens[5],
				tokens[6],
				GetAddress(tokens[7]),
				tokens[8],
				tokens[9],
				GetWorkingPeriods(tokens[10]),
				GetOffice(tokens[11])
				);
		}

		public string ConverterEntityToFormat(Secretary entity)
		{
			string[] words = { entity.FirstName, entity.LastName, entity.DateOfBirth.ToString(_datetimeFormat),entity.PersonalID,entity.Gender.ToString(),entity.PhoneNumber,entity.Email,entity.Address.Id.ToString(),entity.UserName,entity.Password, WorkingPeriodsStringFormat(entity),entity.Office.Name};
			return string.Join(_delimiter, words);
		}

		private Address GetAddress(String addressID)
		{
			AddressRepository addressRepository =  AddressRepository.GetInstance();
			return addressRepository.GetByID(long.Parse(addressID));
		}

		private WorkingPeriod[] GetWorkingPeriods(String workingPeriodsIds)
		{
			WorkingPeriodRepository workingPeriodRepository = WorkingPeriodRepository.GetInstance();
		
			String[] idsArray = workingPeriodsIds.Split('-');
			WorkingPeriod[] workingPeriodsArray = new WorkingPeriod[idsArray.Length];
			for (int i = 0; i < idsArray.Length; i++)
				workingPeriodsArray[i] = workingPeriodRepository.GetByID(long.Parse(idsArray[i]));
			return workingPeriodsArray;
		}

		private Office GetOffice(String officeId)
		{
			OfficeRepository officeRepository =  OfficeRepository.GetInstance();
			return officeRepository.GetByID(officeId);
		}

		private String WorkingPeriodsStringFormat(Secretary entity) {
			string workingPeriod = "";
			foreach (WorkingPeriod wp in entity.WorkingPeriod)
				workingPeriod += wp.SerialID.ToString() + "-";
			return workingPeriod.Remove(workingPeriod.Length - 1, 1);
		}
	}
}
