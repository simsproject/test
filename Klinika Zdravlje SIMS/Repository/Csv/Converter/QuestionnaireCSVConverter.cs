﻿using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class QuestionnaireCSVConverter : ICSVConverter<Questionnaire>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public QuestionnaireCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Questionnaire ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            Doctor doctor = null;
            if (!tokens[4].Equals("NEMA_DOKTORA"))
            {
                doctor = GetDoctor(tokens[4]);
            }
            return new Questionnaire(
                tokens[0],
                tokens[1],
                long.Parse(tokens[2]),
                GetPatient(tokens[3]),
                doctor
                );
        }

        public Doctor GetDoctor(string doctorID)
        {
            DoctorRepository doctorRepository = DoctorRepository.GetInstance();
            return doctorRepository.GetByID(doctorID);
        }

        public Patient GetPatient(string patientID)
        {
            PatientRepository patientRepository = PatientRepository.GetInstance();
            return patientRepository.GetByID(patientID);
        }

        public string ConverterEntityToFormat(Questionnaire entity)
        {
            string doctor = "";
            if (entity.Doctor == null) doctor = "NEMA_DOKTORA";
            else doctor = entity.Doctor.PersonalID;

            string[] words = {entity.Comment, entity.Name, entity.SerialID.ToString(), entity.Patient.PersonalID, doctor};
            return string.Join(_delimiter, words);
        }
    }
}
