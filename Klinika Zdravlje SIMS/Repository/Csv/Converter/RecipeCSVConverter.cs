﻿using Model.MedicalRecords;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class RecipeCSVConverter : ICSVConverter<Recipe>
    {

        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy. HH:mm";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public RecipeCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public Recipe ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Recipe(
                DateTime.Parse(tokens[0]),
                tokens[1],
                int.Parse(tokens[2]),
                tokens[3],
                long.Parse(tokens[4]),
                GetDoctor(tokens[5]),
                GetMedicalRecord(tokens[6])
                );
        }

        public Doctor GetDoctor(string doctorID)
        {
            DoctorRepository doctorRepository = DoctorRepository.GetInstance();
            return doctorRepository.GetByID(doctorID);
        }

        public MedicalRecord GetMedicalRecord(string medicalRecordID)
        {
            MedicalRecordRepository medicalRecordRepository = MedicalRecordRepository.GetInstance();
            return medicalRecordRepository.GetByID(long.Parse(medicalRecordID));
        }

        public string ConverterEntityToFormat(Recipe entity)
        {
            string[] words = { entity.DateOfCreate.ToString(_datetimeFormat), entity.Amount, entity.DrugID.ToString(), entity.Description, entity.SerialID.ToString(), entity.Doctor.PersonalID, entity.MedicalRecord.SerialID.ToString() };
            return string.Join(_delimiter, words);
        }
    }
}
