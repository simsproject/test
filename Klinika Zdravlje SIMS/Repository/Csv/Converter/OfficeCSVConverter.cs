﻿using Model.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
	class OfficeCSVConverter : ICSVConverter<Office>
	{
		private const string CSV_DELIMITER = ",";
		private readonly string _delimiter;

		public OfficeCSVConverter()
		{
			_delimiter = CSV_DELIMITER;
		}

		public Office ConverterCSVFormatToEntity(string entityCSVFormat)
		{
			string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
			return new Office(
                tokens[0],
				int.Parse(tokens[1])
									);
		}

		public string ConverterEntityToFormat(Office entity)
		{
			string[] words = { entity.Name, entity.Floor.ToString() };
			return string.Join(_delimiter, words);
		}
	}
}
