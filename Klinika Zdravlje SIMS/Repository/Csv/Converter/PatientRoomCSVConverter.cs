﻿using Model.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class PatientRoomCSVConverter : ICSVConverter<PatientRoom>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public PatientRoomCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public PatientRoom ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new PatientRoom(
                tokens[0],
                int.Parse(tokens[1])
                );
        }

        public string ConverterEntityToFormat(PatientRoom entity)
        {
            string[] words = {entity.Name, entity.Floor.ToString()};
            return string.Join(_delimiter, words);
        }
    }
}
