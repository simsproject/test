﻿using Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
	class WorkingTimeCSVConverter : ICSVConverter<WorkingTime>
	{
		private const string CSV_DELIMITER = ",";
		private const string DATETIME_FORMAT = "HH:mm";
        private const string DATE_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
		private readonly string _datetimeFormat;
        private readonly string _dateFormat;

        public WorkingTimeCSVConverter()
		{
			_delimiter = CSV_DELIMITER;
			_datetimeFormat = DATETIME_FORMAT;
            _dateFormat = DATE_FORMAT;

		}

		public WorkingTime ConverterCSVFormatToEntity(string entityCSVFormat)
		{
				string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
				return new WorkingTime(
				long.Parse(tokens[0]),
				DateTime.Parse(tokens[1]),
				DateTime.Parse(tokens[2]),
				DateTime.Parse(tokens[3])
			);
		
		}

		public string ConverterEntityToFormat(WorkingTime entity)
		{
			string[] words = { entity.SerialID.ToString(), entity.BeginTime.ToString(_datetimeFormat), entity.EndTime.ToString(_datetimeFormat), entity.Date.ToString(_dateFormat)};
			return string.Join(_delimiter, words);
		}

	}
    
}
