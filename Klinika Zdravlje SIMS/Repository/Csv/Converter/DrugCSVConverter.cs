﻿using Model.Drugs;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
	
	


	class DrugCSVConverter : ICSVConverter<Drug>
	{
		private const string CSV_DELIMITER = ",";
		private const string DATETIME_FORMAT = "dd.MM.yyyy.";
		private readonly string _delimiter;
		private readonly string _datetimeFormat;

		public DrugCSVConverter()
		{
			_delimiter = CSV_DELIMITER;
			_datetimeFormat = DATETIME_FORMAT;
		}

		public Drug ConverterCSVFormatToEntity(string entityCSVFormat)
		{
            
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
			return new Drug(

				tokens[0],
				tokens[1],
				DateTime.Parse(tokens[2]),
				int.Parse(tokens[3]),
				tokens[4],
				tokens[5],
				tokens[6],
			    (DrugState)Enum.Parse(typeof(DrugState), tokens[7], true),
                GetDoctor(tokens[8])
				);
			
		}

		public Doctor GetDoctor(string doctorID)
		{
			DoctorRepository doctorRepository = DoctorRepository.GetInstance();
			return doctorRepository.GetByID(doctorID);
		}

		public string ConverterEntityToFormat(Drug entity)
		{
			string[] words = { entity.SerialID, entity.Name, entity.ExpirationDate.ToString(_datetimeFormat), entity.Amount.ToString(), entity.Ingradients, entity.Description, entity.Instruction, entity.State.ToString(), entity.Doc.PersonalID.ToString() };
			return string.Join(_delimiter, words);
		}

       
	}
}
