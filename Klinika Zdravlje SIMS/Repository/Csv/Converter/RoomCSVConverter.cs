﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Rooms;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class RoomCSVConverter : ICSVConverter<Room>
    {
        private const string CSV_DELIMITER = ",";
        private readonly string _delimiter;


        public RoomCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Room ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Room(
                tokens[0],
                int.Parse(tokens[1])
                );
        }

        public string ConverterEntityToFormat(Room entity)
        {
            string[] words = { entity.Name,entity.Floor.ToString() };
            return string.Join(_delimiter, words);
        }
    }
}
