﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Rooms;
using Repository;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class InventoryCSVConverter : ICSVConverter<Inventory>
    {
        private const string CSV_DELIMITER = ",";
        private readonly string _delimiter;


        public InventoryCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }


        public Inventory ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Inventory(
                tokens[0]
                ,int.Parse(tokens[1])
                ,tokens[2].Equals("Stable") ? TypeOfInvetory.Stable : TypeOfInvetory.Consumable
                ,tokens[3]
                ,long.Parse(tokens[4])
                ,GetRoom(tokens[5])
                );
        }

        public Room GetRoom(string roomID) {
            RoomRepository roomRepository = RoomRepository.GetInstance();
            return roomRepository.GetByID(roomID);
        }

        public string ConverterEntityToFormat(Inventory entity)
        {
            string[] words = { entity.Name,entity.Quantity.ToString(),entity.Type.ToString(),entity.Description,entity.SerialID.ToString(),entity.Room.Name};
            return string.Join(_delimiter, words);
        }
    }
}
