﻿using Model.Other;
using Repository;
using System;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class QuestionCSVConverter : ICSVConverter<Question>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public QuestionCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Question ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Question(
                tokens[0],
                tokens[1],
                GetQuestionnaire(tokens[2]),
                long.Parse(tokens[3])
                );
        }

        public Questionnaire GetQuestionnaire(string questionnaireID) {
            QuestionnaireRepository questionnaireRepository = QuestionnaireRepository.GetInstance();
            return questionnaireRepository.GetByID(long.Parse(questionnaireID));
        }

        public string ConverterEntityToFormat(Question entity)
        {
            string[] words = {entity.Questions, entity.Answer, entity.Questionnaire.SerialID.ToString(), entity.Id.ToString() };
            return string.Join(_delimiter,words);
        }
    }
}
