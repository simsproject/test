﻿using Model.MedicalRecords;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class MedicalRecordCSVConverter : ICSVConverter<MedicalRecord>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public MedicalRecordCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public MedicalRecord ConverterCSVFormatToEntity(string entityCSVFormat)
        {
           
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            if (tokens[1] != "")
            {
                return new MedicalRecord(
                    long.Parse(tokens[0]),
                    GetAllergens(tokens[1])
                    );
            } else
            {
                return new MedicalRecord(
                    long.Parse(tokens[0]),
                    new List<Allergens>()
                    );
            }
        }

        private List<Allergens> GetAllergens(String allergensId)
        {
            List<Allergens> allergensList = new List<Allergens>();
            AllergensRepository allergensRepository = AllergensRepository.GetInstance();
            String[] allergens = allergensId.Split('-');
            foreach (String a in allergens)
            {
                allergensList.Add(allergensRepository.GetByID(a));
            }
            return allergensList;
        }

        private String AllergensStringFormat(MedicalRecord entity)
        {
            string allergens = "";
            foreach (Allergens al in entity.Allergens)
                allergens += al.Name + "-";
            return entity.Allergens.Count!=0 ? allergens.Remove(allergens.Length - 1, 1) : "";
        }



        public string ConverterEntityToFormat(MedicalRecord entity)
        {

            string[] words = {entity.SerialID.ToString(), AllergensStringFormat(entity)};
            return string.Join(_delimiter, words);

        }
    }
    
}
