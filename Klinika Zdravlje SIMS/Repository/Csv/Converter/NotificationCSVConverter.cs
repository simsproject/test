﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Other;
using Model.Users;
using Repository;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class NotificationCSVConverter : ICSVConverter<Notification>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy. HH:mm";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;


        public NotificationCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public Notification ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Notification(tokens[0]
                ,DateTime.Parse(tokens[1])
                ,GetUser(tokens[2]));
        }

        public User GetUser(string userID)
        {
            UserRepository userRepository = UserRepository.GetInstance();
            return userRepository.GetByID(userID);
        }

        public string ConverterEntityToFormat(Notification entity)
        {
            string[] words = { entity.Text, entity.Date.ToString(_datetimeFormat), entity.User.UserName };
            return string.Join(_delimiter, words);
        }
    }
}
