﻿using Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class CountryCSVConverter : ICSVConverter<Country>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;


        public CountryCSVConverter()
        {
            _delimiter = CSV_DELIMITER;

        }

        public Country ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Country(
                tokens[0],
                tokens[1]
                );
        }

        public string ConverterEntityToFormat(Country entity)
        {
            string[] words = { entity.Name, entity.Code };
            return string.Join(_delimiter, words);
        }
    }
}
