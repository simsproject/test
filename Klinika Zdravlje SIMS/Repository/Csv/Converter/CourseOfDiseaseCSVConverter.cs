﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.MedicalRecords;
using Repository;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class CourseOfDiseaseCSVConverter : ICSVConverter<CourseOfDisease>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;



        public CourseOfDiseaseCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }


        public CourseOfDisease ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new CourseOfDisease(
                DateTime.Parse(tokens[0])
                ,tokens[1]
                ,tokens[2]
                ,long.Parse(tokens[3])
                ,GetMedicalHistory(tokens[4])
                );
        }

        public MedicalHistory GetMedicalHistory(string medicalHistoryID) {
            MedicalHistoryRepository medicalHistoryRepository = MedicalHistoryRepository.GetInstance();
            return medicalHistoryRepository.GetByID(long.Parse(medicalHistoryID));
        }

        public string ConverterEntityToFormat(CourseOfDisease entity)
        {
            string[] words = {entity.Date.ToString(_datetimeFormat),entity.Therapy,entity.Note,entity.SerialID.ToString(),entity.MedicalHistory.SerialID.ToString()};
            return string.Join(_delimiter, words);
        }
    }
}
