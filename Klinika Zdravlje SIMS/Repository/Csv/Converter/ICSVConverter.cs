﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
	public interface ICSVConverter<E> where E : class
	{
		string ConverterEntityToFormat(E entity);

		E ConverterCSVFormatToEntity(string entityCSVFormat);


	}
}
