﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Users;
using Repository;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class FeedbackCSVConverter : ICSVConverter<Feedback>
    {

        private const string CSV_DELIMITER = ",";
        private readonly string _delimiter;

        public FeedbackCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Feedback ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Feedback(
                int.Parse(tokens[0])
                ,tokens[1]
                ,GetUser(tokens[2])
                );
        }

        public User GetUser(string userID) {
            UserRepository userRepository = UserRepository.GetInstance();
            return userRepository.GetByID(userID);
        }

        public string ConverterEntityToFormat(Feedback entity)
        {
            string[] words = {entity.Rate.ToString(),entity.Comment,entity.User.UserName};
            return string.Join(_delimiter, words);
        }
    }
}
