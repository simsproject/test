﻿using Model.MedicalRecords;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class DoctorReportCSVConverter : ICSVConverter<DoctorReport>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy. HH:mm";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public DoctorReportCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public DoctorReport ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new DoctorReport(
                tokens[0],
                DateTime.Parse(tokens[1]),
                tokens[2],
                tokens[3],
                tokens[4],
                long.Parse(tokens[5]),
                GetMedicalRecord(tokens[6]),
                GetDoctor(tokens[7])
                );
        }

        public MedicalRecord GetMedicalRecord(string medicalRecordID) {
            MedicalRecordRepository medicalRecordRepository = MedicalRecordRepository.GetInstance();
            return medicalRecordRepository.GetByID(long.Parse(medicalRecordID));
        }

        public Doctor GetDoctor(string doctorID) {
            DoctorRepository doctorRepository = DoctorRepository.GetInstance();
            return doctorRepository.GetByID(doctorID);
        }

        public string ConverterEntityToFormat(DoctorReport entity)
        {
            string[] words = {entity.Name, entity.Date.ToString(_datetimeFormat),entity.DoctorOpinion,entity.DiagnosisAndTeraphy,entity.Anamnesis, entity.SerialID.ToString(), entity.MedicalRecord.SerialID.ToString(),entity.Doctor.PersonalID };
            return string.Join(_delimiter,words);
        }
    }
}
