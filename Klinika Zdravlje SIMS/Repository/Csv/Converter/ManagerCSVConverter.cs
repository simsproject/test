﻿using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class ManagerCSVConverter : ICSVConverter<Manager>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public ManagerCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public Manager ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Manager(
                tokens[0],
                tokens[1],
                DateTime.Parse(tokens[2]),
                tokens[3],
                tokens[4].Equals(Gender.Female.ToString()) ? Gender.Female : Gender.Male,
                tokens[5],
                tokens[6],
                GetAddress(tokens[7]),
                tokens[8],
                tokens[9]
                );
        }

        public Address GetAddress(string addresID)
        {
            AddressRepository addressRepository = AddressRepository.GetInstance();
            return addressRepository.GetByID(long.Parse(addresID));
        }

        public string ConverterEntityToFormat(Manager entity)
        {
            string[] words = {entity.FirstName, entity.LastName,entity.DateOfBirth.ToString(_datetimeFormat),entity.PersonalID, entity.Gender.ToString(), entity.PhoneNumber, entity.Email, entity.Address.Id.ToString(), entity.UserName,entity.Password};
            return string.Join(_delimiter, words);
        }

    }
}

