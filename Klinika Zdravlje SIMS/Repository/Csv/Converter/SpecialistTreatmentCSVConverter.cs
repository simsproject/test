﻿using Model.Appointment;
using Model.MedicalRecords;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
	class SpecialistTreatmentCSVConverter : ICSVConverter<SpecialistTreatment>
	{
		private const string CSV_DELIMITER = ",";
		private readonly string _delimiter;

		public SpecialistTreatmentCSVConverter()
		{
			_delimiter = CSV_DELIMITER;
		}

		public SpecialistTreatment ConverterCSVFormatToEntity(string entityCSVFormat)
		{
			string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
			return new SpecialistTreatment(
				long.Parse(tokens[0]),
				tokens[1],
				GetAppointment(tokens[2]),
				GetMedicalRecord(tokens[3])
                );
		}

		public string ConverterEntityToFormat(SpecialistTreatment entity)
		{
			string[] words = { entity.SerialID.ToString(), entity.Note, entity.Appointment.SerialID.ToString(), entity.MedicalRecord.SerialID.ToString()};
			return string.Join(_delimiter, words);
		}

		public Appointment GetAppointment(string appointmentID)
		{
			AppointmentRepository appointmentRepository =  AppointmentRepository.GetInstance();
			return appointmentRepository.GetByID(long.Parse(appointmentID));
		}

		public MedicalRecord GetMedicalRecord(string medicalRecordID)
		{
			MedicalRecordRepository medicalRecordRepository =  MedicalRecordRepository.GetInstance();
			return medicalRecordRepository.GetByID(long.Parse(medicalRecordID));
		}
	}
}
