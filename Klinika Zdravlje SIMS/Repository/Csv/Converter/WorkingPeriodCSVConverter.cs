﻿using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
	class WorkingPeriodCSVConverter : ICSVConverter<WorkingPeriod>
	{
		private const string CSV_DELIMITER = ",";
		private const string DATETIME_FORMAT = "dd.MM.yyyy.";
		private readonly string _delimiter;
		private readonly string _datetimeFormat;

		public WorkingPeriodCSVConverter()
		{
			_delimiter = CSV_DELIMITER;
			_datetimeFormat = DATETIME_FORMAT;
		}

		public WorkingPeriod ConverterCSVFormatToEntity(string entityCSVFormat)
		{
			string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
			return new WorkingPeriod(
					long.Parse(tokens[0]),
					DateTime.Parse(tokens[1]),
					DateTime.Parse(tokens[2]),
					GetWorkingTimes(tokens[3])
		            );
		}

		public string ConverterEntityToFormat(WorkingPeriod entity)
		{
			string[] words = { entity.SerialID.ToString(), entity.Begin.ToString(_datetimeFormat), entity.End.ToString(_datetimeFormat), WorkingTimesStringFormat(entity) };
			return string.Join(_delimiter, words);
		}

		private List<WorkingTime> GetWorkingTimes(String workingTimesId)
		{
			List<WorkingTime> timesList = new List<WorkingTime>();
			WorkingTimeRepository workingTimeRepository = WorkingTimeRepository.GetInstance();
			String[] workingTimes = workingTimesId.Split('-');
			foreach (String d in workingTimes)
			{
				timesList.Add(workingTimeRepository.GetByID(long.Parse(d)));
			}
			return timesList;
		}

		private String WorkingTimesStringFormat(WorkingPeriod entity)
		{
			string workingTime = "";
			foreach (WorkingTime wt in entity.WorkingTime)
				workingTime += wt.SerialID.ToString() + "-";
			return workingTime.Remove(workingTime.Length - 1, 1);
		}

	}
}
