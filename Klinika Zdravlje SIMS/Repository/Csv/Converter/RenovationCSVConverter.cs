﻿using Model.Rooms;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class RenovationCSVConverter : ICSVConverter<Renovation>
    {

        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public RenovationCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public Renovation ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Renovation(
                DateTime.Parse(tokens[0]),
                DateTime.Parse(tokens[1]),
                tokens[2],
                long.Parse(tokens[3]),
                GetRoom(tokens[4])
                );
        }

        public Room GetRoom(string roomID)
        {
            RoomRepository roomRepository = RoomRepository.GetInstance();
            return roomRepository.GetByID(roomID);
        }

        public string ConverterEntityToFormat(Renovation entity)
        {
            string[] words = { entity.BeginDate.ToString(_datetimeFormat), entity.EndDate.ToString(_datetimeFormat), entity.Description.ToString(), entity.SerialID.ToString(), entity.Room.Name.ToString() };
            return string.Join(_delimiter, words);
        }
    }
}
