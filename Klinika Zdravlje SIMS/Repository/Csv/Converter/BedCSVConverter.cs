﻿using Model.Rooms;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class BedCSVConverter : ICSVConverter<Bed>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public BedCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public Bed ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Bed(
                int.Parse(tokens[0]),
                GetPatient(tokens[1]),
                GetPatientRoom(tokens[2])
                );
        }


        public PatientRoom GetPatientRoom(string patientRoomID)
        {
            PatientRoomRepository patientRoomRepository = PatientRoomRepository.GetInstance();
            return patientRoomRepository.GetByID(patientRoomID);
        }

        public Patient GetPatient(string patientID)
        {
            PatientRepository patientRepository = PatientRepository.GetInstance();
            return patientRepository.GetByID(patientID);
        }

        public string ConverterEntityToFormat(Bed entity)
        {
            string[] words = {entity.SerialID.ToString(), entity.Patient == null ? "" : entity.Patient.PersonalID, entity.PatientRoom.Name};
            return string.Join(_delimiter, words);
        }


    }
}
