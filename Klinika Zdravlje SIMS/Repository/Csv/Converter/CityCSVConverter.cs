﻿using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class CityCSVConverter : ICSVConverter<City>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public CityCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }

        public City ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new City(
                tokens[0],
                long.Parse(tokens[1]),
                GetCountry(tokens[2])
                );
        }

        public Country GetCountry(string countryID) {
            CountryRepository countryRepository = CountryRepository.GetInstance();
            return countryRepository.GetByID(countryID);
        }

        public string ConverterEntityToFormat(City entity)
        {
            string[] words = { entity.Name, entity.PostalCode.ToString(), entity.Country.Code };
            return string.Join(_delimiter, words);
        }
    }
}
