﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.MedicalRecords;
using Repository;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class MedicalHistoryCSVConverter : ICSVConverter<MedicalHistory>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

      

        public MedicalHistoryCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }


        public MedicalHistory ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new MedicalHistory(
                tokens[0]
                ,DateTime.Parse(tokens[1])
                ,tokens[2]
                ,tokens[3]
                ,tokens[4]
                ,tokens[5]
                ,long.Parse(tokens[6])
                ,GetMedicalRecord(tokens[7])
                );
        }

        public MedicalRecord GetMedicalRecord(string medicalRecordID) {
            MedicalRecordRepository medicalRecordRepository = MedicalRecordRepository.GetInstance();
            return medicalRecordRepository.GetByID(long.Parse(medicalRecordID));
        }

        public string ConverterEntityToFormat(MedicalHistory entity)
        {
            string[] words = {entity.Description,entity.DateBegin.ToString(_datetimeFormat),entity.DateEnd,entity.ReasonOfHospitalization,entity.ReasonOfDeath, entity.ReasonOfRelase,entity.SerialID.ToString(),entity.MedicalRecord.SerialID.ToString()};
            return string.Join(_delimiter, words);
        }
    }
}
