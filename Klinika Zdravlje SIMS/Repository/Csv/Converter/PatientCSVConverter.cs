﻿using Model.MedicalRecords;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class PatientCSVConverter : ICSVConverter<Patient>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public PatientCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }

        public Patient ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new Patient(
                tokens[0],
                tokens[1],
                DateTime.Parse(tokens[2]),
                tokens[3],
                Gender.Female.ToString().Equals(tokens[4]) ? Gender.Female : Gender.Male,
                tokens[5],
                tokens[6],
                GetAddress(tokens[7]),
                tokens[8],
                tokens[9],
                bool.TrueString.Equals(tokens[10]),
                GetMedicalRecord(tokens[11])
                );
                
        }

        public string ConverterEntityToFormat(Patient entity)
        {
            string[] words = { entity.FirstName, entity.LastName, entity.DateOfBirth.ToString(_datetimeFormat), entity.PersonalID, entity.Gender.ToString(), entity.PhoneNumber, entity.Email,AddressStringFormat(entity) , entity.UserName, entity.Password, entity.IsGuest.ToString(), entity.MedicalRecord.SerialID.ToString() };
            return string.Join(_delimiter, words);
        }

        public MedicalRecord GetMedicalRecord(string medicalRecordID)
        {
            MedicalRecordRepository medicalRecordRepository = MedicalRecordRepository.GetInstance();
            return medicalRecordRepository.GetByID(long.Parse(medicalRecordID));
        }

        private string AddressStringFormat(Patient entity)
			{
				if (entity.Address == null)
					return "";

				return entity.Address.Id.ToString();
			}

		private Address GetAddress(string idAddress)
		{
			Address address = null;
			AddressRepository addressRepository = AddressRepository.GetInstance();
			if (!idAddress.Equals(""))
			{
				address = addressRepository.GetByID(long.Parse(idAddress));
			}
			return address;
		}
    }
}
