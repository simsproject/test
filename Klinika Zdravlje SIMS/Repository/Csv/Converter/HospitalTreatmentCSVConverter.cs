﻿using Model.MedicalRecords;
using Model.Rooms;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class HospitalTreatmentCSVConverter : ICSVConverter<HospitalTreatment>
    {
        private const string CSV_DELIMITER = ",";
        private const string DATETIME_FORMAT = "dd.MM.yyyy.";
        private readonly string _delimiter;
        private readonly string _datetimeFormat;

        public HospitalTreatmentCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
            _datetimeFormat = DATETIME_FORMAT;
        }


        public HospitalTreatment ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
           
            
            return new HospitalTreatment(
                DateTime.Parse(tokens[0]),
                DateTime.Parse(tokens[1]),
                tokens[2],
                long.Parse(tokens[3]),
                GetPatientRoom(tokens[4]),
                GetMedicalRecord(tokens[5])
                );
        }

       

        public PatientRoom GetPatientRoom(string patientRoomID)
        {
            PatientRoomRepository patientRoomRepository =  PatientRoomRepository.GetInstance();
            return patientRoomRepository.GetByID(patientRoomID);
        }


        public MedicalRecord GetMedicalRecord(string medicalRecordID)
        {
            MedicalRecordRepository medicalRecordRepository = new MedicalRecordRepository();
            return medicalRecordRepository.GetByID(long.Parse(medicalRecordID));
        }

        public string ConverterEntityToFormat(HospitalTreatment entity)
        {
            string[] words = { entity.StartDate.ToString(_datetimeFormat), entity.EndDate.ToString(_datetimeFormat), entity.Note, entity.SerialID.ToString(), entity.PatientRoom.Name, entity.MedicalRecord.SerialID.ToString() };
            return string.Join(_delimiter, words);
        }

    }
}
