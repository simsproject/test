﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model.Rooms;

namespace KlinikaZdravljeCD.Repository.Csv.Converter
{
    class MedicalRoomCSVConverter : ICSVConverter<MedicalRoom>
    {
        private const string CSV_DELIMITER = ",";

        private readonly string _delimiter;

        public MedicalRoomCSVConverter()
        {
            _delimiter = CSV_DELIMITER;
        }
        public MedicalRoom ConverterCSVFormatToEntity(string entityCSVFormat)
        {
            string[] tokens = entityCSVFormat.Split(_delimiter.ToCharArray());
            return new MedicalRoom(
                tokens[0].Equals("ExaminationRoom") ? TypeOfMedicalRoom.ExaminationRoom : TypeOfMedicalRoom.OperationRoom
                ,tokens[1]
                ,int.Parse(tokens[2])
                );
        }

        public string ConverterEntityToFormat(MedicalRoom entity)
        {
            string[] words = {entity.Type.ToString(), entity.Name, entity.Floor.ToString() };
            return string.Join(_delimiter, words);
        }
    }
}
