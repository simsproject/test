// File:    CityCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:13 AM
// Purpose: Definition of Class CityCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class CityCSVRepository : GenericCSVFile<City>
	{
        private readonly CityCSVConverter _converter;

        public CityCSVRepository()
        {
            _converter = new CityCSVConverter();
        }

        public void AppendToFile(string path, City entity)
				=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		  public List<City> Load(string path)
			   => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<City> entities)
				=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}