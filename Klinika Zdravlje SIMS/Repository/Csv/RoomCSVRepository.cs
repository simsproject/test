// File:    RoomCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:02 AM
// Purpose: Definition of Class RoomCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class RoomCSVRepository : GenericCSVFile<Room>
	{
		private readonly RoomCSVConverter _converter;

		public RoomCSVRepository()
		{
			_converter = new RoomCSVConverter();
		}

		public void AppendToFile(string path, Room entity)
			=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Room> Load(string path)
		   => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Room> entities)
		   => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}