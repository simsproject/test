// File:    CountryCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:14 AM
// Purpose: Definition of Class CountryCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class CountryCSVRepository : GenericCSVFile<Country>
	{
		private readonly CountryCSVConverter _converter;

		public CountryCSVRepository()
		{
			_converter = new CountryCSVConverter();
		}

		public void AppendToFile(string path, Country entity)
			=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Country> Load(string path)
			=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Country> entities)
			=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}