// File:    DrugCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:16 AM
// Purpose: Definition of Class DrugCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Drugs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class DrugCSVRepository : GenericCSVFile<Drug>
	{

		private readonly DrugCSVConverter _converter;

		public DrugCSVRepository()
		{
			_converter = new DrugCSVConverter();
		}

		public void AppendToFile(string path, Drug entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<Drug> Load(string path)
		    => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Drug> entities)
		    => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}