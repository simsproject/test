// File:    WorkingTimeCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:05 AM
// Purpose: Definition of Class WorkingTimeCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class WorkingTimeCSVRepository : GenericCSVFile<WorkingTime>
	{
		private readonly WorkingTimeCSVConverter _converter;

		public WorkingTimeCSVRepository()
		{
			_converter = new WorkingTimeCSVConverter();
		}

		public void AppendToFile(string path, WorkingTime entity)
			=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<WorkingTime> Load(string path)
			=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<WorkingTime> entities)
			=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}