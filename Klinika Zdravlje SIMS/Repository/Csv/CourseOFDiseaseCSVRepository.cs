﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;

namespace Repository.Csv
{
    class CourseOfDiseaseCSVRepository : GenericCSVFile<CourseOfDisease>
    {
        private readonly CourseOfDiseaseCSVConverter _converter;

        public CourseOfDiseaseCSVRepository()
        {
            _converter = new CourseOfDiseaseCSVConverter();
        }

        public void AppendToFile(string path, CourseOfDisease entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<CourseOfDisease> Load(string path)
            => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<CourseOfDisease> entities)
            => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}
