// File:    QuestionCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:01 AM
// Purpose: Definition of Class QuestionCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Other;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class QuestionCSVRepository : GenericCSVFile<Question>
	{
		private readonly QuestionCSVConverter _converter;

		public QuestionCSVRepository()
		{
			_converter=new  QuestionCSVConverter();
		}

		public void AppendToFile(string path, Question entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public void SaveAll(string path, List<Question> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());

		public List<Question> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();
	}
}