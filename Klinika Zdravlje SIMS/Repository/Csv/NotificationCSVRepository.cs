// File:    NotificationCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:21 AM
// Purpose: Definition of Class NotificationCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Other;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class NotificationCSVRepository : GenericCSVFile<Notification>
	{
		private readonly NotificationCSVConverter _converter;

		public NotificationCSVRepository()
		{
			_converter = new NotificationCSVConverter();
		}

		public void AppendToFile(string path, Notification entity)
		 => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);
		public List<Notification> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Notification> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}