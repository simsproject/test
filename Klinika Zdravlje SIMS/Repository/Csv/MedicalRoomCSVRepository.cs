// File:    MedicalRoomCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:20 AM
// Purpose: Definition of Class MedicalRoomCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class MedicalRoomCSVRepository : GenericCSVFile<MedicalRoom>
	{
		private readonly MedicalRoomCSVConverter _converter;

		public MedicalRoomCSVRepository()
		{
			_converter = new MedicalRoomCSVConverter();
		}

		public void AppendToFile(string path, MedicalRoom entity)
        => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<MedicalRoom> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<MedicalRoom> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}