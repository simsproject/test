// File:    SpecialistTreatmentCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:03 AM
// Purpose: Definition of Class SpecialistTreatmentCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class SpecialistTreatmentCSVRepository : GenericCSVFile<SpecialistTreatment>
	{

		private readonly SpecialistTreatmentCSVConverter _converter;

		public SpecialistTreatmentCSVRepository()
		{
			_converter = new SpecialistTreatmentCSVConverter();
		}

		public void AppendToFile(string path, SpecialistTreatment entity)
			=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<SpecialistTreatment> Load(string path)
			=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<SpecialistTreatment> entities)
			=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}