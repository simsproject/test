// File:    UserCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:04 AM
// Purpose: Definition of Class UserCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class UserCSVRepository : GenericCSVFile<User>
	{
        private readonly UserCSVConverter _converter;

        public UserCSVRepository()
        {
            _converter = new UserCSVConverter();
        }

        public void AppendToFile(string path, User entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<User> Load(string path)
            => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<User> entities)
            => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}