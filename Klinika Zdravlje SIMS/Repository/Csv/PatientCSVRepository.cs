// File:    PatientCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:23 AM
// Purpose: Definition of Class PatientCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class PatientCSVRepository : GenericCSVFile<Patient>
	{
		private readonly PatientCSVConverter _converter;

		public PatientCSVRepository()
		{
			_converter = new PatientCSVConverter();
		}

		public void AppendToFile(string path, Patient entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Patient> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Patient> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}