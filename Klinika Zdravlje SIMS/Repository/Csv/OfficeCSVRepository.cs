// File:    OfficeCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:22 AM
// Purpose: Definition of Class OfficeCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class OfficeCSVRepository : GenericCSVFile<Office>
	{
		private readonly OfficeCSVConverter _converter;

		public OfficeCSVRepository()
		{
			_converter = new OfficeCSVConverter();
		}

		public void AppendToFile(string path, Office entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Office> Load(string path)
			=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Office> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}