// File:    SecretaryCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:03 AM
// Purpose: Definition of Class SecretaryCSVRepository

using Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class SecretaryCSVRepository : GenericCSVFile<Secretary>
	{
		private readonly SecretaryCSVConverter _converter;

		public SecretaryCSVRepository()
		{
			_converter = new SecretaryCSVConverter();
		}

		public void AppendToFile(string path, Secretary entity)
			 => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Secretary> Load(string path)
			=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Secretary> entities)
			=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}