// File:    MedicalHistoryCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:18 AM
// Purpose: Definition of Class MedicalHistoryCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class MedicalHistoryCSVRepository : GenericCSVFile<MedicalHistory>
	{

		private readonly MedicalHistoryCSVConverter _converter;

		public MedicalHistoryCSVRepository()
		{
			_converter = new MedicalHistoryCSVConverter();
		}

		public void AppendToFile(string path, MedicalHistory entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<MedicalHistory> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<MedicalHistory> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}