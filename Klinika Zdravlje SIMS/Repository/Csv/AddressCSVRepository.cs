// File:    AddressCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:11 AM
// Purpose: Definition of Class AddressCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class AddressCSVRepository : GenericCSVFile<Address>
	{
		private readonly AddressCSVConverter _converter;

		public AddressCSVRepository()
		{
			_converter = new AddressCSVConverter();
		}

		public void AppendToFile(string path, Address entity)
			=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Address> Load(string path)
			=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Address> entities)
			=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}