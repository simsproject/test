// File:    RecipeCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:01 AM
// Purpose: Definition of Class RecipeCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class RecipeCSVRepository : GenericCSVFile<Recipe>
	{
		private readonly RecipeCSVConverter _converter;

		public RecipeCSVRepository()
		{
			_converter = new RecipeCSVConverter();
		}

		public void AppendToFile(string path, Recipe entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Recipe> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Recipe> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}