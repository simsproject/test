// File:    WorkingPeriodCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:05 AM
// Purpose: Definition of Class WorkingPeriodCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class WorkingPeriodCSVRepository : GenericCSVFile<WorkingPeriod>
	{
		private readonly WorkingPeriodCSVConverter _converter;

		public WorkingPeriodCSVRepository()
		{
			_converter = new WorkingPeriodCSVConverter();
		}

		public void AppendToFile(string path, WorkingPeriod entity)
			 => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<WorkingPeriod> Load(string path)
			 => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<WorkingPeriod> entities)
			=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}