﻿using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using Repository.Csv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KlinikaZdravljeCD.Repository.Csv
{
    class BedCSVRepository : GenericCSVFile<Bed>
    {
        private readonly BedCSVConverter _converter;

        public BedCSVRepository()
        {
            _converter = new BedCSVConverter();
        }

        public void AppendToFile(string path, Bed entity)
				=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

	 	  public List<Bed> Load(string path)
				=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<Bed> entities)
				=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}
