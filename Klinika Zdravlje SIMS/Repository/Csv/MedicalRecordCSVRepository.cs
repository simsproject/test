// File:    MedicalRecordCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:19 AM
// Purpose: Definition of Class MedicalRecordCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class MedicalRecordCSVRepository : GenericCSVFile<MedicalRecord>
	{
		private readonly MedicalRecordCSVConverter _converter;

		public MedicalRecordCSVRepository()
		{
			_converter = new MedicalRecordCSVConverter();
		}

		public void AppendToFile(string path, MedicalRecord entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<MedicalRecord> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<MedicalRecord> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}