// File:    AllergensCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:11 AM
// Purpose: Definition of Class AllergensCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class AllergensCSVRepository : GenericCSVFile<Allergens>
	{
		private readonly AllergensCSVConverter _converter;

		public AllergensCSVRepository()
		{
			_converter = new AllergensCSVConverter();
		}

		public void AppendToFile(string path, Allergens entity)
			 => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Allergens> Load(string path)
			 => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Allergens> entities)
			 => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}