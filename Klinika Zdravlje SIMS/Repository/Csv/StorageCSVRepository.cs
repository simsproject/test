// File:    StorageCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:04 AM
// Purpose: Definition of Class StorageCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class StorageCSVRepository : GenericCSVFile<Storage>
	{

        private readonly StorageCSVConverter _converter;

        public StorageCSVRepository()
        {
            _converter = new StorageCSVConverter();
        }

        public void AppendToFile(string path, Storage entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<Storage> Load(string path)
            => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<Storage> entities)
            => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}