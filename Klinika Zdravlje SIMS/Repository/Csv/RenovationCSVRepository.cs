// File:    RenovationCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:01 AM
// Purpose: Definition of Class RenovationCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class RenovationCSVRepository : GenericCSVFile<Renovation>
	{
        private readonly RenovationCSVConverter _converter;

        public RenovationCSVRepository()
        {
            _converter = new RenovationCSVConverter();
        }

        public void AppendToFile(string path, Renovation entity)
        => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<Renovation> Load(string path)
        => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<Renovation> entities)
        => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}