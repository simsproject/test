// File:    BlogCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:13 AM
// Purpose: Definition of Class BlogCSVRepository

using Model.Other;
using System;
using System.Collections.Generic;

namespace Repository.Csv
{
	public class BlogCSVRepository : GenericCSVFile<Blog>
	{
		public void AppendToFile(string path, Blog entity)
		{
			throw new NotImplementedException();
		}

		public List<Blog> Load(string path)
		{
			throw new NotImplementedException();
		}

		public void SaveAll(string path, List<Blog> entities)
		{
			throw new NotImplementedException();
		}
	}
}