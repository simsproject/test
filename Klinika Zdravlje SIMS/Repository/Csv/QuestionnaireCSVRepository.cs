// File:    QuestionaireCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 11:10:00 AM
// Purpose: Definition of Class QuestionaireCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Other;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class QuestionnaireCSVRepository : GenericCSVFile<Questionnaire>
	{
		private readonly QuestionnaireCSVConverter _converter;

		public QuestionnaireCSVRepository()
		{
			_converter = new QuestionnaireCSVConverter();
		}

		public void AppendToFile(string path, Questionnaire entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Questionnaire> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Questionnaire> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}