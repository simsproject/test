// File:    HospitalTreatmentCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:17 AM
// Purpose: Definition of Class HospitalTreatmentCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class HospitalTreatmentCSVRepository : GenericCSVFile<HospitalTreatment>
	{
        private readonly HospitalTreatmentCSVConverter _converter;

        public HospitalTreatmentCSVRepository()
        {
            _converter = new HospitalTreatmentCSVConverter();
        }

        public void AppendToFile(string path, HospitalTreatment entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<HospitalTreatment> Load(string path)
            => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<HospitalTreatment> entities)
            => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}