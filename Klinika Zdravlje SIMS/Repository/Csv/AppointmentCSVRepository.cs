// File:    AppointmentCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:12 AM
// Purpose: Definition of Class AppointmentCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Appointment;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class AppointmentCSVRepository : GenericCSVFile<Appointment>
	{
		private readonly AppointmentCSVConverter _converter;

		public AppointmentCSVRepository()
		{
			_converter = new AppointmentCSVConverter();
		}

		public void AppendToFile(string path, Appointment entity)
			=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<Appointment> Load(string path)
			=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Appointment> entities)
			=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}