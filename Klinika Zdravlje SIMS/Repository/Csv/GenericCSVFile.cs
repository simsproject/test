// File:    GenericCSVFile.cs
// Author:  NEMANJA
// Created: Saturday, May 30, 2020 10:09:34 AM
// Purpose: Definition of Class GenericCSVFile

using System;
using System.Collections.Generic;

namespace Repository.Csv
{
   public interface GenericCSVFile<E>
   {
      void SaveAll(string path,List<E> entities);
      
      void AppendToFile(string path,E entity);
      
      List<E> Load(string path);

		
	}
}