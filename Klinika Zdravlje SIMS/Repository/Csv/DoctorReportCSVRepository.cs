// File:    DoctorReportCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:14 AM
// Purpose: Definition of Class DoctorReportCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class DoctorReportCSVRepository : GenericCSVFile<DoctorReport>
	{
		private readonly DoctorReportCSVConverter _converter;

		public DoctorReportCSVRepository()
		{
			_converter = new DoctorReportCSVConverter();
		}

		public void AppendToFile(string path, DoctorReport entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<DoctorReport> Load(string path)
		    => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<DoctorReport> entities)
		    => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}