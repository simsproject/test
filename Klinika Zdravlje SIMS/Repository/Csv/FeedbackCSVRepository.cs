// File:    FeedbackCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:16 AM
// Purpose: Definition of Class FeedbackCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class FeedbackCSVRepository : GenericCSVFile<Feedback>
	{

		private readonly FeedbackCSVConverter _converter;

		public FeedbackCSVRepository()
		{
			_converter = new FeedbackCSVConverter();
		}

		public void AppendToFile(string path, Feedback entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<Feedback> Load(string path)
		    => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<Feedback> entities)
		    => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}