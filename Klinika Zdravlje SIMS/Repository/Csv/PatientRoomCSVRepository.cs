// File:    PatientRoomCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:24 AM
// Purpose: Definition of Class PatientRoomCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Rooms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{
	public class PatientRoomCSVRepository : GenericCSVFile<PatientRoom>
	{
		private readonly PatientRoomCSVConverter _converter;

		public PatientRoomCSVRepository()
		{
			_converter = new PatientRoomCSVConverter();
		}

		public void AppendToFile(string path, PatientRoom entity)
		=> File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

		public List<PatientRoom> Load(string path)
		=> File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

		public void SaveAll(string path, List<PatientRoom> entities)
		=> File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
	}
}