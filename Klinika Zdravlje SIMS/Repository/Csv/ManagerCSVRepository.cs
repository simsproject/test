// File:    ManagerCSVRepository.cs
// Author:  Korisnik
// Created: Tuesday, June 2, 2020 10:59:18 AM
// Purpose: Definition of Class ManagerCSVRepository

using KlinikaZdravljeCD.Repository.Csv.Converter;
using Model.Users;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Repository.Csv
{

	public class ManagerCSVRepository : GenericCSVFile<Manager>

	{
        private readonly ManagerCSVConverter _converter;

        public ManagerCSVRepository()
        {
            _converter = new ManagerCSVConverter();
        }

        public void AppendToFile(string path, Manager entity)
            => File.AppendAllText(path, _converter.ConverterEntityToFormat(entity) + Environment.NewLine);

        public List<Manager> Load(string path)
            => File.ReadAllLines(path).Select(_converter.ConverterCSVFormatToEntity).ToList();

        public void SaveAll(string path, List<Manager> entities)
            => File.WriteAllLines(path, entities.Select(_converter.ConverterEntityToFormat).ToArray());
    }
}