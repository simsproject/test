// File:    RenovationRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:54:16 PM
// Purpose: Definition of Class RenovationRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;
using Repository.Csv;

namespace Repository
{
   public class RenovationRepository : Repository.Abstract.RoomsPackageRepository.IRenovationRepository
   {
        private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\renovation.csv";
        private GenericCSVFile<Renovation> genericCSVRepository;
        public GenericCSVFileFactory<Renovation> genericCSVFileFactory;

        private static RenovationRepository instance;
        public static RenovationRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new RenovationRepository();
            }
            return instance;
        }

        public RenovationRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Renovation>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public Renovation Create(Renovation entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(Renovation entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Renovation => GetByID(Renovation.SerialID).SerialID.Equals(entity.SerialID));
            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);
            }
        }

        public IEnumerable<Renovation> GetAll()
        => genericCSVRepository.Load(filePath);

        public Renovation GetByID(long id)
       => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}