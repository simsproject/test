// File:    StorageRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:54:18 PM
// Purpose: Definition of Class StorageRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;
using Repository.Csv;

namespace Repository
{
   public class StorageRepository : Repository.Abstract.RoomsPackageRepository.IStorageRepository
   {
        private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\storage.csv";
        private GenericCSVFile<Storage> genericCSVRepository;

        public GenericCSVFileFactory<Storage> genericCSVFileFactory;

        private static StorageRepository instance;
        public static StorageRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new StorageRepository();
            }
            return instance;
        }

        public StorageRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Storage>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }


        public IEnumerable<Storage> GetAll()
        => genericCSVRepository.Load(filePath);

        public Storage Create(Storage entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(Storage entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Storage => GetByID(Storage.Name).Name.Equals(entity.Name));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(Storage entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(Storage => (Storage.Name.Equals(entity.Name)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public Storage GetByID(string id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Name.Equals(id));
    }
}