// File:    PatientRoomRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:54:16 PM
// Purpose: Definition of Class PatientRoomRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;

namespace Repository
{
   public class PatientRoomRepository : Repository.Abstract.RoomsPackageRepository.IPatientRoomRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\patientRooms.csv";
      private Repository.Csv.GenericCSVFile<PatientRoom> genericCSVRepository;
      
      public GenericCSVFileFactory<PatientRoom> genericCSVFileFactory;

        private static PatientRoomRepository instance;
        public static PatientRoomRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new PatientRoomRepository();
            }
            return instance;
        }

        public PatientRoomRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<PatientRoom>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);

        }

        public IEnumerable<PatientRoom> GetAll()
        => genericCSVRepository.Load(filePath);

        public PatientRoom Create(PatientRoom entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(PatientRoom entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(PatientRoom => GetByID(PatientRoom.Name).Name.Equals(entity.Name));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);
            }
        }

        public void Update(PatientRoom entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(PatientRoom => (PatientRoom.Name.Equals(entity.Name)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public PatientRoom GetByID(string id)
       => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Name.Equals(id));

    }
}