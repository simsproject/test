// File:    HospitalTreatmentRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:36:53 PM
// Purpose: Definition of Class HospitalTreatmentRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;

namespace Repository
{
   public class HospitalTreatmentRepository : Repository.Abstract.MedicalRecordPackageRepository.IHospitalTreatmentRepository
   {
        private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\hospitalTreatment.csv";
        private Repository.Csv.GenericCSVFile<HospitalTreatment> genericCSVRepository;
        public GenericCSVFileFactory<HospitalTreatment> genericCSVFileFactory;

        private static HospitalTreatmentRepository instance;
        public static HospitalTreatmentRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new HospitalTreatmentRepository();
            }
            return instance;
        }


        public HospitalTreatmentRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<HospitalTreatment>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public HospitalTreatment Create(HospitalTreatment entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public HospitalTreatment GetByID(long id)
       => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);

        public IEnumerable<HospitalTreatment> GetAll()
            => genericCSVRepository.Load(filePath);
    }
}