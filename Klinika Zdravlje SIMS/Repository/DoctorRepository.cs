// File:    DoctorRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:48 PM
// Purpose: Definition of Class DoctorRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;

namespace Repository
{
   public class DoctorRepository : Repository.Abstract.UserPackageRepository.IDoctorRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\doctor.csv";
      private Repository.Csv.GenericCSVFile<Doctor> genericCSVRepository;
      
      public GenericCSVFileFactory<Doctor> genericCSVFileFactory;

        private static DoctorRepository instance;
        public static DoctorRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new DoctorRepository();
            }
            return instance;
        }

        public DoctorRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Doctor>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<Doctor> GetAll()
          => genericCSVRepository.Load(filePath);

        public Doctor Create(Doctor entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(Doctor entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Doctor => GetByID(Doctor.PersonalID).PersonalID.Equals(entity.PersonalID));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(Doctor entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(Doctor => (Doctor.PersonalID.Equals(entity.PersonalID)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public Doctor GetByID(string id)
       => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.PersonalID.Equals(id));
    }
}