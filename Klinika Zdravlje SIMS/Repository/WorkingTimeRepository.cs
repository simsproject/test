// File:    WorkingTimeRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:04:01 PM
// Purpose: Definition of Class WorkingTimeRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository.Csv;

namespace Repository
{
   public class WorkingTimeRepository : Repository.Abstract.UserPackageRepository.IWorkingTimeRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\workingTime.csv";
      private GenericCSVFile<WorkingTime> genericCSVRepository;
      public GenericCSVFileFactory<WorkingTime> genericCSVFileFactory;

        private static WorkingTimeRepository instance;
        public static WorkingTimeRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new WorkingTimeRepository();
            }
            return instance;
        }

        public WorkingTimeRepository()
		{
			genericCSVFileFactory = new GenericCSVFileFactory<WorkingTime>();
			genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
		}

		public IEnumerable<WorkingTime> GetAll()
		  => genericCSVRepository.Load(filePath);

		public WorkingTime Create(WorkingTime entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(WorkingTime entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(WorkingTime => GetByID(WorkingTime.SerialID).SerialID == entity.SerialID);

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(WorkingTime entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(WorkingTime => (WorkingTime.SerialID == entity.SerialID))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

		public WorkingTime GetByID(long id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);

    }
}