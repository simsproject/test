// File:    MedicalRecordRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:42:54 PM
// Purpose: Definition of Class MedicalRecordRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;

namespace Repository
{
   public class MedicalRecordRepository : Repository.Abstract.MedicalRecordPackageRepository.IMedicalRecordRepository
   {
      private String filePath= "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\medicalRecord.csv";
      private Repository.Csv.GenericCSVFile<MedicalRecord> genericCSVRepository;
      
      public GenericCSVFileFactory<MedicalRecord> genericCSVFileFactory;

        private static MedicalRecordRepository instance;
        public static MedicalRecordRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new MedicalRecordRepository();
            }
            return instance;
        }

        public MedicalRecordRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<MedicalRecord>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);


        }

        public void Update(MedicalRecord entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(MedicalRecord => (MedicalRecord.SerialID == entity.SerialID))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public MedicalRecord GetByID(long id)
       => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);

        public IEnumerable<MedicalRecord> GetAll()
        => genericCSVRepository.Load(filePath);

        public MedicalRecord Create(MedicalRecord entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }
    }
}