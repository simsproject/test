// File:    NotificationRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:48:53 PM
// Purpose: Definition of Class NotificationRepository

using System;
using System.Collections.Generic;
using Model.Other;

namespace Repository
{
   public class NotificationRepository : Repository.Abstract.OtherPackageRepository.INotificationRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\notification.csv";
      private Repository.Csv.GenericCSVFile<Notification> genericCSVRepository;
      
      public GenericCSVFileFactory<Notification> genericCSVFileFactory;

        private static NotificationRepository instance;
        public static NotificationRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new NotificationRepository();
            }
            return instance;
        }


        public NotificationRepository() {
            genericCSVFileFactory = new GenericCSVFileFactory<Notification>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<Notification> GetAll()
            => genericCSVRepository.Load(filePath);

        public Notification Create(Notification entity) 
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }
    }
}