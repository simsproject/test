// File:    RecipeRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:42:53 PM
// Purpose: Definition of Class RecipeRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;

namespace Repository
{
   public class RecipeRepository : Repository.Abstract.MedicalRecordPackageRepository.IRecipeRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\recipe.csv";
      private Repository.Csv.GenericCSVFile<Recipe> genericCSVRepository;
      
      public GenericCSVFileFactory<Recipe> genericCSVFileFactory;

        private static RecipeRepository instance;
        public static RecipeRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new RecipeRepository();
            }
            return instance;
        }

        public RecipeRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Recipe>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public Recipe Create(Recipe entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public IEnumerable<Recipe> GetAll()
        => genericCSVRepository.Load(filePath);

        public Recipe GetByID(long id)
      => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}