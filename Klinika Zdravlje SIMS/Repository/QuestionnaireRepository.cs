// File:    QuestionaireRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:48:51 PM
// Purpose: Definition of Class QuestionaireRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Other;

namespace Repository
{
   public class QuestionnaireRepository : Repository.Abstract.OtherPackageRepository.IQuestionnaireRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\questionnaires.csv";
      private Repository.Csv.GenericCSVFile<Questionnaire> genericCSVRepository;
      
      public GenericCSVFileFactory<Questionnaire> genericCSVFileFactory;

        private static QuestionnaireRepository instance;
        public static QuestionnaireRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new QuestionnaireRepository();
            }
            return instance;
        }

        public QuestionnaireRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Questionnaire>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<Questionnaire> GetAll()
        => genericCSVRepository.Load(filePath);

        public Questionnaire Create(Questionnaire entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public Questionnaire GetByID(long id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}