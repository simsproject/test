// File:    ManagerRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:52 PM
// Purpose: Definition of Class ManagerRepository

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Model.Users;
using Repository.Csv;


namespace Repository
{
    public class ManagerRepository : Repository.Abstract.UserPackageRepository.IManagerRepository
    {
       
        private string filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\manager.csv";
        private GenericCSVFile<Manager> genericCSVRepository;

        private static ManagerRepository instance;
        public static ManagerRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new ManagerRepository();
            }
            return instance;
        }

        public GenericCSVFileFactory<Manager> genericCSVFileFactory;

        public ManagerRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Manager>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public void Update(Manager entity)
        {
            var listOfManager = genericCSVRepository.Load(filePath);
            listOfManager[listOfManager.FindIndex(Manager => (Manager.PersonalID.Equals(entity.PersonalID)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfManager);
        }

        public IEnumerable<Manager> GetAll()
         => genericCSVRepository.Load(filePath);

        public Manager GetByID(string id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.PersonalID.Equals(id));
    }




}
