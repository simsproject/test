// File:    PatientRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:53 PM
// Purpose: Definition of Class PatientRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;

namespace Repository
{
   public class PatientRepository : Repository.Abstract.UserPackageRepository.IPatientRepository
   {
      private String filePath= "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\patients.csv";
      private Repository.Csv.GenericCSVFile<Patient> genericCSVRepository;
      
      public GenericCSVFileFactory<Patient> genericCSVFileFactory;

        private static PatientRepository instance;
        public static PatientRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new PatientRepository();
            }
            return instance;
        }

        public PatientRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Patient>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);

        }

        public IEnumerable<Patient> GetAll()
        => genericCSVRepository.Load(filePath);

        public Patient Create(Patient entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(Patient entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Patient => GetByID(Patient.PersonalID).PersonalID.Equals(entity.PersonalID));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);
            }
        }

        public void Update(Patient entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(Patient => (Patient.PersonalID.Equals(entity.PersonalID)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public Patient GetByID(string id)
       => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.PersonalID.Equals(id));
    }
}