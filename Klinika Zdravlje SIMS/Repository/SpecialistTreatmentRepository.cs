// File:    SpecialistTreatmentRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:36:54 PM
// Purpose: Definition of Class SpecialistTreatmentRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Repository.Csv;

namespace Repository
{
   public class SpecialistTreatmentRepository : Repository.Abstract.MedicalRecordPackageRepository.ISpecialistTreatmentRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\specialistTreatment.csv";
      private GenericCSVFile<SpecialistTreatment> genericCSVRepository;     
      public GenericCSVFileFactory<SpecialistTreatment> genericCSVFileFactory;

        private static SpecialistTreatmentRepository instance;
        public static SpecialistTreatmentRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new SpecialistTreatmentRepository();
            }
            return instance;
        }

        public SpecialistTreatmentRepository()
		{
			genericCSVFileFactory = new GenericCSVFileFactory<SpecialistTreatment>();
			genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
		}

		  public IEnumerable<SpecialistTreatment> GetAll()
			 => genericCSVRepository.Load(filePath);

        public SpecialistTreatment GetByID(long id)
		=> genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);

		public SpecialistTreatment Create(SpecialistTreatment entity)
			{
				entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
				genericCSVRepository.AppendToFile(filePath, entity);
				return entity;
			}
    }
}