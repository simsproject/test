// File:    SecretaryRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:54 PM
// Purpose: Definition of Class SecretaryRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository.Csv;

namespace Repository
{
   public class SecretaryRepository : Repository.Abstract.UserPackageRepository.ISecretaryRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\secretary.csv";
      private GenericCSVFile<Secretary> genericCSVRepository;
      public GenericCSVFileFactory<Secretary> genericCSVFileFactory;

        private static SecretaryRepository instance;
        public static SecretaryRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new SecretaryRepository();
            }
            return instance;
        }

        public SecretaryRepository()
		{
			genericCSVFileFactory = new GenericCSVFileFactory<Secretary>();
			genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
		}

		public IEnumerable<Secretary> GetAll()
		 => genericCSVRepository.Load(filePath);

		public Secretary Create(Secretary entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

      public void Delete(Secretary entity)
      {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Secretary => GetByID(Secretary.PersonalID).PersonalID.Equals(entity.PersonalID));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

      public void Update(Secretary entity)
      {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(Secretary => (Secretary.PersonalID.Equals(entity.PersonalID)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

      public Secretary GetByID(string id)
    => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.PersonalID.Equals(id));
    }
}