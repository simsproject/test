// File:    CityRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:48 PM
// Purpose: Definition of Class CityRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository.Abstract.UserPackageRepository;
using Repository.Csv;

namespace Repository
{
   public class CityRepository : ICityRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\city.csv";
      private GenericCSVFile<City> genericCSVRepository;
      
      public GenericCSVFileFactory<City> genericCSVFileFactory;


        private static CityRepository instance;
        public static CityRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new CityRepository();
            }
            return instance;
        }
        public CityRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<City>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<City> GetAll()
            => genericCSVRepository.Load(filePath);

        public City GetByID(long id)
         => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.PostalCode == id);


    }
}