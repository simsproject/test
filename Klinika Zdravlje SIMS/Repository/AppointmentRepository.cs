// File:    AppointmentRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:31:17 PM
// Purpose: Definition of Class AppointmentRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Appointment;
using Repository.Abstract;
using Repository.Abstract.AppointmentPackageRepository;
using Repository.Csv;

namespace Repository
{
    public class AppointmentRepository : IAppointmentRepository
    {
        private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\appointment.csv";
        private GenericCSVFile<Appointment> genericCSVRepository;

        public GenericCSVFileFactory<Appointment> genericCSVFileFactory;

        private static AppointmentRepository instance;
        public static AppointmentRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new AppointmentRepository();
            }
            return instance;
        }
        public AppointmentRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Appointment>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<Appointment> GetAll()
        => genericCSVRepository.Load(filePath);

        public Appointment Create(Appointment entity)
        {
				entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
				genericCSVRepository.AppendToFile(filePath, entity);
				return entity;
		  }

        public void Delete(Appointment entity)
        {
				var listOfEntites = genericCSVRepository.Load(filePath);
				var entityToRemove = listOfEntites.SingleOrDefault(Appointment => GetByID(Appointment.SerialID).SerialID == entity.SerialID);

				if (entityToRemove != null)
				{
					listOfEntites.Remove(entityToRemove);
					genericCSVRepository.SaveAll(filePath, listOfEntites);

				}
		}

        public void Update(Appointment entity)
        {
				var listOfEntites = genericCSVRepository.Load(filePath);
				listOfEntites[listOfEntites.FindIndex(Appointment => (Appointment.SerialID == entity.SerialID))] = entity;
				genericCSVRepository.SaveAll(filePath, listOfEntites);
		  }

        public Appointment GetByID(long id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);




    }
}