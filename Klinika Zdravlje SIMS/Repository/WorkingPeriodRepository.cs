// File:    WorkingPeriodRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:55 PM
// Purpose: Definition of Class WorkingPeriodRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository.Csv;

namespace Repository
{
   public class WorkingPeriodRepository : Repository.Abstract.UserPackageRepository.IWorkingPeriodRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\workingPeriod.csv";
      private GenericCSVFile<WorkingPeriod> genericCSVRepository; 
      public GenericCSVFileFactory<WorkingPeriod> genericCSVFileFactory;

        private static WorkingPeriodRepository instance;
        public static WorkingPeriodRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new WorkingPeriodRepository();
            }
            return instance;
        }

        public WorkingPeriodRepository()
		{
			genericCSVFileFactory = new GenericCSVFileFactory<WorkingPeriod>();
			genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
		}

		public IEnumerable<WorkingPeriod> GetAll()
		  => genericCSVRepository.Load(filePath);

		public WorkingPeriod Create(WorkingPeriod entity)
        {
            entity.SerialID = GetAll().LastOrDefault() != null ? GetAll().LastOrDefault().SerialID + 1 : 1;
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(WorkingPeriod entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(WorkingPeriod => GetByID(WorkingPeriod.SerialID).SerialID == entity.SerialID);

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(WorkingPeriod entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(WorkingPeriod => (WorkingPeriod.SerialID == entity.SerialID))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

		public WorkingPeriod GetByID(long id)
         => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID == id);
    }
}