// File:    FeedbackRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:49 PM
// Purpose: Definition of Class FeedbackRepository

using System;
using System.Collections.Generic;
using Model.Users;

namespace Repository
{
   public class FeedbackRepository : Repository.Abstract.UserPackageRepository.IFeedbackRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\feedback.csv";
      private Repository.Csv.GenericCSVFile<Feedback> genericCSVRepository;
      
      public GenericCSVFileFactory<Feedback> genericCSVFileFactory;

        private static FeedbackRepository instance;
        public static FeedbackRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new FeedbackRepository();
            }
            return instance;
        }

        public FeedbackRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Feedback>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<Feedback> GetAll()
            => genericCSVRepository.Load(filePath);

        public Feedback Create(Feedback entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }
    }
}