// File:    DrugRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:36:51 PM
// Purpose: Definition of Class DrugRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Drugs;

namespace Repository
{
   public class DrugRepository : Repository.Abstract.DrugsPackageRepository.IDrugRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\drugs.csv";
		private Repository.Csv.GenericCSVFile<Drug> genericCSVRepository; 
      public GenericCSVFileFactory<Drug> genericCSVFileFactory;

        private static DrugRepository instance;
        public static DrugRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new DrugRepository();
            }
            return instance;
        }


        public DrugRepository()
		{
			genericCSVFileFactory = new GenericCSVFileFactory<Drug>();
			genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
		}

		public IEnumerable<Drug> GetAll()
		    => genericCSVRepository.Load(filePath);

		public Drug Create(Drug entity)
        {
            genericCSVRepository.AppendToFile(filePath, entity);
            return entity;
        }

        public void Delete(Drug entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Drug => GetByID(Drug.SerialID).SerialID.Equals(entity.SerialID));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(Drug entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            listOfEntites[listOfEntites.FindIndex(Drug => (Drug.SerialID.Equals(entity.SerialID)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfEntites);
        }

        public Drug GetByID(string id)
         => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.SerialID.Equals(id));

    }
}