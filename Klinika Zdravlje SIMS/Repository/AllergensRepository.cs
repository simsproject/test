// File:    AllergensRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:36:53 PM
// Purpose: Definition of Class AllergensRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Repository.Abstract.MedicalRecordPackageRepository;
using Repository.Csv;

namespace Repository
{
    public class AllergensRepository : IAllergensRepository
    {
        private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\allergens.csv";
        private GenericCSVFile<Allergens> genericCSVRepository;

        public GenericCSVFileFactory<Allergens> genericCSVFileFactory;

        private static AllergensRepository instance;
        public static AllergensRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new AllergensRepository();
            }
            return instance;
        }
        public AllergensRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Allergens>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public Allergens Create(Allergens entity)
        {
				genericCSVRepository.AppendToFile(filePath, entity);
				return entity;
		  }

        public IEnumerable<Allergens> GetAll()
            => genericCSVRepository.Load(filePath);

        public Allergens GetByID(string id)
          => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Name.Equals(id));

    }
}