// File:    AddressRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 3:03:46 PM
// Purpose: Definition of Class AddressRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository.Abstract.UserPackageRepository;

namespace Repository
{
   public class AddressRepository : IAddressRepository
    {
        private string filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\address.csv";
        private Repository.Csv.GenericCSVFile<Address> genericCSVRepository;

        public GenericCSVFileFactory<Address> genericCSVFileFactory;
        
        private static AddressRepository instance;
        public static AddressRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new AddressRepository();
            }
            return instance;
        }

        public AddressRepository()
        {
            genericCSVFileFactory = new GenericCSVFileFactory<Address>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);


        }

        public IEnumerable<Address> GetAll()
            => genericCSVRepository.Load(filePath);

        public Address GetByID(long id)
           => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Id == id);

    }
}