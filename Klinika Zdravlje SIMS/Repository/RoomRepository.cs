// File:    RoomRepository.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 2:54:16 PM
// Purpose: Definition of Class RoomRepository

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;

namespace Repository
{
   public class RoomRepository : Repository.Abstract.RoomsPackageRepository.IRoomRepository
   {
      private String filePath = "..\\..\\..\\..\\Klinika Zdravlje SIMS\\Util\\rooms.csv";
      private Repository.Csv.GenericCSVFile<Room> genericCSVRepository;
      
      public GenericCSVFileFactory<Room> genericCSVFileFactory;

        private static RoomRepository instance;
        public static RoomRepository GetInstance()
        {
            if (instance == null)
            {
                instance = new RoomRepository();
            }
            return instance;
        }

        public RoomRepository() {
            genericCSVFileFactory = new GenericCSVFileFactory<Room>();
            genericCSVRepository = genericCSVFileFactory.GetGenericCSVFIle(filePath);
        }

        public IEnumerable<Room> GetAll()
            => genericCSVRepository.Load(filePath);

        public Room Create(Room entity)
        {
			genericCSVRepository.AppendToFile(filePath,entity);
			return entity;
		}


        public void Delete(Room entity)
        {
            var listOfEntites = genericCSVRepository.Load(filePath);
            var entityToRemove = listOfEntites.SingleOrDefault(Room => GetByID(Room.Name).Name.Equals(entity.Name));

            if (entityToRemove != null)
            {
                listOfEntites.Remove(entityToRemove);
                genericCSVRepository.SaveAll(filePath, listOfEntites);

            }
        }

        public void Update(Room entity)
        {
            var listOfRooms = genericCSVRepository.Load(filePath);
            listOfRooms[listOfRooms.FindIndex(Room => (Room.Name.Equals(entity.Name)))] = entity;
            genericCSVRepository.SaveAll(filePath, listOfRooms);
        }


        public Room GetByID(string id)
        => genericCSVRepository.Load(filePath).SingleOrDefault(Entity => Entity.Name.Equals(id));
    }
}