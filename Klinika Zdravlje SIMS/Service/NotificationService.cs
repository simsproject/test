// File:    NotificationService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:27:23 PM
// Purpose: Definition of Class NotificationService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Other;
using Model.Users;
using Repository;
using Repository.Abstract.OtherPackageRepository;

namespace Service
{
   public class NotificationService : Service.OtherPackageService.INotificationService
   {
      public INotificationRepository iNotificationRepository;

        private static NotificationService instance;
        public static NotificationService GetInstance()
        {
            if (instance == null)
            {
                instance = new NotificationService();
            }
            return instance;
        }

        public NotificationService()
        {
            iNotificationRepository = NotificationRepository.GetInstance();
        }

        public Notification Create(Notification entity)
            => iNotificationRepository.Create(entity);

        public IEnumerable<Notification> GetAll()
            => iNotificationRepository.GetAll();

        public List<Notification> GetNotificationForUser(User user)
            => GetAll().Where(Notification => (Notification.User.PersonalID.Equals(user.PersonalID))).ToList();

  
    }
}