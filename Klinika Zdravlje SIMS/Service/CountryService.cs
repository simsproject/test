// File:    CountryService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:56 PM
// Purpose: Definition of Class CountryService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;
using Service.UserPackageService;

namespace Service
{
   public class CountryService : ICountryService
    {
        public ICountryRepository iCountryRepository;

        private static CountryService instance;
        public static CountryService GetInstance()
        {
            if (instance == null)
            {
                instance = new CountryService();
            }
            return instance;
        }

        public CountryService()
        {
            this.iCountryRepository = CountryRepository.GetInstance();
        }

        public IEnumerable<Country> GetAll()
				 =>iCountryRepository.GetAll();
       

        public Country GetByID(string id)
				 => iCountryRepository.GetByID(id);
 
    }
}