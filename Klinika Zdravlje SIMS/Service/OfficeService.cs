// File:    OfficeService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:29:22 PM
// Purpose: Definition of Class OfficeService

using System;
using System.Collections.Generic;
using Model.Rooms;
using Model.Users;
using Repository;
using Repository.Abstract.RoomsPackageRepository;

namespace Service
{
   public class OfficeService : Service.RoomsPackageService.IOfficeService
   {
      public IOfficeRepository iOfficeRepository;


        private static OfficeService instance;
        public static OfficeService GetInstance()
        {
            if (instance == null)
            {
                instance = new OfficeService();
            }
            return instance;
        }

        public OfficeService()
		{
			this.iOfficeRepository = OfficeRepository.GetInstance();
		}


        public Office Create(Office entity)
        => iOfficeRepository.Create(entity);

        public void Delete(Office entity)
        => iOfficeRepository.Delete(entity);

        public IEnumerable<Office> GetAll()
		=> iOfficeRepository.GetAll();

		public Office GetByID(string id)
		=> iOfficeRepository.GetByID(id);

        public void Update(Office entity)
        => iOfficeRepository.Update(entity);
    }
}