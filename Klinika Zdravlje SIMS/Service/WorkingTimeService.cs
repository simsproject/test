// File:    WorkingTimeService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:31:44 PM
// Purpose: Definition of Class WorkingTimeService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;
using Service.UserPackageService;

namespace Service
{
   public class WorkingTimeService : Service.UserPackageService.IWorkingTimeService
   {
      public IWorkingTimeRepository iWorkingTimeRepository;
      public WorkingPeriodService workingPeriodService;


        private static WorkingTimeService instance;
        public static WorkingTimeService GetInstance()
        {
            if (instance == null)
            {
                instance = new WorkingTimeService();
            }
            return instance;
        }
        public WorkingTimeService()
		{
			this.iWorkingTimeRepository = WorkingTimeRepository.GetInstance();
            this.workingPeriodService = WorkingPeriodService.GetInstance();
		}

		public WorkingTime Create(WorkingTime entity)
            => iWorkingTimeRepository.Create(entity);

        public void Delete(WorkingTime entity)
            => iWorkingTimeRepository.Delete(entity);

        public IEnumerable<WorkingTime> GetAll()
			=> iWorkingTimeRepository.GetAll();

		public WorkingTime GetByID(long id)
			=> iWorkingTimeRepository.GetByID(id);

        public WorkingTime GetWorkingTimeForDate(WorkingPeriod workingPeriod, DateTime date)

		/* try
		 {
			  foreach (WorkingTime workingTime in workingPeriod.WorkingTime)
			  {
					if (dateIsEqual(date, workingTime.Date)) return workingTime;
			  }
			  return null;
		 }
		 catch
		 {
			  return null;
		 }*/
		{
			foreach (WorkingTime workingTime in workingPeriod.WorkingTime)
			{
				if(DateTime.Compare(workingTime.Date.Date, date.Date) == 0)
				{
					return workingTime;
				}
			}
			return null;
		}
			
          
           // workingPeriod.WorkingTime.Where(WorkingTime => (DateTime.Compare(WorkingTime.Date.Date, date.Date) == 0)).FirstOrDefault();
        

        public bool dateIsEqual(DateTime firstDate, DateTime secondDate)
            => DateTime.Compare(firstDate.Date, secondDate.Date) == 0;

        public WorkingTime GetWorkingTimeForDoctorByDate(Doctor doctor, DateTime date)
        {
            WorkingPeriod workingPeriod = workingPeriodService.WorkingPeriodForDoctorByDate(doctor, date);
            if (workingPeriod == null) return null;
            return GetWorkingTimeForDate(workingPeriod, date);
        }

        public void Update(WorkingTime entity)
            => iWorkingTimeRepository.Update(entity);

    }
}