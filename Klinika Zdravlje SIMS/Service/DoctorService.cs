// File:    DoctorService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:52 PM
// Purpose: Definition of Class DoctorService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
   public class DoctorService : Service.UserPackageService.IDoctorService
   {
        public IDoctorRepository iDoctorRepository;
        public WorkingPeriodService workingPeriodService;
        public WorkingTimeService workingTimeService;


        private static DoctorService instance;
        public static DoctorService GetInstance()
        {
            if (instance == null)
            {
                instance = new DoctorService();
            }
            return instance;
        }
        public DoctorService()
        {
            this.iDoctorRepository = DoctorRepository.GetInstance();
            this.workingPeriodService = WorkingPeriodService.GetInstance();
            this.workingTimeService = WorkingTimeService.GetInstance();
        }

        public Doctor Create(Doctor entity)
            => iDoctorRepository.Create(entity);

        public void Delete(Doctor entity)
            => iDoctorRepository.Delete(entity);

        public IEnumerable<Doctor> GetAll()
            => iDoctorRepository.GetAll();

        public Doctor GetByID(string id)
            => iDoctorRepository.GetByID(id);

        public List<Doctor> GetDoctorByTitle(Title title)
        => iDoctorRepository.GetAll().Where(Doctor => Doctor.Title == title).ToList();

        public void Update(Doctor entity)
            => iDoctorRepository.Update(entity);


        public Dictionary<Doctor, WorkingTime> FilterDoctorsByWorkingTime(DateTime date)
        {
            Dictionary<Doctor, WorkingTime> workingTimeForDoctor = new Dictionary<Doctor, WorkingTime>();
            List<Doctor> allDoctor = GetAll().ToList();
            foreach (Doctor doctor in allDoctor)
            {
                WorkingPeriod workingPeriod = workingPeriodService.WorkingPeriodForDoctorByDate(doctor, date);
                if (workingPeriod == null)
                    continue;
                WorkingTime workingTime = workingTimeService.GetWorkingTimeForDate(workingPeriod, date);
                if (workingTime == null)
                    continue;
                workingTimeForDoctor.Add(doctor, workingTime);
            }
            return workingTimeForDoctor;
        }
    }
}