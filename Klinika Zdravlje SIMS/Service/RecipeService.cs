﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Model.Users;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
    public class RecipeService : Service.MedicalRecordPackageService.IRecipeService
    {
        public Repository.Abstract.MedicalRecordPackageRepository.IRecipeRepository recipeRepository;

        private static RecipeService instance;
        public static RecipeService GetInstance()
        {
            if (instance == null)
            {
                instance = new RecipeService();
            }
            return instance;
        }
        public RecipeService()
        {
            this.recipeRepository = RecipeRepository.GetInstance();
        }

        public Recipe Create(Recipe entity)
        => recipeRepository.Create(entity);

        public IEnumerable<Recipe> GetAll()
        => recipeRepository.GetAll();

        public List<Recipe> GetAllRecipesForPatient(MedicalRecord medicalRecord)
        => GetAll().Where(Recipe => (Recipe.MedicalRecord.SerialID == medicalRecord.SerialID)).ToList();

        public Recipe GetByID(long id)
        => recipeRepository.GetByID(id);
    }
}
