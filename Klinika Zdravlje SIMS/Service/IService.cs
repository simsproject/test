// File:    IService.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 4:15:04 PM
// Purpose: Definition of Interface IService

using System;

namespace Service
{
   public interface IService<E> : IUpdateService<E>, IDeleteService<E>, ICreateService<E>, IGetAllService<E>
   {
   }
}