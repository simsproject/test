// File:    WorkingPeriodService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:31:45 PM
// Purpose: Definition of Class WorkingPeriodService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
   public class WorkingPeriodService : Service.UserPackageService.IWorkingPeriodService
   {
      public IWorkingPeriodRepository iWorkingPeriodRepository;

        private static WorkingPeriodService instance;
        public static WorkingPeriodService GetInstance()
        {
            if (instance == null)
            {
                instance = new WorkingPeriodService();
            }
            return instance;
        }
        public WorkingPeriodService()
		{
			this.iWorkingPeriodRepository = WorkingPeriodRepository.GetInstance();
        }

		public WorkingPeriod Create(WorkingPeriod entity)
            => iWorkingPeriodRepository.Create(entity);

        public void Delete(WorkingPeriod entity)
            => iWorkingPeriodRepository.Delete(entity);

        public IEnumerable<WorkingPeriod> GetAll()
			=> iWorkingPeriodRepository.GetAll();

		public WorkingPeriod GetByID(long id)
			=> iWorkingPeriodRepository.GetByID(id);


        public void Update(WorkingPeriod entity)
            => iWorkingPeriodRepository.Update(entity);


        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public WorkingPeriod WorkingPeriodForDoctorByDate(Doctor doctor, DateTime date)
         => doctor.WorkingPeriod.Where(WorkingPeriod => (DateTime.Compare(date.Date, WorkingPeriod.Begin.Date) >= 0 && DateTime.Compare(date.Date, WorkingPeriod.End.Date) <= 0)).FirstOrDefault();
    }
}