// File:    MedicalHistoryService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:26:25 PM
// Purpose: Definition of Class MedicalHistoryService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;

namespace Service
{
   public class MedicalHistoryService : Service.MedicalRecordPackageService.IMedicalHistoryService
   {
      public IMedicalHistoryRepository iMedicalHistoryRepository;

        private static MedicalHistoryService instance;
        public static MedicalHistoryService GetInstance()
        {
            if (instance == null)
            {
                instance = new MedicalHistoryService();
            }
            return instance;
        }
        public MedicalHistoryService()
        {
            this.iMedicalHistoryRepository = MedicalHistoryRepository.GetInstance();
        }

        public MedicalHistory Create(MedicalHistory entity)
        => iMedicalHistoryRepository.Create(entity);

        public IEnumerable<MedicalHistory> GetAll()
        => iMedicalHistoryRepository.GetAll();

        public List<MedicalHistory> GetAllMedicalHistoriesForPatient(MedicalRecord medicalRecord)
            => iMedicalHistoryRepository.GetAll().Where(MedicalHistory =>(MedicalHistory.MedicalRecord.SerialID.ToString().Equals(medicalRecord.SerialID.ToString()))).ToList();

        public MedicalHistory GetByID(long id)
        {
           MedicalHistory medicalHistory = iMedicalHistoryRepository.GetByID(id);
            if (IsDateGood(medicalHistory)) {
                return medicalHistory;
            }
            return null;
        }


        public bool IsDateGood(MedicalHistory medicalHistory)
             => DateTime.Compare(DateTime.Now.Date, medicalHistory.DateBegin.Date) >= 0;

        public void Update(MedicalHistory entity)
        => iMedicalHistoryRepository.Update(entity);
    }
}