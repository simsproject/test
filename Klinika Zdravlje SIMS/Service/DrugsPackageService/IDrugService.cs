// File:    IDrugService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 6:16:56 PM
// Purpose: Definition of Interface IDrugService

using Model.Drugs;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.DrugsPackageService
{
   public interface IDrugService : Service.IService<Drug>, Service.IGetByIDService<Drug, String>
   {
      List<Drug> GetAllWaitingDrugs();
      
      List<Drug> GetAllValidDrugs();
      
      void UpdateWaitingDrugs(List<Drug> listOfWaitingDrugs);

      List<Drug> GetAllSentValidationDrugs();

      List<Drug> GetAllSentValidationDrugsForDoctor(Doctor doctor);

    }
}