// File:    DoctorReportService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:25:41 PM
// Purpose: Definition of Class DoctorReportService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;
using Service.MedicalRecordPackageService;

namespace Service
{
   public class DoctorReportService : IDoctorReportService
   {
        public IDoctorReportRepository iDoctorReportRepository;


        private static DoctorReportService instance;
        public static DoctorReportService GetInstance()
        {
            if (instance == null)
            {
                instance = new DoctorReportService();
            }
            return instance;
        }
        public DoctorReportService()
        {
            this.iDoctorReportRepository = DoctorReportRepozitory.GetInstance();
        }

		 public DoctorReport Create(DoctorReport entity)
				=> iDoctorReportRepository.Create(entity);

       public IEnumerable<DoctorReport> GetAll()
				=> iDoctorReportRepository.GetAll();

        public List<DoctorReport> GetAllDoctorReportForPatient(MedicalRecord medicalRecord)
            => GetAll().Where(DoctorReport => (DoctorReport.MedicalRecord.SerialID.ToString().Equals(medicalRecord.SerialID.ToString()))).ToList();

        public DoctorReport GetByID(long id)
			   => iDoctorReportRepository.GetByID(id);
    }
}