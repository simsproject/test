// File:    IQuestionaireService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 4:33:44 PM
// Purpose: Definition of Interface IQuestionaireService

using Model.Other;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.OtherPackageService
{
   public interface IQuestionnaireService : Service.ICreateService<Questionnaire>, Service.IGetAllService<Questionnaire>, Service.IGetByIDService<Questionnaire, long>
   {
      List<Questionnaire> GetQuestionnairesForDoctor(Doctor doctor);
   
   }
}