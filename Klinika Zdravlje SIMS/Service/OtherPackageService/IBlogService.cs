// File:    IBlogService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 4:32:56 PM
// Purpose: Definition of Interface IBlogService

using Model.Other;
using System;

namespace Service.OtherPackageService
{
   public interface IBlogService : Service.IService<Blog>, Service.IGetByIDService<Blog, long>
   {
   }
}