// File:    INotificationService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 4:32:57 PM
// Purpose: Definition of Interface INotificationService

using Model.Other;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.OtherPackageService
{
   public interface INotificationService : Service.ICreateService<Notification>, Service.IGetAllService<Notification>
   {
      List<Notification> GetNotificationForUser(User user);

   
   }
}