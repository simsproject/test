// File:    IQuestionService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 4:32:57 PM
// Purpose: Definition of Interface IQuestionService

using Model.Other;
using System;

namespace Service.OtherPackageService
{
   public interface IQuestionService : Service.ICreateService<Question>, Service.IGetAllService<Question>, Service.IGetByIDService<Question,long>
   {
   }
}