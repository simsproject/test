// File:    AddressService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:55 PM
// Purpose: Definition of Class AddressService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;
using Service.UserPackageService;

namespace Service
{
   public class AddressService : IAddressService
    {
        public IAddressRepository iAddressRepository;

        private static AddressService instance;
        public static AddressService GetInstance()
        {
            if (instance == null)
            {
                instance = new AddressService();
            }
            return instance;
        }

        public AddressService()
        {
            this.iAddressRepository = AddressRepository.GetInstance();
        }

        public IEnumerable<Address> GetAll()
            => iAddressRepository.GetAll();
 

        public Address GetByID(long id)
            => iAddressRepository.GetByID(id);
       
    }
}