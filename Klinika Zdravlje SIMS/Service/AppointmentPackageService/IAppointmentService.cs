// File:    IAppointmentService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 5:40:10 PM
// Purpose: Definition of Interface IAppointmentService

using Model.Appointment;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.AppointmentPackageService
{
   public interface IAppointmentService : Service.IService<Appointment>, Service.IGetByIDService<Appointment, long>
   {
      
        List<Appointment> GetAllScheduleAppointmenstForDoctor(Doctor doctor);
            
        List<Appointment> GetAppointmentByDate(DateTime date);

        List<Appointment> GetAppointmentsForPatient(Patient patient);

        List<Appointment> GetAppointmentsForPatientByDate(Patient patient, DateTime dateTime);

        List<Appointment> GetAppointmentsForDoctorByDate(Doctor doctor, DateTime dateTime);


    }
}