// File:    InventoryService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:29:06 PM
// Purpose: Definition of Class InventoryService

using System;
using System.Collections.Generic;
using Model.Rooms;
using Repository;
using Repository.Abstract.RoomsPackageRepository;

namespace Service
{
   public class InventoryService : Service.RoomsPackageService.IInventoryService
   {
      public IInventoryRepository iInventoryRepository;


        private static InventoryService instance;
        public static InventoryService GetInstance()
        {
            if (instance == null)
            {
                instance = new InventoryService();
            }
            return instance;
        }
        public InventoryService()
        {
            this.iInventoryRepository = InventoryRepository.GetInstance();
        }

        public Inventory Create(Inventory entity)
            => iInventoryRepository.Create(entity);

        public void Delete(Inventory entity)
            => iInventoryRepository.Delete(entity);

        public IEnumerable<Inventory> GetAll()
            => iInventoryRepository.GetAll();

        public Inventory GetByID(long id)
            => iInventoryRepository.GetByID(id);

        public void Update(Inventory entity)
            => iInventoryRepository.Update(entity);
    }
}