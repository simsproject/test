// File:    FeedbackService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:53 PM
// Purpose: Definition of Class FeedbackService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
   public class FeedbackService : Service.UserPackageService.IFeedbackService
   {
      public IFeedbackRepository iFeedbackRepository;

        private static FeedbackService instance;
        public static FeedbackService GetInstance()
        {
            if (instance == null)
            {
                instance = new FeedbackService();
            }
            return instance;
        }
        public FeedbackService()
        {
            this.iFeedbackRepository = FeedbackRepository.GetInstance();
        }

        public Feedback Create(Feedback entity)
            => iFeedbackRepository.Create(entity);

        public IEnumerable<Feedback> GetAll()
            => iFeedbackRepository.GetAll();
    }
}