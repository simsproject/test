// File:    ISecretaryService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:01:03 PM
// Purpose: Definition of Interface ISecretaryService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface ISecretaryService : Service.IService<Secretary>, Service.IGetByIDService<Secretary,String>
   {
   }
}