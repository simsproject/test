// File:    IFeedbackService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:04:21 PM
// Purpose: Definition of Interface IFeedbackService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface IFeedbackService : Service.IGetAllService<Feedback>, Service.ICreateService<Feedback>
   {
   }
}