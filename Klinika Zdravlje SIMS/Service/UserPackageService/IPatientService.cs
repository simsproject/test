// File:    IPatientService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:00:43 PM
// Purpose: Definition of Interface IPatientService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface IPatientService : Service.IGetByIDService<Patient,String>, Service.IService<Patient>
   {
   
   }
}