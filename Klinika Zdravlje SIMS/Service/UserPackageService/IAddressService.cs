// File:    IAddressService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:03:46 PM
// Purpose: Definition of Interface IAddressService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface IAddressService : Service.IGetAllService<Address>, Service.IGetByIDService<Address,long>
   {
   }
}