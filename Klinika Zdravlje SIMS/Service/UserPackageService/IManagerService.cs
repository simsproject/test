// File:    IManagerService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:01:02 PM
// Purpose: Definition of Interface IManagerService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface IManagerService : Service.IUpdateService<Manager>, Service.IGetAllService<Manager>,IGetByIDService<Manager,string>
   {
   }
}