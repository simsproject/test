// File:    ICountryService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:03:45 PM
// Purpose: Definition of Interface ICountryService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface ICountryService : Service.IGetAllService<Country>, Service.IGetByIDService<Country, string>
   {
   }
}