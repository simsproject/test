// File:    ICityService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:03:45 PM
// Purpose: Definition of Interface ICityService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface ICityService : Service.IGetAllService<City>, Service.IGetByIDService<City, long>
    {
   }
}