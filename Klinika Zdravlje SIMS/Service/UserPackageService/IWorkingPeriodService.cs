// File:    IWorkingPeriodService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:04:34 PM
// Purpose: Definition of Interface IWorkingPeriodService

using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.UserPackageService
{
   public interface IWorkingPeriodService : Service.IService<WorkingPeriod>,IGetByIDService<WorkingPeriod,long>
   {
      
      WorkingPeriod WorkingPeriodForDoctorByDate(Doctor doctor, DateTime date);

      IEnumerable<DateTime> EachDay(DateTime from, DateTime thru);

    }
}