// File:    IDoctorService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:00:53 PM
// Purpose: Definition of Interface IDoctorService

using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.UserPackageService
{
   public interface IDoctorService : Service.IService<Doctor>, Service.IGetByIDService<Doctor,String>
   {
      List<Doctor> GetDoctorByTitle(Model.Users.Title title);
      Dictionary<Doctor, WorkingTime> FilterDoctorsByWorkingTime(DateTime date);


   }
}