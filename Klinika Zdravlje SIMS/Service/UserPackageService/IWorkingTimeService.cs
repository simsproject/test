// File:    IWorkingTimeService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:04:33 PM
// Purpose: Definition of Interface IWorkingTimeService

using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.UserPackageService
{
   public interface IWorkingTimeService : Service.IService<WorkingTime>,IGetByIDService<WorkingTime,long>
   {
                  
      WorkingTime GetWorkingTimeForDoctorByDate(Doctor doctor, DateTime date);
      
      WorkingTime GetWorkingTimeForDate(WorkingPeriod workingPeriod, DateTime date);



   }
}