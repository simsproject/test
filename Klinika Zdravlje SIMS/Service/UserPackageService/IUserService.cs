// File:    IUserService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 10:01:06 PM
// Purpose: Definition of Interface IUserService

using Model.Users;
using System;

namespace Service.UserPackageService
{
   public interface IUserService : Service.IService<User>, Service.IGetByIDService<User,String>
   {
   }
}