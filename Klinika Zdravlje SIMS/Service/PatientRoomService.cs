// File:    PatientRoomService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:29:42 PM
// Purpose: Definition of Class PatientRoomService

using System;
using System.Collections.Generic;
using Model.Rooms;
using Repository;
using Repository.Abstract.RoomsPackageRepository;

namespace Service
{
   public class PatientRoomService : Service.RoomsPackageService.IPatientRoomService
   {
      public Repository.Abstract.RoomsPackageRepository.IPatientRoomRepository iPatientRoomRepository;


        private static PatientRoomService instance;
        public static PatientRoomService GetInstance()
        {
            if (instance == null)
            {
                instance = new PatientRoomService();
            }
            return instance;
        }

        public PatientRoomService()
        {
            this.iPatientRoomRepository = PatientRoomRepository.GetInstance();
        }

        public PatientRoom Create(PatientRoom entity)
        => iPatientRoomRepository.Create(entity);

        public void Delete(PatientRoom entity)
        => iPatientRoomRepository.Delete(entity);

        public IEnumerable<PatientRoom> GetAll()
        =>iPatientRoomRepository.GetAll();
        

        public PatientRoom GetByID(string id)
        =>iPatientRoomRepository.GetByID(id);
        

        public void Update(PatientRoom entity)
        => iPatientRoomRepository.Update(entity);
    }
}