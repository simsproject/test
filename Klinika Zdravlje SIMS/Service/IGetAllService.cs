// File:    IGetAllService.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 4:15:15 PM
// Purpose: Definition of Interface IGetAllService

using System;
using System.Collections.Generic;

namespace Service
{
   public interface IGetAllService<E>
   {
      IEnumerable<E> GetAll();
   
   }
}