// File:    SecretaryService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:02 PM
// Purpose: Definition of Class SecretaryService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
   public class SecretaryService : Service.UserPackageService.ISecretaryService
   {
      public ISecretaryRepository iSecretaryRepository;


        private static SecretaryService instance;
        public static SecretaryService GetInstance()
        {
            if (instance == null)
            {
                instance = new SecretaryService();
            }
            return instance;
        }
        public SecretaryService()
		{
			this.iSecretaryRepository = SecretaryRepository.GetInstance();
		}

		public Secretary Create(Secretary entity)
            => iSecretaryRepository.Create(entity);

        public void Delete(Secretary entity)
            => iSecretaryRepository.Delete(entity);

		public IEnumerable<Secretary> GetAll()
		 => iSecretaryRepository.GetAll();

		public Secretary GetByID(string id)
		 => iSecretaryRepository.GetByID(id);

        public void Update(Secretary entity)
            => iSecretaryRepository.Update(entity);
    }
}