// File:    BedService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:29:05 PM
// Purpose: Definition of Class BedService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;
using Repository;
using Repository.Abstract.RoomsPackageRepository;
using Service.RoomsPackageService;

namespace Service
{
   public class BedService : IBedService
   {
		  public IBedRepository iBedRepository;

        private static BedService instance;
        public static BedService GetInstance()
        {
            if (instance == null)
            {
                instance = new BedService();
            }
            return instance;
        }

        public BedService()
        {
            this.iBedRepository = BedRepository.GetInstance();
        }

		  public Bed Create(Bed entity)
				=> iBedRepository.Create(entity);

		  public void Delete(Bed entity)
				=> iBedRepository.Delete(entity);

        public IEnumerable<Bed> GetAll()
				=> iBedRepository.GetAll();
        

        public Bed GetByID(int id)
            => iBedRepository.GetByID(id);


        public List<Bed> GetFreeBeds(Room room)
            => GetAll().Where(Bed => (Bed.PatientRoom.Name.Equals(room.Name) && IsBedFree(Bed))).ToList();


        public bool IsBedFree(Bed bed)
            => bed.Patient != null ? false : true;

		  public void Update(Bed entity)
				=> iBedRepository.Update(entity);
    }
}