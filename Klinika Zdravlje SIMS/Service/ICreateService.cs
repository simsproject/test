// File:    ICreateService.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 4:15:16 PM
// Purpose: Definition of Interface ICreateService

using System;

namespace Service
{
   public interface ICreateService<E>
   {
      E Create(E entity);
   
   }
}