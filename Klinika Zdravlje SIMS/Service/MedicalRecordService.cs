// File:    MedicalRecordService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:26:24 PM
// Purpose: Definition of Class MedicalRecordService

using System;
using System.Collections.Generic;
using Model.MedicalRecords;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;

namespace Service
{
   public class MedicalRecordService : Service.MedicalRecordPackageService.IMedicalRecordService
   {
      public Repository.Abstract.MedicalRecordPackageRepository.IMedicalRecordRepository iMedicalRecordRepository;

        private static MedicalRecordService instance;
        public static MedicalRecordService GetInstance()
        {
            if (instance == null)
            {
                instance = new MedicalRecordService();
            }
            return instance;
        }

        public MedicalRecordService()
        {
            this.iMedicalRecordRepository = MedicalRecordRepository.GetInstance();
        }

        public void AddAllergenToMedicalRecord(Allergens allergen, long id)
        {
            MedicalRecord md = GetByID(id);
            md.AddAllergens(allergen);
            Update(md);
        }

        public MedicalRecord Create(MedicalRecord entity)
        =>iMedicalRecordRepository.Create(entity);
        

        public IEnumerable<MedicalRecord> GetAll()
        =>iMedicalRecordRepository.GetAll();
        

        public List<Allergens> GetAllergens(long id)
        => GetByID(id).Allergens;

        public MedicalRecord GetByID(long id)
        =>iMedicalRecordRepository.GetByID(id);
        

        public void Update(MedicalRecord entity)
        =>iMedicalRecordRepository.Update(entity);
        
    }
}