// File:    IGetByIDService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 5:15:26 PM
// Purpose: Definition of Interface IGetByIDService

using System;

namespace Service
{
   public interface IGetByIDService<E,ID>
   {
      E GetByID(ID id);
   
   }
}