// File:    BlogService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:27:23 PM
// Purpose: Definition of Class BlogService

using System;
using System.Collections.Generic;
using Model.Other;
using Repository.Abstract.OtherPackageRepository;
using Service.OtherPackageService;

namespace Service
{
   public class BlogService : IBlogService
   {
      public IBlogRepository iBlogRepository;

        public Blog Create(Blog entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Blog entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Blog> GetAll()
        {
            throw new NotImplementedException();
        }

        public Blog GetByID(long id)
        {
            throw new NotImplementedException();
        }

        public void Update(Blog entity)
        {
            throw new NotImplementedException();
        }
    }
}