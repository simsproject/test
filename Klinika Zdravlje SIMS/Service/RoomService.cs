// File:    RoomService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:29:44 PM
// Purpose: Definition of Class RoomService

using System;
using System.Collections.Generic;
using Model.Rooms;
using Repository;
using Repository.Abstract.RoomsPackageRepository;

namespace Service
{
   public class RoomService : Service.RoomsPackageService.IRoomService
   {
      public Repository.Abstract.RoomsPackageRepository.IRoomRepository iRoomRepository;


        private static RoomService instance;
        public static RoomService GetInstance()
        {
            if (instance == null)
            {
                instance = new RoomService();
            }
            return instance;
        }
        public RoomService() {
            this.iRoomRepository = RoomRepository.GetInstance();
        }

		  public Room Create(Room entity)
				=> iRoomRepository.Create(entity);

        public void Delete(Room entity)
            => iRoomRepository.Delete(entity);

        public IEnumerable<Room> GetAll()
            => iRoomRepository.GetAll();

        public Room GetByID(string id)
            => iRoomRepository.GetByID(id);


        public void Update(Room entity)
        => iRoomRepository.Update(entity);
    }
}