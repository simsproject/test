// File:    IAllergensService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:03:12 PM
// Purpose: Definition of Interface IAllergensService

using Model.MedicalRecords;
using System;

namespace Service.MedicalRecordPackageService
{
   public interface IAllergensService : Service.IGetAllService<Allergens>, Service.IGetByIDService<Allergens, String>, Service.ICreateService<Allergens>
   {
   }
}