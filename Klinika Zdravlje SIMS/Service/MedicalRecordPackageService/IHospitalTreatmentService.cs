// File:    IHospitalTreatmentService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:03:37 PM
// Purpose: Definition of Interface IHospitalTreatmentService

using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Service.MedicalRecordPackageService
{
   public interface IHospitalTreatmentService : Service.IGetAllService<HospitalTreatment>, Service.IGetByIDService<HospitalTreatment, long>, Service.ICreateService<HospitalTreatment>
   {
      List<HospitalTreatment> GetAllHospitalTreatmentsForPatient(MedicalRecord medicalRecord);
      
      void CreateMedicalHistory(HospitalTreatment hospitalTreatment);
   
   }
}