// File:    ISpecialistTreatmentService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:04:10 PM
// Purpose: Definition of Interface ISpecialistTreatmentService

using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Service.MedicalRecordPackageService
{
   public interface ISpecialistTreatmentService : Service.IGetByIDService<SpecialistTreatment, long>, Service.IGetAllService<SpecialistTreatment>, Service.ICreateService<SpecialistTreatment>
   {
      List<SpecialistTreatment> GetAllSpecialistTreatmentsForPatient(MedicalRecord medicalRecord);
   
   }
}