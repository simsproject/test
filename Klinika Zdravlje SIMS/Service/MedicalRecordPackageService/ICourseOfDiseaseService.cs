// File:    ICourseOfDiseaseService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:00:26 PM
// Purpose: Definition of Interface ICourseOfDiseaseService

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Service.MedicalRecordPackageService
{
   public interface ICourseOfDiseaseService : Service.IGetAllService<CourseOfDisease>, Service.IGetByIDService<CourseOfDisease, long>, Service.ICreateService<CourseOfDisease>
   {
      List<CourseOfDisease> GetAllCourseOfDiseasesForMedicalHistory(MedicalHistory medicalHistory);
   
   }
}