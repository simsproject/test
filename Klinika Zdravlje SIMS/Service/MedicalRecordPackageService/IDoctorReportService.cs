// File:    IDoctorReportService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:03:24 PM
// Purpose: Definition of Interface IDoctorReportService

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Service.MedicalRecordPackageService
{
   public interface IDoctorReportService : Service.IGetByIDService<DoctorReport, long>, Service.IGetAllService<DoctorReport>, Service.ICreateService<DoctorReport>
   {
      List<DoctorReport> GetAllDoctorReportForPatient(MedicalRecord medicalRecord);
   
   }
}