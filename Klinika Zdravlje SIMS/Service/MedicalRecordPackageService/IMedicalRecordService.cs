// File:    IMedicalRecordService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:00:02 PM
// Purpose: Definition of Interface IMedicalRecordService

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Service.MedicalRecordPackageService
{
   public interface IMedicalRecordService : Service.IUpdateService<MedicalRecord>, Service.IGetByIDService<MedicalRecord, long>, Service.IGetAllService<MedicalRecord>, Service.ICreateService<MedicalRecord>
   {
      List<Allergens> GetAllergens(long id);
      
      void AddAllergenToMedicalRecord(Allergens allergen,long id);
   
   }
}