// File:    IRecipeService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:06:33 PM
// Purpose: Definition of Interface IRecipeService

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Service.MedicalRecordPackageService
{
   public interface IRecipeService : Service.IGetByIDService<Recipe,long>, Service.IGetAllService<Recipe>, Service.ICreateService<Recipe>
   {
      List<Recipe> GetAllRecipesForPatient(MedicalRecord medicalRecord);
   
   }
}