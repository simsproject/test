// File:    IMedicalHistoryService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 6:00:15 PM
// Purpose: Definition of Interface IMedicalHistoryService

using Model.MedicalRecords;
using System;
using System.Collections.Generic;

namespace Service.MedicalRecordPackageService
{
   public interface IMedicalHistoryService : Service.IUpdateService<MedicalHistory>, Service.ICreateService<MedicalHistory>, Service.IGetByIDService<MedicalHistory, long>, Service.IGetAllService<MedicalHistory>
   {
      List<MedicalHistory> GetAllMedicalHistoriesForPatient(MedicalRecord medicalRecord);
      
      Boolean IsDateGood(MedicalHistory medicalHistory);

   
   }
}