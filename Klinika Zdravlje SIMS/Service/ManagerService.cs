// File:    ManagerService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:54 PM
// Purpose: Definition of Class ManagerService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
   public class ManagerService : Service.UserPackageService.IManagerService
   {
      public  IManagerRepository iManagerRepository;

        private static ManagerService instance;
        public static ManagerService GetInstance()
        {
            if (instance == null)
            {
                instance = new ManagerService();
            }
            return instance;
        }
        public ManagerService()
        {
            iManagerRepository = ManagerRepository.GetInstance();
        }
        public IEnumerable<Manager> GetAll() 
            => iManagerRepository.GetAll();

        public Manager GetByID(string id)
            => iManagerRepository.GetByID(id);

        public void Update(Manager entity)
            => iManagerRepository.Update(entity);
    }
}