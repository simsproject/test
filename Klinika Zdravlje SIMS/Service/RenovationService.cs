// File:    RenovationService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:29:44 PM
// Purpose: Definition of Class RenovationService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Rooms;
using Repository;
using Repository.Abstract.RoomsPackageRepository;

namespace Service
{
   public class RenovationService : Service.RoomsPackageService.IRenovationService
   {
        public IRenovationRepository iRenovationRepository;

        private static RenovationService instance;
        public static RenovationService GetInstance()
        {
            if (instance == null)
            {
                instance = new RenovationService();
            }
            return instance;
        }

        public RenovationService()
        {
            this.iRenovationRepository = RenovationRepository.GetInstance();
        }

        public Renovation Create(Renovation entity)
        => iRenovationRepository.Create(entity);

        public void Delete(Renovation entity)
        => iRenovationRepository.Delete(entity);

        public IEnumerable<Renovation> GetAll()
        => iRenovationRepository.GetAll();

        public Renovation GetByID(long id)
        => iRenovationRepository.GetByID(id);


        public List<Renovation> GetRenovationsForRoom(Room room)
         => GetAll().Where(Renovation => (Renovation.Room.Name.Equals(room.Name))).ToList();
    }
}