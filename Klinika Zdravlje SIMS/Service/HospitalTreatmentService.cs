// File:    HospitalTreatmentService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:25:42 PM
// Purpose: Definition of Class HospitalTreatmentService

using System;
using System.Collections.Generic;
using System.Linq;
using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;

namespace Service
{
   public class HospitalTreatmentService : Service.MedicalRecordPackageService.IHospitalTreatmentService
   {
      public MedicalHistoryService medicalHistoryService;
      public IHospitalTreatmentRepository iHospitalTreatmentRepository;

        private static HospitalTreatmentService instance;
        public static HospitalTreatmentService GetInstance()
        {
            if (instance == null)
            {
                instance = new HospitalTreatmentService();
            }
            return instance;
        }

        public HospitalTreatmentService()
        {
            this.iHospitalTreatmentRepository = HospitalTreatmentRepository.GetInstance();
            this.medicalHistoryService = MedicalHistoryService.GetInstance();
        }

        public HospitalTreatment Create(HospitalTreatment entity)
        {
            CreateMedicalHistory(entity);
            return iHospitalTreatmentRepository.Create(entity);
        }

        public void CreateMedicalHistory(HospitalTreatment hospitalTreatment)
        {
            MedicalHistory mh = new MedicalHistory("Opis"
                , hospitalTreatment.StartDate
                , "na lecenju"
                , hospitalTreatment.Note, "", "", 0, hospitalTreatment.MedicalRecord);
            medicalHistoryService.Create(mh);
        }

        public IEnumerable<HospitalTreatment> GetAll()
            => iHospitalTreatmentRepository.GetAll();

        public List<HospitalTreatment> GetAllHospitalTreatmentsForPatient(MedicalRecord medicalRecord)
        => GetAll().Where(SpecialistTreatment => (SpecialistTreatment.MedicalRecord.SerialID == medicalRecord.SerialID)).ToList();

        public HospitalTreatment GetByID(long id)
            => iHospitalTreatmentRepository.GetByID(id);
    }
}