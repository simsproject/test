// File:    PatientService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:01 PM
// Purpose: Definition of Class PatientService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
   public class PatientService : Service.UserPackageService.IPatientService
   {
      public Repository.Abstract.UserPackageRepository.IPatientRepository iPatientRepository;

        private static PatientService instance;
        public static PatientService GetInstance()
        {
            if (instance == null)
            {
                instance = new PatientService();
            }
            return instance;
        }

        public PatientService()
        {
            this.iPatientRepository = PatientRepository.GetInstance();
        }

        public Patient Create(Patient entity)
        => iPatientRepository.Create(entity);

        public void Delete(Patient entity)
        => iPatientRepository.Delete(entity);

        public IEnumerable<Patient> GetAll()
        =>iPatientRepository.GetAll();
        

        public Patient GetByID(string id)
        =>iPatientRepository.GetByID(id);    
        

        public void Update(Patient entity)
        => iPatientRepository.Update(entity);
    }
}