// File:    MedicalRoomService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:29:32 PM
// Purpose: Definition of Class MedicalRoomService

using System;
using System.Collections.Generic;
using Model.Rooms;
using Model.Users;
using Repository;
using Repository.Abstract.RoomsPackageRepository;

namespace Service
{
   public class MedicalRoomService : Service.RoomsPackageService.IMedicalRoomService
   {
      public Repository.Abstract.RoomsPackageRepository.IMedicalRoomRepository iMedicalRoomRepository;


        private static MedicalRoomService instance;
        public static MedicalRoomService GetInstance()
        {
            if (instance == null)
            {
                instance = new MedicalRoomService();
            }
            return instance;
        }
        public MedicalRoomService()
        {
            this.iMedicalRoomRepository = MedicalRoomRepository.GetInstance();
        }

        public MedicalRoom Create(MedicalRoom entity)
        => iMedicalRoomRepository.Create(entity);

        public void Delete(MedicalRoom entity)
        => iMedicalRoomRepository.Delete(entity);

        public IEnumerable<MedicalRoom> GetAll()
        => iMedicalRoomRepository.GetAll();


        public MedicalRoom GetByID(string id)
            => iMedicalRoomRepository.GetByID(id);


        public void Update(MedicalRoom entity)
            => iMedicalRoomRepository.Update(entity);
    }
}