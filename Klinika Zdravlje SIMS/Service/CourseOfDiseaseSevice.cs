// File:    CourseOfDiseaseSevice.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:25:14 PM
// Purpose: Definition of Class CourseOfDiseaseSevice

using System;
using System.Collections.Generic;
using System.Linq;
using Model.MedicalRecords;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;
using Service.MedicalRecordPackageService;

namespace Service
{
   public class CourseOfDiseaseSevice : ICourseOfDiseaseService
   {
      public ICourseOfDiseaseRepository iCourseOfDiseaseRepository;


        private static CourseOfDiseaseSevice instance;
        public static CourseOfDiseaseSevice GetInstance()
        {
            if (instance == null)
            {
                instance = new CourseOfDiseaseSevice();
            }
            return instance;
        }


        public CourseOfDiseaseSevice()
        {
            this.iCourseOfDiseaseRepository = CourseOfDiseaseRepository.GetInstance();
        }

        public CourseOfDisease Create(CourseOfDisease entity)
            => iCourseOfDiseaseRepository.Create(entity);

        public IEnumerable<CourseOfDisease> GetAll()
            => iCourseOfDiseaseRepository.GetAll();

        public List<CourseOfDisease> GetAllCourseOfDiseasesForMedicalHistory(MedicalHistory medicalHistory)
            => GetAll().Where(CourseOfDisease => (CourseOfDisease.MedicalHistory.SerialID.ToString().Equals(medicalHistory.SerialID.ToString()))).ToList();

        public CourseOfDisease GetByID(long id)
            => iCourseOfDiseaseRepository.GetByID(id);
    }
}