// File:    AllergensService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:25:13 PM
// Purpose: Definition of Class AllergensService

using System;
using System.Collections.Generic;
using Model.MedicalRecords;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;
using Service.MedicalRecordPackageService;

namespace Service
{
    public class AllergensService : IAllergensService
    {
        public IAllergensRepository iAllergensRepository;

        private static AllergensService instance;
        public static AllergensService GetInstance()
        {
            if (instance == null)
            {
                instance = new AllergensService();
            }
            return instance;
        }


        public AllergensService()
        {
            this.iAllergensRepository = AllergensRepository.GetInstance();
        }

		  public Allergens Create(Allergens entity)
				=> iAllergensRepository.Create(entity);

        public IEnumerable<Allergens> GetAll()
            => iAllergensRepository.GetAll();

        public Allergens GetByID(string id)
            => iAllergensRepository.GetByID(id);
    }
}