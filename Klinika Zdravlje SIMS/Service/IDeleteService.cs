// File:    IDeleteService.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 4:15:16 PM
// Purpose: Definition of Interface IDeleteService

using System;

namespace Service
{
   public interface IDeleteService<E>
   {
      void Delete(E entity);
   
   }
}