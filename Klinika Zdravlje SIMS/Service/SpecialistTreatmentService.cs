// File:    SpecialistTreatmentService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:25:42 PM
// Purpose: Definition of Class SpecialistTreatmentService

using System;
using System.Collections.Generic;
using System.Linq;
using Controller.MedicalRecordPackageController;
using Model.MedicalRecords;
using Repository;
using Repository.Abstract.MedicalRecordPackageRepository;

namespace Service
{
   public class SpecialistTreatmentService : Service.MedicalRecordPackageService.ISpecialistTreatmentService
   {
      public ISpecialistTreatmentRepository iSpecialistTreatmentRepository;

        private static SpecialistTreatmentService instance;
        public static SpecialistTreatmentService GetInstance()
        {
            if (instance == null)
            {
                instance = new SpecialistTreatmentService();
            }
            return instance;
        }
        public SpecialistTreatmentService()
		{
			this.iSpecialistTreatmentRepository = SpecialistTreatmentRepository.GetInstance();
		}

        public SpecialistTreatment Create(SpecialistTreatment entity)
            => iSpecialistTreatmentRepository.Create(entity);

		public IEnumerable<SpecialistTreatment> GetAll()
		 => iSpecialistTreatmentRepository.GetAll();


        public List<SpecialistTreatment> GetAllSpecialistTreatmentsForPatient(MedicalRecord medicalRecord)
        => GetAll().Where(SpecialistTreatment => (SpecialistTreatment.MedicalRecord.SerialID == medicalRecord.SerialID)).ToList();

        public SpecialistTreatment GetByID(long id)
		 => iSpecialistTreatmentRepository.GetByID(id);
    }
}