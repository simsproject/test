// File:    QuestionService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:27:25 PM
// Purpose: Definition of Class QuestionService

using System;
using System.Collections.Generic;
using Model.Other;
using Repository;
using Repository.Abstract.OtherPackageRepository;

namespace Service
{
   public class QuestionService : Service.OtherPackageService.IQuestionService
   {
      public Repository.Abstract.OtherPackageRepository.IQuestionRepository iQuestionRepository;


        private static QuestionService instance;
        public static QuestionService GetInstance()
        {
            if (instance == null)
            {
                instance = new QuestionService();
            }
            return instance;
        }

        public QuestionService()
        {
            this.iQuestionRepository = QuestionRepository.GetInstance();
        }

        public Question Create(Question entity)
        => iQuestionRepository.Create(entity);

        public IEnumerable<Question> GetAll()
        => iQuestionRepository.GetAll();

        public Question GetByID(long id)
        => iQuestionRepository.GetByID(id);
    }
}