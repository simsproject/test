// File:    AppointmentService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:22:43 PM
// Purpose: Definition of Class AppointmentService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Appointment;
using Model.Users;
using Repository;
using Repository.Abstract.AppointmentPackageRepository;
using Service.AppointmentPackageService;

namespace Service
{
   public class AppointmentService : IAppointmentService
   {

      public IAppointmentRepository iAppointmentRepository;
	  
		private static AppointmentService instance;
		public static AppointmentService GetInstance()
		{
			if (instance == null)
			{
				instance = new AppointmentService();
			}
			return instance;
		}

		public AppointmentService()
        {
            this.iAppointmentRepository = AppointmentRepository.GetInstance();				
        }

		  public Appointment Create(Appointment entity)
				=> iAppointmentRepository.Create(entity);

		  public void Delete(Appointment entity)
			   => iAppointmentRepository.Delete(entity);


		public IEnumerable<Appointment> GetAll()
            => iAppointmentRepository.GetAll();


        public List<Appointment> GetAllScheduleAppointmenstForDoctor(Doctor doctor)
            => GetAll().Where(Appointment => (Appointment.Doctor.PersonalID.Equals(doctor.PersonalID))).ToList();

		public List<Appointment> GetAppointmentByDate(DateTime date)
			=> GetAll().Where(Appointment =>(DateTime.Compare(Appointment.StartDate.Date,date.Date) == 0)).ToList();



		public Appointment GetByID(long id)
            => iAppointmentRepository.GetByID(id);


		public void Update(Appointment entity)
			=> iAppointmentRepository.Update(entity);

		public List<Appointment> GetAppointmentsForDoctorByDate(Doctor doctor, DateTime dateTime)
			=> GetAllScheduleAppointmenstForDoctor(doctor).Where(Appointment => (DateTime.Compare(Appointment.StartDate.Date, dateTime.Date) == 0)).ToList();

		public List<Appointment> GetAppointmentsForPatient(Patient patient)
			=> GetAll().Where(Appointment => (Appointment.Patient.PersonalID.Equals(patient.PersonalID))).ToList();

		public List<Appointment> GetAppointmentsForPatientByDate(Patient patient, DateTime dateTime)
			=> GetAppointmentsForPatient(patient).Where(Appointment => DateTime.Compare(Appointment.StartDate, DateTime.Now) > 0).ToList();
	}
}