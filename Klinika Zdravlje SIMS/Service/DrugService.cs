// File:    DrugService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:23:25 PM
// Purpose: Definition of Class DrugService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Drugs;
using Model.Users;
using Repository;
using Repository.Abstract.DrugsPackageRepository;

namespace Service
{
    public class DrugService : Service.DrugsPackageService.IDrugService
    {
       public IDrugRepository iDrugRepository;

        private static DrugService instance;
        public static DrugService GetInstance()
        {
            if (instance == null)
            {
                instance = new DrugService();
            }
            return instance;
        }

        public DrugService()
		{
			this.iDrugRepository = DrugRepository.GetInstance();
		}

		public Drug Create(Drug entity)
            => iDrugRepository.Create(entity);

        public void Delete(Drug entity)
        => iDrugRepository.Delete(entity);

       public IEnumerable<Drug> GetAll() 
			=> iDrugRepository.GetAll();


        public List<Drug> GetAllValidDrugs()
            => GetAll().Where(Drug => (Drug.State.ToString().Equals("valid"))).ToList();

        public List<Drug> GetAllWaitingDrugs()
          => GetAll().Where(Drug => (Drug.State.ToString().Equals("waiting"))).ToList();

      public Drug GetByID(string id) 
			=> iDrugRepository.GetByID(id);

	public void Update(Drug entity)
        => iDrugRepository.Update(entity);

        public void UpdateWaitingDrugs(List<Drug> listOfWaitingDrugs)
        {
            foreach (Drug d in listOfWaitingDrugs) {
                d.State = DrugState.sentForValidation;
                Update(d);
            }
        }

        public List<Drug> GetAllSentValidationDrugs()
            => GetAll().Where(Drug => (Drug.State.ToString().Equals("sentForValidation"))).ToList();

        public List<Drug> GetAllSentValidationDrugsForDoctor(Doctor doctor)
            => GetAllSentValidationDrugs().Where(Drug => (Drug.Doc.PersonalID.Equals(doctor.PersonalID))).ToList();
    }
}