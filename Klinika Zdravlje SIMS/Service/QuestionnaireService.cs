// File:    QuestionaireService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:27:24 PM
// Purpose: Definition of Class QuestionaireService

using System;
using System.Collections.Generic;
using System.Linq;
using Model.Other;
using Model.Users;
using Repository;
using Repository.Abstract.OtherPackageRepository;

namespace Service
{
   public class QuestionnaireService : Service.OtherPackageService.IQuestionnaireService
   {
      public Repository.Abstract.OtherPackageRepository.IQuestionnaireRepository iQuestionaireRepository;

        private static QuestionnaireService instance;
        public static QuestionnaireService GetInstance()
        {
            if (instance == null)
            {
                instance = new QuestionnaireService();
            }
            return instance;
        }

        public QuestionnaireService()
        {
            this.iQuestionaireRepository = QuestionnaireRepository.GetInstance();
        }

        public Questionnaire Create(Questionnaire entity)
        => iQuestionaireRepository.Create(entity);

        public IEnumerable<Questionnaire> GetAll()
        => iQuestionaireRepository.GetAll();

        public Questionnaire GetByID(long id)
        => iQuestionaireRepository.GetByID(id);

        public List<Questionnaire> GetQuestionnairesForDoctor(Doctor doctor)
          => GetAll().Where(Questionnaire => (Questionnaire.Doctor != null)).ToList().Where(Questionnaire => (Questionnaire.Doctor.PersonalID.Equals(doctor.PersonalID))).ToList();
    }
}