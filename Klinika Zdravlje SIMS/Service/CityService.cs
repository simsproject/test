// File:    CityService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:56 PM
// Purpose: Definition of Class CityService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;
using Service.UserPackageService;

namespace Service
{
   public class CityService : ICityService
   {
        public ICityRepository iCityRepository;

        private static CityService instance;
        public static CityService GetInstance()
        {
            if (instance == null)
            {
                instance = new CityService();
            }
            return instance;
        }

        public CityService()
        {
            iCityRepository = CityRepository.GetInstance();
        }

        public IEnumerable<City> GetAll()
				=> iCityRepository.GetAll();

        public City GetByID(long id)
				=> iCityRepository.GetByID(id);
    }
}