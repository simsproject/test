// File:    UserService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:32:03 PM
// Purpose: Definition of Class UserService

using System;
using System.Collections.Generic;
using Model.Users;
using Repository;
using Repository.Abstract.UserPackageRepository;

namespace Service
{
   public class UserService : Service.UserPackageService.IUserService
   {
      public IUserRepository iUserRepository;

        private static UserService instance;
        public static UserService GetInstance()
        {
            if (instance == null)
            {
                instance = new UserService();
            }
            return instance;
        }
        public UserService()
        {
            this.iUserRepository = UserRepository.GetInstance();
        }

        public User Create(User entity)
            => iUserRepository.Create(entity);

        public void Delete(User entity)
            => iUserRepository.Delete(entity);

        public IEnumerable<User> GetAll()
            => iUserRepository.GetAll();

        public User GetByID(string id)
            => iUserRepository.GetByID(id);

        public void Update(User entity)
            => iUserRepository.Update(entity);
    }
}