// File:    IUpdateService.cs
// Author:  Korisnik
// Created: Sunday, May 24, 2020 4:15:17 PM
// Purpose: Definition of Interface IUpdateService

using System;

namespace Service
{
   public interface IUpdateService<E>
   {
      void Update(E entity);
   
   }
}