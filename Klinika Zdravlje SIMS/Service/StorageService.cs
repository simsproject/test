// File:    StorageService.cs
// Author:  Velickovic
// Created: Monday, May 25, 2020 11:30:10 PM
// Purpose: Definition of Class StorageService

using System;
using System.Collections.Generic;
using Model.Rooms;
using Repository;
using Repository.Abstract.RoomsPackageRepository;

namespace Service
{
   public class StorageService : Service.RoomsPackageService.IStorageService
   {
      public IStorageRepository iStorageRepository;


        private static StorageService instance;
        public static StorageService GetInstance()
        {
            if (instance == null)
            {
                instance = new StorageService();
            }
            return instance;
        }
        public StorageService()
        {
            this.iStorageRepository = StorageRepository.GetInstance();
        }

        public Storage Create(Storage entity)
            => iStorageRepository.Create(entity);

        public void Delete(Storage entity)
            => iStorageRepository.Delete(entity);

        public IEnumerable<Storage> GetAll()
        => iStorageRepository.GetAll();

        public Storage GetByID(string id)
        => iStorageRepository.GetByID(id);

        public void Update(Storage entity)
            => iStorageRepository.Update(entity);
    }
}