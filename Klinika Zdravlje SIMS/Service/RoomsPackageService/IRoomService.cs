// File:    IRoomService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:30:00 PM
// Purpose: Definition of Interface IRoomService

using Model.Rooms;
using System;
using System.Collections.Generic;

namespace Service.RoomsPackageService
{
   public interface IRoomService : Service.IService<Room>, Service.IGetByIDService<Room, string>
   {
   }
}