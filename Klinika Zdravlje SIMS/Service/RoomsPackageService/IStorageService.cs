// File:    IStorageService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:29:37 PM
// Purpose: Definition of Interface IStorageService

using Model.Rooms;
using System;

namespace Service.RoomsPackageService
{
   public interface IStorageService : Service.IService<Storage>, Service.IGetByIDService<Storage, string>
    {
   }
}