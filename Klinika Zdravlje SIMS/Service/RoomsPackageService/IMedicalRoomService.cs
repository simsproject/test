// File:    IMedicalRoomService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:28:57 PM
// Purpose: Definition of Interface IMedicalRoomService

using Model.Rooms;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.RoomsPackageService
{
   public interface IMedicalRoomService : Service.IService<MedicalRoom>,IGetByIDService<MedicalRoom,string>
   {
   }
}