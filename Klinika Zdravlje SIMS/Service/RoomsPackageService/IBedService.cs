// File:    IBedService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:31:00 PM
// Purpose: Definition of Interface IBedService

using Model.Rooms;
using System;
using System.Collections.Generic;

namespace Service.RoomsPackageService
{
   public interface IBedService : Service.IGetByIDService<Bed, int>, Service.IService<Bed>
   {
      Boolean IsBedFree(Bed bed);
      
      List<Bed> GetFreeBeds(Room room);
   
   }
}