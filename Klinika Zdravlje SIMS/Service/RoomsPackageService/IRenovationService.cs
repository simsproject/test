// File:    IRenovationService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:31:02 PM
// Purpose: Definition of Interface IRenovationService

using Model.Rooms;
using System;
using System.Collections.Generic;

namespace Service.RoomsPackageService
{
   public interface IRenovationService : Service.ICreateService<Renovation>, Service.IDeleteService<Renovation>, Service.IGetAllService<Renovation>, Service.IGetByIDService<Renovation, long>
   {
        List<Renovation> GetRenovationsForRoom(Room room);
   }
}