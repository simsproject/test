// File:    IOfficeService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:29:26 PM
// Purpose: Definition of Interface IOfficeService

using Model.Rooms;
using Model.Users;
using System;
using System.Collections.Generic;

namespace Service.RoomsPackageService
{
   public interface IOfficeService : Service.IService<Office>,IGetByIDService<Office,string>
   {
   }
}