// File:    IInventoryService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:30:17 PM
// Purpose: Definition of Interface IInventoryService

using Model.Rooms;
using System;

namespace Service.RoomsPackageService
{
   public interface IInventoryService : Service.IService<Inventory>, Service.IGetByIDService<Inventory, long>
   {
   }
}