// File:    IPatientRoomService.cs
// Author:  Velickovic
// Created: Sunday, May 24, 2020 7:30:01 PM
// Purpose: Definition of Interface IPatientRoomService

using Model.Rooms;
using System;

namespace Service.RoomsPackageService
{
   public interface IPatientRoomService : Service.IService<PatientRoom>, Service.IGetByIDService<PatientRoom,string>
   {
        
   }
}