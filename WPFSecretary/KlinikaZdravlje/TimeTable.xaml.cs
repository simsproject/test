﻿using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for TimeTable.xaml
	/// </summary>
	public partial class TimeTable : Page
	{
		public static DateTime selectedDate;
		public List<AllAppointments> allOpenAppointments = null;
		public bool ucitano = false;
		public AllAppointments selectedAppointment = null;
		public TimeTable()
		{



			InitializeComponent();
			ucitano = true;


			selectedDate = dateTime.SelectedDate.Value.Date;

			//DateTime date = DatePicker.SelectedDate.Value;
			List<Appointment> appointmentsByDate = filterAllAppointmentsByDate(selectedDate); //zakazani termini za izabrani datum
			Dictionary<Doctor, WorkingTime> dictionaryForWorkingTime = filterDoctorByWorkingPeriod(selectedDate);
			allOpenAppointments = GetOpenAppointments(appointmentsByDate, dictionaryForWorkingTime);
			




			OpenAppointmentDataGrid.ItemsSource = allOpenAppointments;
		}
		public class Pregled
		{
			public string Datum { get; set; }
			public string Vreme { get; set; }
			public string Lekar { get; set; }
			public string Soba { get; set; }
		}


	






		public List<Appointment> filterAllAppointmentsByDate(DateTime dateTime) //daj mi sve zauzete termine za izabrani datum
		{
			List<Appointment> appointmentsByDate = new List<Appointment>();
			foreach (Appointment appointment in MainWindow.appointments)
			{
				if (appointment.StartDateTime.Date.Equals(dateTime))
				{
					appointmentsByDate.Add(appointment);

				}
			}

			return appointmentsByDate;
		}

		private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
		{
			var filtered = allOpenAppointments.Where(patient => (patient.Doctor.FirstName.Contains(SearchTextBox.Text) || patient.Doctor.MedicalRoom.ToString().Contains(SearchTextBox.Text)));


			OpenAppointmentDataGrid.ItemsSource = filtered;
		}

		private void OpenAppointmentDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{

			selectedAppointment = this.OpenAppointmentDataGrid.SelectedItem as AllAppointments;
			if (selectedAppointment != null)
			{
			
				ScheduleAppointmentInfo s = new ScheduleAppointmentInfo();
				s.nazivLekara.Text = selectedAppointment.Doctor.FirstName + " " + selectedAppointment.Doctor.LastName;
				s.datum.Text = selectedAppointment.StartDateTime.ToString("dd.MM.yyyy");
				s.Vreme.Text = selectedAppointment.StartDateTime.ToString("HH:mm");
				s.Soba.Text = selectedAppointment.Doctor.MedicalRoom.Name;
				s.vrstaSobe.Text = selectedAppointment.Doctor.MedicalRoom.Type.ToString();
				if(selectedAppointment.Patient == null)
				{
					s.nazivPacijenta.Text = "/";
					s.JMBG.Text = "/";
					s.BrojKartona.Text = "/";
				}
				else
				{
					s.nazivPacijenta.Text = selectedAppointment.Patient.FirstName + " " + selectedAppointment.Patient.LastName;
					s.JMBG.Text = selectedAppointment.Patient.PersonalID.ToString();
					s.BrojKartona.Text = selectedAppointment.Patient.MedicalRedordID.ToString();
				}
			
				
			
				AppointmentInfoFrame.NavigationService.Navigate(s);

			}
			else
			{
				Console.WriteLine("bbbbbbbbbbbbb");
			}


		}

	

		

	


		public Dictionary<Doctor, WorkingTime> filterDoctorByWorkingPeriod(DateTime date)   //trazimo radno vreme svakog lekara na osnovu datuma
		{
			Dictionary<Doctor, WorkingTime> dictionaryForWorkingTime = new Dictionary<Doctor, WorkingTime>();


			foreach (Doctor doctor in MainWindow.doctors)
			{

				WorkingPeriod workingPeriod = workingPeriodForDoctorsByDate(doctor, date.Date);


				if (workingPeriod == null)
				{
					continue;
				}
				WorkingTime workingTime = getWorkingTimeForDate(workingPeriod, date);

				if (workingTime == null)
				{
					continue;
				}
				dictionaryForWorkingTime.Add(doctor, workingTime);
			}



			return dictionaryForWorkingTime;
		}


		public WorkingPeriod workingPeriodForDoctorsByDate(Doctor doctor, DateTime date)
		{
			WorkingPeriod workingPeriod = null;
			foreach (WorkingPeriod wp in doctor.WorkingPeriods)
			{
				if (date >= wp.BeginDate.Date && date < wp.EndDate.Date)
				{
					workingPeriod = wp;
					break;
				}
			}
			return workingPeriod;
		}

	



		public WorkingTime getWorkingTimeForDate(WorkingPeriod workingPeriod, DateTime date)
		{
			WorkingTime workingTime = null;
			foreach (WorkingTime wt in workingPeriod.WorkingTimes)
			{
				foreach (Day day in wt.Days)
				{

					if (day.ToString().Equals(date.DayOfWeek.ToString()))
					{

						workingTime = wt;
						break;
					}
				}
			}

			return workingTime;

		}


		public List<AllAppointments> GetOpenAppointments(List<Appointment> allAppointments, Dictionary<Doctor, WorkingTime> dictionary)
		{
			List<AllAppointments> openAppointments = new List<AllAppointments>();

			foreach (Doctor doctor in dictionary.Keys)
			{

				Console.WriteLine(doctor.FirstName);
				DateTime startSlot = dictionary[doctor].BeginTime; // Starts at 8:00AM
				List<DateTime> listOfTime = new List<DateTime>();
				List<DateTime> openAppointmentsForDoctor = new List<DateTime>();


				while (startSlot.Hour < dictionary[doctor].EndTime.Hour)
				{
					// Construct time slot class

					startSlot = startSlot.AddMinutes(15);
					Console.WriteLine(startSlot);
					listOfTime.Add(startSlot);
				}

				foreach (DateTime d in listOfTime)
				{

					
						openAppointmentsForDoctor.Add(d);

					

				}

				foreach (DateTime start in openAppointmentsForDoctor)
				{
					AllAppointments newOpenAppointments;
					bool dodao = false;
					foreach (Appointment a in allAppointments)
					{
					
						if (a.Doctor.Equals(doctor))
						{

							if (a.StartDateTime.Hour.Equals(start.Hour) && a.StartDateTime.Minute.Equals(start.Minute))
							{
								/*bool isFree = true;
								if(a.Patient != null)
								{
									isFree = false;
								}*/
								 newOpenAppointments = new AllAppointments(doctor, start, start.AddMinutes(15),false,a.Patient);
								openAppointments.Add(newOpenAppointments);
								newOpenAppointments = null;
								dodao = true;
								break;
							}


						}
					}
					if (!dodao)
					{
						newOpenAppointments = new AllAppointments(doctor, start, start.AddMinutes(15));
						openAppointments.Add(newOpenAppointments);
						newOpenAppointments = null;
					}
				
				}



			}

			return openAppointments;
		}







		private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
		{

			if (ucitano)
			{

				selectedDate = dateTime.SelectedDate.Value.Date;
				Console.WriteLine(selectedDate.Date);

				List<Appointment> appointmentsByDate = filterAllAppointmentsByDate(selectedDate); //zakazani termini za izabrani datum
				Dictionary<Doctor, WorkingTime> dictionaryForWorkingTime = filterDoctorByWorkingPeriod(selectedDate);
				allOpenAppointments = GetOpenAppointments(appointmentsByDate, dictionaryForWorkingTime);





				OpenAppointmentDataGrid.ItemsSource = allOpenAppointments;


			}







		}
	}
}
