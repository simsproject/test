﻿#pragma checksum "..\..\changeTimetable.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4FB6E38C4083AB69589C84F11C60544FE7DF0013"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KlinikaZdravlje;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KlinikaZdravlje {
    
    
    /// <summary>
    /// changeTimetable
    /// </summary>
    public partial class changeTimetable : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 48 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateTime;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SearchTextBox;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid AppointmentDataGrid;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Frame AppointmentInfoFrame;
        
        #line default
        #line hidden
        
        
        #line 224 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancelAppointmentButton;
        
        #line default
        #line hidden
        
        
        #line 241 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button moveAppointmentButton;
        
        #line default
        #line hidden
        
        
        #line 266 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border BorderMessage;
        
        #line default
        #line hidden
        
        
        #line 278 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image CloseImg;
        
        #line default
        #line hidden
        
        
        #line 289 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBoxDoctor;
        
        #line default
        #line hidden
        
        
        #line 290 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBoxPatient;
        
        #line default
        #line hidden
        
        
        #line 300 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancleButton;
        
        #line default
        #line hidden
        
        
        #line 313 "..\..\changeTimetable.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancleAppButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KlinikaZdravlje;component/changetimetable.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\changeTimetable.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.dateTime = ((System.Windows.Controls.DatePicker)(target));
            
            #line 62 "..\..\changeTimetable.xaml"
            this.dateTime.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.DatePicker_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.SearchTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 89 "..\..\changeTimetable.xaml"
            this.SearchTextBox.KeyUp += new System.Windows.Input.KeyEventHandler(this.SearchTextBox_KeyUp);
            
            #line default
            #line hidden
            return;
            case 3:
            this.AppointmentDataGrid = ((System.Windows.Controls.DataGrid)(target));
            
            #line 123 "..\..\changeTimetable.xaml"
            this.AppointmentDataGrid.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.AppointmentDataGrid_MouseDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.AppointmentInfoFrame = ((System.Windows.Controls.Frame)(target));
            return;
            case 5:
            this.CancelAppointmentButton = ((System.Windows.Controls.Button)(target));
            
            #line 233 "..\..\changeTimetable.xaml"
            this.CancelAppointmentButton.Click += new System.Windows.RoutedEventHandler(this.CancelAppointmentButton_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.moveAppointmentButton = ((System.Windows.Controls.Button)(target));
            
            #line 250 "..\..\changeTimetable.xaml"
            this.moveAppointmentButton.Click += new System.Windows.RoutedEventHandler(this.MoveAppointmentButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.BorderMessage = ((System.Windows.Controls.Border)(target));
            return;
            case 8:
            this.CloseImg = ((System.Windows.Controls.Image)(target));
            
            #line 278 "..\..\changeTimetable.xaml"
            this.CloseImg.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.CloseImg_MouseDown);
            
            #line default
            #line hidden
            return;
            case 9:
            this.checkBoxDoctor = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 10:
            this.checkBoxPatient = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.cancleButton = ((System.Windows.Controls.Button)(target));
            
            #line 311 "..\..\changeTimetable.xaml"
            this.cancleButton.Click += new System.Windows.RoutedEventHandler(this.CancleButton_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.cancleAppButton = ((System.Windows.Controls.Button)(target));
            
            #line 324 "..\..\changeTimetable.xaml"
            this.cancleAppButton.Click += new System.Windows.RoutedEventHandler(this.CancleAppButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

