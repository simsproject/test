﻿#pragma checksum "..\..\scheduleAppointmentInfo.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "007540C239B2101F34427480F90DBEF13B59C84E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KlinikaZdravlje;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KlinikaZdravlje {
    
    
    /// <summary>
    /// ScheduleAppointmentInfo
    /// </summary>
    public partial class ScheduleAppointmentInfo : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 78 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock nazivLekara;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock datum;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Vreme;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Soba;
        
        #line default
        #line hidden
        
        
        #line 109 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock vrstaSobe;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock nazivPacijenta;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock JMBG;
        
        #line default
        #line hidden
        
        
        #line 130 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock BrojKartona;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\scheduleAppointmentInfo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock typeAppointment;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KlinikaZdravlje;component/scheduleappointmentinfo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\scheduleAppointmentInfo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.nazivLekara = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.datum = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.Vreme = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.Soba = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.vrstaSobe = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.nazivPacijenta = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.JMBG = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.BrojKartona = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.typeAppointment = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

