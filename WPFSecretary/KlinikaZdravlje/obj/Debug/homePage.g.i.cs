﻿#pragma checksum "..\..\homePage.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "43B979558D39ADF5810DA3088702E4471C1C93EA"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KlinikaZdravlje;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KlinikaZdravlje {
    
    
    /// <summary>
    /// Profile
    /// </summary>
    public partial class Profile : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 314 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FirstNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 329 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LastNameTextBox;
        
        #line default
        #line hidden
        
        
        #line 341 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox DateOfBirthTextBox;
        
        #line default
        #line hidden
        
        
        #line 352 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox EmailTextBox;
        
        #line default
        #line hidden
        
        
        #line 363 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox PhoneNumberTextBox;
        
        #line default
        #line hidden
        
        
        #line 374 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CountryComboBox;
        
        #line default
        #line hidden
        
        
        #line 393 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CityComboBox;
        
        #line default
        #line hidden
        
        
        #line 412 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox AdressTextBox;
        
        #line default
        #line hidden
        
        
        #line 428 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox PersonalIDTextBox;
        
        #line default
        #line hidden
        
        
        #line 440 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditButton;
        
        #line default
        #line hidden
        
        
        #line 465 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveButton;
        
        #line default
        #line hidden
        
        
        #line 492 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancleButton;
        
        #line default
        #line hidden
        
        
        #line 703 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox UsernameTextBox;
        
        #line default
        #line hidden
        
        
        #line 714 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox PasswordTextBox;
        
        #line default
        #line hidden
        
        
        #line 729 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label greska;
        
        #line default
        #line hidden
        
        
        #line 731 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button EditButton2;
        
        #line default
        #line hidden
        
        
        #line 757 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveButton2;
        
        #line default
        #line hidden
        
        
        #line 782 "..\..\homePage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancleButton2;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KlinikaZdravlje;component/homepage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\homePage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.FirstNameTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 325 "..\..\homePage.xaml"
            this.FirstNameTextBox.KeyDown += new System.Windows.Input.KeyEventHandler(this.FirstNameTextBox_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.LastNameTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 339 "..\..\homePage.xaml"
            this.LastNameTextBox.KeyDown += new System.Windows.Input.KeyEventHandler(this.LastNameTextBox_KeyDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.DateOfBirthTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.EmailTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.PhoneNumberTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.CountryComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 383 "..\..\homePage.xaml"
            this.CountryComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CountryComboBox_SelectionChanged);
            
            #line default
            #line hidden
            
            #line 391 "..\..\homePage.xaml"
            this.CountryComboBox.KeyUp += new System.Windows.Input.KeyEventHandler(this.Country_KeyUp);
            
            #line default
            #line hidden
            return;
            case 7:
            this.CityComboBox = ((System.Windows.Controls.ComboBox)(target));
            
            #line 409 "..\..\homePage.xaml"
            this.CityComboBox.KeyUp += new System.Windows.Input.KeyEventHandler(this.Cmb_KeyUp);
            
            #line default
            #line hidden
            
            #line 410 "..\..\homePage.xaml"
            this.CityComboBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CityComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 8:
            this.AdressTextBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.PersonalIDTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.EditButton = ((System.Windows.Controls.Button)(target));
            
            #line 452 "..\..\homePage.xaml"
            this.EditButton.Click += new System.Windows.RoutedEventHandler(this.EditButton_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.SaveButton = ((System.Windows.Controls.Button)(target));
            
            #line 477 "..\..\homePage.xaml"
            this.SaveButton.Click += new System.Windows.RoutedEventHandler(this.SaveButton_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.CancleButton = ((System.Windows.Controls.Button)(target));
            
            #line 505 "..\..\homePage.xaml"
            this.CancleButton.Click += new System.Windows.RoutedEventHandler(this.CancleButton_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.UsernameTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.PasswordTextBox = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 15:
            this.greska = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.EditButton2 = ((System.Windows.Controls.Button)(target));
            
            #line 742 "..\..\homePage.xaml"
            this.EditButton2.Click += new System.Windows.RoutedEventHandler(this.EditButton2_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.SaveButton2 = ((System.Windows.Controls.Button)(target));
            
            #line 769 "..\..\homePage.xaml"
            this.SaveButton2.Click += new System.Windows.RoutedEventHandler(this.SaveButton2_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.CancleButton2 = ((System.Windows.Controls.Button)(target));
            
            #line 795 "..\..\homePage.xaml"
            this.CancleButton2.Click += new System.Windows.RoutedEventHandler(this.CancleButton2_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

