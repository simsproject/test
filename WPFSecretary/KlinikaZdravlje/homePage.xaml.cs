﻿using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for profil.xaml
	/// </summary>
	public partial class Profile : Page
	{
		public Profile()
		{
			InitializeComponent();
			CountryComboBox.ItemsSource = MainWindow.countryController.GetAll().ToList();


			FirstNameTextBox.Text = login.logInSecretary.FirstName;
			LastNameTextBox.Text = login.logInSecretary.LastName;
			PersonalIDTextBox.Text = login.logInSecretary.PersonalID;
			DateOfBirthTextBox.Text = login.logInSecretary.DateOfBirth.ToString("dd.MM.yyyy.");
			PhoneNumberTextBox.Text = login.logInSecretary.PhoneNumber;
			//CountryComboBox.SelectedItem = login.logInSecretary.Address.City.Country;
			//CityComboBox.SelectedItem = login.logInSecretary.Address.City;

			foreach (Model.Users.City a in CityComboBox.Items)
			{
				if (a.Name == login.logInSecretary.Address.City.Name)
				{
					CityComboBox.SelectedItem = a;
					break;
				}
			}

			foreach (Model.Users.Country a in CountryComboBox.Items)
			{
				if (a.Name == login.logInSecretary.Address.City.Country.Name)
				{
					CountryComboBox.SelectedItem = a;
					break;
				}
			}


			foreach (Model.Users.Address a in AdressTextBox.Items)
			{
				if(a.Id == login.logInSecretary.Address.Id)
				{
					AdressTextBox.SelectedItem = a;
					break;
				}
			}
			EmailTextBox.Text = login.logInSecretary.Email;
			UsernameTextBox.Text = login.logInSecretary.UserName;
			PasswordTextBox.Password = login.logInSecretary.Password;
			


		}

		private string textFirstNamedValue;
		private string textLastNameValue;
		private string textDateOfBirthValue;
		private string textEmailValue;
		private string textPhoneNumberValue;
		private string textAdressValue;
		private string textUserNameValue;
		private string textPasswordValue;
		private string textCountryValue;
		private string textCityValue;
		


		private void EditButton_Click(object sender, RoutedEventArgs e)
		{

			EditButton.Visibility = Visibility.Hidden;
			SaveButton.Visibility = Visibility.Visible;
			CancleButton.Visibility = Visibility.Visible;
			EditButton2.IsEnabled = false;
			FirstNameTextBox.IsEnabled = true;
			LastNameTextBox.IsEnabled = true;
			EmailTextBox.IsEnabled = true;
			PhoneNumberTextBox.IsEnabled = true;
			AdressTextBox.IsEnabled = true;
			CityComboBox.IsEnabled = true;
			CountryComboBox.IsEnabled = true;

			textFirstNamedValue = FirstNameTextBox.Text;
			textLastNameValue = LastNameTextBox.Text;
			textDateOfBirthValue = DateOfBirthTextBox.Text;
			textEmailValue = EmailTextBox.Text;
			textPhoneNumberValue = PhoneNumberTextBox.Text;
			textAdressValue = AdressTextBox.Text;
			textCountryValue = CountryComboBox.Text;
			textCityValue = CityComboBox.Text;

		}
		private void SaveButton_Click(object sender, RoutedEventArgs e)
		{


			bool isFirstName = false;
			bool isCity = false;
			bool isCountry = false;
			bool isLastName = false;
			bool isPersonalID = false;
			bool isAddress = false;
			bool isPhoneNumber = false;

			if (FirstNameTextBox.Text == "")
			{
				FirstNameTextBox.BorderBrush = Brushes.Red;
				FirstNameTextBox.ToolTip = "Polje mora biti popunjeno";
				FirstNameTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				isFirstName = false;
				//iconFirstName.Visibility = Visibility.Visible;


			}
			else
			{
				FirstNameTextBox.BorderBrush = Brushes.Gray;
				FirstNameTextBox.ToolTip = "Unesite ime pacijenta";
				FirstNameTextBox.BorderThickness = new Thickness(0, 0, 0, 1);
				isFirstName = true;
				//iconFirstName.Visibility = Visibility.Collapsed;
			}

			if (LastNameTextBox.Text == "")
			{
				LastNameTextBox.BorderBrush = Brushes.Red;
				LastNameTextBox.ToolTip = "Polje mora biti popunjeno";
				LastNameTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				isLastName = false;
				//iconLastName.Visibility = Visibility.Visible;

			}
			else
			{
				LastNameTextBox.BorderBrush = Brushes.Gray;
				LastNameTextBox.ToolTip = "Unesite prezime pacijenta";
				LastNameTextBox.BorderThickness = new Thickness(0, 0, 0, 1);
				isLastName = true;
				//iconLastName.Visibility = Visibility.Collapsed;



			}

			if (PersonalIDTextBox.Text == "")
			{
				PersonalIDTextBox.BorderBrush = Brushes.Red;
				PersonalIDTextBox.ToolTip = "Polje mora biti popunjeno";
				PersonalIDTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				isPersonalID = false;
				//iconPersonalID.Visibility = Visibility.Visible;


			}
			else
			{
				PersonalIDTextBox.BorderBrush = Brushes.Gray;
				PersonalIDTextBox.ToolTip = "Unesite JMBG pacijenta";
				PersonalIDTextBox.BorderThickness = new Thickness(0, 0, 0, 1);
				isPersonalID = true;
				//iconPersonalID.Visibility = Visibility.Collapsed;



			}

			if (PhoneNumberTextBox.Text == "")
			{
				PhoneNumberTextBox.BorderBrush = Brushes.Red;
				PhoneNumberTextBox.ToolTip = "Polje mora biti popunjeno";
				PhoneNumberTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				isPhoneNumber = false;
				//iconPhoneNumber.Visibility = Visibility.Visible;


			}
			else
			{
				PhoneNumberTextBox.BorderBrush = Brushes.Gray;
				PhoneNumberTextBox.ToolTip = "Unesite broj telefona pacijenta";
				PhoneNumberTextBox.BorderThickness = new Thickness(0, 0, 0, 1);
				isPhoneNumber = true;
				//iconPhoneNumber.Visibility = Visibility.Collapsed;


			}

			if (AdressTextBox.Text == "")
			{
				AdressTextBox.BorderBrush = Brushes.Red;
				AdressTextBox.ToolTip = "Polje mora biti popunjeno";
				AdressTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				isAddress = false;
				//iconAddress.Visibility = Visibility.Visible;




			}
			else
			{
				AdressTextBox.BorderBrush = Brushes.Gray;
				AdressTextBox.ToolTip = "Unesite adresu stanovanja pacijenta";
				AdressTextBox.BorderThickness = new Thickness(0, 0, 0, 1);
				isAddress = true;
				//iconAddress.Visibility = Visibility.Collapsed;


			}

			if (CityComboBox.SelectedItem == null)
			{

				CityComboBox.BorderBrush = Brushes.Red;
				CityComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				CityComboBox.ToolTip = "Polje mora biti popunjeno";
				isCity = false;

			}
			else
			{
				CityComboBox.BorderBrush = Brushes.Gray;
				CityComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				CityComboBox.ToolTip = "Unesite dan rodjenja pacijenta";
				isCity = true;



			}
			if (CountryComboBox.SelectedItem == null)
			{

				CountryComboBox.BorderBrush = Brushes.Red;
				CountryComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				CountryComboBox.ToolTip = "Polje mora biti popunjeno";
				isCountry = false;


			}
			else
			{
				CountryComboBox.BorderBrush = Brushes.Gray;
				CountryComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				CountryComboBox.ToolTip = "Izaberite drzavu";
				isCountry = true;

			}

			bool isValid = true;
			if (EmailTextBox.Text != "")
			{
				isValid = IsValid(EmailTextBox.Text);
			}

			if (!isValid)
			{
				EmailTextBox.BorderBrush = Brushes.Red;
				EmailTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
			}
			else
			{
				EmailTextBox.BorderBrush = Brushes.Gray;
				EmailTextBox.BorderThickness = new Thickness(0, 0, 0, 1);

			}


			bool okRegex = true;
			Regex PhoneNumberRegex = new Regex("[0-9]{3}/[0-9]{3,4}-[0-9]{3}");
		
			if (isPhoneNumber)
			{


				if (PhoneNumberRegex.IsMatch(PhoneNumberTextBox.Text))
				{

					PhoneNumberTextBox.BorderBrush = Brushes.Gray;
					PhoneNumberTextBox.BorderThickness = new Thickness(0, 0, 0, 1);


				}
				else
				{
					okRegex = false;
					PhoneNumberTextBox.BorderBrush = Brushes.Red;
					PhoneNumberTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				}
			}




			if (isFirstName && isLastName && isAddress && isPersonalID && isPhoneNumber && isCountry && isCity)
			{
				if (isValid && okRegex)
				{
					login.logInSecretary.FirstName = FirstNameTextBox.Text;
					login.logInSecretary.LastName = LastNameTextBox.Text;
					login.logInSecretary.PhoneNumber = PhoneNumberTextBox.Text;
					login.logInSecretary.Address.City = CityComboBox.SelectedValue as Model.Users.City;
					login.logInSecretary.Address.City.Country = CountryComboBox.SelectedValue as Model.Users.Country;
					login.logInSecretary.Address = AdressTextBox.SelectedValue as Model.Users.Address;

					//login.logInSecretary.Address = AdressTextBox.
					MainWindow.secretaryController.Update(login.logInSecretary);
					MainWindow.userController.Update(login.logInSecretary);
					EditButton.Visibility = Visibility.Visible;
					SaveButton.Visibility = Visibility.Hidden;
					CancleButton.Visibility = Visibility.Hidden;
					EditButton2.IsEnabled = true;
					FirstNameTextBox.IsEnabled = false;
					LastNameTextBox.IsEnabled = false;
					DateOfBirthTextBox.IsEnabled = false;
					EmailTextBox.IsEnabled = false;
					PhoneNumberTextBox.IsEnabled = false;
					AdressTextBox.IsEnabled = false;
					CountryComboBox.IsEnabled = false;
					CityComboBox.IsEnabled = false;



				}

				

			}

















		
		}

		private void CancleButton_Click(object sender, RoutedEventArgs e)
		{
			EditButton.Visibility = Visibility.Visible;
			SaveButton.Visibility = Visibility.Hidden;
			CancleButton.Visibility = Visibility.Hidden;
			EditButton2.IsEnabled = true;
			FirstNameTextBox.IsEnabled = false;
			LastNameTextBox.IsEnabled = false;
			DateOfBirthTextBox.IsEnabled = false;
			EmailTextBox.IsEnabled = false;
			PhoneNumberTextBox.IsEnabled = false;
			AdressTextBox.IsEnabled = false;
			CountryComboBox.IsEnabled = false;
			CityComboBox.IsEnabled = false; 
			FirstNameTextBox.Text = textFirstNamedValue;
			LastNameTextBox.Text = textLastNameValue;
			DateOfBirthTextBox.Text = textDateOfBirthValue;
			EmailTextBox.Text = textEmailValue;
			PhoneNumberTextBox.Text = textPhoneNumberValue;
			AdressTextBox.Text = textAdressValue;
			CountryComboBox.Text = textCountryValue;
			CityComboBox.Text = textCityValue;

		}

		private void EditButton2_Click(object sender, RoutedEventArgs e)
		{
			EditButton2.Visibility = Visibility.Hidden;
			SaveButton2.Visibility = Visibility.Visible;
			CancleButton2.Visibility = Visibility.Visible;
			EditButton.IsEnabled = false;
			UsernameTextBox.IsEnabled = false;
			PasswordTextBox.IsEnabled = true;
			textUserNameValue = UsernameTextBox.Text;
			textPasswordValue = PasswordTextBox.Password;


		}

		private void SaveButton2_Click(object sender, RoutedEventArgs e)
		{
			bool isUsername = false;
			bool isPassword = false;

			/*if (UsernameTextBox.Text == "")
			{
				UsernameTextBox.BorderBrush = Brushes.Red;
				UsernameTextBox.ToolTip = "Polje mora biti popunjeno";
				UsernameTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				isUsername = false;
				//iconFirstName.Visibility = Visibility.Visible;


			}
			else
			{
				UsernameTextBox.BorderBrush = Brushes.Gray;
				UsernameTextBox.ToolTip = "Unesite ime pacijenta";
				UsernameTextBox.BorderThickness = new Thickness(0, 0, 0, 1);
				isUsername = true;
				//iconFirstName.Visibility = Visibility.Collapsed;
			}*/

			if (PasswordTextBox.Password == "")
			{
				PasswordTextBox.BorderBrush = Brushes.Red;
				PasswordTextBox.ToolTip = "Polje mora biti popunjeno";
				PasswordTextBox.BorderThickness = new Thickness(0, 0, 0, 2);
				isPassword = false;
				//iconFirstName.Visibility = Visibility.Visible;


			}
			else
			{
				PasswordTextBox.BorderBrush = Brushes.Gray;
				PasswordTextBox.ToolTip = "Unesite ime pacijenta";
				PasswordTextBox.BorderThickness = new Thickness(0, 0, 0, 1);
				isPassword = true;
				//iconFirstName.Visibility = Visibility.Collapsed;
			}

			

			if(isPassword)
			{
				bool postoji = false;
				bool isStrength = false;
				foreach (Secretary s in MainWindow.secretaries)
			/*	{
					if (s.Username.Equals(UsernameTextBox.Text))
					{
						if (!s.Username.Equals(textUserNameValue))
						{
							postoji = true;
							greska.Content = "Korisnicko ime vec postoji";
							break;
						}
						

					}
				}*/

				if(PasswordTextBox.Password.Length >= 6)
				{
					isStrength = true;
				}
				else
				{
					if (postoji)
					{
						greska.Content = "Lozinka mora sadrzati minimalno 5 karaktera";

					}

				}

				if (!postoji && isStrength)
				{

					login.logInSecretary.Password = PasswordTextBox.Password;
				
					//login.logInSecretary.Address = AdressTextBox.
					MainWindow.secretaryController.Update(login.logInSecretary);
					MainWindow.userController.Update(login.logInSecretary);

					EditButton2.Visibility = Visibility.Visible;
					SaveButton2.Visibility = Visibility.Hidden;
					CancleButton2.Visibility = Visibility.Hidden;
					EditButton.IsEnabled = true;
					UsernameTextBox.IsEnabled = false;
					PasswordTextBox.IsEnabled = false;
					greska.Content = "";

				}


			}






			
		}

		private void CancleButton2_Click(object sender, RoutedEventArgs e)
		{

			EditButton2.Visibility = Visibility.Visible;
			SaveButton2.Visibility = Visibility.Hidden;
			CancleButton2.Visibility = Visibility.Hidden;
			EditButton.IsEnabled = true;
			UsernameTextBox.IsEnabled = false;
			PasswordTextBox.IsEnabled = false;
			UsernameTextBox.Text = textUserNameValue;
			PasswordTextBox.Password= textPasswordValue;
		}



		private void Cmb_KeyUp(object sender, KeyEventArgs e)
		{
			CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(CityComboBox.ItemsSource);

			itemsViewOriginal.Filter = ((o) =>
			{
				if (String.IsNullOrEmpty(CityComboBox.Text)) return true;
				else
				{
					if (((string)o).Contains(CityComboBox.Text)) return true;
					else return false;
				}
			});

			itemsViewOriginal.Refresh();
		}

		private void Country_KeyUp(object sender, KeyEventArgs e)
		{
			CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(CountryComboBox.ItemsSource);

			itemsViewOriginal.Filter = ((o) =>
			{
				if (String.IsNullOrEmpty(CountryComboBox.Text)) return true;
				else
				{
					if (((string)o).Contains(CountryComboBox.Text)) return true;
					else return false;
				}
			});

			itemsViewOriginal.Refresh();
		}


		public class FilterViewModel
		{
			public IEnumerable<string> DataSource { get; set; }

			public FilterViewModel()
			{
				DataSource = new[] { "Novi Sad", "Beograd" };
			}
		}

		public class FilterViewModel2
		{
			public IEnumerable<string> DataSource2 { get; set; }

			public FilterViewModel2()
			{
				DataSource2 = new[] { "Srbija","Crna Gora" };
			}
		}

		private void FirstNameTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			Regex r = new Regex(@"(?i)^[a-z’'()/.,\s-]+$");
			if (!r.IsMatch(e.Key.ToString()))
			{
				e.Handled = true;
			}
		}

		private void LastNameTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			Regex r = new Regex(@"(?i)^[a-z’'()/.,\s-]+$");
			if (!r.IsMatch(e.Key.ToString()))
			{
				e.Handled = true;
			}
		}



		public bool IsValid(string emailaddress)
		{
			try
			{
				MailAddress m = new MailAddress(emailaddress);

				return true;
			}
			catch (FormatException)
			{
				return false;
			}
		}

		private void CountryComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(CountryComboBox.SelectedItem != null)
			{
				List<Model.Users.City> sviGradovi = MainWindow.cityController.GetAll().ToList();
				List<Model.Users.City> gradovi = new List<Model.Users.City>();
				foreach (Model.Users.City ad in sviGradovi)
				{
					if (ad.Country.Name.Equals((this.CountryComboBox.SelectedItem as Model.Users.Country).Name))
					{
						gradovi.Add(ad);
					}
				}
				this.CityComboBox.ItemsSource = gradovi;
			}
		
		}

		private void CityComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if(CityComboBox.SelectedItem != null)
			{
				List<Model.Users.Address> sveAdrese = MainWindow.adressController.GetAll().ToList();
				List<Model.Users.Address> adrese = new List<Model.Users.Address>();
				foreach (Model.Users.Address ad in sveAdrese)
				{
					if (ad.City.Name.Equals((this.CityComboBox.SelectedItem as Model.Users.City).Name))
					{
						adrese.Add(ad);
					}
				}
				this.AdressTextBox.ItemsSource = adrese;
			}
			
		}


	}
}
