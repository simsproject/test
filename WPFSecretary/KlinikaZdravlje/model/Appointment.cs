﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.model
{
	public enum AppointmentType
	{
		PREGLED,
		OPERACIJA

	}
	public class Appointment
	{
		private DateTime _startDateTime;
		private DateTime _finishDateTime;
		private AppointmentType _type;
		private long _serialID;

		private Patient _patient;
		private Doctor _doctor;
		private MedicalRoom _room;


		public DateTime StartDateTime
		{
			get { return _startDateTime; }
			set
			{
				if (value != _startDateTime)
				{
					_startDateTime = value;
					
				}
			}
		}


		public AppointmentType Type
		{
			get { return _type; }
			set
			{
				if (value != _type)
				{
					_type = value;

				}
			}
		}


		public DateTime FinishDateTime
		{
			get { return _finishDateTime; }
			set
			{
				if (value != _startDateTime)
				{
					_finishDateTime = value;
				
				}
			}
		}


	/*	public AppointmentType Type
		{
			get { return _type; }
			set
			{
				if (value != _type)
				{
					_type = value;

				}
			}
		}
		*/
		public long SerialID
		{
			get { return _serialID; }
			set
			{
				if (value != _serialID)
				{
					_serialID = value;

				}
			}
		}

		public Patient Patient
		{
			get { return _patient; }
			set
			{
				if (value != _patient)
				{
					_patient = value;

				}
			}
		}

		public Doctor Doctor
		{
			get { return _doctor; }
			set
			{
				if (value != _doctor)
				{
					_doctor = value;

				}
			}
		}

		public MedicalRoom Room
		{
			get { return _room; }
			set
			{
				if (value != _room)
				{
					_room = value;

				}
			}
		}


	}
}
