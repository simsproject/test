﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.model
{
	public class AllAppointments
	{
		private Doctor _doctor;
		private DateTime _startDateTime;
		private DateTime _finishDateTime;
		private Patient _patient;
		private bool _isFree;
		private MedicalRoom _room;


		public AllAppointments(Doctor d, DateTime s, DateTime f)
		{

			_doctor = d;
			_startDateTime = s;
			_finishDateTime = f;
			_isFree = true;
			_patient = null;


		}

		public AllAppointments(Doctor d, DateTime s, DateTime f,bool isFree,Patient p)
		{

			_doctor = d;
			_startDateTime = s;
			_finishDateTime = f;
			_isFree = isFree;
			_patient = p;


		}

		public Doctor Doctor
		{
			get { return _doctor; }
			set
			{
				if (value != _doctor)
				{
					_doctor = value;

				}
			}
		}

		public Patient Patient
		{
			get { return _patient; }
			set
			{
				if (value != _patient)
				{
					_patient = value;

				}
			}
		}
		public bool IsFree
		{
			get { return _isFree; }
			set
			{
				if (value != _isFree)
				{
					_isFree = value;

				}
			}
		}

		public MedicalRoom room
		{
			get { return _room; }
			set
			{
				if (value != _room)
				{
					_room = value;

				}
			}
		}


		public DateTime FinishDateTime
		{
			get { return _finishDateTime; }
			set
			{
				if (value != _startDateTime)
				{
					_finishDateTime = value;

				}
			}
		}


		public DateTime StartDateTime
		{
			get { return _startDateTime; }
			set
			{
				if (value != _startDateTime)
				{
					_startDateTime = value;

				}
			}
		}



	}
}

