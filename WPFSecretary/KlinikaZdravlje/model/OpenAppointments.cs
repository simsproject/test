﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.model
{
	
	public class OpenAppointments
	{
		private Model.Users.Doctor _doctor;
		private DateTime _startDateTime;
		private DateTime _finishDateTime;



		public OpenAppointments(Model.Users.Doctor d,DateTime s,DateTime f)
		{

			_doctor = d;
			_startDateTime = s;
			_finishDateTime = f;



		}


		public Model.Users.Doctor Doctor
		{
			get { return _doctor; }
			set
			{
				if (value != _doctor)
				{
					_doctor = value;

				}
			}
		}

		public DateTime FinishDateTime
		{
			get { return _finishDateTime; }
			set
			{
				if (value != _startDateTime)
				{
					_finishDateTime = value;

				}
			}
		}


		public DateTime StartDateTime
		{
			get { return _startDateTime; }
			set
			{
				if (value != _startDateTime)
				{
					_startDateTime = value;

				}
			}
		}

	}

	
}
