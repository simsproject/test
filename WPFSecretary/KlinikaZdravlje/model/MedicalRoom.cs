﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.model
{
	public enum TypeOfMedicalRoom
	{
		OPERACIONA,
		ZA_PREGLED
	}

	public class MedicalRoom
	{
		private String _name;
	//	private int _floor;
		private List<Doctor> _doctors;
		private TypeOfMedicalRoom _type;


		public String Name
		{
			get { return _name; }
			set
			{
				_name = value;

			}
		}

	/*	public int Floor
		{
			get { return _floor; }
			set
			{
				_floor = value;

			}
		}*/

		public TypeOfMedicalRoom Type
		{
			get { return _type; }
			set
			{
				_type = value;

			}
		}

		public List<Doctor> Doctors
		{
			get { return _doctors; }
			set
			{
				_doctors = value;

			}
		}

		public void addDoctorsToRoom(Doctor doctor)
		{
			Doctors.Add(doctor);
		}
	}
}
