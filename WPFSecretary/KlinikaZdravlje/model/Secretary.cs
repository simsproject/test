﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.model
{
	public class Secretary : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private enum Gender {
			Male,
			Female
		}

		protected virtual void OnPropertyChanged(string name)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}




		private String _firstName;
		private String _lastName;
		private DateTime _dateOfBirth;
		private String _personalID;
		//private Gender _gender;
		private String _phoneNumber;
		private String _email;
		private String _username;
		private String _password;

		private String _address;
		private String _country;
		private String _city;


		public string FirstName
		{
			get { return _firstName; }
			set {

				if (value != _firstName)
				{
					_firstName = value;
					OnPropertyChanged("Ime");
				}
			}
		}

		public string LastName
		{
			get { return _lastName; }
			set {
				if (value != _lastName)
				{
					_lastName = value;
					OnPropertyChanged("Prezime");
				}

			}
		}

		public DateTime DateOfBirth
		{
			get { return _dateOfBirth; }
			set {
				if (value != _dateOfBirth)
				{
					_dateOfBirth = value;
					OnPropertyChanged("Datum rodjenja");
				}

			}
		}

		public string PersonalID
		{
			get { return _personalID; }
			set {

				if (value != _personalID)
				{
					_personalID = value;
					OnPropertyChanged("JMBG");
				}

			}
		}

		/*public Gender Gender
		{
			get { return _gender; }
			set { _gender = value; }
		}*/

		public string PhoneNumber
		{
			get { return _phoneNumber; }
			set {
				if (value != _phoneNumber)
				{
					_phoneNumber = value;
					OnPropertyChanged("Broj telefona");
				}
			}
		}

		public string Email
		{
			get { return _email; }
			set {
				if (value != _email)
				{
					_email = value;
					OnPropertyChanged("Email");
				}
			}
		}

		public string Username
		{
			get { return _username; }
			set
			{
				if (value != _username)
				{
					_username = value;
					OnPropertyChanged("Email");
				}
			}
		}

		public string Password
		{
			get { return _password; }
			set
			{
				if (value != _password)
				{
					_password = value;
				
				}
			}
		}


		public string Country
		{
			get { return _country; }
			set
			{

				if (value != _country)
				{
					_country = value;
				
				}
			}
		}

		public string City
		{
			get { return _city; }
			set
			{

				if (value != _city)
				{
					_city = value;
					OnPropertyChanged("Ime");
				}
			}
		}

		public string Address
		{
			get { return _address; }
			set
			{

				if (value != _address)
				{
					_address = value;
					OnPropertyChanged("Ime");
				}
			}
		}

		/*	public Address Address
			{
				get { return _address; }
				set { _address = value; }
			}*/











	}
}
