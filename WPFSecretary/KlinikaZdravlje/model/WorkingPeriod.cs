﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.model
{

	
	public class WorkingPeriod
	{
		private DateTime _beginDate;
		private DateTime _endDate;

		public List<WorkingTime> _workingTimes;


		public DateTime BeginDate
		{
			get { return _beginDate; }
			set
			{
				_beginDate = value;

			}
		}

		public DateTime EndDate
		{
			get { return _endDate; }
			set
			{
				_endDate = value;

			}
		}

		public List<WorkingTime> WorkingTimes
		{
			get { return _workingTimes; }
			set
			{
				_workingTimes = value;

			}
		}

		public void addWorkingTime(WorkingTime workingTime)
		{
			WorkingTimes.Add(workingTime);
		}



	}
}
