﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.model
{


	public enum Day
	{
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday,
		Satursay,
		Sunday

	}
	public class WorkingTime
	{
		private DateTime _beginTime;
		private DateTime _endTime;
		private List<Day> _days;


		public DateTime BeginTime
		{
			get { return _beginTime; }
			set
			{
				_beginTime = value;
		
			}
		}

		public DateTime EndTime
		{
			get { return _endTime; }
			set
			{
				_endTime = value;

			}
		}

		public List<Day> Days
		{
			get { return _days; }
			set
			{
				_days = value;

			}
		}

		public void addWorkingDays(Day day)
		{
			
			Days.Add(day);
		}

	}
}
