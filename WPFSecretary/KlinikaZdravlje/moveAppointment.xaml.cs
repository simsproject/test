﻿using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for pomeranjePregleda.xaml
    /// </summary>
    public partial class MoveAppointment : Page
    {
		public static DateTime selectedDate;
		public  List<OpenAppointments> allOpenAppointments = null;
		public bool ucitano = false;
		public OpenAppointments selectedAppointment = null;
		public MoveAppointment()
        {
            InitializeComponent();
				ucitano = true;
				this.dateTime.BlackoutDates.AddDatesInPast();

			if (allOpenAppointments != null)
			{
				allOpenAppointments.Clear();
			}
			selectedDate = dateTime.SelectedDate.Value.Date;

			//DateTime date = DatePicker.SelectedDate.Value;
			List<Model.Appointment.Appointment> appointmentsByDate = MainWindow.appointmentController.GetAppointmentByDate(selectedDate);
			Dictionary<Model.Users.Doctor, Model.Users.WorkingTime> workingTimeForDoctor = MainWindow.doctorController.FilterDoctorsByWorkingTime(selectedDate);
			allOpenAppointments = GetOpenAppointments(appointmentsByDate, workingTimeForDoctor);




			OpenAppointmentDataGrid.ItemsSource = allOpenAppointments;






			Model.Appointment.Appointment p = changeTimetable.SelectedAppointment;

			lekar.Content = p.Doctor.FirstName + " " + p.Doctor.LastName;
			datum.Content = p.StartDate.ToString("dd.MM.yyyy");
			vreme.Content = p.StartDate.ToString("HH:mm");
			soba.Content = p.Room.Name;
			vrstaSobe.Content = p.Room.Type.ToString();
			pacijent.Content = p.Patient.FirstName + " " + p.Patient.LastName;
			JMBG.Content = p.Patient.PersonalID.ToString();
			brojKartona.Content = p.Patient.MedicalRecord.ToString();
			

		
		}
		public class Pregled
		{
			public string Datum { get; set; }
			public string Vreme { get; set; }
			public string Lekar { get; set; }
			public string Soba { get; set; }
			public string Pacijent { get; set; }
			public AppointmentType TipPregleda { get; set; }

		}

		public enum AppointmentType
		{
			OPERACIJA,
			REGULARAN,
			KONTORLA
		}

		private void YasButton_Click(object sender, RoutedEventArgs e)
		{
			
		}

		private void NoButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new changeTimetable());
		}


		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new changeTimetable());
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Visible;
			OkButton.IsEnabled = false;
			cancleButton.IsEnabled = false;
			OpenAppointmentDataGrid.IsEnabled = false;
		}

		private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
		{
			var filtered = allOpenAppointments.Where(patient => (patient.Doctor.FirstName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.LastName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.MedicalRoom.Type.ToString().ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.MedicalRoom.Name.ToUpper().Contains(SearchTextBox.Text.ToUpper()) ||  patient.StartDateTime.ToString("hh:mm").Contains(SearchTextBox.Text)));


			OpenAppointmentDataGrid.ItemsSource = filtered;
		}


	/*	public List<Model.Appointment.Appointment> filterAllAppointmentsByDate(DateTime dateTime) //daj mi sve zauzete termine za izabrani datum
		{
			List<Model.Appointment.Appointment> appointmentsByDate = new List<Model.Appointment.Appointment>();
			foreach (Appointment appointment in MainWindow.appointments)
			{
				if (appointment.StartDateTime.Date.Equals(dateTime))
				{
					appointmentsByDate.Add(appointment);

				}
			}

			return appointmentsByDate;
		}

	*/

		private void OpenAppointmentDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			CancelButton.Visibility = Visibility.Visible;
			OkButton.Visibility = Visibility.Visible;
			selectedAppointment = this.OpenAppointmentDataGrid.SelectedItem as OpenAppointments;

			if (selectedAppointment != null)
			{
				if (DateTime.Compare(selectedAppointment.StartDateTime, DateTime.Now) >= 0)
				{


					Model.Users.Patient p = changeTimetable.SelectedAppointment.Patient;
					MoveAppointmentInfo s = new MoveAppointmentInfo();
					s.nazivLekara.Text = selectedAppointment.Doctor.FirstName + " " + selectedAppointment.Doctor.LastName;
					s.datum.Text = selectedAppointment.StartDateTime.ToString("dd.MM.yyyy");
					s.Vreme.Text = selectedAppointment.StartDateTime.ToString("HH:mm");
					s.Soba.Text = selectedAppointment.Doctor.MedicalRoom.Name;
					s.vrstaSobe.Text = selectedAppointment.Doctor.MedicalRoom.Type.ToString();
					if (selectedAppointment.Doctor.MedicalRoom.Type.Equals(TypeOfMedicalRoom.ZA_PREGLED))
					{
						s.typeAppointment.Text = "PREGLED";
					}
					else
					{
						s.typeAppointment.Text = "PREGLED";
					}
					s.nazivPacijenta.Text = p.FirstName + " " + p.LastName;
					s.JMBG.Text = p.PersonalID.ToString();
					s.BrojKartona.Text = p.MedicalRecord.ToString();
					NewAppointmentFrame.NavigationService.Navigate(s);
				}
				else
				{
					MessageBox.Show("Termin je prosao");
				}
			}
		}

		private void CloseImg_MouseDown(object sender, MouseButtonEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			OkButton.IsEnabled = true;
			cancleButton.IsEnabled = true;
			OpenAppointmentDataGrid.IsEnabled = true;
		}

		private void CancleButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			OkButton.IsEnabled = true;
			cancleButton.IsEnabled = true;
			OpenAppointmentDataGrid.IsEnabled = true;
		}

		private void MoveButton_Click(object sender, RoutedEventArgs e)
		{
			Model.Appointment.Appointment a = changeTimetable.SelectedAppointment;

			selectedAppointment= this.OpenAppointmentDataGrid.SelectedItem as OpenAppointments;
			a.StartDate = selectedAppointment.StartDateTime;
			a.FinishDate = selectedAppointment.FinishDateTime;
			a.Room = selectedAppointment.Doctor.MedicalRoom;
			a.Doctor = selectedAppointment.Doctor;
			Model.Appointment.AppointmentType tip = Model.Appointment.AppointmentType.operation;
			if (selectedAppointment.Doctor.MedicalRoom.Type.Equals(TypeOfMedicalRoom.ZA_PREGLED))
			{
				tip = Model.Appointment.AppointmentType.regularAp;
			}
			a.Type = tip;
			if (checkBoxDoctor.IsChecked == true)
			{
				String message = "Termin pacijenta " + a.Patient.FirstName + " " + a.Patient.LastName + "(" + a.StartDate +")" + " je pomeren";
				MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, a.Doctor));
			}
			if(checkBoxPatient.IsChecked == true)
			{
				String message = "Termin pacijenta " + a.Patient.FirstName + " " + a.Patient.LastName + "(" + a.StartDate + ")" + " je pomeren";
				MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, a.Patient));
			}
			MainWindow.appointmentController.Update(a);

		

			/*
						foreach (Appointment ap in MainWindow.appointments)
						{
							if (ap.Equals(a))
							{
								ap.StartDateTime = selectedAppointment.StartDateTime;
								ap.FinishDateTime = selectedAppointment.FinishDateTime;
							//	ap.Room = selectedAppointment.Doctor.MedicalRoom;
								//ap.Doctor = selectedAppointment.Doctor;
								ap.SerialID = 1;

							}
						}*/

			this.NavigationService.Navigate(new changeTimetable());
		}


	/*	public Dictionary<Doctor, WorkingTime> filterDoctorByWorkingPeriod(DateTime date)   //trazimo radno vreme svakog lekara na osnovu datuma
		{
			Dictionary<Doctor, WorkingTime> dictionaryForWorkingTime = new Dictionary<Doctor, WorkingTime>();


			foreach (Doctor doctor in MainWindow.doctors)
			{

				WorkingPeriod workingPeriod = workingPeriodForDoctorsByDate(doctor, date.Date);


				if (workingPeriod == null)
				{
					continue;
				}
				WorkingTime workingTime = getWorkingTimeForDate(workingPeriod, date);

				if (workingTime == null)
				{
					continue;
				}
				dictionaryForWorkingTime.Add(doctor, workingTime);
			}



			return dictionaryForWorkingTime;
		}


		public WorkingPeriod workingPeriodForDoctorsByDate(Doctor doctor, DateTime date)
		{
			WorkingPeriod workingPeriod = null;
			foreach (WorkingPeriod wp in doctor.WorkingPeriods)
			{
				if (date >= wp.BeginDate.Date && date < wp.EndDate.Date)
				{
					workingPeriod = wp;
					break;
				}
			}
			return workingPeriod;
		}


		public WorkingTime getWorkingTimeForDate(WorkingPeriod workingPeriod, DateTime date)
		{
			WorkingTime workingTime = null;
			foreach (WorkingTime wt in workingPeriod.WorkingTimes)
			{
				foreach (Day day in wt.Days)
				{

					if (day.ToString().Equals(date.DayOfWeek.ToString()))
					{

						workingTime = wt;
						break;
					}
				}
			}

			return workingTime;

		}

	*/
		public List<OpenAppointments> GetOpenAppointments(List<Model.Appointment.Appointment> allAppointments, Dictionary<Model.Users.Doctor, Model.Users.WorkingTime> dictionary)
		{
			List<OpenAppointments> openAppointments = new List<OpenAppointments>();

			foreach (Model.Users.Doctor doctor in dictionary.Keys)
			{


				DateTime startSlot = dictionary[doctor].BeginTime; // Starts at 8:00AM
				List<DateTime> listOfTime = new List<DateTime>();
				List<DateTime> openAppointmentsForDoctor = new List<DateTime>();
				bool existsScheduleAppointemnt = false;

				while (startSlot.Hour < dictionary[doctor].EndTime.Hour)
				{
					// Construct time slot class

					startSlot = startSlot.AddMinutes(15);
					listOfTime.Add(startSlot);
				}

				foreach (DateTime d in listOfTime)
				{

					bool find = false;
					foreach (Model.Appointment.Appointment a in allAppointments)
					{

						if (a.Doctor.PersonalID.Equals(doctor.PersonalID))
						{
							existsScheduleAppointemnt = true;
							if (a.StartDate.Hour.Equals(d.Hour) && a.StartDate.Minute.Equals(d.Minute))
							{
								find = true;
							}

						}
					}
					if (!find)
					{
						openAppointmentsForDoctor.Add(d);

					}

				}

				foreach (DateTime start in openAppointmentsForDoctor)
				{
					DateTime date = selectedDate;
					TimeSpan ts = new TimeSpan(start.Hour, start.Minute, 0);
					date = date.Date + ts;
					OpenAppointments newOpenAppointments = new OpenAppointments(doctor, date, start.AddMinutes(15));
					openAppointments.Add(newOpenAppointments);
					newOpenAppointments = null;
				}
			


			}

			return openAppointments;
		}







		private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
		{

			if (ucitano)
			{

				selectedDate = dateTime.SelectedDate.Value.Date;
				

				List<Model.Appointment.Appointment> appointmentsByDate = MainWindow.appointmentController.GetAppointmentByDate(selectedDate);
				Dictionary<Model.Users.Doctor, Model.Users.WorkingTime> workingTimeForDoctor = MainWindow.doctorController.FilterDoctorsByWorkingTime(selectedDate);
				allOpenAppointments = GetOpenAppointments(appointmentsByDate, workingTimeForDoctor);
			





				OpenAppointmentDataGrid.ItemsSource = allOpenAppointments;


			}







		}




	}

}
