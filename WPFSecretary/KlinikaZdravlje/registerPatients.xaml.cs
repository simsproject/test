﻿using KlinikaZdravlje.model;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for registracija.xaml
	/// </summary>
	public partial class RegisterPatients : Page
	{
		public bool potvrdi;
		public RegisterPatients()
		{
			InitializeComponent();
			potvrdi = false;

			//FilterViewModel vm = new FilterViewModel();
			//City.DataContext = vm;
			//City.ItemsSource = MainWindow.cityController.GetAll().ToList();
			//FilterViewModel2 vm2 = new FilterViewModel2();
			Country.ItemsSource = MainWindow.countryController.GetAll().ToList();
			//Address.ItemsSource = MainWindow.adressController.GetAll().ToList();
			//Country.DataContext = vm2;
		}


		private void DayComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			List<string> data = new List<string>();


			//int currentYear = DateTime.Now.Year;
			for (int i = 1; i <= 31; i++)
			{
				data.Add(i.ToString());
			}

			DayComboBox.ItemsSource = data;

		}

		private void YearComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			List<string> data = new List<string>();


			int currentYear = DateTime.Now.Year;
			for (int i = currentYear; i >= 1920; i--)
			{
				data.Add(i.ToString());
			}

			YearComboBox.ItemsSource = data;

		}

		private void MonthComboBox_Loaded(object sender, RoutedEventArgs e)
		{
			List<string> data = new List<string>();



			data.Add("Januar");
			data.Add("Februar");
			data.Add("Mart");
			data.Add("April");
			data.Add("Maj");
			data.Add("Jun");
			data.Add("Jul");
			data.Add("Avgust");
			data.Add("Septembar");
			data.Add("Oktobar");
			data.Add("Novembar");
			data.Add("Decembar");




			MonthComboBox.ItemsSource = data;

		}

		private void DayComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

			if (DayComboBox.SelectedItem != null && MonthComboBox.SelectedItem != null && YearComboBox.SelectedItem != null)
			{
				int year = Int32.Parse(YearComboBox.SelectedItem.ToString());
				int month = MonthComboBox.SelectedIndex + 1;
				int day = Int32.Parse(DayComboBox.SelectedItem.ToString());
				if (day <= DateTime.DaysInMonth(year, month))
				{


					DayComboBox.BorderBrush = Brushes.Gray;
					DayComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					MonthComboBox.BorderBrush = Brushes.Gray;
					MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					YearComboBox.BorderBrush = Brushes.Gray;
					YearComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					godina.Text = "	Izaberite godinu rodjenja pacijenta";
					dan.Text = "Izaberite dan rodjenja pacijenta";
					this.mesec.Text = "	Izaberite mesec rodjenja pacijenta";
				}
				else
				{
					DayComboBox.BorderBrush = Brushes.Red;
					DayComboBox.BorderThickness = new Thickness(0, 0, 0, 2);

					MonthComboBox.BorderBrush = Brushes.Red;
					MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 2);

					YearComboBox.BorderBrush = Brushes.Red;
					YearComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				godina.Text = "Datum nije validan";
					dan.Text = "Datum nije validan";
					this.mesec.Text= "Datum nije validan";


				}
			}
		}

		private void MonthComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

			if (DayComboBox.SelectedItem != null && MonthComboBox.SelectedItem != null && YearComboBox.SelectedItem != null)
			{
				int year = Int32.Parse(YearComboBox.SelectedItem.ToString());
				int month = MonthComboBox.SelectedIndex + 1;
				int day = Int32.Parse(DayComboBox.SelectedItem.ToString());
				if (day <= DateTime.DaysInMonth(year, month))
				{


					DayComboBox.BorderBrush = Brushes.Gray;
					DayComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					MonthComboBox.BorderBrush = Brushes.Gray;
					MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					YearComboBox.BorderBrush = Brushes.Gray;
					YearComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					godina.Text = "zaberite godinu rodjenja pacijenta";
					dan.Text = "Izaberite dan rodjenja pacijenta";
				mesec.Text = "	Izaberite mesec rodjenja pacijenta";
				}
				else
				{
					DayComboBox.BorderBrush = Brushes.Red;
					DayComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
					MonthComboBox.BorderBrush = Brushes.Red;
					MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
					YearComboBox.BorderBrush = Brushes.Red;
					YearComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				godina.Text = "Datum nije validan";
					dan.Text = "Datum nije validan";
					mesec.Text = "Datum nije validan";


				}
			}
		}

		private void YearComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{

			if (DayComboBox.SelectedItem != null && MonthComboBox.SelectedItem != null && YearComboBox.SelectedItem != null)
			{
				int year = Int32.Parse(YearComboBox.SelectedItem.ToString());
				int month = MonthComboBox.SelectedIndex + 1;
				int day = Int32.Parse(DayComboBox.SelectedItem.ToString());
				if (day <= DateTime.DaysInMonth(year, month))
				{


					DayComboBox.BorderBrush = Brushes.Gray;
					DayComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					MonthComboBox.BorderBrush = Brushes.Gray;
					MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					YearComboBox.BorderBrush = Brushes.Gray;
					YearComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
					godina.Text= "Izaberite godinu rodjenja pacijenta";
					dan.Text = "Izaberite dan rodjenja pacijenta";
					mesec.Text = "	Izaberite mesec rodjenja pacijenta";
				}
				else
				{

					DayComboBox.BorderBrush = Brushes.Red;
					DayComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
					MonthComboBox.BorderBrush = Brushes.Red;
					MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
					YearComboBox.BorderBrush = Brushes.Red;
					YearComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				godina.Text = "Datum nije validan";
					dan.Text = "Datum nije validan";
				mesec.Text = "Datum nije validan";



				}
			}
		}

		private void SubmitButton_Click(object sender, RoutedEventArgs e)
		{
			potvrdi = true;
			bool isFirstName = false;
			bool isCity = false;
			bool isCountry = false;
			bool isLastName = false;
			bool isPersonalID = false;
			bool isAddress = false;
			bool isDay = false;
			bool isMonth = false;
			bool isYear = false;
			bool isPhoneNumber = false;
			bool isUsername = false;

			if (FirstName.Text == "")
			{
				FirstName.BorderBrush = Brushes.Red;
				ime.Text = "Polje mora biti popunjeno, unesite ime pacijenta kog zelite da registrujete";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 2);
				isFirstName = false;
				iconFirstName.Visibility = Visibility.Visible;


			}
			else
			{
				FirstName.BorderBrush = Brushes.Gray;
				ime.Text = "Unesite ime pacijenta kog zelite da registrujete";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 1);
				isFirstName = true;
				iconFirstName.Visibility = Visibility.Collapsed;
			}

			if (LastName.Text == "")
			{
				LastName.BorderBrush = Brushes.Red;
				prezime.Text = "Polje mora biti popunjeno, unesite prezime pacijenta kog zelite da registrujete";
				LastName.BorderThickness = new Thickness(0, 0, 0, 2);
				isLastName = false;
				iconLastName.Visibility = Visibility.Visible;

			}
			else
			{
				LastName.BorderBrush = Brushes.Gray;
				prezime.Text = "Unesite prezime pacijenta kog zelite da registrujete";
				LastName.BorderThickness = new Thickness(0, 0, 0, 1);
				isLastName = true;
				iconLastName.Visibility = Visibility.Collapsed;



			}

			if (PersonalID.Text == "")
			{
				PersonalID.BorderBrush = Brushes.Red;
				jmbg.Text = "Polje mora biti popunjeno, unesite JMBG pacijenta kog zelite da registrujete";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 2);
				isPersonalID = false;
				iconPersonalID.Visibility = Visibility.Visible;


			}
			else
			{
				PersonalID.BorderBrush = Brushes.Gray;
				jmbg.Text = "Unesite JMBG pacijenta kog zelite da registrujete";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 1);
				isPersonalID = true;
				iconPersonalID.Visibility = Visibility.Collapsed;



			}

			if (PhoneNumber.Text == "")
			{
				PhoneNumber.BorderBrush = Brushes.Red;
				telefon.Text = "Polje mora biti popunjeno, unesite kontakt telefon pacijenta kog zelite da registrujete";
				PhoneNumber.BorderThickness = new Thickness(0, 0, 0, 2);
				isPhoneNumber = false;
				iconPhoneNumber.Visibility = Visibility.Visible;


			}
			else
			{
				PhoneNumber.BorderBrush = Brushes.Gray;
				telefon.Text = "	Unesite kontakt telefon pacijenta kog zelite da registrujete";
				PhoneNumber.BorderThickness = new Thickness(0, 0, 0, 1);
				isPhoneNumber = true;
				iconPhoneNumber.Visibility = Visibility.Collapsed;


			}

			if (Address.Text == "")
			{
				Address.BorderBrush = Brushes.Red;
				adresa.Text = "Polje mora biti popunjeno, unesite ulicu i broj pacijenta kog zelite da registrujete";
				Address.BorderThickness = new Thickness(0, 0, 0, 2);
				isAddress = false;
				iconAddress.Visibility = Visibility.Visible;




			}
			else
			{
				Address.BorderBrush = Brushes.Gray;
				adresa.Text = "Unesite ulicu i broj pacijenta kog zelite da registrujete";
				Address.BorderThickness = new Thickness(0, 0, 0, 1);
				isAddress = true;
				iconAddress.Visibility = Visibility.Collapsed;


			}


			if(DayComboBox.SelectedItem == null || MonthComboBox.SelectedItem == null || YearComboBox.SelectedItem == null)
			{
				iconDate.Visibility = Visibility.Visible;
			}
			else
			{
				iconAddress.Visibility = Visibility.Collapsed;
			}


			if (DayComboBox.SelectedItem == null)
			{

				DayComboBox.BorderBrush = Brushes.Red;
				DayComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				dan.Text = "Polje mora biti popunjeno, izaberite dan rodjenja pacijenta";
				isDay = false;

			}
			else
			{
				DayComboBox.BorderBrush = Brushes.Gray;
				DayComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				dan.Text = "Izaberite dan rodjenja pacijenta";
				isDay = true;



			}
			if (MonthComboBox.SelectedItem == null)
			{

				MonthComboBox.BorderBrush = Brushes.Red;
				MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
			mesec.Text = "Polje mora biti popunjeno";
				isMonth = false;


			}
			else
			{
				MonthComboBox.BorderBrush = Brushes.Gray;
				MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				mesec.Text = "	Izaberite mesec rodjenja pacijenta";
				isMonth = true;

			}
			if (YearComboBox.SelectedItem == null)
			{
				YearComboBox.BorderBrush = Brushes.Red;
				YearComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				godina.Text = "Polje mora biti popunjeno, Izaberite godinu rodjenja pacijenta";
				isYear = false;

			}
			else
			{
				YearComboBox.BorderBrush = Brushes.Gray;
				YearComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				godina.Text = "Izaberite godinu rodjenja pacijenta";
				isYear = true;
			}

			if(Country.SelectedItem == null)
			{
				Country.BorderBrush = Brushes.Red;
				Country.BorderThickness = new Thickness(0, 0, 0, 2);
				drzava.Text = "Polje mora biti popunjeno, Izaberite drzavu u kojoj zivi pacijent kog zelite da registrujete";
				iconCountry.Visibility = Visibility.Visible;
				isCountry = false;
			}
			else
			{
				Country.BorderBrush = Brushes.Gray;
				Country.BorderThickness = new Thickness(0, 0, 0, 1);
				drzava.Text= "Izaberite drzavu u kojoj zivi pacijent kog zelite da registrujete";
				iconCountry.Visibility = Visibility.Collapsed;

				isCountry = true;
			}

			if (City.SelectedItem == null)
			{
				City.BorderBrush = Brushes.Red;
				City.BorderThickness = new Thickness(0, 0, 0, 2);
				city.Text = "Polje mora biti popunjeno";
				iconCity.Visibility = Visibility.Visible;

				isCity = false;
			}
			else
			{
				City.BorderBrush = Brushes.Gray;
				City.BorderThickness = new Thickness(0, 0, 0, 1);
				city.Text = "Izaberite grad u kom zivi pacijent kog zelite da registrujete";
				iconCity.Visibility = Visibility.Collapsed;

				isCity = true;
			}

			if (Username.Text == "")
			{
				Username.BorderBrush = Brushes.Red;
				Username.BorderThickness = new Thickness(0, 0, 0, 2);
									
				korisnickoIme.Text = "Polje mora biti popunjeno, unesite novo korisnicko ime za pacijenta";
				iconUsername.Visibility = Visibility.Visible;

				isUsername = false;
			}
			else
			{
				Username.BorderBrush = Brushes.Gray;
				Username.BorderThickness = new Thickness(0, 0, 0, 1);
				korisnickoIme.Text = "Unesite novo korisnicko ime za pacijenta";
				iconUsername.Visibility = Visibility.Collapsed;

				isUsername = true;
			}

			bool okRegex = true;

			//Regex FirstLast = new Regex("")




			Regex PhoneNumberRegex = new Regex("[0-9]{3}/[0-9]{3,4}-[0-9]{3}");
			Regex JMBGregex = new Regex("[0-9]{13}");
			if (isPhoneNumber)
			{

				
				if (PhoneNumberRegex.IsMatch(PhoneNumber.Text))
				{
					PhoneNumber.BorderBrush = Brushes.Gray;
					PhoneNumber.BorderThickness = new Thickness(0, 0, 0, 1);

					iconPhoneNumber.Visibility = Visibility.Collapsed;
					iconPhoneNumber.ToolTip = "";
				}
				else
				{
					okRegex = false;

					PhoneNumber.BorderBrush = Brushes.Red;
					PhoneNumber.BorderThickness = new Thickness(0, 0, 0, 2);

					iconPhoneNumber.Visibility = Visibility.Visible;
					iconPhoneNumber.ToolTip = "Neispravan format kontakt telefona. Unesite ";
				}
			}

			if (isPersonalID)
			{
				if (JMBGregex.IsMatch(PersonalID.Text))
				{
					PersonalID.BorderBrush = Brushes.Gray;
					PersonalID.BorderThickness = new Thickness(0, 0, 0, 1);
					iconPersonalID.Visibility = Visibility.Collapsed;
					iconPersonalID.ToolTip = "";
				}
				else
				{
					PersonalID.BorderBrush = Brushes.Red;
					PersonalID.BorderThickness = new Thickness(0, 0, 0, 2);
					okRegex = false;
					iconPersonalID.Visibility = Visibility.Visible;
					iconPersonalID.ToolTip = "Neispravan format JMBG-a. Mora sadrazti 13 karaktera.";
				}

			}


			

			if (isFirstName && isLastName && isAddress && isDay && isCity && isCountry && isMonth && isYear  && isPersonalID && isPhoneNumber && isUsername)
			{


			



				if (okRegex)
				{
					bool isExists = false;
					foreach (Patient patient in MainWindow.Patients)
					{
						if (patient.Username != null)
						{
							if (patient.Username.Equals(Username.Text))
							{
								isExists = true;
								break;
							}
						}

					}
					if (!isExists)
					{
						BorderMessage.Visibility = Visibility.Visible;
						submitButton.IsEnabled = false;


					}
					else
					{
						labela.Content = "Korisnicko ime vec postoji";
					}

				}
			
			
			}
		}

		private void FirstName_LostFocus(object sender, RoutedEventArgs e)
		{
			if (FirstName.Text != "")
			{
				FirstName.BorderBrush = Brushes.Gray;
				ime.Text = "Unesite ime pacijenta kog zelite da registrujete";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{
				FirstName.BorderBrush = Brushes.Red;
				ime.Text= "Polje mora biti popunjeno, unesite ime pacijenta kog zelite da registrujet";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 2);

			}
		}

		private void LastName_LostFocus(object sender, RoutedEventArgs e)
		{
			if (LastName.Text != "")
			{

				LastName.BorderBrush = Brushes.Gray;
				prezime.Text= "Unesite prezime pacijenta kog zelite da registrujete";
				LastName.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{
				LastName.BorderBrush = Brushes.Red;
				prezime.Text= "Polje mora biti popunjeno, unesite prezime pacijenta kog zelite da registrujete";
				LastName.BorderThickness = new Thickness(0, 0, 0, 2);

			}
		}

		private void PersonalID_LostFocus(object sender, RoutedEventArgs e)
		{
			if (PersonalID.Text != "")
			{
				PersonalID.BorderBrush = Brushes.Gray;
				jmbg.Text = "	Unesite JMBG pacijenta kog zelite da registrujete";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{

				PersonalID.BorderBrush = Brushes.Red;
				jmbg.Text = "Polje mora biti popunjeno, unesite JMBG pacijenta kog zelite da registrujete";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 2);
			}
		}

		private void PhoneNumber_LostFocus(object sender, RoutedEventArgs e)
		{
			if (PhoneNumber.Text != "")
			{
				PhoneNumber.BorderBrush = Brushes.Gray;
				telefon.Text = "Unesite kontakt telefon pacijenta kog zelite da registrujete";
				PhoneNumber.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{

				PhoneNumber.BorderBrush = Brushes.Red;
			telefon.Text = "Polje mora biti popunjeno, unesite kontakt telefon pacijenta kog zelite da registrujete";
				PhoneNumber.BorderThickness = new Thickness(0, 0, 0, 2);
			}
		}

		private void Address_LostFocus(object sender, RoutedEventArgs e)
		{
			if (Address.Text != "")
			{
				Address.BorderBrush = Brushes.Gray;
				adresa.Text = "Unesite ulicu i broj pacijenta kog zelite da registrujete";
				Address.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{
				Address.BorderBrush = Brushes.Red;
				adresa.Text = "Polje mora biti popunjeno, unesite ulicu i broj pacijenta kog zelite da registrujete";
				Address.BorderThickness = new Thickness(0, 0, 0, 2);
			}
		}

		private void RegisterButton_Click(object sender, RoutedEventArgs e)
		{
			String month = "";
			switch (MonthComboBox.SelectedIndex)
			{
				case 0:
					month = "Jan";
					break;
				case 1:
					month = "Feb";
					break;
				case 2:
					month = "Mar";
					break;
				case 3:
					month = "Apr";
					break;
				case 4:
					month = "May";
					break;
				case 5:
					month = "Jun";
					break;
				case 6:
					month = "Jul";
					break;
				case 7:
					month = "Avg";
					break;
				case 8:
					month = "Sep";
					break;
				case 9:
					month = "Oct";
					break;
				case 10:
					month = "Nov";
					break;
				case 11:
					month = "Dec";
					break;

			}
			String dateOfBirth = month + " " + DayComboBox.SelectedValue.ToString() + "," + " " + YearComboBox.SelectedValue.ToString();

			bool isExists = false;
			List<Model.Users.Patient> listOfPatient = MainWindow.patientController.GetAll().ToList();

			Model.Users.Gender gender = Model.Users.Gender.Female;
			if (musko.IsChecked == true)
			{
				gender = Model.Users.Gender.Male;
			}

			foreach (Model.Users.Patient p in listOfPatient)
			{
				if (p.IsGuest)
				{
					if (p.PersonalID.Equals(PersonalID.Text))
					{
						isExists = true;
						p.PhoneNumber = PhoneNumber.Text;
						p.Email = "";
						p.UserName = Username.Text;
						p.Password = PersonalID.Text;
						p.IsGuest = false;
						p.Gender = gender;
						p.Address = Address.SelectedItem as Model.Users.Address;
						MainWindow.patientController.Update(p);
						MainWindow.userController.Update(p);


					}
				}
			}

			if (!isExists)
			{

				if (Username.Text.Length >= 6)
				{
					Model.Users.Patient p = new Model.Users.Patient(FirstName.Text, LastName.Text, DateTime.Parse(dateOfBirth), PersonalID.Text, gender, PhoneNumber.Text, "", Address.SelectedItem as Model.Users.Address, Username.Text, PersonalID.Text, false, MainWindow.medicalRecordController.Create(new Model.MedicalRecords.MedicalRecord(0, new List<Model.MedicalRecords.Allergens>())));
					MainWindow.patientController.Create(p);
					MainWindow.userController.Create(p);

				}

				else
					MessageBox.Show("Korisnicko ime mora iamti 6 ili vise karaktera");
			}

			this.NavigationService.Navigate(new RegisterPatients());
		}

		private void CancleButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			submitButton.IsEnabled = true;
		}

		private void CloseImg_MouseDown(object sender, MouseButtonEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			submitButton.IsEnabled = true;
		}

		private void DayComboBox_LostFocus(object sender, RoutedEventArgs e)
		{
			if (DayComboBox.SelectedItem == null)
			{

				DayComboBox.BorderBrush = Brushes.Red;
				DayComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				dan.Text = "Polje mora biti popunjeno, izaberite dan rodjenja pacijenta";


			}
			else
			{
				DayComboBox.BorderBrush = Brushes.Gray;
				DayComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				dan.Text = "Izaberite dan rodjenja pacijenta";

			}

		}

		private void MonthComboBox_LostFocus(object sender, RoutedEventArgs e)
		{
			if (MonthComboBox.SelectedItem == null)
			{

				MonthComboBox.BorderBrush = Brushes.Red;
				MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
			mesec.Text = "Polje mora biti popunjeno";


			}
			else
			{
				MonthComboBox.BorderBrush = Brushes.Gray;
				MonthComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				mesec.Text = "	Izaberite mesec rodjenja pacijenta";

			}
		}

		private void YearComboBox_LostFocus(object sender, RoutedEventArgs e)
		{
			if (YearComboBox.SelectedItem == null)
			{

				YearComboBox.BorderBrush = Brushes.Red;
				YearComboBox.BorderThickness = new Thickness(0, 0, 0, 2);
				godina.Text = "Polje mora biti popunjeno, izaberite godinu rodjenja pacijenta";


			}
			else
			{
				YearComboBox.BorderBrush = Brushes.Gray;
				YearComboBox.BorderThickness = new Thickness(0, 0, 0, 1);
				godina.Text= "	Izaberite godinu rodjenja pacijenta";

			}
		}

		private void FirstName_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (FirstName.Text == "")
				{
					iconFirstName.Visibility = Visibility.Visible;
				}
				else
				{
					iconFirstName.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void LastName_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (LastName.Text == "")
				{
					iconLastName.Visibility = Visibility.Visible;
				}
				else
				{
					iconLastName.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void PersonalID_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (PersonalID.Text == "")
				{
					iconPersonalID.Visibility = Visibility.Visible;
				}
				else
				{
					iconPersonalID.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void PhoneNumber_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (PhoneNumber.Text == "")
				{
					iconPhoneNumber.Visibility = Visibility.Visible;
				}
				else
				{
					iconPhoneNumber.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void Address_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (Address.Text == "")
				{
					iconAddress.Visibility = Visibility.Visible;
				}
				else
				{
					iconAddress.Visibility = Visibility.Collapsed;
				}
			}
		}
		private void Cmb_KeyUp(object sender, KeyEventArgs e)
		{
			CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(City.ItemsSource);

			itemsViewOriginal.Filter = ((o) =>
			{
				if (String.IsNullOrEmpty(City.Text)) return true;
				else
				{
					if (((string)o).Contains(City.Text)) return true;
					else return false;
				}
			});

			itemsViewOriginal.Refresh();
		}

		private void Country_KeyUp(object sender, KeyEventArgs e)
		{
			CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(Country.ItemsSource);

			itemsViewOriginal.Filter = ((o) =>
			{
				if (String.IsNullOrEmpty(Country.Text)) return true;
				else
				{
					if (((string)o).Contains(Country.Text)) return true;
					else return false;
				}
			});

			itemsViewOriginal.Refresh();
		}


		public class FilterViewModel
		{
			public List<Model.Users.Country> dateSource;

			public FilterViewModel()
			{
				dateSource = MainWindow.countryController.GetAll().ToList();
			}
		}

		public class FilterViewModel2
		{

			public List<Model.Users.City> dateSource2;

			public FilterViewModel2()
			{
				dateSource2 = MainWindow.cityController.GetAll().ToList();
			}
		}

		private void Username_LostFocus(object sender, RoutedEventArgs e)
		{
			if (Username.Text != "")
			{
				Username.BorderBrush = Brushes.Gray;
				korisnickoIme.Text = "Unesite novo korisnicko ime za pacijenta";
				Username.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{
				Username.BorderBrush = Brushes.Red;
				korisnickoIme.Text = "Polje mora biti popunjeno, unesite novo korisnicko ime za pacijenta";
				Username.BorderThickness = new Thickness(0, 0, 0, 2);
			}
		}

		private void Username_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (Username.Text == "")
				{
					iconUsername.Visibility = Visibility.Visible;
				}
				else
				{
					iconUsername.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void City_LostFocus(object sender, RoutedEventArgs e)
		{
			if (City.SelectedItem == null)
			{

				City.BorderBrush = Brushes.Red;
				City.BorderThickness = new Thickness(0, 0, 0, 2);
				city.Text = "Polje mora biti popunjeno, izaberite grad u kom zivi pacijent kog zelite da registrujete";


			}
			else
			{
				City.BorderBrush = Brushes.Gray;
				City.BorderThickness = new Thickness(0, 0, 0, 1);
				city.Text = "Izaberite grad u kom zivi pacijent kog zelite da registrujete";

			}
		}

				private void Country_LostFocus(object sender, RoutedEventArgs e)
			{


			if (Country.SelectedItem == null)
			{

				Country.BorderBrush = Brushes.Red;
				Country.BorderThickness = new Thickness(0, 0, 0, 2);
				drzava.Text = "Polje mora biti popunjeno,Izaberite drzavu u kojoj zivi pacijent kog zelite da registrujete";


			}
			else
			{
				Country.BorderBrush = Brushes.Gray;
				Country.BorderThickness = new Thickness(0, 0, 0, 1);
				drzava.Text = "Izaberite drzavu u kojoj zivi pacijent kog zelite da registrujete";

			}

		}

		private void FirstName_KeyDown(object sender, KeyEventArgs e)
		{
			Regex r = new Regex(@"(?i)^[a-z’'()/.,\s-]+$");
			if (!r.IsMatch(e.Key.ToString()))
			{
				e.Handled = true;
			}
		}

		private void LastName_KeyDown(object sender, KeyEventArgs e)
		{
			Regex r = new Regex(@"(?i)^[a-z’'()/.,\s-]+$");
			if (!r.IsMatch(e.Key.ToString()))
			{
				e.Handled = true;
			}
		}

		private void City_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			List<Model.Users.Address> sveAdrese = MainWindow.adressController.GetAll().ToList();
			List<Model.Users.Address> adrese = new List<Model.Users.Address>();
			foreach (Model.Users.Address ad in sveAdrese)
			{
				if (ad.City.Name.Equals((this.City.SelectedItem as Model.Users.City).Name))
				{
					adrese.Add(ad);
				}
			}
			this.Address.ItemsSource = adrese;
		}

		private void Country_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			List<Model.Users.City> sviGradovi = MainWindow.cityController.GetAll().ToList();
			List<Model.Users.City> gradovi = new List<Model.Users.City>();
			foreach (Model.Users.City ad in sviGradovi)
			{
				if (ad.Country.Name.Equals((this.Country.SelectedItem as Model.Users.Country).Name))
				{
					gradovi.Add(ad);
				}
			}
			this.City.ItemsSource = gradovi;
		}
	}

}
