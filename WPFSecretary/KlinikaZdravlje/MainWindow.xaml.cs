﻿using Controller;
using KlinikaZdravlje.model;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{

		public static List<MedicalRoom> medicalRooms = new List<MedicalRoom>();
		public static List<Doctor> doctors = new List<Doctor>();
		public static List<Appointment> appointments = new List<Appointment>();
		public static List<Secretary> secretaries = new List<Secretary>();
		public static List<Patient> Patients = new List<Patient>();




		public static AllergensController allergensController = AllergensController.GetInstance();
		public static MedicalRecordController medicalRecordController = MedicalRecordController.GetInstance();
		public static BedController bedController = BedController.GetInstance();
		public static PatientRoomController patientRoomController = PatientRoomController.GetInstance();
		public static MedicalHistoryController medicalHistoryController = MedicalHistoryController.GetInstance();
		public static CourseOfDieaseController courseOfDieaseController = CourseOfDieaseController.GetInstance();
		public static DoctorReportController doctorReportController = DoctorReportController.GetInstance();
		public static DoctorController doctorController = DoctorController.GetInstance();
		public static CityController cityController = CityController.GetInstance();
		public static AdressController adressController = AdressController.GetInstance();
		public static AppointmentController appointmentController = AppointmentController.GetInstance();
		public static DrugController drugController = DrugController.GetInstance();
		public static PatientController patientController = PatientController.GetInstance();
		public static RecipeController recipeController = RecipeController.GetInstance();
		public static QuestionnaireController questionnaireController = QuestionnaireController.GetInstance();
		public static QuestionController questionController = QuestionController.GetInstance();
		public static UserController userController = UserController.GetInstance();
		public static SecretaryController secretaryController = SecretaryController.GetInstance();
		public static NotificationController notificationController = NotificationController.GetInstance();
		public static CountryController countryController = CountryController.GetInstance();


		public MainWindow()
		{
			InitializeComponent();
			Main.NavigationService.Navigate(new login());
			MedicalRoom medicalRoom1 = new MedicalRoom { Name = "100A", Type = TypeOfMedicalRoom.ZA_PREGLED, Doctors = new List<Doctor>() };
			MedicalRoom medicalRoom2 = new MedicalRoom { Name = "100B", Type = TypeOfMedicalRoom.OPERACIONA, Doctors = new List<Doctor>() };
			MedicalRoom medicalRoom3 = new MedicalRoom { Name = "200C", Type = TypeOfMedicalRoom.OPERACIONA, Doctors = new List<Doctor>() };

			string iDate = "06.13.2020";
			DateTime oDate = new DateTime(2020, 06, 13);
			string iDate1 = "07.13.2020";
			DateTime oDate1 = new DateTime(2020, 07, 13);
			WorkingPeriod workingPeriod1 = new WorkingPeriod { BeginDate = oDate , EndDate = oDate1,WorkingTimes = new List<WorkingTime>() };
			WorkingTime workingTime1 = new WorkingTime { BeginTime = DateTime.Parse("06:00:00"), EndTime = DateTime.Parse("17:00:00"), Days = new List<Day>() };
			workingTime1.addWorkingDays(Day.Monday);
			workingTime1.addWorkingDays(Day.Tuesday);
			workingTime1.addWorkingDays(Day.Wednesday);
			workingTime1.addWorkingDays(Day.Thursday);
			workingTime1.addWorkingDays(Day.Friday);

			workingPeriod1.addWorkingTime(workingTime1);



			Doctor doctor1 = new Doctor() { FirstName = "Pera", LastName = "Peric", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", MedicalRoom = null, WorkingPeriods = new List<WorkingPeriod>() };
			Doctor doctor2 = new Doctor() { FirstName = "Mika", LastName = "Mikic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "123456", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "aaa", Password = "123", MedicalRoom = null, WorkingPeriods = new List<WorkingPeriod>() };

			doctor1.addWorkingPeriod(workingPeriod1);
			doctor2.addWorkingPeriod(workingPeriod1);
			doctor2.MedicalRoom = medicalRoom2;
			doctor1.MedicalRoom = medicalRoom1;
			medicalRoom1.addDoctorsToRoom(doctor1);
			medicalRoom2.addDoctorsToRoom(doctor2);
			doctors.Add(doctor1);
			doctors.Add(doctor2);



			//	appointments.Add(new Appointment {StartDateTime = Convert.ToDateTime("06/16/2020 09:15:00"),FinishDateTime = Convert.ToDateTime("06/16/2020 09:00:00"),Doctor = doctor1,Room=doctor1.MedicalRoom,Patient=null,SerialID=1 });
			//	appointments.Add(new Appointment { StartDateTime = Convert.ToDateTime("06/16/2020 09:30:00"), FinishDateTime = Convert.ToDateTime("06/16/2020 09:00:00"), Doctor = doctor2, Room =doctor2.MedicalRoom, Patient = null, SerialID = 1 });
			//	appointments.Add(new Appointment { StartDateTime = Convert.ToDateTime("06/17/2020 15:00:00"), FinishDateTime = Convert.ToDateTime("06/17/2020 15:15:00"), Doctor = doctor2, Room = doctor2.MedicalRoom, Patient = null, SerialID = 1 });


		

			Secretary secretary1 = new Secretary { FirstName = "Maja", LastName = "Tomic", DateOfBirth = DateTime.Parse("Dec 2, 1998"), PhoneNumber = "065/911-329", Email = "maja@gmail.com", PersonalID = "0212998", Username = "maja", Password = "majaa", Address ="Bulevar oslobodjenja 12",City ="Novi Sad", Country = "Srbija"};
			Secretary secretary2 = new Secretary { FirstName = "Pera", LastName = "Peric", DateOfBirth = DateTime.Parse("Jan 2, 1950"), PhoneNumber = "065/1111-329", Email = "pera@gmail.com", PersonalID = "0212998", Username = "pera", Password = "pera", Address = "Mileticeva 12", City = "Beograd", Country = "Srbija" };

			secretaries.Add(secretary1);
			secretaries.Add(secretary2);




			

		
			Patients.Add(new Patient() { FirstName = "Nikola", LastName = "Nikolic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false, MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Petar", LastName = "Petrovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false , MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Vuk", LastName = "Vukmanovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false, MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Stefa", LastName = "Stefanovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123",GuestAccount = false, MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Marija", LastName = "Marijanovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123" , GuestAccount = false , MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Andjela", LastName = "Andjelkovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123",GuestAccount = false , MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Mirjana", LastName = "Miric", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false, MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Tijana", LastName = "Boskovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false , MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Natasa", LastName = "Petrovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123" , GuestAccount = false , MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Sladjana", LastName = "Nikolic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false , MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Anastasija", LastName = "Lukic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123" , GuestAccount = false , MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Srdjan", LastName = "Peric", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false, MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Nemanja", LastName = "Nemanjic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false, MedicalRedordID = 1 });
			Patients.Add(new Patient() { FirstName = "Igor", LastName = "Pavlovic", DateOfBirth = DateTime.Parse("Jan 1, 2009"), PersonalID = "111111", PhoneNumber = "111111", Email = "maja@gmail.com", Username = "maja", Password = "123", GuestAccount = false, MedicalRedordID = 1 });
	}
	}
}
