﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for generisanjeIzvestaja.xaml
	/// </summary>
	public partial class generatingReports : Page
	{
		public generatingReports()
		{
			InitializeComponent();
		}



		DataTable MakeDataTable()
		{
			//Create friend table object
			DataTable friend = new DataTable();

			//Define columns
			friend.Columns.Add("Datum");
			friend.Columns.Add("Lekar");
			friend.Columns.Add("Vreme");
			friend.Columns.Add("Br sobe");
			friend.Columns.Add("Vrsta sobe");
			friend.Columns.Add("Pacijent");

			List<Appointment> appointments = getAppointmentForPeriod(StartDate.SelectedDate.Value, EndDate.SelectedDate.Value);

			foreach (Appointment a in appointments)
			{
				friend.Rows.Add(a.StartDateTime.ToString("dd.MM.yyyy."),a.Doctor.FullName, a.StartDateTime.ToString("hh:mm"),a.Room.Name,a.Room.Type.ToString(),a.Patient.FullName);
				
			}
			//Populate with friends :)
		

			return friend;
		}

		public List<Appointment> getAppointmentForPeriod(DateTime start,DateTime end)
		{
			List<Appointment> appointmentsForPDF = new List<Appointment>();
			foreach (Appointment a in MainWindow.appointments)
			{
				Console.WriteLine(a.StartDateTime.ToString("dd.MM.yyyy."));
				Console.WriteLine(start.ToString("dd.MM.yyyy."));
				Console.WriteLine(end.ToString("dd.MM.yyyy."));
				if (a.StartDateTime.Date >= start.Date && a.StartDateTime.Date <= end.Date)
				{
					appointmentsForPDF.Add(a);
				}

			}

			return appointmentsForPDF;
		}

		
		void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
		{
			System.IO.FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
			Document document = new Document();
			document.SetPageSize(iTextSharp.text.PageSize.A4);
			PdfWriter writer = PdfWriter.GetInstance(document, fs);
			document.Open();

			//Report Header
			BaseFont bfntHead = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			Font fntHead = new Font(bfntHead, 16, 1, BaseColor.GRAY);
			Paragraph prgHeading = new Paragraph();
			prgHeading.Alignment = Element.ALIGN_CENTER;
			prgHeading.Add(new Chunk(strHeader.ToUpper(), fntHead));
			document.Add(prgHeading);

			//Author
			iTextSharp.text.Paragraph prgAuthor = new Paragraph();
			BaseFont btnAuthor = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			Font fntAuthor = new Font(btnAuthor, 8, 2, BaseColor.GRAY);
			prgAuthor.Alignment = Element.ALIGN_RIGHT;
			prgAuthor.Add(new Chunk("Generisao izvrsio/la : " + login.logInSecretary.FirstName + " "+ login.logInSecretary.LastName, fntAuthor));
			prgAuthor.Add(new Chunk("\nDatum : " + DateTime.Now.ToString("dd.MM.yyyy. hh:mm:ss"), fntAuthor));
			document.Add(prgAuthor);

			//Add a line seperation
			Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
			document.Add(p);

			//Add line break
			document.Add(new Chunk("\n", fntHead));

			//Write the table
			PdfPTable table = new PdfPTable(dtblTable.Columns.Count);
		
			table.SetWidths(new float[] { 80, 100, 50, 50, 120, 100 });
			//Table header
			BaseFont btnColumnHeader = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
			Font fntColumnHeader = new Font(btnColumnHeader, 10, 1, BaseColor.WHITE);
			for (int i = 0; i < dtblTable.Columns.Count; i++)
			{
				PdfPCell cell = new PdfPCell();
				cell.BackgroundColor = BaseColor.GRAY;
				cell.AddElement(new Chunk(dtblTable.Columns[i].ColumnName.ToUpper(), fntColumnHeader));
				table.AddCell(cell);
			}
			//table Data
			for (int i = 0; i < dtblTable.Rows.Count; i++)
			{
				for (int j = 0; j < dtblTable.Columns.Count; j++)
				{
					table.AddCell(dtblTable.Rows[i][j].ToString());
				}
			}

			document.Add(table);
			document.Close();
			writer.Close();
			fs.Close();
		}



		private void GenerateButton_Click(object sender, RoutedEventArgs e)
		{
			bool generisi = true;
			if (this.StartDate.Text.Equals("") || this.EndDate.Text.Equals("")) {
				generisi = false;
			}

			if (generisi)
			{
				greska.Content = "";
				try
				{
					DataTable dtbl = MakeDataTable();
					ExportDataTableToPdf(dtbl, @"C:\test.pdf", "Generisanje izvestaja o zakazanim operacijama i pregledima u periodu od " + StartDate.SelectedDate.Value.ToString("dd.MM.yyyy.") + " do " + EndDate.SelectedDate.Value.ToString("dd.MM.yyyy."));

				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "Error Message");
				}
			}
			else
			{
				greska.Content = "Morate uneti vremenski interval u kom zelite da izgenerisete izvestaj!";
			}

		}



	}
}
