﻿using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for logovanje.xaml
	/// </summary>
	public partial class login : Page
	{
		public static Model.Users.Secretary logInSecretary = null;
		public String err = null;
		public login()
		{
			InitializeComponent();
		}

		private void LoginButton_Click(object sender, RoutedEventArgs e)
		{

			if (UsernameTextBox.Text !="" && PasswordBox.Password != "")
			{
				Model.Users.User user = MainWindow.userController.GetByID(UsernameTextBox.Text);
				Model.Users.Secretary s = MainWindow.secretaryController.GetByID(user.PersonalID);
				if (s != null)
				{
					if (s.Password.Equals(PasswordBox.Password))
					{
						logInSecretary = s;
						this.NavigationService.Navigate(new basicPage());
					}
					else
					{
						greska.Content = "Pogresno korisnicko ime/lozinka";

					}


				}
				else
				{
					greska.Content = "Ne postoji korisnik sa unetim username-om";
				}
				
			}
			else
			{
				greska.Content = "Niste uneli korisnicko ime/lozinku";
			}
				
		}
	}
}
