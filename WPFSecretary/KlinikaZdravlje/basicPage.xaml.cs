﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for osnovna.xaml
	/// </summary>
	public partial class basicPage : Page
	{
		
		public basicPage()
		{
		
			InitializeComponent();
			//customizeDesign();

		}
		
		private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			int index = ListViewMenu.SelectedIndex;
		

			switch (index)
			{
				case 0:
					DropDownBorder.Visibility = Visibility.Collapsed;
					basicFrame.NavigationService.Navigate(new SearchPatient());
					break;
				case 1:
					DropDownBorder.Visibility = Visibility.Collapsed;
					basicFrame.NavigationService.Navigate(new changeTimetable());
					break;
				case 2:
					DropDownBorder.Visibility = Visibility.Collapsed;
					basicFrame.NavigationService.Navigate(new TimeTable());
					break;
				case 3:
					DropDownBorder.Visibility = Visibility.Collapsed;
					basicFrame.NavigationService.Navigate(new RegisterPatients());
					break;
				case 4:
					DropDownBorder.Visibility = Visibility.Collapsed;
					basicFrame.NavigationService.Navigate(new generatingReports());
					break;
				case 5:
					DropDownBorder.Visibility = Visibility.Collapsed;
					basicFrame.NavigationService.Navigate(new Profile());
					break;
				case 6:
					DropDownBorder.Visibility = Visibility.Collapsed;
					break;
				default:
					break;
			}

		}
		
		/*
		private void customizeDesign()
		{
			subMenuPanel.Visibility = Visibility.Collapsed;
		}
		*/

		private void PackIcon_MouseDown_home(object sender, MouseButtonEventArgs e)
		{

			basicFrame.NavigationService.Navigate(new HomePage());

		}
		private void PackIcon_MouseDown_notification(object sender, MouseButtonEventArgs e)
		{

		

		}

		private void PackIcon_MouseDown_account(object sender, MouseButtonEventArgs e)
		{

			basicFrame.NavigationService.Navigate(new Profile());

		}
		private void PackIcon_MouseDown_logout(object sender, MouseButtonEventArgs e)
		{

		
				this.NavigationService.Navigate(new login());

		}
		

		private void Page_Loaded(object sender, RoutedEventArgs e)
		{
			basicFrame.NavigationService.Navigate(new HomePage());
		}

	

		private void DropDown_MouseDown(object sender, MouseButtonEventArgs e)
		{
			DropDownBorder.Visibility = Visibility.Visible;
		}

		private void FeedbackButton_Click(object sender, RoutedEventArgs e)
		{
			DropDownBorder.Visibility = Visibility.Collapsed;
			basicFrame.NavigationService.Navigate(new feedback());
			
		}

		private void HelpButton_Click(object sender, RoutedEventArgs e)
		{

		}

		private void LogOutButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new login());
		}





	

		private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			

		}


		private void BasicFrame_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (DropDownBorder.IsVisible)
			{
				DropDownBorder.Visibility = Visibility.Collapsed;
			}
		
		}

	}

}
