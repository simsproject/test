﻿

using KlinikaZdravlje.model;
using Model.Appointment;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for Page1.xaml
	/// </summary>
	public partial class scheduleAppointment : Page
	{

		public static DateTime selectedDate;
		public  List<OpenAppointments> allOpenAppointments = null;
		public bool ucitano = false;
		public OpenAppointments selectedAppointment = null;
		public scheduleAppointment()
		{
			
		

			InitializeComponent();
			this.dateTime.BlackoutDates.AddDatesInPast();
			ucitano = true;
			
		
			selectedDate = dateTime.SelectedDate.Value.Date;
		
		
			List<Model.Appointment.Appointment> appointmentsByDate = MainWindow.appointmentController.GetAppointmentByDate(selectedDate);
			Dictionary<Model.Users.Doctor, Model.Users.WorkingTime> workingTimeForDoctor = MainWindow.doctorController.FilterDoctorsByWorkingTime(selectedDate);
			allOpenAppointments = GetOpenAppointments(appointmentsByDate, workingTimeForDoctor);




			OpenAppointmentDataGrid.ItemsSource = allOpenAppointments;
		}
		public class Pregled
		{
			public string Datum { get; set; }
			public string Vreme { get; set; }
			public string Lekar { get; set; }
			public string Soba { get; set; }
		}


		private void YasButton_Click(object sender, RoutedEventArgs e)
		{
			Model.Users.Patient p = SearchPatient.selectedpatient;
			OpenAppointments a = this.OpenAppointmentDataGrid.SelectedItem as OpenAppointments;
			Model.Appointment.AppointmentType tip = Model.Appointment.AppointmentType.operation;
			if (a.Doctor.MedicalRoom.Type.Equals(TypeOfMedicalRoom.ZA_PREGLED))
			{
				tip = Model.Appointment.AppointmentType.regularAp;
			}
			MainWindow.appointmentController.Create(new Model.Appointment.Appointment(a.StartDateTime, a.FinishDateTime, tip, 0, p, a.Doctor, a.Doctor.MedicalRoom));
			if (checkBoxDoctor.IsChecked == true)
			{
				String message = "Hitan slucaj u sali " + a.Doctor.MedicalRoom.Name;
				MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, a.Doctor));
			}
					
			this.NavigationService.Navigate(new SearchPatient());

		}

		private void NoButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new SearchPatient());
		}

	


		public List<model.Appointment> filterAllAppointmentsByDate(DateTime dateTime) //daj mi sve zauzete termine za izabrani datum
		{
			List < model.Appointment > appointmentsByDate = new List< model.Appointment > ();
			foreach ( model.Appointment appointment in MainWindow.appointments)
			{
				if (appointment.StartDateTime.Date.Equals(dateTime))
				{
					appointmentsByDate.Add(appointment);
				
				}
			}

			return appointmentsByDate;
		}


		private void OpenAppointmentDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{

			ScheduleButton.Visibility = Visibility.Visible;
			CancelButton.Visibility = Visibility.Visible;
			OpenAppointments a = this.OpenAppointmentDataGrid.SelectedItem as OpenAppointments;
			if(a != null)
			{
				if (DateTime.Compare(a.StartDateTime, DateTime.Now) >= 0)
				{
					selectedAppointment = a;
					Model.Users.Patient p = SearchPatient.selectedpatient;
					ScheduleAppointmentInfo s = new ScheduleAppointmentInfo();
					s.nazivLekara.Text = a.Doctor.FirstName + " " + a.Doctor.LastName;
					s.datum.Text = a.StartDateTime.ToString("dd.MM.yyyy");
					s.Vreme.Text = a.StartDateTime.ToString("HH:mm");
					s.Soba.Text = a.Doctor.MedicalRoom.Name;
					s.vrstaSobe.Text = a.Doctor.MedicalRoom.Type.ToString();
					s.nazivPacijenta.Text = p.FirstName + " " + p.LastName;
					s.JMBG.Text = p.PersonalID.ToString();
					s.BrojKartona.Text = p.MedicalRecord.ToString();
					if (a.Doctor.MedicalRoom.Type.Equals(TypeOfMedicalRoom.ZA_PREGLED))
					{
						s.typeAppointment.Text = model.AppointmentType.PREGLED.ToString();
					}
					else
					{
						s.typeAppointment.Text = model.AppointmentType.OPERACIJA.ToString();
					}
					AppointmentInfoFrame.NavigationService.Navigate(s);
				}
				else
				{
					MessageBox.Show("Termin je prosao");
				}
			}
	

		}

		private void ScheduleButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Visible;
			CancelButton.IsEnabled = false;
			ScheduleButton.IsEnabled = false;
			OpenAppointmentDataGrid.IsEnabled = false;
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new SearchPatient());
		}

		private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
		{
			var filtered = allOpenAppointments.Where(patient => (patient.StartDateTime.ToString("hh:mm").Contains(SearchTextBox.Text) || patient.Doctor.FirstName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.LastName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.MedicalRoom.Name.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.MedicalRoom.Type.ToString().ToUpper().Contains(SearchTextBox.Text.ToUpper())));


			OpenAppointmentDataGrid.ItemsSource = filtered;
		}

		private void CancleButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			CancelButton.IsEnabled = true;
			ScheduleButton.IsEnabled = true;
			OpenAppointmentDataGrid.IsEnabled = true;
		}

		private void CloseImg_MouseDown(object sender, MouseButtonEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			CancelButton.IsEnabled = true;
			ScheduleButton.IsEnabled = true;
			OpenAppointmentDataGrid.IsEnabled = true;
		}

		private void ScheduleButton_Click_1(object sender, RoutedEventArgs e)
		{
			if (selectedAppointment != null)
			{
				Model.Users.Patient p = SearchPatient.selectedpatient;
				Model.Appointment.AppointmentType tip = Model.Appointment.AppointmentType.operation;
				if (selectedAppointment.Doctor.MedicalRoom.Type.Equals(TypeOfMedicalRoom.ZA_PREGLED))
				{
					tip = Model.Appointment.AppointmentType.regularAp;
				}

				

					MainWindow.appointmentController.Create(new Model.Appointment.Appointment(selectedAppointment.StartDateTime, selectedAppointment.FinishDateTime, tip, 0, p, selectedAppointment.Doctor, selectedAppointment.Doctor.MedicalRoom));
					if (checkBoxDoctor.IsChecked == true)
					{
						String message = "Hitan slucaj u sali " + selectedAppointment.Doctor.MedicalRoom.Name;
						MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, selectedAppointment.Doctor));
					}
					//	MainWindow.appointments.Add(new model.Appointment { Doctor = selectedAppointment.Doctor, StartDateTime = selectedAppointment.StartDateTime, FinishDateTime = selectedAppointment.FinishDateTime, Room = selectedAppointment.Doctor.MedicalRoom, SerialID = 1, Patient = p });
					allOpenAppointments.Clear();
					this.NavigationService.Navigate(new SearchPatient());
				
			
			}
			CancelButton.IsEnabled = true;
			ScheduleButton.IsEnabled = true;
			OpenAppointmentDataGrid.IsEnabled = true;


		}

/*
		public Dictionary<Doctor, WorkingTime> filterDoctorByWorkingPeriod(DateTime date)   //trazimo radno vreme svakog lekara na osnovu datuma
		{
			Dictionary<Doctor, WorkingTime> dictionaryForWorkingTime = new Dictionary<Doctor, WorkingTime>();
		

			foreach (Doctor doctor in MainWindow.doctors)
			{
			
				WorkingPeriod workingPeriod = workingPeriodForDoctorsByDate(doctor, date.Date);
				
				
					if(workingPeriod == null)
					{
						continue;
					}
					WorkingTime workingTime = getWorkingTimeForDate(workingPeriod, date);
				
					if (workingTime == null)
					{
						continue;
					}
					dictionaryForWorkingTime.Add(doctor, workingTime);
			}



				return dictionaryForWorkingTime;
		}


		public WorkingPeriod workingPeriodForDoctorsByDate(Doctor doctor, DateTime date)
		{
			WorkingPeriod workingPeriod = null;
			foreach (WorkingPeriod wp in doctor.WorkingPeriods)
			{
				if (date >= wp.BeginDate.Date && date < wp.EndDate.Date)
				{
					workingPeriod = wp;
					break;
				}
			}
			return workingPeriod;
		}


		public WorkingTime getWorkingTimeForDate(WorkingPeriod workingPeriod, DateTime date)
		{
			WorkingTime workingTime = null;
			foreach (WorkingTime wt in workingPeriod.WorkingTimes)
			{
				foreach (Day day in wt.Days)
				{
			
					if (day.ToString().Equals(date.DayOfWeek.ToString()))
					{
					
						workingTime = wt;
						break;
					}
				}
			}

			return workingTime;

		}

*/
		public List<OpenAppointments> GetOpenAppointments(List<Model.Appointment.Appointment> allAppointments, Dictionary<Model.Users.Doctor, Model.Users.WorkingTime> dictionary)
		{
			List<OpenAppointments> openAppointments = new List<OpenAppointments>();

			foreach (Model.Users.Doctor doctor in dictionary.Keys)
			{

				Console.WriteLine(doctor.FirstName);
				DateTime startSlot = dictionary[doctor].BeginTime; // Starts at 8:00AM
				List<DateTime> listOfTime = new List<DateTime>();
				List<DateTime> openAppointmentsForDoctor = new List<DateTime>();
		

				while (startSlot.Hour < dictionary[doctor].EndTime.Hour)
				{
					// Construct time slot class

					startSlot = startSlot.AddMinutes(15);
					Console.WriteLine(startSlot);
					listOfTime.Add(startSlot);
				}

				foreach (DateTime d in listOfTime)
				{

					bool find = false;
					foreach (Model.Appointment.Appointment a in allAppointments)
					{
						if (a.Doctor.PersonalID.Equals(doctor.PersonalID))
						{
						
							if (a.StartDate.Hour.Equals(d.Hour) && a.StartDate.Minute.Equals(d.Minute))
							{
								find = true;
								break;
							}
					

						}
					}

					if (!find)
					{
						openAppointmentsForDoctor.Add(d);
						
					}

				}

				foreach (DateTime start in openAppointmentsForDoctor)
				{
					DateTime date = selectedDate;
					TimeSpan ts = new TimeSpan(start.Hour, start.Minute, 0);
					date = date.Date + ts;
					OpenAppointments newOpenAppointments = new OpenAppointments(doctor, date, date.AddMinutes(15));
					openAppointments.Add(newOpenAppointments);
					newOpenAppointments = null;
				}



			}

			return openAppointments;
		}
		





		
				private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
				{
			
					if (ucitano)
					{

						selectedDate = dateTime.SelectedDate.Value.Date;
					

						List<Model.Appointment.Appointment> appointmentsByDate = MainWindow.appointmentController.GetAppointmentByDate(selectedDate);
						Dictionary<Model.Users.Doctor, Model.Users.WorkingTime> workingTimeForDoctor = MainWindow.doctorController.FilterDoctorsByWorkingTime(selectedDate);
						allOpenAppointments = GetOpenAppointments(appointmentsByDate, workingTimeForDoctor);





						OpenAppointmentDataGrid.ItemsSource = allOpenAppointments;


					}
					
					
					



						
				}

	


	}
}
