﻿using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for pregled.xaml
	/// </summary>
	/// 
	
	public partial class SearchPatient : Page
	{
		
		public static int medicalRecordID = 0;
		public static Model.Users.Patient selectedpatient = null;
		public SearchPatient()
		{
			InitializeComponent();

			this.DataContext = this;

			PatientsDataGrid.ItemsSource = MainWindow.patientController.GetAll();
		}


	

		/*public class Patient
		{
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public string JMBG { get; set; }
			public AccountType Type { get; set; }
		}

		public enum AccountType
		{
			GUEST,
			PRAVI
		}*/


		private void GuestButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new guestAccount());
		}

		private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
		{
			
			var filtered = MainWindow.Patients.Where(patient => (patient.FirstName.ToUpper().Contains(searchTextBox.Text.ToUpper()) || patient.LastName.ToUpper().Contains(searchTextBox.Text.ToUpper()) || patient.PersonalID.ToUpper().Contains(searchTextBox.Text.ToUpper())));


			PatientsDataGrid.ItemsSource = filtered;
		}

		private void PatientsDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			selectedpatient = PatientsDataGrid.SelectedItem as Model.Users.Patient;
			this.NavigationService.Navigate(new scheduleAppointment());
		}

	/*	private void PatientsDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
		{
			if (e.Column.Header.ToString() == "FirstName")
			{
				e.Column.Header = "Ime pacijenta";

			}
			if (e.Column.Header.ToString() == "LastName")
			{
				e.Column.Header = "Prezime pacijenta";

			}
			if (e.Column.Header.ToString() == "PersonalID")
			{
				e.Column.Header = "JMBG pacijenta";

			}
			if (e.Column.Header.ToString() == "DateOfBirth")
			{
				e.Column.Header = "Vrsta naloga";


			}
		}*/

		private void SearchPatientGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{
			PatientsDataGrid.UnselectAllCells();
		}
	}
}
