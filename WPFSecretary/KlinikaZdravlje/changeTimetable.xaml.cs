﻿using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for otkazivanjeTermina.xaml
    /// </summary>
    public partial class changeTimetable : Page
    {
		//List<Pregled> termini = new List<Pregled>();
		public static Model.Appointment.Appointment SelectedAppointment = null;
		public static DateTime selectedDate;
		public List<Model.Appointment.Appointment> appointmentsByDate = null;
		public bool ucitano = false;
		public  List<Model.Appointment.Appointment> scheduleAppointment = new List<Model.Appointment.Appointment>();
		public changeTimetable()
      {
			InitializeComponent();
			this.dateTime.BlackoutDates.AddDatesInPast();
			ucitano = true;
			selectedDate = dateTime.SelectedDate.Value.Date;
			appointmentsByDate = MainWindow.appointmentController.GetAppointmentByDate(selectedDate);
			AppointmentDataGrid.ItemsSource = appointmentsByDate;
			
		}


		public class Pregled
		{
			public string Datum { get; set; }
			public string Vreme { get; set; }
			public string Lekar { get; set; }
			public string Soba { get; set; }
			public string Pacijent { get; set; }
			public AppointmentType TipPregleda { get; set; }

		}

		public enum AppointmentType
		{
			OPERACIJA,
			REGULARAN,
			KONTORLA
		}

		
		private void YasButton_Click(object sender, RoutedEventArgs e)
		{
			Model.Appointment.Appointment a = this.AppointmentDataGrid.SelectedItem as Model.Appointment.Appointment;
			//MainWindow.appointments.Remove(a);

				MainWindow.appointmentController.Delete(a);
				if (checkBoxDoctor.IsChecked == true)
				{
					String message = "Termin pacijenta " + a.Patient.FirstName + " " + a.Patient.LastName + "(" + a.StartDate + ")" + " je otkazan";
					MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, a.Doctor));
				}
				if (checkBoxPatient.IsChecked == true)
				{
					String message = "Termin pacijenta " + a.Patient.FirstName + " " + a.Patient.LastName + "(" + a.StartDate + ")" + " je otkazan";
					MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, a.Patient));
				}
				this.NavigationService.Navigate(new changeTimetable());



			
		}

		private void NoButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed; 
		}

		private void CancelAppointmentButton_Click(object sender, RoutedEventArgs e)
		{

			BorderMessage.Visibility = Visibility.Visible;
			CancelAppointmentButton.IsEnabled = false;
			moveAppointmentButton.IsEnabled = false;
			AppointmentDataGrid.IsEnabled = false;

		}

		private void MoveAppointmentButton_Click(object sender, RoutedEventArgs e)
		{
			SelectedAppointment = this.AppointmentDataGrid.SelectedItem as Model.Appointment.Appointment;
			this.NavigationService.Navigate(new MoveAppointment());
		}

		private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
		{
			var filtered = MainWindow.appointments.Where(patient => (patient.Doctor.FirstName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.LastName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Patient.FirstName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Patient.LastName.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.MedicalRoom.Name.ToUpper().Contains(SearchTextBox.Text.ToUpper()) || patient.Doctor.MedicalRoom.Type.ToString().ToUpper().Contains(SearchTextBox.Text.ToUpper())));


			AppointmentDataGrid.ItemsSource = filtered;
		}

		private void AppointmentDataGrid_MouseDown(object sender, MouseButtonEventArgs e)
		{

			
		
			
			SelectedAppointment = this.AppointmentDataGrid.SelectedItem as Model.Appointment.Appointment;
			if (SelectedAppointment != null)
			{
				if (DateTime.Compare(SelectedAppointment.StartDate, DateTime.Now) >= 0)
				{
					CancelAppointmentButton.Visibility = Visibility.Visible;
					moveAppointmentButton.Visibility = Visibility.Visible;
					CancelAppointmentInfo s = new CancelAppointmentInfo();
					s.nazivLekara.Text = SelectedAppointment.Doctor.FirstName + " " + SelectedAppointment.Doctor.LastName;
					s.datum.Text = SelectedAppointment.StartDate.ToString("dd.MM.yyyy");
					s.Vreme.Text = SelectedAppointment.StartDate.ToString("HH:mm");
					s.Soba.Text = SelectedAppointment.Room.Name;
					s.vrstaSobe.Text = SelectedAppointment.Room.Type.ToString();
					s.nazivPacijenta.Text = SelectedAppointment.Patient.FirstName + " " + SelectedAppointment.Patient.LastName;
					s.JMBG.Text = SelectedAppointment.Patient.PersonalID.ToString();
					if (SelectedAppointment.Doctor.MedicalRoom.Type.Equals(TypeOfMedicalRoom.ZA_PREGLED))
					{
						s.vrstaPregleda.Text = "PREGLED";
					}
					else
					{
						s.vrstaPregleda.Text = "OPERACIJA";
					}
					s.BrojKartona.Text = SelectedAppointment.Patient.MedicalRecord.ToString();
					AppointmentInfoFrame.NavigationService.Navigate(s);
				}
				else
				{
					MessageBox.Show("Termin je prosao");
				}
		
			}
			
		}

		private void AppointmentDataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
		{

			if (e.Column.Header.ToString() == "Datum")
			{
				e.Column.Header = "Datum";

			}
			if (e.Column.Header.ToString() == "Vreme")
			{
				e.Column.Header = "vreme";

			}
			if (e.Column.Header.ToString() == "Lekar")
			{
				e.Column.Header = "Lekar";

			}
			if (e.Column.Header.ToString() == "Soba")
			{
				e.Column.Header = "Soba";


			}
		}

		private void CancleAppButton_Click(object sender, RoutedEventArgs e)
		{
			if(SelectedAppointment != null)
			{
				//MainWindow.appointments.Remove(SelectedAppointment);
				MainWindow.appointmentController.Delete(SelectedAppointment);

				if (checkBoxDoctor.IsChecked == true)
				{
					String message = "Termin pacijenta " + SelectedAppointment.Patient.FirstName + " " + SelectedAppointment.Patient.LastName + "(" + SelectedAppointment.StartDate + ")" + " je otkazan";
					MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, SelectedAppointment.Doctor));
				}
				if (checkBoxPatient.IsChecked == true)
				{
					String message = "Termin pacijenta " + SelectedAppointment.Patient.FirstName + " " + SelectedAppointment.Patient.LastName + "(" + SelectedAppointment.StartDate + ")" + " je otkazan";
					MainWindow.notificationController.Create(new Model.Other.Notification(message, DateTime.Now, SelectedAppointment.Patient));
				}
				//appointmentsByDate.Remove(SelectedAppointment);
				BorderMessage.Visibility = Visibility.Collapsed;
				CancelAppointmentButton.IsEnabled = true;
				moveAppointmentButton.IsEnabled = true;
				AppointmentDataGrid.IsEnabled = true;
				AppointmentDataGrid.Items.Refresh();
				this.NavigationService.Navigate(new changeTimetable());


			}

		

		}

		private void CancleButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			CancelAppointmentButton.IsEnabled = true;
			moveAppointmentButton.IsEnabled = true;
			AppointmentDataGrid.IsEnabled = true;

		}

		private void CloseImg_MouseDown(object sender, MouseButtonEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			CancelAppointmentButton.IsEnabled = true;
			moveAppointmentButton.IsEnabled = true;
			AppointmentDataGrid.IsEnabled = true;
		}



		public List<Appointment> filterAllAppointmentsByDate(DateTime dateTime) //daj mi sve zauzete termine za izabrani datum
		{
			List<Appointment> appointmentsByDate = new List<Appointment>();
			foreach (Appointment appointment in MainWindow.appointments)
			{
				if (appointment.StartDateTime.Date.Equals(dateTime))
				{
					appointmentsByDate.Add(appointment);

				}
			}

			return appointmentsByDate;
		}

		private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
		{
			if (ucitano)
			{

				selectedDate = dateTime.SelectedDate.Value.Date;
				appointmentsByDate = MainWindow.appointmentController.GetAppointmentByDate(selectedDate); //zakazani termini za izabrani datum
				AppointmentDataGrid.ItemsSource = appointmentsByDate;


			}
		}
	}
}
