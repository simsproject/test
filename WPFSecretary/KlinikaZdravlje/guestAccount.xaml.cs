﻿using KlinikaZdravlje.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for guestNalog.xaml
	/// </summary>
	public partial class guestAccount : Page
	{
		public bool potvrdi;
		public guestAccount()
		{
			InitializeComponent();
			potvrdi = false;
		}

		private void CancleButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new SearchPatient());
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			potvrdi = true;
			bool isFirstName = false;
			bool isLastName = false;
			bool isPersonalID = false;

			if (FirstName.Text == "")
			{
				FirstName.BorderBrush = Brushes.Red;
				FirstName.ToolTip = "Polje mora biti popunjeno";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 2);
				isFirstName = false;
				iconFirstName.Visibility = Visibility.Visible;


			}
			else
			{
				FirstName.BorderBrush = Brushes.Gray;
				FirstName.ToolTip = "Unesite ime pacijenta";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 1);
				isFirstName = true;
				iconFirstName.Visibility = Visibility.Collapsed;
			}

			if (LastName.Text == "")
			{
				LastName.BorderBrush = Brushes.Red;
				LastName.ToolTip = "Polje mora biti popunjeno";
				LastName.BorderThickness = new Thickness(0, 0, 0, 2);
				isLastName = false;
				iconLastName.Visibility = Visibility.Visible;

			}
			else
			{
				LastName.BorderBrush = Brushes.Gray;
				LastName.ToolTip = "Unesite prezime pacijenta";
				LastName.BorderThickness = new Thickness(0, 0, 0, 1);
				isLastName = true;
				iconLastName.Visibility = Visibility.Collapsed;



			}

			if (PersonalID.Text == "")
			{
				PersonalID.BorderBrush = Brushes.Red;
				PersonalID.ToolTip = "Polje mora biti popunjeno";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 2);
				isPersonalID = false;
				iconPersonalID.Visibility = Visibility.Visible;


			}
			else
			{
				PersonalID.BorderBrush = Brushes.Gray;
				PersonalID.ToolTip = "Unesite JMBG pacijenta";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 1);
				isPersonalID = true;
				iconPersonalID.Visibility = Visibility.Collapsed;



			}

			bool okRegex = true;
			Regex PhoneNumberRegex = new Regex("[0-9]{3}/[0-9]{3,4}-[0-9]{3}");
			Regex JMBGregex = new Regex("[0-9]{13}");


			if (isPersonalID)
			{
				if (JMBGregex.IsMatch(PersonalID.Text))
				{
					PersonalID.BorderBrush = Brushes.Gray;
					PersonalID.BorderThickness = new Thickness(0, 0, 0, 1);
					iconPersonalID.Visibility = Visibility.Collapsed;
					iconPersonalID.ToolTip = "";
				}
				else
				{
					PersonalID.BorderBrush = Brushes.Red;
					PersonalID.BorderThickness = new Thickness(0, 0, 0, 2);
					okRegex = false;
					iconPersonalID.Visibility = Visibility.Visible;
					iconPersonalID.ToolTip = "Neispravan format JMBG-a. Mora sadrazti 13 karaktera.";
				}

			}






			if (isFirstName && isLastName && isPersonalID)
			{
				if (okRegex)
				{

					foreach (Patient p in MainWindow.Patients)
					{
						if (p.PersonalID.Equals(PersonalID.Text))
						{
							labela.Content = "Pacijent vec ima nalog";
							break;
						}
						else
						{
							labela.Content = "";
						}
					}
					BorderMessage.Visibility = Visibility.Visible;
					OkButton.IsEnabled = false;
					CancleButton.IsEnabled = false;


				}
			
			}

		}

		private void FirstName_LostFocus(object sender, RoutedEventArgs e)
		{
			if (FirstName.Text != "")
			{
				FirstName.BorderBrush = Brushes.Gray;
				FirstName.ToolTip = "Unesite ime pacijenta";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{
				FirstName.BorderBrush = Brushes.Red;
				FirstName.ToolTip = "Polje mora biti popunjeno";
				FirstName.BorderThickness = new Thickness(0, 0, 0, 2);

			}
		}

		private void LastName_LostFocus(object sender, RoutedEventArgs e)
		{
			if (LastName.Text != "")
			{

				LastName.BorderBrush = Brushes.Gray;
				LastName.ToolTip = "Unesite prezime pacijenta";
				LastName.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{
				LastName.BorderBrush = Brushes.Red;
				LastName.ToolTip = "Polje mora biti popunjeno";
				LastName.BorderThickness = new Thickness(0, 0, 0, 2);

			}
		}

		private void PersonalID_LostFocus(object sender, RoutedEventArgs e)
		{
			if (PersonalID.Text != "")
			{
				PersonalID.BorderBrush = Brushes.Gray;
				PersonalID.ToolTip = "Unesite JMBG pacijenta";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 1);
			}
			else
			{

				PersonalID.BorderBrush = Brushes.Red;
				PersonalID.ToolTip = "Polje mora biti popunjeno";
				PersonalID.BorderThickness = new Thickness(0, 0, 0, 2);
			}


		}

		private void RegisterButton_Click(object sender, RoutedEventArgs e)
		{

			//MainWindow.Patients.Add(new Model.Users.Patient() { FirstName = FirstName.Text, LastName = LastName.Text, PersonalID = PersonalID.Text,GuestAccount = true});
			MainWindow.patientController.Create(new Model.Users.Patient(FirstName.Text, LastName.Text, dateOfBirth.SelectedDate.Value.Date, PersonalID.Text, Model.Users.Gender.Male, "", "", null, "", "", true, MainWindow.medicalRecordController.Create(new Model.MedicalRecords.MedicalRecord(0, new List<Model.MedicalRecords.Allergens>()))));
			this.NavigationService.Navigate(new SearchPatient());
		}

		private void CancleButton_Click_1(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			OkButton.IsEnabled = true;
			CancleButton.IsEnabled = true;
		}

		private void CloseImg_MouseDown(object sender, MouseButtonEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			OkButton.IsEnabled = true;
			CancleButton.IsEnabled = true;
		}

		private void FirstName_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (FirstName.Text == "")
				{
					iconFirstName.Visibility = Visibility.Visible;
				}
				else
				{
					iconFirstName.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void LastName_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (LastName.Text == "")
				{
					iconLastName.Visibility = Visibility.Visible;
				}
				else
				{
					iconLastName.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void PersonalID_KeyUp(object sender, KeyEventArgs e)
		{
			if (potvrdi == true)
			{
				if (PersonalID.Text == "")
				{
					iconPersonalID.Visibility = Visibility.Visible;
				}
				else
				{
					iconPersonalID.Visibility = Visibility.Collapsed;
				}
			}
		}

		private void FirstName_KeyDown(object sender, KeyEventArgs e)
		{
			Regex r = new Regex(@"(?i)^[a-z’'()/.,\s-]+$");
			if (!r.IsMatch(e.Key.ToString()))
			{
				e.Handled = true;
			}
		}

		private void LastName_KeyDown(object sender, KeyEventArgs e)
		{
			Regex r = new Regex(@"(?i)^[a-z’'()/.,\s-]+$");
			if (!r.IsMatch(e.Key.ToString()))
			{
				e.Handled = true;
			}
		}

		private void DateOfBirth_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
		{

		}
	}
}
