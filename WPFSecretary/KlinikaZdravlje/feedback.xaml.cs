﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
	/// <summary>
	/// Interaction logic for feedback.xaml
	/// </summary>
	public partial class feedback : Page
	{
		public feedback()
		{
			InitializeComponent();
		}

		private void RateButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Visible;
			cancleButton.IsEnabled = false;
			rateButton.IsEnabled = false;
		}

		private void CancleButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new HomePage());
		
		}

		private void YasButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			cancleButton.IsEnabled = true;
			rateButton.IsEnabled = true;
		}

		private void NoButton_Click(object sender, RoutedEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			cancleButton.IsEnabled = true;
			rateButton.IsEnabled = true;
		}

		private void CloseImg_MouseDown(object sender, MouseButtonEventArgs e)
		{
			BorderMessage.Visibility = Visibility.Collapsed;
			cancleButton.IsEnabled = true;
			rateButton.IsEnabled = true;
		}
	}
}
