﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for Pocetna_strana.xaml
    /// </summary>
    /// 
    public enum StanjePrikaza
    {
        POCETNA, OBAVJESTENJA
    }
    public partial class Pocetna_strana : Window
    {
        public int brojKlikovaNaTriTacke = 0;
        public int brojKlikovaNaUsera = 0;
        public Patient patient { get; set; }

        internal Patient Patient { get => patient; set => patient = value; }
        
        //public Patient patient;
        public StanjePrikaza stanjePrikaza;

        private List<Model2.Notification> getListNotificationForPatient(Patient patient)
        {
            List<Model2.Notification> retVal = new List<Model2.Notification>();
            Patient patient1 = App.patientController.GetByID(patient.PersonalID);
            foreach(Model2.Notification notificationIt in App.Notifications)
            {
                if (notificationIt.patient.PersonalID == patient.PersonalID) retVal.Add(notificationIt);
            }
            return retVal;
        }

        public Pocetna_strana(StanjePrikaza sp, Patient patient)
        {
            InitializeComponent();
            this.patient = patient;
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            listaBlogova.ItemsSource = App.Blogs;
            listaObavjestenja.ItemsSource = getListNotificationForPatient(patient);
            
            this.stanjePrikaza = sp;
            if(this.stanjePrikaza== StanjePrikaza.POCETNA) {
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;
                

                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;
                

                
                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;
                

                listaBlogova.Visibility = Visibility.Visible;
                NaslovZaPocetnu.Visibility = Visibility.Visible;
                listaObavjestenja.Visibility = Visibility.Hidden;
                NaslovZaObavjestenja.Visibility = Visibility.Hidden;
            }
            else
            {
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible; //
                

                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;//
                

               
                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;//
                

                listaBlogova.Visibility = Visibility.Hidden;
                NaslovZaPocetnu.Visibility = Visibility.Hidden;
                listaObavjestenja.Visibility = Visibility.Visible; 
                NaslovZaObavjestenja.Visibility = Visibility.Visible; 
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaTriTacke++;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;
                
            }
            if (brojKlikovaNaTriTacke % 2 == 0)
            {
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;
                
                
            }
            else
            {
                TriTackeMenu.Visibility = Visibility.Visible;
                TriTackeIkonica.Visibility = Visibility.Visible;
                
            }
        }

        private void logOutButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            //patient = null;
            win2.Show();
            this.Close();
        }

        private void changePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void feedbackButton_Click(object sender, RoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Acount_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaUsera++;
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;
                
            }
            if (brojKlikovaNaUsera % 2 == 0)
            {
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;
                
            }
            else
            {
                accountMenu.Visibility = Visibility.Visible;
                UserIkonica.Visibility = Visibility.Visible;
                
            }
        }

        private void userButton_Click(object sender, RoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void medicalRecord_Click(object sender, RoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Click(object sender, RoutedEventArgs e)
        {
            stanjePrikaza = StanjePrikaza.OBAVJESTENJA;
            if (accountMenu.Visibility == Visibility.Visible )
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;
                
            } 
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;
                
            }
            if (this.stanjePrikaza == StanjePrikaza.POCETNA)
            {
                listaBlogova.Visibility = Visibility.Visible;
                NaslovZaPocetnu.Visibility = Visibility.Visible;
                listaObavjestenja.Visibility = Visibility.Hidden;
                NaslovZaObavjestenja.Visibility = Visibility.Hidden;

               
                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;
                
            }
            else
            {
                listaBlogova.Visibility = Visibility.Hidden;
                NaslovZaPocetnu.Visibility = Visibility.Hidden;
                listaObavjestenja.Visibility = Visibility.Visible;
                NaslovZaObavjestenja.Visibility = Visibility.Visible;

                

                
                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;//
                
            }
        }

        private void HoomPage_Click(object sender, RoutedEventArgs e)
        {
            stanjePrikaza = StanjePrikaza.POCETNA;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;
                
            }
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;
                
            }
            if (this.stanjePrikaza == StanjePrikaza.POCETNA)
            {
                listaBlogova.Visibility = Visibility.Visible;
                NaslovZaPocetnu.Visibility = Visibility.Visible;
                listaObavjestenja.Visibility = Visibility.Hidden;
                NaslovZaObavjestenja.Visibility = Visibility.Hidden;

                
                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;
               
            }
            else
            {
                listaBlogova.Visibility = Visibility.Hidden;
                NaslovZaPocetnu.Visibility = Visibility.Hidden;
                listaObavjestenja.Visibility = Visibility.Visible;
                NaslovZaObavjestenja.Visibility = Visibility.Visible;

                
                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;//
               
            }
        }
        private void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void LogOut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangePassword_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Feedback_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void MedicalRecord_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangeInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangeInfoWindow win = new ChangeInfoWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void UserInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            stanjePrikaza = StanjePrikaza.OBAVJESTENJA;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
            if (this.stanjePrikaza == StanjePrikaza.POCETNA)
            {
                listaBlogova.Visibility = Visibility.Visible;
                NaslovZaPocetnu.Visibility = Visibility.Visible;
                listaObavjestenja.Visibility = Visibility.Hidden;
                NaslovZaObavjestenja.Visibility = Visibility.Hidden;


                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;

            }
            else
            {
                listaBlogova.Visibility = Visibility.Hidden;
                NaslovZaPocetnu.Visibility = Visibility.Hidden;
                listaObavjestenja.Visibility = Visibility.Visible;
                NaslovZaObavjestenja.Visibility = Visibility.Visible;




                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;//

            }
        }

        private void HomePage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            stanjePrikaza = StanjePrikaza.POCETNA;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
            if (this.stanjePrikaza == StanjePrikaza.POCETNA)
            {
                listaBlogova.Visibility = Visibility.Visible;
                NaslovZaPocetnu.Visibility = Visibility.Visible;
                listaObavjestenja.Visibility = Visibility.Hidden;
                NaslovZaObavjestenja.Visibility = Visibility.Hidden;


                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;

            }
            else
            {
                listaBlogova.Visibility = Visibility.Hidden;
                NaslovZaPocetnu.Visibility = Visibility.Hidden;
                listaObavjestenja.Visibility = Visibility.Visible;
                NaslovZaObavjestenja.Visibility = Visibility.Visible;


                HoomPageIkonica.Visibility = Visibility.Visible;
                NotificationIkonica.Visibility = Visibility.Visible;//

            }
        }

        private void AppoointmentView_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Appointment win = new Appointment(Patient); //dodati appointment umjesto null
            win.Show();
            this.Close();
        }

        private void AppointmentCreate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
        }

        private void AppointmentEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentRemove_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void QuestionnaireDoctor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireWindow win = new QuestionaireWindow(patient);
            win.Show();
            this.Close();
        }

        private void QuestionnaireClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireHospital win = new QuestionaireHospital(patient);
            win.Show();
            this.Close();
        }

        private void InfoDoctors_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoDoctor win = new InfoDoctor(patient);
            win.Show();
            this.Close();
        }

        private void InfoClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoClinic win = new InfoClinic(patient);
            win.Show();
            this.Close();
        }

        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            HelpWindow win = new HelpWindow();
            win.ShowDialog();
        }

        private void Shortcut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShortcutWindow win = new ShortcutWindow();
            win.ShowDialog();
        }

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            AbourWindow win = new AbourWindow();
            win.ShowDialog();
        }
       
    }
}
