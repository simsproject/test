﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for QuestionaireWindow.xaml
    /// </summary>
    public partial class QuestionaireWindow : Window
    {
        public int brojKlikovaNaUsera = 0;
        public int brojKlikovaNaTriTacke = 0;
        public Patient patient;
        public Doctor doctor;

        public QuestionaireWindow(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
            this.doctor = null;
            this.ljekar.ItemsSource = App.doctorController.GetAll().ToList(); 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaTriTacke++;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaTriTacke % 2 == 0)
            {
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;


            }
            else
            {
                TriTackeMenu.Visibility = Visibility.Visible;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
        }

        private void logOutButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            //patient = null;
            win2.Show();
            this.Close();
        }

        private void changePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void feedbackButton_Click(object sender, RoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Acount_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaUsera++;
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaUsera % 2 == 0)
            {
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            else
            {
                accountMenu.Visibility = Visibility.Visible;
                UserIkonica.Visibility = Visibility.Visible;

            }
        }

        private void userButton_Click(object sender, RoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void medicalRecord_Click(object sender, RoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, patient);
            win.Show();
            this.Close();
        }

        private void HoomPage_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }


        private void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void LogOut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangePassword_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Feedback_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void MedicalRecord_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangeInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangeInfoWindow win = new ChangeInfoWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void UserInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, patient);
            win.Show();
            this.Close();
        }

        private void HomePage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }

        private void AppoointmentView_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Appointment win = new Appointment(patient); //dodati appointment umjesto null
            win.Show();
            this.Close();
        }

        private void AppointmentCreate_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentRemove_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void QuestionnaireDoctor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            /*QuestionaireWindow win = new QuestionaireWindow();
            win.Show();
            this.Close(); */
        }

        private void QuestionnaireClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireHospital win = new QuestionaireHospital(patient);
            win.Show();
            this.Close();
        }

        private void InfoDoctors_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoDoctor win = new InfoDoctor(patient);
            win.Show();
            this.Close();
        }

        private void InfoClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoClinic win = new InfoClinic(patient);
            win.Show();
            this.Close();
        }

        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            HelpWindow win = new HelpWindow();
            win.ShowDialog();
        }

        private void Shortcut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShortcutWindow win = new ShortcutWindow();
            win.ShowDialog();
        }

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            AbourWindow win = new AbourWindow();
            win.ShowDialog();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }

        private void buttonSubmit_Click(object sender, RoutedEventArgs e)
        {
            
            if (this.doctor != null)
            {
                Questionnaire questionnaire = new Questionnaire(this.napomena.Text, "ANKETA ZA LJEKARA", 0, this.patient, this.doctor);

                String answer = "1";
                if (this.funkcionalnosti1.IsChecked == true) answer = "1";
                else if (this.funkcionalnosti2.IsChecked == true) answer = "2";
                else if (this.funkcionalnosti3.IsChecked == true) answer = "3";
                else if (this.funkcionalnosti4.IsChecked == true) answer = "4";
                else answer = "5";
                
                Question question1 = new Question("1", answer, questionnaire, 0);
                if (this.izgled1.IsChecked == true) answer = "1";
                else if (this.izgled2.IsChecked == true) answer = "2";
                else if (this.izgled3.IsChecked == true) answer = "3";
                else if (this.izgled4.IsChecked == true) answer = "4";
                else answer = "5";
                
                Question question2 = new Question("2", answer, questionnaire, 0);
                App.questionnaireController.Create(questionnaire);
                App.questionController.Create(question1);
                App.questionController.Create(question2);
                MessageBox.Show("Uspjesno ste popunili anketu o doktoru dr. "+doctor.FirstName+" "+ doctor.LastName);
                
            }
            else
            {
                MessageBox.Show("Niste izabrali doktora");
            }
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }

        private void ljekar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Doctor selectedDoctor;
            selectedDoctor = ljekar.SelectedItem as Doctor;


                //MessageBox.Show(selectedDoctor.FirstName + " " + selectedDoctor.LastName + " " + selectedDoctor.PersonalID + " " + selectedDoctor.Title.ToString());
                if (selectedDoctor.Title == Model.Users.Title.DoctorOfGeneralPractice)
                {
                    this.doctor = App.doctorController.GetByID(selectedDoctor.PersonalID);
                    
                }
                else
                {
                    this.doctor = App.doctorController.GetByID((ljekar.SelectedItem as Doctor).PersonalID);
                    
                }
            
        }
    }
}
