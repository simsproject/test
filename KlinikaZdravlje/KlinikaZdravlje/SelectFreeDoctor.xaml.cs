﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for SelectFreeDoctor.xaml
    /// </summary>
    public partial class SelectFreeDoctor : Window
    {
        public Model2.WorkingTime workingTime;
        public Patient patient;
        /*
        private List<Model.Appointment> getAllAppointmentForDoctorForDate(Model.Doctor doctor,DateTime dateTime)
        {
            List<Model.Appointment> retval = new List<Model.Appointment>();
            foreach(Model.Appointment appointmentIt in App.Appointments)
            {
                if (appointmentIt.begin.Year==dateTime.Year && appointmentIt.begin.Month == dateTime.Month && appointmentIt.begin.Day == dateTime.Day)
                {
                    if (appointmentIt.doctor.JMBG == doctor.JMBG) retval.Add(appointmentIt);
                }
            }
            return retval;
        }

        private bool isFreeDoctor(Model.Doctor doctor, WorkingTime workingTime)
        {
            List<Model.Appointment> appointments = getAllAppointmentForDoctorForDate(doctor, workingTime.odSati);
            foreach(Model.Appointment appointment in appointments)
            {
                if (appointment.begin == workingTime.odSati || appointment.end == workingTime.doSati) return false;
            }
            return true;
        }

        private List<Model.Doctor> getAllFreeDoctorOfGeneralPractice(WorkingTime workingTime)
        {
            List<Model.Doctor> doctorsRetVal = new List<Model.Doctor>();
            foreach (Model.Doctor doctor in App.Doctors) 
            {

                WorkingTime workingTimeForDoctor = new WorkingTime(new DateTime(workingTime.odSati.Year, workingTime.odSati.Month, workingTime.odSati.Day, doctor.radnoVrijeme.odSati.Hour, doctor.radnoVrijeme.odSati.Minute, doctor.radnoVrijeme.odSati.Second),
                    new DateTime(workingTime.doSati.Year, workingTime.doSati.Month, workingTime.doSati.Day, doctor.radnoVrijeme.doSati.Hour, doctor.radnoVrijeme.doSati.Minute, doctor.radnoVrijeme.doSati.Second));
                if (doctor.titula == Model.Title.SPECIJALISTA) continue;
                if (workingTimeForDoctor.odSati > workingTime.odSati || workingTimeForDoctor.doSati < workingTime.doSati) continue;
                if (isFreeDoctor(doctor, workingTime))
                {
                    doctorsRetVal.Add(doctor);  
                }
            }
            return doctorsRetVal;
        }*/

        public SelectFreeDoctor(Patient patient,Model2.WorkingTime workingTime,List<Doctor> listOfFreeDoctor)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
            this.listaSlobodnihLjekara.ItemsSource = listOfFreeDoctor;
            this.workingTime = workingTime;
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {

            Doctor selectedDoctor = listaSlobodnihLjekara.SelectedItem as Doctor;
            if (selectedDoctor == null)
            {
                MessageBox.Show("Nije selektovan doktor!");
                return;
            }
            Model.Appointment.Appointment appointment = new Model.Appointment.Appointment(workingTime.odSati, workingTime.doSati, Model.Appointment.AppointmentType.regularAp, 0, this.patient, selectedDoctor, selectedDoctor.MedicalRoom);
            AppointmentAccept win = new AppointmentAccept(appointment); //umjesto null dodati appointment
            win.ShowDialog();
            if (AppointmentAccept.terminZakazan) this.Close();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
