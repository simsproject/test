﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for Recipe.xaml
    /// </summary>
    public partial class Recipe : Window
    {
        public Model.MedicalRecords.Recipe recipe;

        public Recipe(Model.MedicalRecords.Recipe recipe)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            Patient patient = App.patientController.GetAll().Where(Patient => Patient.MedicalRecord.SerialID == recipe.MedicalRecord.SerialID).FirstOrDefault();

            this.recipe = recipe;
            this.datumPrepisivanja.Content = String.Format("{0:d}", recipe.DateOfCreate);
            this.detaljnije.Text = this.recipe.Description;
            this.kolicinaLijeka.Content =this.recipe.Amount;
            this.ljekar.Content ="dr. "+this.recipe.Doctor.FirstName+" "+ this.recipe.Doctor.LastName;
            this.pacijent.Content = patient.FirstName+ " " + patient.LastName;
            this.sifraLijeka.Content = recipe.DrugID;
        }
    }
}
