﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for AppointmentAccept.xaml
    /// </summary>
    public partial class AppointmentAccept : Window
    {
        public static bool terminZakazan { get; set; }
        public Model.Appointment.Appointment appointment { get; set; }
        public AppointmentAccept(Model.Appointment.Appointment appointment)
        {
            InitializeComponent();
            this.appointment = appointment;
            if (this.appointment != null)
            {
                this.ljekarKodKojegSeOdrzavaTermin.Content = "dr. " + this.appointment.Doctor.FirstName + " " + this.appointment.Doctor.LastName;
                this.prostorijaOdrzavanjaTermina.Content = this.appointment.Room.Name;
                this.vrijemePocetkaTermin.Content = String.Format("{0:g}", this.appointment.StartDate);
                this.vrijemeZavrsetkaTermina.Content = String.Format("{0:g}", this.appointment.FinishDate);
                terminZakazan = false;
            }
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
           
        }

        private void dugmeZaPotvrduTermina_Click(object sender, RoutedEventArgs e)
        {
            terminZakazan = true;
            if (this.appointment != null)
            {
                App.appointmentController.Create(this.appointment);
                MessageBox.Show("Zakazan je termin");
            }
            this.Close();
        }

        private void dugmeZaNazad_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
