﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for SelectOpenAppointment.xaml
    /// </summary>
    public partial class SelectOpenAppointment : Window
    {
        public Doctor doctor;
        public Patient patient { get; set; }

        public Model.Appointment.Appointment term;
        public Model2.WorkingTime workingTime { get; set; }

        public Model2.WorkingTime workingTimeSelected;

        public SelectOpenAppointment(Patient patient,Doctor doctor)
        {
            InitializeComponent(); 
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
            this.doctor = doctor;

            this.buttonIzmjeni.Visibility = Visibility.Hidden;
            this.buttonNext.Visibility = Visibility.Visible;

        }

        public SelectOpenAppointment(Model.Appointment.Appointment appointment)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = appointment.Patient;
            this.doctor = appointment.Doctor;
            this.workingTimeSelected = new Model2.WorkingTime(appointment.StartDate, appointment.FinishDate);
            this.datumTermina.SelectedDate = appointment.StartDate.Date;
            this.term = appointment;
            this.pitanje.Text = appointment.StartDate.ToString("dd.MM.yyyy. HH:mm") + " - " + appointment.FinishDate.ToString("dd.MM.yyyy. HH:mm")+" - dr. "+appointment.Doctor.FirstName+" "+ appointment.Doctor.LastName;

            this.buttonIzmjeni.Visibility = Visibility.Visible;
            this.buttonNext.Visibility = Visibility.Hidden;

            List<Model2.WorkingTime> workingTimes = new List<Model2.WorkingTime>();
            DateTime dateTime = appointment.StartDate.Date;
            WorkingTime workingTime = App.workingTimeController.GetWorkingTimeForDoctorByDate(this.doctor, dateTime);
            if (workingTime != null)
            {
                Model2.WorkingTime workingTime1 = new Model2.WorkingTime(new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.BeginTime.Hour, workingTime.BeginTime.Minute, workingTime.BeginTime.Second),
                    new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.EndTime.Hour, workingTime.EndTime.Minute, workingTime.EndTime.Second));

                workingTimes = workingTimesForDay(workingTime1);
                List<Model.Appointment.Appointment> appointments = App.appointmentController.GetAppointmentsForDoctorByDate(doctor, workingTime.Date);

                removeAllScheduleWorkingTime(appointments, workingTimes);
            }
            listaSaSlobodnimTerminima.ItemsSource = workingTimes;

        }


        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            Model2.WorkingTime workingTimeSelected = listaSaSlobodnimTerminima.SelectedItem as Model2.WorkingTime;
            if (workingTimeSelected == null)
            {
                System.Windows.MessageBox.Show("Nije izabran valjan termin!");
                return;
            }
            Model.Appointment.Appointment appointment = new Model.Appointment.Appointment(workingTimeSelected.odSati, workingTimeSelected.doSati, Model.Appointment.AppointmentType.regularAp,0, this.patient, this.doctor, this.doctor.MedicalRoom);
            AppointmentAccept win = new AppointmentAccept(appointment); // kad se uradi dodati appointment umjesto null
            win.ShowDialog();
            if (AppointmentAccept.terminZakazan) this.Close();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void datumTermina_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            List<Model2.WorkingTime> workingTimes = new List<Model2.WorkingTime>();
            DateTime dateTime = this.datumTermina.SelectedDate.GetValueOrDefault();
            if (dateTime==null || DateTime.Compare(dateTime, DateTime.Now.Date.AddDays(1))<=0)
            {
                System.Windows.MessageBox.Show("Nije moguce izabrati taj datum!");
                return;
            }
            WorkingTime workingTime = App.workingTimeController.GetWorkingTimeForDoctorByDate(this.doctor,dateTime);
            if (workingTime != null)
            {
                Model2.WorkingTime workingTime1 = new Model2.WorkingTime(new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.BeginTime.Hour, workingTime.BeginTime.Minute, workingTime.BeginTime.Second),
                    new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.EndTime.Hour, workingTime.EndTime.Minute, workingTime.EndTime.Second));

                workingTimes = workingTimesForDay(workingTime1);
                List<Model.Appointment.Appointment> appointments = App.appointmentController.GetAppointmentsForDoctorByDate(doctor, workingTime.Date);

                removeAllScheduleWorkingTime(appointments, workingTimes);
            }
            listaSaSlobodnimTerminima.ItemsSource = workingTimes;
                
        }

        void removeAllScheduleWorkingTime(List<Model.Appointment.Appointment> appointmentsForDoctor, List<Model2.WorkingTime> returnValue)
        {
            Model2.WorkingTime helpVariable;
            foreach (Model.Appointment.Appointment appointment in appointmentsForDoctor)
            {
                helpVariable = new Model2.WorkingTime(appointment.StartDate, appointment.FinishDate);
                if (DateTime.Compare(helpVariable.odSati, appointment.StartDate) == 0 && DateTime.Compare(helpVariable.doSati, appointment.FinishDate) == 0) removeFromList(returnValue, helpVariable);


            }
        }

        bool removeFromList(List<Model2.WorkingTime> listOfWorkingTimesForDay, Model2.WorkingTime workingTimeWhoNeedRemove)
        {
            foreach (Model2.WorkingTime workingTimeForDay in listOfWorkingTimesForDay)
            {
                if (workingTimeWhoNeedRemove.odSati == workingTimeForDay.odSati && workingTimeWhoNeedRemove.doSati == workingTimeForDay.doSati) return listOfWorkingTimesForDay.Remove(workingTimeForDay);
            }
            return false;
        }

        List<Model2.WorkingTime> workingTimesForDay(Model2.WorkingTime workingTime)
        { // vraca mi listu potencijalnih termina za odredjeni datum --
            List<Model2.WorkingTime> returnValue = new List<Model2.WorkingTime>();
            DateTime begin = workingTime.odSati;
            DateTime end = workingTime.doSati;
            for (DateTime date = begin; date < end; date = date.AddMinutes(15))
            {
                Model2.WorkingTime helpVariable = new Model2.WorkingTime(date, date.AddMinutes(15));
                returnValue.Add(helpVariable);
                helpVariable = null;
            }
            return returnValue;

        }

        private void buttonIzmjeni_Click(object sender, RoutedEventArgs e)
        {
            Model2.WorkingTime workingTimeSelected = listaSaSlobodnimTerminima.SelectedItem as Model2.WorkingTime;
            term.StartDate = workingTimeSelected.odSati;
            term.FinishDate = workingTimeSelected.doSati;
            App.appointmentController.Update(term);
            System.Windows.MessageBox.Show("Uspjesno ste pomjerili termin!");
            this.Close();
        }
        /*
private List<Model.Appointment> getAllAppointmentForDoctorForDate(Model.Doctor doctor, DateTime dateTime)
{
List<Model.Appointment> retval = new List<Model.Appointment>();
foreach (Model.Appointment appointmentIt in App.Appointments)
{
if (appointmentIt.begin.Year == dateTime.Year && appointmentIt.begin.Month == dateTime.Month && appointmentIt.begin.Day == dateTime.Day)
{
  if (appointmentIt.doctor.JMBG == doctor.JMBG) retval.Add(appointmentIt);
}
}
return retval;
}

private bool isAvaibleAppointment(Model.Doctor doctor, WorkingTime workingTime)
{
WorkingTime workingTimeForDoctor = new WorkingTime(new DateTime(workingTime.odSati.Year, workingTime.odSati.Month, workingTime.odSati.Day, doctor.radnoVrijeme.odSati.Hour, doctor.radnoVrijeme.odSati.Minute, doctor.radnoVrijeme.odSati.Second),
  new DateTime(workingTime.doSati.Year, workingTime.doSati.Month, workingTime.doSati.Day, doctor.radnoVrijeme.doSati.Hour, doctor.radnoVrijeme.doSati.Minute, doctor.radnoVrijeme.doSati.Second));
foreach (Model.Appointment appointment in getAllAppointmentForDoctorForDate(doctor, workingTime.doSati.Date))
{
if (appointment.begin == workingTime.odSati || appointment.end == workingTime.doSati) return false;
if (workingTimeForDoctor.odSati>workingTime.odSati || workingTimeForDoctor.doSati < workingTime.doSati) continue;
return true;
}

return false;
}

private bool findWorkingTimeForDoctorInAppointment(Model.Doctor doctor, WorkingTime workingTime)
{
List<Model.Appointment> appointments = getAllAppointmentForDoctorForDate(doctor, workingTime.doSati.Date);
foreach(Model.Appointment appointment in appointments)
{
WorkingTime helpVar = new WorkingTime(appointment.begin, appointment.end);
if (helpVar.doSati.Hour == workingTime.doSati.Hour & helpVar.doSati.Minute == workingTime.doSati.Minute & helpVar.doSati.Second == workingTime.doSati.Second  & helpVar.odSati.Hour == workingTime.odSati.Hour & helpVar.odSati.Minute == workingTime.odSati.Minute & helpVar.odSati.Second == workingTime.odSati.Second)
{
  MessageBox.Show("Pronadjen");
  return true;
}
}
return false;
}

private void izbrisiIzListe(WorkingTime wt,List<WorkingTime> lwt)
{
foreach (WorkingTime wti in lwt)
{
if (wti.doSati.Hour == wt.doSati.Hour & wti.doSati.Minute == wt.doSati.Minute & wti.doSati.Second == wt.doSati.Second & wti.odSati.Hour == wt.odSati.Hour & wti.odSati.Minute == wt.odSati.Minute & wti.odSati.Second == wt.odSati.Second)
{
  lwt.Remove(wti);
  return;
}
}
}

private List<WorkingTime> getAllOpenWorkingTimesForDateAndDoctor(Model.Doctor doctor, DateTime date)
{
List<WorkingTime> retVal = new List<WorkingTime>();
foreach(WorkingTime wt in App.WorkingTimeForAppointment)
{
retVal.Add(wt);
}

foreach (WorkingTime workingTimeIt in App.WorkingTimeForAppointment)
{
WorkingTime workingTime = new WorkingTime(new DateTime(date.Year,date.Month,date.Day,workingTimeIt.odSati.Hour, workingTimeIt.odSati.Minute, workingTimeIt.odSati.Second), new DateTime(date.Year, date.Month, date.Day, workingTimeIt.doSati.Hour, workingTimeIt.doSati.Minute, workingTimeIt.doSati.Second));
WorkingTime workingTime1= new WorkingTime(new DateTime(workingTime.odSati.Year, workingTime.odSati.Month, workingTime.odSati.Day, doctor.radnoVrijeme.odSati.Hour, doctor.radnoVrijeme.odSati.Minute, doctor.radnoVrijeme.odSati.Second),
  new DateTime(workingTime.doSati.Year, workingTime.doSati.Month, workingTime.doSati.Day, doctor.radnoVrijeme.doSati.Hour, doctor.radnoVrijeme.doSati.Minute, doctor.radnoVrijeme.doSati.Second));

if (findWorkingTimeForDoctorInAppointment(doctor, workingTime)) izbrisiIzListe(workingTime,retVal);
if (workingTime.doSati>workingTime1.doSati || workingTime.odSati< workingTime1.odSati) izbrisiIzListe(workingTime, retVal);
}
return retVal;
}

private void datumTermina_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
{
List<WorkingTime> openAppointments = getAllOpenWorkingTimesForDateAndDoctor(this.doctor,this.datumTermina.SelectedDate.Value);
listaSaSlobodnimTerminima.ItemsSource = openAppointments;
}
*/
    }
}
