﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for ZdravstveniKarton.xaml
    /// </summary>
    public partial class ZdravstveniKarton : Window
    {
        public int brojKlikovaNaTriTacke = 0;
        public int brojKlikovaNaUsera = 0;
        public Patient patient;
        public MedicalRecord zkarton;
        public List<Model.MedicalRecords.Recipe> recipes;
        public List<SpecialistTreatment> specialistTreatments;
        public List<HospitalTreatment> hospitalTreatments;
        public List<Model.MedicalRecords.DoctorReport> doctorReports;
           
        private MedicalRecord getMedicalRecordForPatient(Patient patient)
        {
            doctorReports = App.doctorReportController.GetAllDoctorReportForPatient(patient.MedicalRecord);
            recipes = App.recipeController.GetAllRecipesForPatient(patient.MedicalRecord);
            hospitalTreatments = App.hospitalTreatmentController.GetAllHospitalTreatmentsForPatient(patient.MedicalRecord);
            specialistTreatments = App.specialistTreatmentController.GetAllSpecialistTreatmentsForPatient(patient.MedicalRecord);
            return patient.MedicalRecord;
        }

        public ZdravstveniKarton(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
            this.imePacijenta.Content = patient.FirstName;
            this.prezimePacijenta.Content = patient.LastName;
            this.zkarton = getMedicalRecordForPatient(patient);
            if (this.zkarton != null)
                this.brojKartona.Content = zkarton.SerialID;
            else
                this.brojKartona.Content = "Greska u trazenju kartona";
            
            this.doctorReportTable.ItemsSource = doctorReports;
            this.recipeTable.ItemsSource = recipes;
            this.hospitalTreatmentTable.ItemsSource = hospitalTreatments;
            this.specialistTreatmentTable.ItemsSource = specialistTreatments;
        }

        private void medicalReportButton_Click(object sender, RoutedEventArgs e)
        {

            specTreatmentBuuton.Background = Brushes.LightGray;
            recipeButton.Background = Brushes.LightGray;
            medicalReportButton.Background = Brushes.White;
            hospitalTreatmentButton.Background = Brushes.LightGray;

            specialistTreatmentTable.Visibility = Visibility.Hidden;
            recipeTable.Visibility = Visibility.Hidden;
            doctorReportTable.Visibility = Visibility.Visible;
            hospitalTreatmentTable.Visibility = Visibility.Hidden;
        }

        private void recipeButton_Click(object sender, RoutedEventArgs e)
        {
            

            specTreatmentBuuton.Background = Brushes.LightGray;
            recipeButton.Background = Brushes.White;
            medicalReportButton.Background = Brushes.LightGray;
            hospitalTreatmentButton.Background = Brushes.LightGray;

            specialistTreatmentTable.Visibility = Visibility.Hidden;
            recipeTable.Visibility = Visibility.Visible;
            doctorReportTable.Visibility = Visibility.Hidden;
            hospitalTreatmentTable.Visibility = Visibility.Hidden;
        }

        private void hospitalTreatmentButton_Click(object sender, RoutedEventArgs e)
        {
            
            

            hospitalTreatmentButton.Background = Brushes.White;
            recipeButton.Background = Brushes.LightGray;
            medicalReportButton.Background = Brushes.LightGray;
            specTreatmentBuuton.Background = Brushes.LightGray;

            specialistTreatmentTable.Visibility = Visibility.Hidden;
            recipeTable.Visibility = Visibility.Hidden;
            doctorReportTable.Visibility = Visibility.Hidden;
            hospitalTreatmentTable.Visibility = Visibility.Visible;
        }

        private void specTreatmentBuuton_Click(object sender, RoutedEventArgs e)
        {
            
            
            specTreatmentBuuton.Background = Brushes.White;
            recipeButton.Background = Brushes.LightGray;
            medicalReportButton.Background = Brushes.LightGray;
            hospitalTreatmentButton.Background = Brushes.LightGray;

            specialistTreatmentTable.Visibility = Visibility.Visible;
            recipeTable.Visibility = Visibility.Hidden;
            doctorReportTable.Visibility = Visibility.Hidden;
            hospitalTreatmentTable.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaTriTacke++;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaTriTacke % 2 == 0)
            {
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;


            }
            else
            {
                TriTackeMenu.Visibility = Visibility.Visible;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
        }

        private void logOutButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            //patient = null;
            win2.Show();
            this.Close();
        }

        private void changePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void feedbackButton_Click(object sender, RoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Acount_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaUsera++;
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaUsera % 2 == 0)
            {
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            else
            {
                accountMenu.Visibility = Visibility.Visible;
                UserIkonica.Visibility = Visibility.Visible;

            }
        }

        private void userButton_Click(object sender, RoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void medicalRecord_Click(object sender, RoutedEventArgs e)
        {
            /*ZdravstveniKarton win = new ZdravstveniKarton();
            win.Visibility = Visibility.Visible;
            this.Close();*/
        }

        private void Notification_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, patient);
            win.Show();
            this.Close();
        }

        private void HoomPage_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }


        private void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void LogOut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangePassword_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Feedback_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void MedicalRecord_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            /*ZdravstveniKarton win = new ZdravstveniKarton();
            win.Visibility = Visibility.Visible;
            this.Close();*/
        }

        private void ChangeInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangeInfoWindow win = new ChangeInfoWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void UserInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, patient);
            win.Show();
            this.Close();
        }

        private void HomePage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }

        private void AppoointmentView_Executed(object sender, ExecutedRoutedEventArgs e)
        {
             Appointment win = new Appointment(patient);
             win.Show();
             this.Close(); 
        }

        private void AppointmentCreate_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentRemove_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void QuestionnaireDoctor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireWindow win = new QuestionaireWindow(patient);
            win.Show();
            this.Close();
        }

        private void QuestionnaireClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireHospital win = new QuestionaireHospital(patient);
            win.Show();
            this.Close();
        }

        private void InfoDoctors_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoDoctor win = new InfoDoctor(patient);
            win.Show();
            this.Close();
        }

        private void InfoClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoClinic win = new InfoClinic(patient);
            win.Show();
            this.Close();
        }

        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            HelpWindow win = new HelpWindow();
            win.ShowDialog();
        }

        private void Shortcut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShortcutWindow win = new ShortcutWindow();
            win.ShowDialog();
        }

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            AbourWindow win = new AbourWindow();
            win.ShowDialog();
        }

        public Model.MedicalRecords.DoctorReport selectedDoctorReport { get; set; }
        public Model.MedicalRecords.Recipe selectedRecipe { get; set; }
        public SpecialistTreatment selectedSpecialistTreatment { get; set; }
        public HospitalTreatment selectedHospitalTreatment { get; set; }

        private void recipeTable_SelectionChanged(object sender, MouseButtonEventArgs e)
        {
            
            this.selectedRecipe = recipeTable.SelectedItem as Model.MedicalRecords.Recipe;
            Recipe win = new Recipe(this.selectedRecipe);
            win.ShowDialog();
        }

        private void hospitalTreatmentTable_SelectionChanged(object sender, MouseButtonEventArgs e)
        {
            
            this.selectedHospitalTreatment = hospitalTreatmentTable.SelectedItem as HospitalTreatment;
            HospitalTreatmentWindow win = new HospitalTreatmentWindow(this.selectedHospitalTreatment);
            win.ShowDialog();
        }

        private void specialistTreatmentTable_SelectionChanged(object sender, MouseButtonEventArgs e)
        {
          
            this.selectedSpecialistTreatment = specialistTreatmentTable.SelectedItem as SpecialistTreatment;
            SpecialistTreatmentWindow win = new SpecialistTreatmentWindow(this.selectedSpecialistTreatment);
            win.ShowDialog();
        }

        private void doctorReportTable_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            
            this.selectedDoctorReport = doctorReportTable.SelectedItem as Model.MedicalRecords.DoctorReport;
            DoctorReport win = new DoctorReport(this.selectedDoctorReport);
            win.ShowDialog();
        }


    }
}
