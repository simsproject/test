﻿
using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using Application = System.Windows.Application;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static List<Patient> patients;
        private static List<Address> addresses;
        private static List<Doctor> doctors;
        private static List<Model.MedicalRecords.MedicalRecord> medicalRecords;
        private static List<Model2.Blog> blogs;
        private static List<Model2.Notification> notifications;
        private static List<Model.Appointment.Appointment> appointments;
        private static List<Model2.WorkingTime> workingTimeForAppointment;
        private static List<Questionnaire> questionnaires;
        private static List<User> users;

        public static DoctorController doctorController;
        public static UserController userController;
        public static PatientController patientController;
        public static AdressController addressController;
        public static AppointmentController appointmentController;
        public static MedicalRecordController medicalRecordController;
        public static DoctorReportController doctorReportController;
        public static RecipeController recipeController;
        public static SpecialistTreatmentController specialistTreatmentController;
        public static HospitalTreatmentController hospitalTreatmentController;
        public static QuestionController questionController;
        public static QuestionnaireController questionnaireController;
        public static FeedbackController feedbackController;
        public static WorkingTimeController workingTimeController;
        public static WorkingPeriodController workingPeriodController;

        public App()
        {
            Patients = new List<Patient>();
            Addresses = new List<Address>();
            Doctors = new List<Doctor>();
            Questionnaires = new List<Questionnaire>();
            Notifications = new List<Model2.Notification>();
            Blogs = new List<Model2.Blog>();
            Appointments = new List<Model.Appointment.Appointment>();
            WorkingTimeForAppointment = new List<Model2.WorkingTime>();
            MedicalRecords = new List<Model.MedicalRecords.MedicalRecord>();
          
            patientController = PatientController.GetInstance();
            userController = UserController.GetInstance();
            

            doctorController = DoctorController.GetInstance();

            addressController = AdressController.GetInstance();
            appointmentController = AppointmentController.GetInstance();
            medicalRecordController = MedicalRecordController.GetInstance();
            doctorReportController = DoctorReportController.GetInstance();
            recipeController = RecipeController.GetInstance();

            specialistTreatmentController = SpecialistTreatmentController.GetInstance();
            hospitalTreatmentController = HospitalTreatmentController.GetInstance();
            questionController = QuestionController.GetInstance();
            questionnaireController = QuestionnaireController.GetInstance();
            feedbackController = FeedbackController.GetInstance();
            workingPeriodController = WorkingPeriodController.GetInstance();
            workingTimeController = WorkingTimeController.GetInstance();
            


            // Patients = patientController.GetAll().ToList();
            //Addresses=addressController.GetAll().ToList();
            //Doctors=doctorController.GetAll().ToList();


            /*
            Addresses.Add(new Address("Jasa Tomic", 12, "Novi Sad"));
            Addresses.Add(new Address("Mihajla Pupina", 21, "Beograd"));
            Addresses.Add(new Address("Cara Lazara", 13, "Nis"));
            Addresses.Add(new Address("Desanke Maksimovic", 31, "Pancevo"));
            Addresses.Add(new Address("Brace Ribnikar", 14, "Novi Sad"));
            Addresses.Add(new Address("Narodnog fronta", 41, "Beograd"));
            Addresses.Add(new Address("Branka Radicevica", 15, "Subotica"));
            Addresses.Add(new Address("Balzakova", 51, "Kragujevac"));
            Addresses.Add(new Address("Nikole Tesle", 1, "Kraljevo"));
            Addresses.Add(new Address("Lava Tolstoja", 10, "Novi Sad"));
            Addresses.Add(new Address("Dostojevskog", 2, "Indjija"));
            Addresses.Add(new Address("Branislava Nusica", 20, "Zrenjenin"));

            

            Patients.Add(new Patient("Marko", "Ostojic", 29, 6, 1998, "Bulevar cara Lazara", 9, "Novi Sad", "2906998187654", "marko24a@gmail.com", "065-899-844", "marko24a", "12345678",Pol.MUSKO,false));
            Patients.Add(new Patient("Slavisa", "Milakovic", 27, 7, 1998, "Bulevar cara Lazara", 9, "Novi Sad", "2707998998877", "slava31@gmail.com", "065-989-844", "slavaaa", "87654321", Pol.MUSKO,false));
            Patients.Add(new Patient("Nikola", "Stojcevic", 14, 7, 1998, "Pape Pavla", 12, "Beograd", "1407998187454", "stojkeba@gmail.com", "065-833-748", "stojke", "12345678", Pol.MUSKO,false));
            Patients.Add(new Patient("Tamara", "Velickovic", 2, 12, 1998, "Jasa Tomic", 64, "Beograd", "0212998112234", "velicko.tam@gmail.com", "065-433-5533", "tasa212", "12345678", Pol.ZENSKO,true));
            Patients.Add(new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false));



            Doctors.Add(new Model.Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "seka99@gmail.com", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8,0,15,30,"102b"));
            Doctors.Add(new Model.Doctor("Milena", "Dravic", 3, 3, 1940, "Branka Radicevica", 14, "Subotica", "0303940883245", "", "061-455-6433", "milena_d", "12345678", Pol.ZENSKO, Title.SPECIJALISTA, "Bla,bla,bla", 15, 30, 23, 0, "103"));
            Doctors.Add(new Model.Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "gigamoravac98@yahoo.com", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"));
            Doctors.Add(new Model.Doctor("Dragan", "Nikolic", 17, 9, 1951, "Dostojevskog", 5, "Indjija", "1709951883285", "", "061-400-6433", "gaga", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 8, 0, 16, 0, "308"));
            Doctors.Add(new Model.Doctor("Rahela", "Ferari", 13, 12, 1970, "Narodnog fronta", 51, "Beograd", "1312970281145", "", "061-333-3364", "brmbrm", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 12,0,20,30,"209"));
            Doctors.Add(new Model.Doctor("Radmila", "Savicevic", 12, 3, 1975, "Balzakova", 61, "Kragujevac", "1203975883445", "", "061-159-6433", "riska", "12345678", Pol.ZENSKO, Title.SPECIJALISTA, "Bla,bla,bla", 8, 0, 14, 45, "112a"));
            Doctors.Add(new Model.Doctor("Velimir", "Zivoinovic", 14, 3, 1968, "Lava Tolstoja", 14, "Novi Sad", "1403968834525", "", "061-754-3364", "bata", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 14, 0, 22, 0, "306"));
            Doctors.Add(new Model.Doctor("Ljubisa", "Samardzic", 27, 3, 1969, "Branislava Nusica", 23, "Zrenjenin", "2703969827182", "", "061-654-6433", "smoki", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 14, 0, 22, 0, "313c"));
            Doctors.Add(new Model.Doctor("Olivera", "Markovic", 13, 11, 1975, "Desanke Maksimovic", 43, "Pancevo", "1311975231428", "", "061-555-3364", "oliveram", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 12, 0, 20, 30, "100a"));
            Doctors.Add(new Model.Doctor("Neda", "Arneric", 12, 8, 1963, "Jasa Tomic", 22, "Novi Sad", "1208963865536", "", "061-000-6433", "nedaaa", "12345678", Pol.ZENSKO, Title.SPECIJALISTA, "Bla,bla,bla", 9, 0, 16, 45, "202"));
            Doctors.Add(new Model.Doctor("Miodrag", "Petrovic", 14, 7, 1980, "Cara Lazara", 44, "Nis", "1407980834255", "", "061-211-3364", "ckalja", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 30, 16, 15, "303"));
            Doctors.Add(new Model.Doctor("Mija", "Aleksic", 27, 6, 1978, "Balzakova", 1, "Kragujevac", "2706978823588", "", "061-909-6433", "mija_a", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 10, 30, 19, 0, "411"));
            long rbr = 0;

            List<Model.DoctorReport> reports = new List<Model.DoctorReport>();
            reports.Add(new Model.DoctorReport(new Model.Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"),new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), TypeOfAppointment.PREGLED, new DateTime(2019, 1, 1), "Anamneza1 za prvog pacijenta", 1, "Dijagnoza1 za prvog pacijenta", "Misljenje doktora 1 za prvog pacijenta"));
            reports.Add(new Model.DoctorReport(new Model.Doctor("Dragan", "Nikolic", 17, 9, 1951, "Dostojevskog", 5, "Indjija", "1709951883285", "", "061-400-6433", "gaga", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 8, 0, 16, 0, "308"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), TypeOfAppointment.OPERACIJA, new DateTime(2019, 6, 17), "Anamneza2 za prvog pacijenta", 1, "Dijagnoza2 za prvog pacijenta", "Misljenje doktora 2 za prvog pacijenta"));
            reports.Add(new Model.DoctorReport(new Model.Doctor("Ljubisa", "Samardzic", 27, 3, 1969, "Branislava Nusica", 23, "Zrenjenin", "2703969827182", "", "061-654-6433", "smoki", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 14, 0, 22, 0, "313c"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), TypeOfAppointment.KONTROLA, new DateTime(2020, 6, 3), "Anamneza3 za prvog pacijenta", 1, "Dijagnoza3 za prvog pacijenta", "Misljenje doktora 3 za prvog pacijenta"));
            reports.Add(new Model.DoctorReport(new Model.Doctor("Olivera", "Markovic", 13, 11, 1975, "Desanke Maksimovic", 43, "Pancevo", "1311975231428", "", "061-555-3364", "oliveram", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 12, 0, 20, 30, "100a"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), TypeOfAppointment.PREGLED, new DateTime(2020, 4, 25), "Anamneza4 za prvog pacijenta", 1, "Dijagnoza4 za prvog pacijenta", "Misljenje doktora 4 za prvog pacijenta"));

            List<Model.SpecialistTreatment> specialistTreatments = new List<SpecialistTreatment>();
            specialistTreatments.Add(new Model.SpecialistTreatment(new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), new DateTime(2019, 5,31,18,0,0), new DateTime(2019, 6, 17, 11, 0, 0), new Doctor("Dragan", "Nikolic", 17, 9, 1951, "Dostojevskog", 5, "Indjija", "1709951883285", "", "061-400-6433", "gaga", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 8, 0, 16, 0, "308"),"Uput kod specijaliste zbog.."));
            specialistTreatments.Add(new Model.SpecialistTreatment(new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), new Doctor("Miodrag", "Petrovic", 14, 7, 1980, "Cara Lazara", 44, "Nis", "1407980834255", "", "061-211-3364", "ckalja", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 30, 16, 15, "303"), new DateTime(2020, 6,17,17,0,0), new DateTime(2020, 6, 24, 11, 0, 0), new Doctor("Mija", "Aleksic", 27, 6, 1978, "Balzakova", 1, "Kragujevac", "2706978823588", "", "061-909-6433", "mija_a", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 10, 30, 19, 0, "411"),"Uput kod specijaliste zbog.."));

            List<HospitalTreatment> hospitalTreatments = new List<HospitalTreatment>();
            hospitalTreatments.Add(new HospitalTreatment(new DateTime(2020, 4, 5, 12, 0, 0), new DateTime(2020, 4, 15, 11, 0, 0), "Razlog uputa 1 u bolnicu pacijenta:Dusan Trkulja", 1, "pr103", "332", new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), new Doctor("Mija", "Aleksic", 27, 6, 1978, "Balzakova", 1, "Kragujevac", "2706978823588", "", "061-909-6433", "mija_a", "12345678", Pol.MUSKO, Title.SPECIJALISTA, "Bla,bla,bla", 10, 30, 19, 0, "411"),new DateTime(2020,4,4,12,45,0)));
            hospitalTreatments.Add(new HospitalTreatment(new DateTime(2016, 2, 29, 12, 0, 0), new DateTime(2016, 3, 8, 11, 0, 0), "Razlog uputa 2 u bolnicu pacijenta:Dusan Trkulja", 1, "pr318", "532", new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), new Doctor("Radmila", "Savicevic", 12, 3, 1975, "Balzakova", 61, "Kragujevac", "1203975883445", "", "061-159-6433", "riska", "12345678", Pol.ZENSKO, Title.SPECIJALISTA, "Bla,bla,bla", 8, 0, 14, 45, "112a"), new DateTime(2016, 2, 28, 17, 15, 0)));

            List<Model.Recipe> recipes = new List<Model.Recipe>();
            recipes.Add(new Model.Recipe(new Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), new DateTime(2019, 6, 7, 15, 15, 0), "Nacin kako se pije lijek - detaljnije1", "12332A", 2));
            recipes.Add(new Model.Recipe(new Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), new DateTime(2019, 6, 7, 15, 15, 0), "Nacin kako se pije lijek - detaljnije2", "2211A", 3));
            recipes.Add(new Model.Recipe(new Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), new DateTime(2019, 8, 19, 14, 45, 0), "Nacin kako se pije lijek - detaljnije3", "2211A", 1));
            
            foreach (Patient patient in App.Patients)
            {
                rbr++;
                if (patient.JMBG=="2903999181500")
                {
                    MedicalRecords.Add(new MedicalRecord(rbr, patient,reports, recipes, hospitalTreatments,specialistTreatments));
                } else MedicalRecords.Add(new MedicalRecord(rbr, patient));
            }

            */
            for(int i = 8; i < 22; i++)
            {
                WorkingTimeForAppointment.Add(new Model2.WorkingTime(i, 0, i, 15));
                WorkingTimeForAppointment.Add(new Model2.WorkingTime(i, 15, i, 30));
                WorkingTimeForAppointment.Add(new Model2.WorkingTime(i, 30, i, 45));
                WorkingTimeForAppointment.Add(new Model2.WorkingTime(i, 45, i+1, 0));
            }
            /*
            Appointments.Add(new Model.Appointment(new DateTime(2020,6,18,12,0,0), new DateTime(2020, 6, 18, 12, 15, 0), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210",TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 18, 8, 0,0), new DateTime(2020, 6, 18, 8, 15, 0), new Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 19, 10, 30, 0), new DateTime(2020, 6, 18, 10, 45, 0), new Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 18, 12, 0, 0), new DateTime(2020, 6, 18, 12, 15, 0), new Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 19, 8, 15, 0), new DateTime(2020, 6, 19, 8, 30, 0), new Doctor("Radmila", "Savicevic", 12, 3, 1975, "Balzakova", 61, "Kragujevac", "1203975883445", "", "061-159-6433", "riska", "12345678", Pol.ZENSKO, Title.SPECIJALISTA, "Bla,bla,bla", 8, 0, 14, 45, "112a"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 20, 9, 45, 0), new DateTime(2020, 6, 20, 10, 0, 0), new Doctor("Radmila", "Savicevic", 12, 3, 1975, "Balzakova", 61, "Kragujevac", "1203975883445", "", "061-159-6433", "riska", "12345678", Pol.ZENSKO, Title.SPECIJALISTA, "Bla,bla,bla", 8, 0, 14, 45, "112a"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 20, 12, 30, 0), new DateTime(2020, 6, 20, 12, 45, 0), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 20, 15, 0, 0), new DateTime(2020, 6, 20, 15, 15, 0), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 20, 17, 15, 0), new DateTime(2020, 6, 20, 17, 30, 0), new Doctor("Olivera", "Markovic", 13, 11, 1975, "Desanke Maksimovic", 43, "Pancevo", "1311975231428", "", "061-555-3364", "oliveram", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 12, 0, 20, 30, "100a"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 21, 19, 45, 0), new DateTime(2020, 6, 21, 20, 0, 0), new Doctor("Olivera", "Markovic", 13, 11, 1975, "Desanke Maksimovic", 43, "Pancevo", "1311975231428", "", "061-555-3364", "oliveram", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 12, 0, 20, 30, "100a"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));
            Appointments.Add(new Model.Appointment(new DateTime(2020, 6, 21, 21, 30, 0), new DateTime(2020, 6, 21, 21, 45, 0), new Doctor("Velimir", "Zivoinovic", 14, 3, 1968, "Lava Tolstoja", 14, "Novi Sad", "1403968834525", "", "061-754-3364", "bata", "12345678", Pol.MUSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 14, 0, 22, 0, "306"), new Patient("Dusan", "Trkulja", new DateTime(1999, 3, 29), new Address("Bulevar cara Lazara", 9, "Novi Sad"), "2903999181500", "trkulja.ra153.2017@uns.ac.rs", "065-483-231", "trx293", "trki2903", Pol.MUSKO, false), "210", TypeOfAppointment.PREGLED));

            List<Question> list1 = new List<Question>();
            list1.Add(new Question("1.", 5));
            list1.Add(new Question("2.", 3));
            List<Question> list2 = new List<Question>();
            list2.Add(new Question("1.", 4));
            list2.Add(new Question("2.", 4));
            List<Question> list3 = new List<Question>();
            list3.Add(new Question("1.", 3));
            list3.Add(new Question("2.", 3));
            List<Question> list4 = new List<Question>();
            list4.Add(new Question("1.", 1));
            list4.Add(new Question("2.", 4));
            List<Question> list5 = new List<Question>();
            list5.Add(new Question("1.", 4));
            list5.Add(new Question("2.", 1));
            List<Question> list6 = new List<Question>();
            list6.Add(new Question("1.", 5));
            list6.Add(new Question("2.", 5));

            Questionnaires.Add(new Questionnaire("Nebitna napomena", new Patient("Nikola", "Stojcevic", 14, 7, 1998, "Pape Pavla", 12, "Beograd", "1407998187454", "stojkeba@gmail.com", "065-833-748", "stojke", "12345678", Pol.MUSKO, false), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), list1));
            Questionnaires.Add(new Questionnaire("Nebitna napomena", new Patient("Nikola", "Stojcevic", 14, 7, 1998, "Pape Pavla", 12, "Beograd", "1407998187454", "stojkeba@gmail.com", "065-833-748", "stojke", "12345678", Pol.MUSKO, false), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), list2));
            Questionnaires.Add(new Questionnaire("Nebitna napomena", new Patient("Marko", "Ostojic", 29, 6, 1998, "Bulevar cara Lazara", 9, "Novi Sad", "2906998187654", "marko24a@gmail.com", "065-899-844", "marko24a", "12345678", Pol.MUSKO, false), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), list3));
            Questionnaires.Add(new Questionnaire("Nebitna napomena", new Patient("Marko", "Ostojic", 29, 6, 1998, "Bulevar cara Lazara", 9, "Novi Sad", "2906998187654", "marko24a@gmail.com", "065-899-844", "marko24a", "12345678", Pol.MUSKO, false), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), list4));
            Questionnaires.Add(new Questionnaire("Nebitna napomena", new Patient("Nikola", "Stojcevic", 14, 7, 1998, "Pape Pavla", 12, "Beograd", "1407998187454", "stojkeba@gmail.com", "065-833-748", "stojke", "12345678", Pol.MUSKO, false), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), list5));
            Questionnaires.Add(new Questionnaire("Nebitna napomena", new Patient("Nikola", "Stojcevic", 14, 7, 1998, "Pape Pavla", 12, "Beograd", "1407998187454", "stojkeba@gmail.com", "065-833-748", "stojke", "12345678", Pol.MUSKO, false), new Doctor("Jelisaveta", "Sablic", 26, 6, 1942, "Desanke Maksimovic", 20, "Pancevo", "2606942881145", "", "061-455-3364", "seka", "12345678", Pol.ZENSKO, Title.OPSTE_PRAKSE, "Bla,bla,bla", 8, 0, 15, 30, "102b"), list6));
            */
            Notifications.Add(new Model2.Notification("Vas termin je pomjeren kod doktora Zivoinovica je pomjeren na 19.06.2020. u 13:00h.",new DateTime(2020,5,31,18,46,32), patientController.GetByID("2222222222222")));
            Notifications.Add(new Model2.Notification("Vas termin je otkazan kod doktora spec. Aleksica. Javite se sekretarima nase bolnice za zakazivanje novog termina.", new DateTime(2020, 6, 1, 12, 44, 4), patientController.GetByID("2222222222222")));
            Notifications.Add(new Model2.Notification("Postovani gospodine Trkulja, nasa klinika Vas obavjestava...", new DateTime(2020, 3,3, 9, 21, 59), patientController.GetByID("2222222222222")));
            Notifications.Add(new Model2.Notification("Vas termin kod doktora Samardzica je pomjeren za 15.06.2020. u 20:00h.", new DateTime(2020, 6, 19, 19, 0, 17), patientController.GetByID("2222222222222")));

            Blogs.Add(new Model2.Blog("COVID 19 - PREVENCIJE", "Tekst vezan za prevencije epidemije COVID 19...", new DateTime(2020, 3, 21), 1));
            Blogs.Add(new Model2.Blog("STANJE U DRZAVI POVODOM EPIDEMIJE", "Tekst vezan za trenutnog stanja u Srbiji...", new DateTime(2020, 4, 11), 2));
            Blogs.Add(new Model2.Blog("POCETAK RADA KLINIKE ZDRAVLJE I MJERE ZASTITE", "Tekst vezan za nastavak rada klinike...", new DateTime(2020, 5, 25), 3));
            Blogs.Add(new Model2.Blog("UPOZORENJE PRED PRAZNIK RADA", "Tekst vezan za praznik rada i COVID 19...", new DateTime(2020, 4, 29), 4));
            Blogs.Add(new Model2.Blog("NAGLI RAST BROJA OBOLJELIH", "Tekst vezan za nagli rast oboljelih...", new DateTime(2020, 3, 27), 5));
            Blogs.Add(new Model2.Blog("BLOG 6.", "Tekst vezan za bla bla...", new DateTime(2020, 1 , 1), 6));
        }

        public static List<User> Users { get => users; set => users = value; }
        internal static List<Model.Appointment.Appointment> Appointments { get => appointments; set => appointments = value; }// //
        internal static List<Patient> Patients { get => patients; set => patients = value; }// //
        internal static List<Address> Addresses { get => addresses; set => addresses = value; }// //
        internal static List<Doctor> Doctors { get => doctors; set => doctors = value; }// //
        internal static List<Questionnaire> Questionnaires { get => questionnaires; set => questionnaires = value; }// //
        internal static List<Model2.WorkingTime> WorkingTimeForAppointment { get => workingTimeForAppointment; set => workingTimeForAppointment = value; }// //
        internal static List<Model2.Notification> Notifications { get => notifications; set => notifications = value; }// //
        internal static List<Model2.Blog> Blogs { get => blogs; set => blogs = value; }// //
        internal static List<Model.MedicalRecords.MedicalRecord> MedicalRecords { get => medicalRecords; set => medicalRecords = value; } // //
    }
}
