﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for SelectAppointment.xaml
    /// </summary>
    public partial class SelectAppointment : Window
    {
        public Patient patient;
        public SelectAppointment(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
            this.listaTermina.ItemsSource = App.WorkingTimeForAppointment;
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.listaTermina.SelectedItem as Model2.WorkingTime == null || this.datumTermina.SelectedDate.Value == null || DateTime.Compare(DateTime.Now.Date.AddDays(1), this.datumTermina.SelectedDate.Value) >=0)
                {
                    MessageBox.Show("Pokusajte ponovo sve izabrati");
                    return;
                }

                DateTime odSati = new DateTime(this.datumTermina.SelectedDate.Value.Year, this.datumTermina.SelectedDate.Value.Month, this.datumTermina.SelectedDate.Value.Day, (this.listaTermina.SelectedItem as Model2.WorkingTime).odSati.Hour, (this.listaTermina.SelectedItem as Model2.WorkingTime).odSati.Minute, (this.listaTermina.SelectedItem as Model2.WorkingTime).odSati.Second);
                DateTime doSati = new DateTime(this.datumTermina.SelectedDate.Value.Year, this.datumTermina.SelectedDate.Value.Month, this.datumTermina.SelectedDate.Value.Day, (this.listaTermina.SelectedItem as Model2.WorkingTime).doSati.Hour, (this.listaTermina.SelectedItem as Model2.WorkingTime).doSati.Minute, (this.listaTermina.SelectedItem as Model2.WorkingTime).doSati.Second);
                
                
                Model2.WorkingTime parametar = new Model2.WorkingTime(odSati, doSati);
                List<Doctor> doctorsd = App.doctorController.GetDoctorByTitle(Model.Users.Title.DoctorOfGeneralPractice);
                List<Model.Appointment.Appointment> appointments = App.appointmentController.GetAppointmentByDate(odSati);
                List<Doctor> doctorsFree = removeAllBusyDoctorFromList(parametar, appointments, doctorsd);
                List<Doctor> avaibleDoctor = listOfAvaibleDoctor(doctorsFree, parametar);

                SelectFreeDoctor win = new SelectFreeDoctor(patient, parametar, avaibleDoctor); //dodati workingtime kad se uradi umjesto null
                win.ShowDialog();
                if (AppointmentAccept.terminZakazan) this.Close();
            } catch (Exception)
            {
                MessageBox.Show("Pokusajte ponovo sve izabrati");
                return;
            }
        }

        List<Model2.WorkingTime> workingTimesForDay(Model2.WorkingTime workingTime)
        { // vraca mi listu potencijalnih termina za odredjeni datum --
            List<Model2.WorkingTime> returnValue = new List<Model2.WorkingTime>();
            DateTime begin = workingTime.odSati;
            DateTime end = workingTime.doSati;
            for (DateTime date = begin; date < end; date = date.AddMinutes(15))
            {
                Model2.WorkingTime helpVariable = new Model2.WorkingTime(date, date.AddMinutes(15));
                returnValue.Add(helpVariable);
                helpVariable = null;
            }
            return returnValue;

        }

        List<Doctor> listOfAvaibleDoctor(List<Doctor> freeDoctor, Model2.WorkingTime workingTimeForDay)
        {
            List<Doctor> returnValue = freeDoctor;
            DateTime dateTime = workingTimeForDay.odSati.Date;
            foreach (Doctor doctor in freeDoctor)
            {
                WorkingTime workingTime = App.workingTimeController.GetWorkingTimeForDoctorByDate(doctor, dateTime);
                if (workingTime == null) continue;
                Model2.WorkingTime workingTime1= new Model2.WorkingTime(new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.BeginTime.Hour, workingTime.BeginTime.Minute, workingTime.BeginTime.Second),
                new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.EndTime.Hour, workingTime.EndTime.Minute, workingTime.EndTime.Second));
                List<Model2.WorkingTime> workingTimes= workingTimesForDay(workingTime1);
                if (existInList(workingTimes, workingTimeForDay) == false || workingTime == null) removeDoctorFromList(doctor, returnValue);
            }
            return returnValue;
}

        bool existInList(List<Model2.WorkingTime> workingTimesForDay, Model2.WorkingTime workingTimeForDay)
        {
            foreach (Model2.WorkingTime workingTime in workingTimesForDay) {
                if (DateTime.Compare(workingTime.odSati.Date, workingTimeForDay.odSati.Date) == 0 && DateTime.Compare(workingTime.doSati.Date, workingTimeForDay.doSati.Date) == 0) return true;
            }
            return false;
        }

        List<Doctor> removeAllBusyDoctorFromList(Model2.WorkingTime workingTimeForDay, List<Model.Appointment.Appointment> appointments,List<Doctor> doctors)
        { // trebalo bi da ukloni sve doktore koji su zauzeti(imaju termine zakazane u predvidjeno vrijeme)
            List<Doctor> returnValue = doctors;// dohvati mi sve doktore opšte namjene
            foreach(Model.Appointment.Appointment appointment in appointments)
            { // prodji mi kroz sve termine
                if (workingTimeForDayExistInList(workingTimeForDay, appointments)) removeDoctorFromList(appointment.Doctor, returnValue);//provjeri da li proslijedjeni termin postoji u terminima i ako da izbrisi doktora koji vrsi pregled ako postoji 

    }
            return returnValue;
        }

        bool removeDoctorFromList(Doctor doctor, List<Doctor> doctors)
        { // koristi je removeAllBusyDoctorFromList
	        foreach(Doctor doctorVariable in doctors)
            {
                if (doctorVariable.PersonalID == doctor.PersonalID) return doctors.Remove(doctorVariable);
            }
            return false;
        }

        bool workingTimeForDayExistInList(Model2.WorkingTime workingTimeForDay, List<Model.Appointment.Appointment> appointments)
        { // koristi je removeAllBusyDoctorFromList
            foreach (Model.Appointment.Appointment appointment in appointments)
            {
                Model2.WorkingTime helpVariable = new Model2.WorkingTime(appointment.StartDate, appointment.FinishDate);
                if (DateTime.Compare(helpVariable.odSati,workingTimeForDay.odSati)==0 && DateTime.Compare(helpVariable.doSati,workingTimeForDay.doSati)==0) return true;
            }
            return false;
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
