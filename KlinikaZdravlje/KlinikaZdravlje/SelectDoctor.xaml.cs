﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for SelectDoctor.xaml
    /// </summary>
    public partial class SelectDoctor : Window
    {
        public Patient patient;

        private List<Doctor> getAllDoctorOfGeneralPractice()
        => App.doctorController.GetAll().Where(Doctor => (Doctor.Title == Model.Users.Title.DoctorOfGeneralPractice)).ToList();
            
        
        public SelectDoctor(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
            this.listaDoktoraOpstePrakse.ItemsSource = App.doctorController.GetDoctorByTitle(Model.Users.Title.DoctorOfGeneralPractice);
            this.selectedDoctor = null;
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        { 
            selectedDoctor = this.listaDoktoraOpstePrakse.SelectedItem as Doctor;
            if (selectedDoctor == null)
            {
                MessageBox.Show("Nije izabran doktor!");
                return;
            }
            SelectOpenAppointment win = new SelectOpenAppointment(patient,selectedDoctor); //dodati doktora kad se uradi funkcijonalnost
            win.ShowDialog();
            if (AppointmentAccept.terminZakazan) this.Close();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public Doctor selectedDoctor { get; set; }

        
    }
}
