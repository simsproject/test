﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            addressCombobox.ItemsSource = App.addressController.GetAll().ToList();
        }

        private void Hyperlink_RequestNavigate(object sender, RoutedEventArgs e)
        {
            MainWindow win1 = new MainWindow();
            win1.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String ime = imePacijenta.Text;
            String prezime = prezimePacijenta.Text;
            DateTime datumRodjenja = datumRodjenjaPacijenta.DisplayDate;
            String jmbg = jmbgPacijenta.Text;
            Address adresa = addressCombobox.SelectedItem as Address; 
            String brojTelefona = brojTelefonaPacijenta.Text;
            String email = emailPacijenta.Text;
            String username = korisnickoImePacijenta.Text;
            String password;
            if (prikaziLozinku.IsChecked == false) password = sifraSkrivena.Password;
            else password = lozinkaPrikazana.Text;
            Gender gender;
            if (radioButtonMusko.IsChecked == true) gender = Gender.Male;
            else gender = Gender.Female;
            MedicalRecord medicalRecord = new MedicalRecord(0, new List<Allergens>());
            App.medicalRecordController.Create(medicalRecord);
            Patient patientRegister = new Patient(ime, prezime, datumRodjenja, jmbg, gender, brojTelefona, email, adresa, username, password, false, medicalRecord);
            Model.Users.User userRegister = new Model.Users.User(ime, prezime, datumRodjenja, jmbg, gender, brojTelefona, email, adresa, username, password);

            if (ime.Length<1 || prezime.Length<1 || DateTime.Compare(DateTime.Now,datumRodjenja)<0 || jmbg.Length<1 || adresa==null || brojTelefona.Length<1 || email.Length<1 || username.Length<1 || password.Length < 8)
            {
                MessageBox.Show("Morate unijeti sve podatke, i moraju biti validni!");
                return;
            }
            Model.Users.User user = App.userController.GetByID(username);
            Patient patient = App.patientController.GetByID(jmbg);

            if(patient==null && user == null)
            {
                App.patientController.Create(patientRegister);
                App.userController.Create(patientRegister);
                Pocetna_strana pocetna_Strana = new Pocetna_strana(StanjePrikaza.POCETNA, patientRegister);
                pocetna_Strana.Show();
                this.Close();
            } else if (patient!=null || user!=null)
            {
                if (patient!=null && patient.IsGuest)
                {
                    patient.IsGuest = false;
                    App.userController.Update(patient);
                    App.patientController.Update(patient);
                    MessageBox.Show("Postojeci korisnik je guest!");
                    Pocetna_strana pocetna_Strana = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
                    pocetna_Strana.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Postojeci korisnik!");
                    return;
                }
            } else
            {
                MessageBox.Show("Postojeci ili korisnik ili pacijent!");
                return;
            }


        }

        private void prikaziLozinku_Click(object sender, RoutedEventArgs e)
        {
            if (prikaziLozinku.IsChecked == true)
            {
                lozinkaPrikazana.Visibility = Visibility.Visible;
                sifraSkrivena.Visibility = Visibility.Hidden;
                lozinkaPrikazana.Text = sifraSkrivena.Password;
            }
            else
            {
                lozinkaPrikazana.Visibility = Visibility.Hidden;
                sifraSkrivena.Visibility = Visibility.Visible;
                sifraSkrivena.Password = lozinkaPrikazana.Text;
            }
        }

        
    }
}
