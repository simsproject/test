﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for HospitalTreatmentWindow.xaml
    /// </summary>
    public partial class HospitalTreatmentWindow : Window
    {
        public HospitalTreatment hospitalTreatment;

        public HospitalTreatmentWindow(HospitalTreatment hospitalTreatment)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.hospitalTreatment = hospitalTreatment;
            this.sobaZaLezanje.Content = this.hospitalTreatment.PatientRoom.Name;
            this.detaljno.Text = this.hospitalTreatment.Note;
            this.pocetakLezanja.Content = String.Format("{0:g}", this.hospitalTreatment.StartDate);
            Patient patient = App.patientController.GetAll().Where(Patient => Patient.MedicalRecord.SerialID == hospitalTreatment.MedicalRecord.SerialID).FirstOrDefault();
            this.pacijent.Content = patient.FirstName + " " + patient.LastName;
        }
    }
}
