﻿#pragma checksum "..\..\ChangeInfoWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "C14F6C9FBBD14E033FD0EE0D46410B90F5CCE558586AAB2FE566E16E925331F3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KlinikaZdravlje;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KlinikaZdravlje {
    
    
    /// <summary>
    /// ChangeInfoWindow
    /// </summary>
    public partial class ChangeInfoWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 624 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menu_korisnik;
        
        #line default
        #line hidden
        
        
        #line 674 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menu_atermini;
        
        #line default
        #line hidden
        
        
        #line 698 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menu_ankente;
        
        #line default
        #line hidden
        
        
        #line 710 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menu_informacije;
        
        #line default
        #line hidden
        
        
        #line 722 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem menu_pomoc;
        
        #line default
        #line hidden
        
        
        #line 723 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem shorcut_menu;
        
        #line default
        #line hidden
        
        
        #line 728 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem help_menu;
        
        #line default
        #line hidden
        
        
        #line 733 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem about_menu;
        
        #line default
        #line hidden
        
        
        #line 749 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button HoomPage;
        
        #line default
        #line hidden
        
        
        #line 751 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image HoomPageIkonica;
        
        #line default
        #line hidden
        
        
        #line 755 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Notification;
        
        #line default
        #line hidden
        
        
        #line 757 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image NotificationIkonica;
        
        #line default
        #line hidden
        
        
        #line 762 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Acount;
        
        #line default
        #line hidden
        
        
        #line 764 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image UserIkonica;
        
        #line default
        #line hidden
        
        
        #line 768 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button TriTackeDugm;
        
        #line default
        #line hidden
        
        
        #line 770 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image TriTackeIkonica;
        
        #line default
        #line hidden
        
        
        #line 782 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonSave;
        
        #line default
        #line hidden
        
        
        #line 789 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaIme;
        
        #line default
        #line hidden
        
        
        #line 790 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaPrezime;
        
        #line default
        #line hidden
        
        
        #line 792 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelZaDatumRodjenja;
        
        #line default
        #line hidden
        
        
        #line 793 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelZaBrojTelefona;
        
        #line default
        #line hidden
        
        
        #line 794 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelZaAdresuStanovanja;
        
        #line default
        #line hidden
        
        
        #line 795 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelZaJMBG;
        
        #line default
        #line hidden
        
        
        #line 796 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaEmail;
        
        #line default
        #line hidden
        
        
        #line 797 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaPol;
        
        #line default
        #line hidden
        
        
        #line 798 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonCancel;
        
        #line default
        #line hidden
        
        
        #line 804 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ime;
        
        #line default
        #line hidden
        
        
        #line 805 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox prezime;
        
        #line default
        #line hidden
        
        
        #line 808 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox jmbg;
        
        #line default
        #line hidden
        
        
        #line 809 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox brojTelefona;
        
        #line default
        #line hidden
        
        
        #line 810 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox mail;
        
        #line default
        #line hidden
        
        
        #line 811 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker datumRodjenjaPacijenta;
        
        #line default
        #line hidden
        
        
        #line 812 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox addressCombobox;
        
        #line default
        #line hidden
        
        
        #line 833 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonMusko;
        
        #line default
        #line hidden
        
        
        #line 834 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton radioButtonZensko;
        
        #line default
        #line hidden
        
        
        #line 838 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border accountMenu;
        
        #line default
        #line hidden
        
        
        #line 840 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button userButton;
        
        #line default
        #line hidden
        
        
        #line 846 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button medicalRecord;
        
        #line default
        #line hidden
        
        
        #line 854 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border TriTackeMenu;
        
        #line default
        #line hidden
        
        
        #line 859 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button logOutButton;
        
        #line default
        #line hidden
        
        
        #line 865 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button changePassword;
        
        #line default
        #line hidden
        
        
        #line 871 "..\..\ChangeInfoWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button feedbackButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KlinikaZdravlje;component/changeinfowindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ChangeInfoWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 601 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.Exit_Executed);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 602 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.LogOut_Executed);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 603 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.ChangePassword_Executed);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 604 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.Feedback_Executed);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 605 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.MedicalRecord_Executed);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 606 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.ChangeInfo_Executed);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 607 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.UserInfo_Executed);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 608 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.Notification_Executed);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 609 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.HomePage_Executed);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 610 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.AppoointmentView_Executed);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 611 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.AppointmentCreate_Executed);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 612 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.AppointmentEdit_Executed);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 613 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.AppointmentRemove_Executed);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 614 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.QuestionnaireDoctor_Executed);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 615 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.QuestionnaireClinic_Executed);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 616 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.InfoDoctors_Executed);
            
            #line default
            #line hidden
            return;
            case 17:
            
            #line 617 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.InfoClinic_Executed);
            
            #line default
            #line hidden
            return;
            case 18:
            
            #line 618 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.Help_Executed);
            
            #line default
            #line hidden
            return;
            case 19:
            
            #line 619 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.Shortcut_Executed);
            
            #line default
            #line hidden
            return;
            case 20:
            
            #line 620 "..\..\ChangeInfoWindow.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.About_Executed);
            
            #line default
            #line hidden
            return;
            case 21:
            this.menu_korisnik = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 22:
            this.menu_atermini = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 23:
            this.menu_ankente = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 24:
            this.menu_informacije = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 25:
            this.menu_pomoc = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 26:
            this.shorcut_menu = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 27:
            this.help_menu = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 28:
            this.about_menu = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 29:
            this.HoomPage = ((System.Windows.Controls.Button)(target));
            
            #line 749 "..\..\ChangeInfoWindow.xaml"
            this.HoomPage.Click += new System.Windows.RoutedEventHandler(this.HoomPage_Click);
            
            #line default
            #line hidden
            return;
            case 30:
            this.HoomPageIkonica = ((System.Windows.Controls.Image)(target));
            return;
            case 31:
            this.Notification = ((System.Windows.Controls.Button)(target));
            
            #line 755 "..\..\ChangeInfoWindow.xaml"
            this.Notification.Click += new System.Windows.RoutedEventHandler(this.Notification_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.NotificationIkonica = ((System.Windows.Controls.Image)(target));
            return;
            case 33:
            this.Acount = ((System.Windows.Controls.Button)(target));
            
            #line 762 "..\..\ChangeInfoWindow.xaml"
            this.Acount.Click += new System.Windows.RoutedEventHandler(this.Acount_Click);
            
            #line default
            #line hidden
            return;
            case 34:
            this.UserIkonica = ((System.Windows.Controls.Image)(target));
            return;
            case 35:
            this.TriTackeDugm = ((System.Windows.Controls.Button)(target));
            
            #line 768 "..\..\ChangeInfoWindow.xaml"
            this.TriTackeDugm.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.TriTackeIkonica = ((System.Windows.Controls.Image)(target));
            return;
            case 37:
            this.buttonSave = ((System.Windows.Controls.Button)(target));
            
            #line 782 "..\..\ChangeInfoWindow.xaml"
            this.buttonSave.Click += new System.Windows.RoutedEventHandler(this.buttonSave_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            this.labelaZaIme = ((System.Windows.Controls.Label)(target));
            return;
            case 39:
            this.labelaZaPrezime = ((System.Windows.Controls.Label)(target));
            return;
            case 40:
            this.labelZaDatumRodjenja = ((System.Windows.Controls.Label)(target));
            return;
            case 41:
            this.labelZaBrojTelefona = ((System.Windows.Controls.Label)(target));
            return;
            case 42:
            this.labelZaAdresuStanovanja = ((System.Windows.Controls.Label)(target));
            return;
            case 43:
            this.labelZaJMBG = ((System.Windows.Controls.Label)(target));
            return;
            case 44:
            this.labelaZaEmail = ((System.Windows.Controls.Label)(target));
            return;
            case 45:
            this.labelaZaPol = ((System.Windows.Controls.Label)(target));
            return;
            case 46:
            this.buttonCancel = ((System.Windows.Controls.Button)(target));
            
            #line 798 "..\..\ChangeInfoWindow.xaml"
            this.buttonCancel.Click += new System.Windows.RoutedEventHandler(this.buttonCancel_Click);
            
            #line default
            #line hidden
            return;
            case 47:
            this.ime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 48:
            this.prezime = ((System.Windows.Controls.TextBox)(target));
            return;
            case 49:
            this.jmbg = ((System.Windows.Controls.TextBox)(target));
            return;
            case 50:
            this.brojTelefona = ((System.Windows.Controls.TextBox)(target));
            return;
            case 51:
            this.mail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 52:
            this.datumRodjenjaPacijenta = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 53:
            this.addressCombobox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 54:
            this.radioButtonMusko = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 55:
            this.radioButtonZensko = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 56:
            this.accountMenu = ((System.Windows.Controls.Border)(target));
            return;
            case 57:
            this.userButton = ((System.Windows.Controls.Button)(target));
            
            #line 840 "..\..\ChangeInfoWindow.xaml"
            this.userButton.Click += new System.Windows.RoutedEventHandler(this.userButton_Click);
            
            #line default
            #line hidden
            return;
            case 58:
            this.medicalRecord = ((System.Windows.Controls.Button)(target));
            
            #line 846 "..\..\ChangeInfoWindow.xaml"
            this.medicalRecord.Click += new System.Windows.RoutedEventHandler(this.medicalRecord_Click);
            
            #line default
            #line hidden
            return;
            case 59:
            this.TriTackeMenu = ((System.Windows.Controls.Border)(target));
            return;
            case 60:
            this.logOutButton = ((System.Windows.Controls.Button)(target));
            
            #line 859 "..\..\ChangeInfoWindow.xaml"
            this.logOutButton.Click += new System.Windows.RoutedEventHandler(this.logOutButton_Click);
            
            #line default
            #line hidden
            return;
            case 61:
            this.changePassword = ((System.Windows.Controls.Button)(target));
            
            #line 865 "..\..\ChangeInfoWindow.xaml"
            this.changePassword.Click += new System.Windows.RoutedEventHandler(this.changePassword_Click);
            
            #line default
            #line hidden
            return;
            case 62:
            this.feedbackButton = ((System.Windows.Controls.Button)(target));
            
            #line 871 "..\..\ChangeInfoWindow.xaml"
            this.feedbackButton.Click += new System.Windows.RoutedEventHandler(this.feedbackButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

