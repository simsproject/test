﻿#pragma checksum "..\..\DoctorPreviewWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "0CB9DDAFC0F8DCE25AECA006DBFAD3E98F90E2FC5B4DCFFC26F27C9B45938FCD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KlinikaZdravlje;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KlinikaZdravlje {
    
    
    /// <summary>
    /// DoctorPreviewWindow
    /// </summary>
    public partial class DoctorPreviewWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image slikaDoktora;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaIme;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaPrezime;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaTitulu;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaEmail;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ime;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label prezime;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label titula;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label email;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaBrojTelefona;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\DoctorPreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label brojTelefona;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KlinikaZdravlje;component/doctorpreviewwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\DoctorPreviewWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.slikaDoktora = ((System.Windows.Controls.Image)(target));
            return;
            case 2:
            this.labelaZaIme = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.labelaZaPrezime = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.labelaZaTitulu = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.labelaZaEmail = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.ime = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.prezime = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.titula = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.email = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.labelaZaBrojTelefona = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.brojTelefona = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

