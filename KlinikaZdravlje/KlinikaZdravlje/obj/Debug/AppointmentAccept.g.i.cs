﻿#pragma checksum "..\..\AppointmentAccept.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "AD958334A6576ADEA6E44EED35C0376CD4DADB5908297FC46422494A559B332D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using KlinikaZdravlje;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace KlinikaZdravlje {
    
    
    /// <summary>
    /// AppointmentAccept
    /// </summary>
    public partial class AppointmentAccept : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaLjekaraKodKojegSeOdrzavaTermin;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaProstorijaOdrzavanjaTermina;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaVrijemePocetkaTermina;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label labelaZaVrijemeZavrsetkaTermina;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ljekarKodKojegSeOdrzavaTermin;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label prostorijaOdrzavanjaTermina;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label vrijemePocetkaTermin;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label vrijemeZavrsetkaTermina;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button dugmeZaPotvrduTermina;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\AppointmentAccept.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button dugmeZaNazad;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/KlinikaZdravlje;component/appointmentaccept.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AppointmentAccept.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.labelaZaLjekaraKodKojegSeOdrzavaTermin = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.labelaZaProstorijaOdrzavanjaTermina = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.labelaZaVrijemePocetkaTermina = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.labelaZaVrijemeZavrsetkaTermina = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.ljekarKodKojegSeOdrzavaTermin = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.prostorijaOdrzavanjaTermina = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.vrijemePocetkaTermin = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.vrijemeZavrsetkaTermina = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.dugmeZaPotvrduTermina = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\AppointmentAccept.xaml"
            this.dugmeZaPotvrduTermina.Click += new System.Windows.RoutedEventHandler(this.dugmeZaPotvrduTermina_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.dugmeZaNazad = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\AppointmentAccept.xaml"
            this.dugmeZaNazad.Click += new System.Windows.RoutedEventHandler(this.dugmeZaNazad_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

