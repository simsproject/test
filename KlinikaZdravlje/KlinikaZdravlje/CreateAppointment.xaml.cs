﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for CreateAppointment.xaml
    /// </summary>
    public partial class CreateAppointment : Window
    {
        private Patient patient { get; set; }

        internal Patient Patient { get => patient; set => patient = value; }
        public CreateAppointment(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Patient = patient;
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            AppointmentAccept.terminZakazan = false;
            if (zakazivanjePoLjekaru.IsChecked == true)
            {
                SelectDoctor win = new SelectDoctor(patient);
                win.ShowDialog();
            } else if (zakazivanjePoTerminu.IsChecked == true)
            {
                SelectAppointment win = new SelectAppointment(patient);
                win.ShowDialog();
            }
            else
            {
                RecommendedTerm win = new RecommendedTerm(patient);
                win.ShowDialog();
            }
            if (AppointmentAccept.terminZakazan) this.Close();
        }
    }
}
