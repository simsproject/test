﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

        }

        private void prikaziLozinku_Click(object sender, RoutedEventArgs e)
        {
            if (prikaziLozinku.IsChecked == true)
            {
                sifraOtkrivena.Visibility = Visibility.Visible;
                sifraSkrivena.Visibility = Visibility.Hidden;
                sifraOtkrivena.Text = sifraSkrivena.Password;
            }
            else
            {
                sifraOtkrivena.Visibility = Visibility.Hidden;
                sifraSkrivena.Visibility = Visibility.Visible;
                sifraSkrivena.Password = sifraOtkrivena.Text;
            }
        }

        private void Hyperlink_RequestNavigate(object sender, RoutedEventArgs e)
        {
            Window1 win1 = new Window1();
            win1.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            String username = KorinickoIme.Text;
            String password;
            if (prikaziLozinku.IsChecked == false) password = sifraSkrivena.Password;
            else password = sifraOtkrivena.Text;
            Patient patient1 = patientExists(username, password);
            if (username == "" && password == "")
            {
                labelaZaNetacnuLozinku.Visibility = Visibility.Visible;
                labelaZaGuestKorisnika.Visibility = Visibility.Visible;
                labelaZaNepostojecegKorisnika.Visibility = Visibility.Hidden;
                return;
            }
            else if (username == "")
            {
                labelaZaNetacnuLozinku.Visibility = Visibility.Hidden;
                labelaZaGuestKorisnika.Visibility = Visibility.Visible;
                labelaZaNepostojecegKorisnika.Visibility = Visibility.Hidden;
                return;
            }
            else if(password == "") 
            {
                labelaZaNetacnuLozinku.Visibility = Visibility.Visible;
                labelaZaGuestKorisnika.Visibility = Visibility.Hidden;
                labelaZaNepostojecegKorisnika.Visibility = Visibility.Hidden;
                return;
            }
            if (patient1 != null)
            {
                Pocetna_strana win2 = new Pocetna_strana(StanjePrikaza.POCETNA,patient1);
                win2.Show();
                this.Close();
                return;
            }
            if (!passwordValid(username, password))
            {
                labelaZaNetacnuLozinku.Visibility = Visibility.Visible;
                labelaZaNepostojecegKorisnika.Visibility = Visibility.Hidden;
                labelaZaGuestKorisnika.Visibility = Visibility.Hidden;
                return;
            }
            if (patientGuest(username,password))
            {
                labelaZaNepostojecegKorisnika.Visibility = Visibility.Hidden;
                labelaZaNetacnuLozinku.Visibility = Visibility.Hidden;
                labelaZaGuestKorisnika.Visibility = Visibility.Visible;
                return;
            }
            if (patient1== null)
            { 

                labelaZaNepostojecegKorisnika.Visibility = Visibility.Visible;
                labelaZaNetacnuLozinku.Visibility = Visibility.Hidden;
                labelaZaGuestKorisnika.Visibility = Visibility.Hidden;
                return;
            }
        }


        private Patient patientExists(String username, String password)
        {
            foreach (Patient patient in App.patientController.GetAll())
            {
                if (patient.UserName == username && patient.Password == password && !patient.IsGuest)
                    return patient;
            }
            return null;
        }

        private bool passwordValid(String username, String password)
        {
            foreach (Patient patient in App.patientController.GetAll())
            {
                if (patient.UserName == username)
                {
                    if (patient.Password == password)
                        return true;
                    else return false;
                }
            }
            return true;
        }

        private bool patientGuest(String username, String password)
        {
            foreach (Patient patient in App.Patients)
            {
                if (patient.UserName == username)
                {
                    if (patient.Password == password)
                        return patient.IsGuest;
                    else return false;
                }
            }
            return false;
        }
    }

}
