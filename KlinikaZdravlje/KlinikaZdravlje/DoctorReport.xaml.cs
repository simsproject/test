﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for DoctorReport.xaml
    /// </summary>
    public partial class DoctorReport : Window
    {
        public Model.MedicalRecords.DoctorReport doctorReport;

        public DoctorReport(Model.MedicalRecords.DoctorReport doctorReport)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.doctorReport = doctorReport;

            Patient patient = App.patientController.GetAll().Where(Patient => Patient.MedicalRecord.SerialID ==doctorReport.MedicalRecord.SerialID).FirstOrDefault();

            this.pacijent.Content = patient.FirstName + " " + patient.LastName;
            this.anamneza.Text = this.doctorReport.Anamnesis;
            this.datumPregleda.Content = String.Format("{0:d}",this.doctorReport.Date);
            this.ljekar.Content = "dr " + this.doctorReport.Doctor.FirstName + " " + this.doctorReport.Doctor.LastName;
            this.dijagnoza.Text = this.doctorReport.DiagnosisAndTeraphy;
            this.nalaziIMisljenje.Text = this.doctorReport.DoctorOpinion;
            
        }
    }
}
