﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for ChangePasswordxaml.xaml
    /// </summary>
    public partial class ChangePasswordxaml : Window
    {
        public int brojKlikovaNaTriTacke = 0;
        public int brojKlikovaNaUsera = 0;
        private Patient patient { get; set; }

        internal Patient Patient { get => patient; set => patient = value; }

        public ChangePasswordxaml(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Patient = patient;
        }

        private void prikaziLozinku1_Click(object sender, RoutedEventArgs e)
        {
            if (prikaziLozinkuStara.IsChecked == true)
            {
                sifraOtkrivena1.Visibility = Visibility.Visible;
                sifraSkrivena1.Visibility = Visibility.Hidden;
                sifraOtkrivena1.Text = sifraSkrivena1.Password;
            }
            else
            {
                sifraOtkrivena1.Visibility = Visibility.Hidden;
                sifraSkrivena1.Visibility = Visibility.Visible;
                sifraSkrivena1.Password = sifraOtkrivena1.Text;
            }
        }

        private void prikaziLozinku2_Click(object sender, RoutedEventArgs e)
        {
            if (prikaziLozinkuNovo.IsChecked == true)
            {
                sifraOtkrivena2.Visibility = Visibility.Visible;
                sifraSkrivena2.Visibility = Visibility.Hidden;
                sifraOtkrivena2.Text = sifraSkrivena2.Password;
            }
            else
            {
                sifraOtkrivena2.Visibility = Visibility.Hidden;
                sifraSkrivena2.Visibility = Visibility.Visible;
                sifraSkrivena2.Password = sifraOtkrivena2.Text;
            }
        }

        private void prikaziLozinku3_Click(object sender, RoutedEventArgs e)
        {
            if (prikaziLozinkuPonovo.IsChecked == true)
            {
                sifraOtkrivena3.Visibility = Visibility.Visible;
                sifraSkrivena3.Visibility = Visibility.Hidden;
                sifraOtkrivena3.Text = sifraSkrivena3.Password;
            }
            else
            {
                sifraOtkrivena3.Visibility = Visibility.Hidden;
                sifraSkrivena3.Visibility = Visibility.Visible;
                sifraSkrivena3.Password = sifraOtkrivena3.Text;
            }
        }



        private void Button_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaTriTacke++;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaTriTacke % 2 == 0)
            {
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;


            }
            else
            {
                TriTackeMenu.Visibility = Visibility.Visible;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
        }

        private void logOutButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            //patient = null;
            win2.Show();
            this.Close();
        }

        private void changePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void feedbackButton_Click(object sender, RoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Acount_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaUsera++;
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaUsera % 2 == 0)
            {
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            else
            {
                accountMenu.Visibility = Visibility.Visible;
                UserIkonica.Visibility = Visibility.Visible;

            }
        }

        private void userButton_Click(object sender, RoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void medicalRecord_Click(object sender, RoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, patient);
            win.Show();
            this.Close();
        }

        private void HoomPage_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }


        private void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void LogOut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangePassword_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Feedback_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void MedicalRecord_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangeInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            /*ChangeInfoWindow win = new ChangeInfoWindow();
            win.Visibility = Visibility.Visible;
            this.Close();*/
        }

        private void UserInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            User win = new User(patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, patient);
            win.Show();
            this.Close();
        }

        private void HomePage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }

        private void AppoointmentView_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Appointment win = new Appointment(Patient); //dodati appointment umjesto null
            win.Show();
            this.Close();
        }

        private void AppointmentCreate_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentRemove_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void QuestionnaireDoctor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireWindow win = new QuestionaireWindow(patient);
            win.Show();
            this.Close();
        }

        private void QuestionnaireClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireHospital win = new QuestionaireHospital(patient);
            win.Show();
            this.Close();
        }

        private void InfoDoctors_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoDoctor win = new InfoDoctor(patient);
            win.Show();
            this.Close();
        }

        private void InfoClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoClinic win = new InfoClinic(patient);
            win.Show();
            this.Close();
        }

        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            HelpWindow win = new HelpWindow();
            win.ShowDialog();
        }

        private void Shortcut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShortcutWindow win = new ShortcutWindow();
            win.ShowDialog();
        }

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            AbourWindow win = new AbourWindow();
            win.ShowDialog();
        }

        private void buttonAccept_Click(object sender, RoutedEventArgs e)
        {
            string staraLozinka = prikaziLozinkuStara.IsChecked == true ? sifraOtkrivena1.Text : sifraSkrivena1.Password;
            string novaLozinka = prikaziLozinkuNovo.IsChecked == true ? sifraOtkrivena2.Text : sifraSkrivena2.Password;
            string ponovoNovaLozinka = prikaziLozinkuPonovo.IsChecked == true ? sifraOtkrivena3.Text : sifraSkrivena3.Password;

            if (staraLozinka != patient.Password)
            {
                MessageBox.Show("Stara lozinka nije tacna! Ponovo unesite");
                return;
            }
            else if (novaLozinka != ponovoNovaLozinka && novaLozinka.Length<8)
            {
                MessageBox.Show("Nova lozinka se ne podudara sa ponovo unsesnom! Ponovo unesite");
                return;
            }

            patient.Password = novaLozinka;

            App.patientController.Update(patient);
            App.userController.Update(patient);
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, patient);
            win.Show();
            this.Close();
        }
    }
}
