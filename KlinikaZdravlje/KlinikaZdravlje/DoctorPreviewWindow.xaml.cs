﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for DoctorPreviewWindow.xaml
    /// </summary>
    public partial class DoctorPreviewWindow : Window
    {
        public Doctor doctor { get; set; }


        public DoctorPreviewWindow(Doctor doctor)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.doctor = doctor;
            this.brojTelefona.Content = this.doctor.PhoneNumber;
            this.email.Content = this.doctor.Email;
            this.ime.Content = this.doctor.FirstName;
            this.prezime.Content = this.doctor.LastName;
            if (doctor.Title == Model.Users.Title.DoctorOfGeneralPractice) this.titula.Content = "DR. OPSTE PRAKSE";
            else this.titula.Content = "DR. SPECIJALISTA";

        }
    }
}
