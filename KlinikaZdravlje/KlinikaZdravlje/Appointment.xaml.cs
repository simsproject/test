﻿using Controller;
using Controller.UserPackageController;
using Model.Appointment;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for Appointment.xaml
    /// </summary>
    public partial class Appointment : Window
    {
        public int brojKlikovaNaTriTacke = 0;
        public int brojKlikovaNaUsera = 0;
        private Patient patient { get; set; }
        private List<Model.Appointment.Appointment> appointments;

        internal Patient Patient { get => patient; set => patient = value; }

        private List<Model.Appointment.Appointment> listOfAppoitmentsForPatient(Patient patient)
        {

            return App.appointmentController.GetAppointmentsForPatientByDate(patient, DateTime.Now);
            /*
            List<Model.Appointment.Appointment> retVal=new List<Model.Appointment.Appointment>();
            foreach (Model.Appointment appointmentIt in App.Appointments)
            {
                if (appointmentIt.patient.JMBG == patient.JMBG && appointmentIt.begin>DateTime.Now) retVal.Add(appointmentIt);
            }
            return retVal;
            */
        }

        public Appointment(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Patient = patient;
            this.appointments= listOfAppoitmentsForPatient(this.Patient);
            this.tabelaZaTermine.ItemsSource = this.appointments;
        }

        private void ScheduleAppointmentButton_Click(object sender, RoutedEventArgs e)
        {
            CreateAppointment win = new CreateAppointment(Patient);
            win.ShowDialog();
            App.Appointments.Add(null/*new Model.Appointment(new DateTime(2020, 6, 19, 17, 0, 0), new DateTime(2020, 6, 19, 17, 15, 0), new Model.Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "gigamoravac98@yahoo.com", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Model.Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"), this.patient, "203", TypeOfAppointment.PREGLED)*/);

            this.tabelaZaTermine.ItemsSource = listOfAppoitmentsForPatient(patient);

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaTriTacke++;
            if (accountMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaUsera++;
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaTriTacke % 2 == 0)
            {
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;


            }
            else
            {
                TriTackeMenu.Visibility = Visibility.Visible;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
        }

        private void logOutButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow win2 = new MainWindow();
            //patient = null;
            win2.Show();
            this.Close();
        }

        private void changePassword_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void feedbackButton_Click(object sender, RoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Acount_Click(object sender, RoutedEventArgs e)
        {
            brojKlikovaNaUsera++;
            if (TriTackeMenu.Visibility == Visibility.Visible)
            {
                brojKlikovaNaTriTacke++;
                TriTackeMenu.Visibility = Visibility.Hidden;
                TriTackeIkonica.Visibility = Visibility.Visible;

            }
            if (brojKlikovaNaUsera % 2 == 0)
            {
                accountMenu.Visibility = Visibility.Hidden;
                UserIkonica.Visibility = Visibility.Visible;

            }
            else
            {
                accountMenu.Visibility = Visibility.Visible;
                UserIkonica.Visibility = Visibility.Visible;

            }
        }

        private void userButton_Click(object sender, RoutedEventArgs e)
        {
            User win = new User(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void medicalRecord_Click(object sender, RoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, Patient);
            win.Show();
            this.Close();
        }

        private void HoomPage_Click(object sender, RoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, Patient);
            win.Show();
            this.Close();
        }

        
        private void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void LogOut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangePassword_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangePasswordxaml win = new ChangePasswordxaml(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Feedback_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FeedbackWindow win = new FeedbackWindow(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void MedicalRecord_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void ChangeInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ChangeInfoWindow win = new ChangeInfoWindow(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void UserInfo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            User win = new User(Patient);
            win.Visibility = Visibility.Visible;
            this.Close();
        }

        private void Notification_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.OBAVJESTENJA, Patient);
            win.Show();
            this.Close();
        }

        private void HomePage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Pocetna_strana win = new Pocetna_strana(StanjePrikaza.POCETNA, Patient);
            win.Show();
            this.Close();
        }

        private void AppoointmentView_Executed(object sender, ExecutedRoutedEventArgs e)
        {
           /* Appointment win = new Appointment();
            win.Show();
            this.Close(); */
        }

        private void AppointmentCreate_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentEdit_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void AppointmentRemove_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void QuestionnaireDoctor_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireWindow win = new QuestionaireWindow(Patient);
            win.Show();
            this.Close();
        }

        private void QuestionnaireClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            QuestionaireHospital win = new QuestionaireHospital(Patient);
            win.Show();
            this.Close();
        }

        private void InfoDoctors_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoDoctor win = new InfoDoctor(Patient);
            win.Show();
            this.Close();
        }

        private void InfoClinic_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InfoClinic win = new InfoClinic(Patient);
            win.Show();
            this.Close();
        }

        private void Help_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            HelpWindow win = new HelpWindow();
            win.ShowDialog();
        }

        private void Shortcut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ShortcutWindow win = new ShortcutWindow();
            win.ShowDialog();
        }

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            AbourWindow win = new AbourWindow();
            win.ShowDialog();
        }

        private void tekstualnoPoljeZaPretragu_TextChanged(object sender, TextChangedEventArgs e)
        {
            //(Doctor.FirstName.ToUpper().Contains(poljeZaPretraguTabele.Text.ToUpper()) || Doctor.LastName.ToUpper().Contains(poljeZaPretraguTabele.Text.ToUpper()))7
            var filtered = appointments.Where(Appointment => (Appointment.Doctor.FirstName.ToUpper().Contains(tekstualnoPoljeZaPretragu.Text.ToUpper()) || Appointment.Doctor.LastName.ToUpper().Contains(tekstualnoPoljeZaPretragu.Text.ToUpper()))).ToList();

            if (tekstualnoPoljeZaPretragu.Text != "")
            {
                tabelaZaTermine.ItemsSource = filtered;
            } else
            {
                tabelaZaTermine.ItemsSource = listOfAppoitmentsForPatient(patient);
            }
        }

        private void UpdateAppointmentButton_Click(object sender, RoutedEventArgs e)
        {
            Model.Appointment.Appointment appointment = this.tabelaZaTermine.SelectedItem as Model.Appointment.Appointment;
            if (appointment != null) {
                SelectOpenAppointment win = new SelectOpenAppointment(appointment);
                win.ShowDialog();
                this.tabelaZaTermine.ItemsSource = listOfAppoitmentsForPatient(patient);
            }
            else
            {
                MessageBox.Show("Izaberite termin koji zelite izmjeniti!");
            }
        }

        private void cancelAppointmentButton_Click(object sender, RoutedEventArgs e)
        {
            Model.Appointment.Appointment appointment = this.tabelaZaTermine.SelectedItem as Model.Appointment.Appointment;
            if (appointment != null)
            {
                MessageBoxResult messageBoxOptions= MessageBox.Show("Otkazivanje","Zelite stvarno otkazati termin?",MessageBoxButton.YesNo);
                switch (messageBoxOptions)
                {
                    case MessageBoxResult.Yes:
                        App.appointmentController.Delete(appointment);
                        this.tabelaZaTermine.ItemsSource = listOfAppoitmentsForPatient(patient);
                        break;
                    case MessageBoxResult.No:
                        return;
                }

            }
            else
            {
                MessageBox.Show("Izaberite termin koji zelite izmjeniti!");
            }
        }
    }

}

    


