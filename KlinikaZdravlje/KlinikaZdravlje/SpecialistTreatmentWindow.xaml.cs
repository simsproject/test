﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for SpecialistTreatmentWindow.xaml
    /// </summary>
    public partial class SpecialistTreatmentWindow : Window
    {
        public SpecialistTreatment specialistTreatment;

        public SpecialistTreatmentWindow(SpecialistTreatment specialistTreatment)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            this.specialistTreatment = specialistTreatment;
            this.detaljnije.Text = this.specialistTreatment.Note;
            this.zakazaniTermin.Content = String.Format("{0:g}", this.specialistTreatment.Appointment.StartDate); 
            this.pacijent.Content = this.specialistTreatment.Appointment.Patient.FirstName+" "+ specialistTreatment.Appointment.Patient.LastName;
            this.doktorSpecijalista.Content = "dr. spec. "+ this.specialistTreatment.Appointment.Doctor.FirstName+" " + specialistTreatment.Appointment.Doctor.LastName;
        }
    }
}
