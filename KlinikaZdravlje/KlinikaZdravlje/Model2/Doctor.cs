﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{

    public class WorkingTime
    {
        public DateTime odSati { get; set; }
        public DateTime doSati { get; set; }

        public String terminString { get; set; }

        public WorkingTime(DateTime odSati, DateTime doSati)
        {
            this.odSati = odSati;
            this.doSati = doSati;
            this.terminString = String.Format("{0:t}", this.odSati) + " - " + String.Format("{0:t}", this.doSati);
        }

        public WorkingTime(int sati1,int minut1, int sati2,int minut2)
        {
            this.odSati = new DateTime(2000,1,1,sati1,minut1,0);
            this.doSati = new DateTime(2000, 1, 1, sati2, minut2, 0);
            this.terminString = String.Format("{0:t}", this.odSati) + " - " + String.Format("{0:t}", this.doSati);
        }
    }
    public enum Title
    {
        SPECIJALISTA, OPSTE_PRAKSE
    }

    public class Doctor
    {
        public String ime { get; set; }
        public String prezime { get; set; }
        public DateTime datumRodjenja { get; set; }
        public Address adresaStanovanja { get; set; }
        public String JMBG { get; set; }
        public String email { get; set; }
        public String brojTelefona { get; set; }
        public String korisnickoIme { get; set; }
        public String lozinka { get; set; }
        public Pol pol { get; set; }
        public Title titula { get; set; }

        public String titulaString { get; set; }
        public String biografija { get; set; }
        public WorkingTime radnoVrijeme { get; set; }
        public String prostorija { get; set; }
        public String doctorString { get; set; }

        public Doctor(String ime, String prezime, DateTime datumRodjenja, Address adresaStanovanja, String JMBG, String email, String brojTelefona, String korisnickoIme, String lozinka, Pol pol, Title titula, String biografija, int sati1,int minuti1,int sati2,int minuti2, String prostorija)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = datumRodjenja;
            this.adresaStanovanja = adresaStanovanja;
            this.JMBG = JMBG;
            this.email = email;
            this.brojTelefona = brojTelefona;
            this.korisnickoIme = korisnickoIme;
            this.lozinka = lozinka;
            this.pol = pol;
            this.titula = titula;
            this.biografija = biografija;
            this.radnoVrijeme = new WorkingTime(new DateTime(2000, 1, 1, sati1, minuti1, 0), new DateTime(2000, 1, 1, sati2, minuti2, 0));
            this.prostorija = prostorija;
            this.doctorString = "dr. " + ime + " " + prezime;
            this.titulaString = this.titula == Title.OPSTE_PRAKSE ? "OPSTE PRAKSE" : "SPECIJALISTA";
        }
        public Doctor(String ime, String prezime, int dan, int mjesec, int godina, String ulica, int broj, String grad, String JMBG, String email, String brojTelefona, String korisnickoIme, String lozinka, Pol pol, Title titula, String biografija, int sati1, int minuti1, int sati2, int minuti2,String prostorija)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = new DateTime(godina, mjesec, dan);
            this.adresaStanovanja = new Address(ulica, broj, grad);
            this.JMBG = JMBG;
            this.email = email;
            this.brojTelefona = brojTelefona;
            this.korisnickoIme = korisnickoIme;
            this.lozinka = lozinka;
            this.pol = pol;
            this.titula = titula;
            this.biografija = biografija;
            this.radnoVrijeme = new WorkingTime(new DateTime(2000, 1, 1, sati1, minuti1, 0), new DateTime(2000, 1, 1, sati2, minuti2, 0));
            this.prostorija = prostorija;
            this.doctorString = "dr. " + ime + " " + prezime;
            this.titulaString = this.titula == Title.OPSTE_PRAKSE ? "OPSTE PRAKSE" : "SPECIJALISTA";
        }
    }
}
