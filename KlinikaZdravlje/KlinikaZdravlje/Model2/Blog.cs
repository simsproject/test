﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class Blog
    {
        public String title { get; set; }
        public String text { get; set; }
        public DateTime dateOfCreate { get; set; }

        public long id { get; set; }

        public Blog(string title, string text, DateTime dateOfCreate, long id)
        {
            this.title = title;
            this.text = text;
            this.dateOfCreate = dateOfCreate;
            this.id = id;
        }
    }
}
