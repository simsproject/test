﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{

    public enum TypeOfAppointment
    {
        PREGLED,KONTROLA,OPERACIJA
    }

    public class Appointment
    {
        public DateTime begin { get; set; }
        public DateTime end { get; set; }
        public Model2.Doctor doctor { get; set; }
        public Model2.Patient patient { get; set; }
        public String room { get; set; }
        public Model2.TypeOfAppointment type { get; set; }

        public String doctorString {get;set;}

        public String typeOfAppointment { get; set; }

        public String stringPatient { get; set; }

    public  Appointment(DateTime begin, DateTime end, Model2.Doctor doctor, Model2.Patient patient, String room, Model2.TypeOfAppointment type)
        {
            this.begin = begin;
            this.end = end;
            this.doctor = doctor;
            this.patient = patient;
            this.room = room;
            this.type = type;
            this.doctorString= "dr. " + doctor.ime + " " + doctor.prezime;
            this.typeOfAppointment = this.type ==TypeOfAppointment.OPERACIJA ? "OPERACIJA" : (this.type == TypeOfAppointment.KONTROLA ? "KONTROLA" : "PREGLED");
            this.stringPatient = this.patient.ime + " " + this.patient.prezime;
        }

    }
}
