﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class Questionnaire
    {
        public String note;
        public Model2.Patient patient;
        public Model2.Doctor doctor;
        public List<Model2.Question> questions;

        public Questionnaire(string note, Model2.Patient patient, Model2.Doctor doctor, List<Model2.Question> questions)
        {
            this.note = note;
            this.patient = patient;
            this.doctor = doctor;
            this.questions = questions;
        }

        public Questionnaire(string note, Model2.Patient patient, List<Model2.Question> questions)
        {
            this.note = note;
            this.patient = patient;
            this.questions = questions;
            this.doctor = null;
        }
    }
}
