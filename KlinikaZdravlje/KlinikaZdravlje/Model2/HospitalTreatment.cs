﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class HospitalTreatment
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public String note { get; set; }
        public long serialID { get; set; }

        public String patientRoom { get; set; }
        public String bed { get; set; }
        public Model2.Patient patient { get; set; }
        public Model2.Doctor doctor { get; set; }
        public DateTime dateOfAppointment { get; set; }

        public String dateString1 { get; set; }
        public String titleString1 { get; set; }

        public String doctorString1 { get; set; }

        public HospitalTreatment(DateTime startDate, DateTime endDate, string note, long serialID, string patientRoom, string bed, Model2.Patient patient, Model2.Doctor doctor, DateTime dateTime)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.note = note;
            this.serialID = serialID;
            this.patientRoom = patientRoom;
            this.bed = bed;
            this.patient = patient;
            this.doctor = doctor;
            this.dateOfAppointment = dateTime;
            this.doctorString1 = "dr. " + doctor.ime + " " + doctor.prezime;
            this.titleString1 = "Uputnicu napisao " + this.doctorString1 + " dana " + String.Format("{0:d}", this.dateOfAppointment);
            this.dateString1 = String.Format("{0:d}", this.dateOfAppointment);
        }
    }
}
