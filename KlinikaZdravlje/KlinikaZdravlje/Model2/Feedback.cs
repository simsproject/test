﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class Question
    {
        public String question;
        public int answer;
        
        public Question(String question,int answer)
        {
            this.question = question;
            this.answer = answer;
        }
    }

    public class Feedback
    {
        public String note;
        public List<Model2.Question> questions;
        public Model2.Patient patient;


        public Feedback(String note, List<Model2.Question> questions, Model2.Patient patient) 
        {
            this.note = note;
            this.questions = questions;
            this.patient = patient;
        }
    }
}
