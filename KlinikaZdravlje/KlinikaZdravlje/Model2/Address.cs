﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class Address
    {
        public String ulica { get; set; }
        public int broj { get; set; }
        public String grad { get; set; }
        public String drzava { get; set; }
        public String adresa { get; set; }

        public Address(String ulica, int broj, String grad)
        {
            this.ulica = ulica;
            this.broj = broj;
            this.grad = grad;
            this.drzava = "Srbija";
            this.adresa = ulica+" "+broj+"., "+grad;
        }
    }
}
