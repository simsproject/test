﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class SpecialistTreatment
    {
        public Patient patient { get; set; }
        public Doctor doctor { get; set; }
        public DateTime date { get; set; }
        public Appointment appointment { get; set; }
        public String note { get; set; }
        public String dateString1 { get; set; }
        public String titleString1 { get; set; }

        public String doctorString1 { get; set; }

        public SpecialistTreatment(Model2.Patient patient, Model2.Doctor doctor, DateTime date, DateTime begin, Model2.Doctor doctorSpecialist, String note)
        {
            this.patient = patient;
            this.doctor = doctor;
            this.date = date;
            this.appointment = new Appointment(begin, begin.AddMinutes(15), doctorSpecialist, patient, doctorSpecialist.prostorija, TypeOfAppointment.PREGLED);
            this.note = note;
            this.doctorString1 = "dr. " + doctor.ime + " " + doctor.prezime;
            this.titleString1 = "Uputnicu napisao " + this.doctorString1 + " dana " + String.Format("{0:d}", this.date);
            this.dateString1 = String.Format("{0:d}", this.date);
        }
    }
}
