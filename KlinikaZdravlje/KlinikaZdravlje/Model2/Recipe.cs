﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class Recipe
    {
        public Model2.Doctor doctor { get; set; }
        public Model2.Patient patient { get; set; }
        public DateTime date { get; set; }
        public String detail { get; set; }
        public String drugCode { get; set; }
        public int amount { get; set; }

        public String dateString1 { get; set; }
        public String titleString1 { get; set; }

        public String doctorString1 { get; set; }

        public Recipe(Model2.Doctor doctor, Model2.Patient patient, DateTime date, string detail, string drugCode, int amount)
        {
            this.doctor = doctor;
            this.patient = patient;
            this.date = date;
            this.detail = detail;
            this.drugCode = drugCode;
            this.amount = amount;
            this.doctorString1 = "dr. " + doctor.ime + " " + doctor.prezime;
            this.titleString1 = "Recept prepisao " + this.doctorString1 + " dana " + String.Format("{0:d}", this.date);
            this.dateString1 = String.Format("{0:d}", this.date);
        }
    }
}
