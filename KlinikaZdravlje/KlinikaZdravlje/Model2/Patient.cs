﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace KlinikaZdravlje.Model2
{
    public enum Pol
    {
        MUSKO, ZENSKO
    }

    public class Patient
    {
        public String ime;
        public String prezime;
        public DateTime datumRodjenja;
        public Model2.Address adresaStanovanja;
        public String JMBG;
        public String email;
        public String brojTelefona;
        public String korisnickoIme;
        public String lozinka;
        public Model2.Pol pol;
        public bool isGuest;

        public Patient(String ime,String prezime,DateTime datumRodjenja, Model2.Address adresaStanovanja,String JMBG,String email,String brojTelefona,String korisnickoIme,String lozinka, Model2.Pol pol, bool isGuest)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = datumRodjenja;
            this.adresaStanovanja = adresaStanovanja;
            this.JMBG = JMBG;
            this.email = email;
            this.brojTelefona = brojTelefona;
            this.korisnickoIme = korisnickoIme;
            this.lozinka = lozinka;
            this.pol = pol;
            this.isGuest = isGuest;
        }
        public Patient(String ime, String prezime, int dan,int mjesec, int godina, String ulica,int broj, String grad, String JMBG, String email, String brojTelefona, String korisnickoIme, String lozinka, Model2.Pol pol,bool isGuest)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.datumRodjenja = new DateTime(godina, mjesec, dan);
            this.adresaStanovanja = new Address(ulica, broj, grad);
            this.JMBG = JMBG;
            this.email = email;
            this.brojTelefona = brojTelefona;
            this.korisnickoIme = korisnickoIme;
            this.lozinka = lozinka;
            this.pol = pol;
            this.isGuest = isGuest;
        }
    }
}
