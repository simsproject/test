﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class Notification
    {
        public String text { get; set; }
        public DateTime datum { get; set; }

        public Model.Users.Patient patient { get; set; }

        public String dateString { get; set; }

        public Notification(string text, DateTime datum, Model.Users.Patient patient)
        {
            this.text = text;
            this.datum = datum;
            this.patient = patient;
            this.dateString = String.Format("{0:g}",this.datum);
        }
    }
}
