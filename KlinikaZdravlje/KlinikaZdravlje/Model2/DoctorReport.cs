﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{

    public class DoctorReport
    {
        public Model2.Doctor doctor { get; set; }
        public Model2.Patient patient { get; set; }
        public Model2.TypeOfAppointment type { get; set; }
        public DateTime date { get; set; }
        public String anamnesis { get; set; }
        public long serialID { get; set; }
        public String diagnosisAndTeraphy { get; set; }
        public String doctorOpinion { get; set; }
        public String dateString { get; set; }
        public String titleString { get; set; }

        public String doctorString { get; set; }

        public DoctorReport(Model2.Doctor doctor, Model2.Patient patient, Model2.TypeOfAppointment type, DateTime date, string anamnesis, long serialID, string diagnosisAndTeraphy, string doctorOpinion)
        {
            this.doctor = doctor;
            this.patient = patient;
            this.type = type;
            this.date = date;
            this.anamnesis = anamnesis;
            this.serialID = serialID;
            this.diagnosisAndTeraphy = diagnosisAndTeraphy;
            this.doctorOpinion = doctorOpinion;
            this.doctorString = "dr. " + doctor.ime + " " + doctor.prezime;
            this.titleString = "Izvjestaj kod "+this.doctorString+" dana " + String.Format("{0:d}", this.date);
            this.dateString= String.Format("{0:d}", this.date);
        }
    }
}
