﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlinikaZdravlje.Model2
{
    public class MedicalRecord
    {
        public long serialID { get; set; }
        public Patient patient;
        public List<Model2.DoctorReport> doctorReports;
        public List<Model2.Recipe> recipes;
        public List<Model2.HospitalTreatment> hospitalTreatments;
        public List<Model2.SpecialistTreatment> specialistTreatments;

        public MedicalRecord(long serialID, Model2.Patient patient)
        {
            this.serialID = serialID;
            this.patient = patient;
            this.doctorReports =new List<DoctorReport>();
            this.recipes = new List<Recipe>();
            this.hospitalTreatments = new List<HospitalTreatment>();
            this.specialistTreatments = new List<SpecialistTreatment>();
        }

        public MedicalRecord(long serialID, Model2.Patient patient, List<Model2.DoctorReport> doctorReports, List<Model2.Recipe> recipes, List<Model2.HospitalTreatment> hospitalTreatments, List<Model2.SpecialistTreatment> specialistTreatments)
        {
            this.serialID = serialID;
            this.patient = patient;
            this.doctorReports = doctorReports;
            this.recipes = recipes;
            this.hospitalTreatments = hospitalTreatments;
            this.specialistTreatments = specialistTreatments;
        }
    }
}
