﻿using Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace KlinikaZdravlje
{

    public enum Prioritet
    {
        DOKTOR,TERMIN
    }

    /// <summary>
    /// Interaction logic for RecommendedTerm.xaml
    /// </summary>
    public partial class RecommendedTerm : Window
    {
        public Patient patient;

        public Doctor doctor;



        public RecommendedTerm(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
            this.comboBoxLjekari.ItemsSource = App.doctorController.GetAll().Where(Doctor => Doctor.Title == Model.Users.Title.DoctorOfGeneralPractice).ToList();
        }

        private void buttonBack_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonNext_Click(object sender, RoutedEventArgs e)
        {
            AppointmentAccept win;
            Model.Appointment.Appointment appointment;
            DateTime datumOD = this.datumOd.SelectedDate.GetValueOrDefault();
            DateTime datumDO = this.datumDo.SelectedDate.GetValueOrDefault();
            if (datumOD==null || datumDO==null || DateTime.Compare(datumOD, datumDO) > 0 || DateTime.Compare(datumOD, DateTime.Now.AddDays(1).Date) <= 0)
            {
                MessageBox.Show("Datumi nisu validni!");
                return;
            }
            TimeSpan ts = datumDO - datumOD;
            Doctor doctor = comboBoxLjekari.SelectedItem as Doctor;
            if (doctor == null)
            {
                MessageBox.Show("Niste izabrali doktora!");
                return;
            }
            Prioritet prioritet = radioButtonLjekar.IsChecked == true ? Prioritet.DOKTOR : Prioritet.TERMIN;
            appointment = getAppointment(datumOD, datumDO, doctor);
            if (appointment == null) {
                if (prioritet == Prioritet.DOKTOR)
                {
                    while (appointment == null && ts.TotalDays < 14) {
                        datumDO = datumDO.AddDays(1);
                        ts = datumDO - datumOD;
                        appointment = getAppointment(datumDO, datumDO, doctor);
                    }
                }
                else
                {
                    List<Doctor> doctorsOfGeneralPractice = App.doctorController.GetDoctorByTitle(Model.Users.Title.DoctorOfGeneralPractice);
                    removeDoctorFromList(doctor, doctorsOfGeneralPractice);
                    foreach (Doctor doctorIt in doctorsOfGeneralPractice)
                    {
                        appointment = getAppointment(datumOD, datumDO, doctorIt);
                        if (appointment != null) break;
                    }
                }
            }

            if (appointment == null)
            {
                MessageBox.Show("Nije pronadjen datum");
                return;
            }

            win = new AppointmentAccept(appointment); //dodati appointment umjesto null kad se uradi funkcijonalnost
            win.ShowDialog();
            if (AppointmentAccept.terminZakazan) this.Close();

        }
    


    /*        win = new AppointmentAccept(null/*new Model.Appointment(new DateTime(2020,6,19,17,0,0), new DateTime(2020, 6,19,17,15,0), new Model.Doctor("Marko", "Nikolic", 18, 5, 1966, "Mihajla Pupina", 22, "Beograd", "1805966881125", "gigamoravac98@yahoo.com", "061-457-3364", "giga_moravac", "12345678", Pol.MUSKO, Model.Title.OPSTE_PRAKSE, "Bla,bla,bla", 9, 0, 18, 0, "204"),this.patient,"203",TypeOfAppointment.PREGLED)); //dodati appointment umjesto null kad se uradi funkcijonalnost */
         /*   win.ShowDialog();
            if (AppointmentAccept.terminZakazan) this.Close(); */

        

        public bool removeDoctorFromList(Doctor doctor, List<Doctor> doctors)
        { // koristi je removeAllBusyDoctorFromList
	        foreach(Doctor doctorVariable in doctors)
            {
                if (doctorVariable.PersonalID == doctor.PersonalID) return doctors.Remove(doctorVariable);
            }
            return false;
        }


        public Model.Appointment.Appointment getAppointment(DateTime dateOd,DateTime dateDo,Doctor doctor)
        {
            Model2.WorkingTime workingTime12 = null;
            List<DateTime> dateTimes = App.workingPeriodController.EachDay(dateOd, dateDo).ToList();
            Model.Appointment.Appointment appointment = null;
            foreach(DateTime dateTime in dateTimes)
            {
                WorkingTime workingTime = App.workingTimeController.GetWorkingTimeForDoctorByDate(doctor, dateTime);
                if (workingTime == null) continue;
                Model2.WorkingTime workingTime1 = new Model2.WorkingTime(new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.BeginTime.Hour, workingTime.BeginTime.Minute, workingTime.BeginTime.Second),
                   new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, workingTime.EndTime.Hour, workingTime.EndTime.Minute, workingTime.EndTime.Second));

                List<Model2.WorkingTime> workingTimes = workingTimesForDay(workingTime1);
                List<Model.Appointment.Appointment> appointments = App.appointmentController.GetAppointmentsForDoctorByDate(doctor, dateTime);

                removeAllScheduleWorkingTime(appointments, workingTimes);
                if (workingTimes.First() != null)
                {
                    workingTime12 = workingTimes.First();
                    appointment = new Model.Appointment.Appointment(workingTime12.odSati, workingTime12.doSati, Model.Appointment.AppointmentType.regularAp, 0, this.patient, doctor, doctor.MedicalRoom);
                    break;
                }
            }
            return appointment;
        }

        void removeAllScheduleWorkingTime(List<Model.Appointment.Appointment> appointmentsForDoctor, List<Model2.WorkingTime> returnValue)
        {
            Model2.WorkingTime helpVariable;
            foreach (Model.Appointment.Appointment appointment in appointmentsForDoctor)
            {
                helpVariable = new Model2.WorkingTime(appointment.StartDate, appointment.FinishDate);
                if (DateTime.Compare(helpVariable.odSati, appointment.StartDate) == 0 && DateTime.Compare(helpVariable.doSati, appointment.FinishDate) == 0) removeFromList(returnValue, helpVariable);


            }
        }

        bool removeFromList(List<Model2.WorkingTime> listOfWorkingTimesForDay, Model2.WorkingTime workingTimeWhoNeedRemove)
        {
            foreach (Model2.WorkingTime workingTimeForDay in listOfWorkingTimesForDay)
            {
                if (workingTimeWhoNeedRemove.odSati == workingTimeForDay.odSati && workingTimeWhoNeedRemove.doSati == workingTimeForDay.doSati) return listOfWorkingTimesForDay.Remove(workingTimeForDay);
            }
            return false;
        }

        List<Model2.WorkingTime> workingTimesForDay(Model2.WorkingTime workingTime)
        { // vraca mi listu potencijalnih termina za odredjeni datum --
            List<Model2.WorkingTime> returnValue = new List<Model2.WorkingTime>();
            DateTime begin = workingTime.odSati;
            DateTime end = workingTime.doSati;
            for (DateTime date = begin; date < end; date = date.AddMinutes(15))
            {
                Model2.WorkingTime helpVariable = new Model2.WorkingTime(date, date.AddMinutes(15));
                returnValue.Add(helpVariable);
                helpVariable = null;
            }
            return returnValue;

        }
    }
}
