﻿using Controller;
using Controller.UserPackageController;
using Model.MedicalRecords;
using Model.Other;
using Model.Users;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KlinikaZdravlje
{
    /// <summary>
    /// Interaction logic for GenericReport.xaml
    /// </summary>
    public partial class GenericReport : Window
    {
        public Patient patient;

        public GenericReport(Patient patient)
        {
            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.patient = patient;
        }

        private void buttonGenerate_Click(object sender, RoutedEventArgs e)
        {
            ZdravstveniKarton win = new ZdravstveniKarton(patient);
            win.Show();
            this.Close();
        }
    }
}
